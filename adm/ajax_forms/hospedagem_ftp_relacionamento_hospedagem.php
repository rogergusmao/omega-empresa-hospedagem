<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: hospedagem_ftp
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            hospedagem_ftp.php
 * TABELA MYSQL:       hospedagem_ftp
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */

if (isset($_GET["contador"])) {

    $numeroRegistroInterno = Helper::GET("contador");
}

if (isset($identificadorRelacionamento) && is_numeric($identificadorRelacionamento)) {

    $objHospedagem_ftp = new EXTDAO_Hospedagem_ftp();
    $objHospedagem_ftp->select($identificadorRelacionamento);
} else {

    $objHospedagem_ftp = new EXTDAO_Hospedagem_ftp();
}

$objArgHospedagem_ftp = new Generic_Argument();
$objHospedagem_ftp->formatarParaExibicao();
?>

<input type="hidden" name="hospedagem_ftp_id_<?= $numeroRegistroInterno ?>" id="hospedagem_ftp_id_<?= $numeroRegistroInterno ?>" value="<?= $objHospedagem_ftp->getId(); ?>">

<table class="tabela_form">

    <tr class="tr_form">


        <?
        $objArgHospedagem_ftp = new Generic_Argument();

        $objArgHospedagem_ftp->numeroDoRegistro = "";
        $objArgHospedagem_ftp->label = $objHospedagem_ftp->label_host_ftp;
        $objArgHospedagem_ftp->valor = $objHospedagem_ftp->getHost_ftp();
        $objArgHospedagem_ftp->classeCss = "input_text";
        $objArgHospedagem_ftp->classeCssFocus = "focus_text";
        $objArgHospedagem_ftp->obrigatorio = true;
        $objArgHospedagem_ftp->largura = 300;
        $objArgHospedagem_ftp->nome = "hospedagem_ftp_host_ftp_{$numeroRegistroInterno}";
        $objArgHospedagem_ftp->id = "hospedagem_ftp_host_ftp_{$numeroRegistroInterno}";
        ?>

        <td class="td_form_label"><?= $objArgHospedagem_ftp->getLabel() ?></td>
        <td class="td_form_campo"><?= $objHospedagem_ftp->campoTexto($objArgHospedagem_ftp); ?></td>


        <?
        $objArgHospedagem_ftp = new Generic_Argument();

        $objArgHospedagem_ftp->numeroDoRegistro = "";
        $objArgHospedagem_ftp->label = $objHospedagem_ftp->label_porta_ftp_INT;
        $objArgHospedagem_ftp->valor = $objHospedagem_ftp->getPorta_ftp_INT();
        $objArgHospedagem_ftp->classeCss = "input_text";
        $objArgHospedagem_ftp->classeCssFocus = "focus_text";
        $objArgHospedagem_ftp->obrigatorio = true;
        $objArgHospedagem_ftp->largura = 100;
        $objArgHospedagem_ftp->nome = "hospedagem_ftp_porta_ftp_INT_{$numeroRegistroInterno}";
        $objArgHospedagem_ftp->id = "hospedagem_ftp_porta_ftp_INT_{$numeroRegistroInterno}";
        ?>

        <td class="td_form_label"><?= $objArgHospedagem_ftp->getLabel() ?></td>
        <td class="td_form_campo"><?= $objHospedagem_ftp->campoInteiro($objArgHospedagem_ftp); ?></td>
    </tr>
    <tr class="tr_form">


        <?
        $objArgHospedagem_ftp = new Generic_Argument();

        $objArgHospedagem_ftp->numeroDoRegistro = "";
        $objArgHospedagem_ftp->label = $objHospedagem_ftp->label_usuario_ftp;
        $objArgHospedagem_ftp->valor = $objHospedagem_ftp->getUsuario_ftp();
        $objArgHospedagem_ftp->classeCss = "input_text";
        $objArgHospedagem_ftp->classeCssFocus = "focus_text";
        $objArgHospedagem_ftp->obrigatorio = true;
        $objArgHospedagem_ftp->largura = 200;
        $objArgHospedagem_ftp->nome = "hospedagem_ftp_usuario_ftp_{$numeroRegistroInterno}";
        $objArgHospedagem_ftp->id = "hospedagem_ftp_usuario_ftp_{$numeroRegistroInterno}";
        ?>

        <td class="td_form_label"><?= $objArgHospedagem_ftp->getLabel() ?></td>
        <td class="td_form_campo"><?= $objHospedagem_ftp->campoTexto($objArgHospedagem_ftp); ?></td>


        <?
        $objArgHospedagem_ftp = new Generic_Argument();

        $objArgHospedagem_ftp->numeroDoRegistro = "";
        $objArgHospedagem_ftp->label = $objHospedagem_ftp->label_senha_ftp;
        $objArgHospedagem_ftp->valor = $objHospedagem_ftp->getSenha_ftp();
        $objArgHospedagem_ftp->classeCss = "input_text";
        $objArgHospedagem_ftp->classeCssFocus = "focus_text";
        $objArgHospedagem_ftp->obrigatorio = true;
        $objArgHospedagem_ftp->largura = 200;
        $objArgHospedagem_ftp->nome = "hospedagem_ftp_senha_ftp_{$numeroRegistroInterno}";
        $objArgHospedagem_ftp->id = "hospedagem_ftp_senha_ftp_{$numeroRegistroInterno}";
        ?>

        <td class="td_form_label"><?= $objArgHospedagem_ftp->getLabel() ?></td>
        <td class="td_form_campo"><?= $objHospedagem_ftp->campoTexto($objArgHospedagem_ftp); ?></td>
    </tr>
    <tr><td colspan="4" class="td_botao_remover_da_lista"><input class="botoes_form" type="button" value="Remover FTP" onclick="javascript:removerDivAjaxEmLista(this);"></td></tr>	</table><br />


<?php
    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     acesso
    * DATA DE GERA��O:    08.02.2010
    * ARQUIVO:            acesso.php
    * TABELA MYSQL:       acesso
    * BANCO DE DADOS:     dep_pesquisas
    * -------------------------------------------------------
    *
    * GERENCIADOR DE FILTROS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Acesso();
    
    $objArg = new Generic_Argument();
    
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";    
    
    $nextActions = array("add_acesso"=>I18N::getExpression("Adicionar  acesso"), 
    					 "list_acesso"=>I18N::getExpression("Listar acesso"));
    
    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);
    
       $obj->setBySession();
        
    }
    
    $obj->setByGet("1");
        
    $obj->formatarParaExibicao();
    					 
    ?>
    
    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="acesso">
    	
        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro"><?=I18N::getExpression("Pesquisar Acesso"); ?></legend>
        
        <table class="tabela_form">
        
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_usuario_id_INT;
    			$objArg->valor = $obj->getUsuario_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllUsuario($objArg); ?>
    			</td>

        		
    			<?
    
    			$objArg->label = $obj->label_data_login_DATETIME;
    			$objArg->valor = $obj->getData_login_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_login_DATETIME($objArg); ?></td>
			</tr>
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_data_logout_DATETIME;
    			$objArg->valor = $obj->getData_logout_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_logout_DATETIME($objArg); ?></td>

                
            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">
        		
        		<?=Helper::imprimirBotoesList(true, true); ?>
        
        	</td>
        </tr>
	</table>
     
     </fieldset>
    
	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    
    
	<?=$obj->getRodapeFormulario(); ?>
    

<?php
    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     backup_automatico
    * DATA DE GERAÇÃO:    25.08.2010
    * ARQUIVO:            backup_automatico.php
    * TABELA MYSQL:       backup_automatico
    * BANCO DE DADOS:     DEP_pesquisas_config
    * -------------------------------------------------------
    *
    * GERENCIADOR DE FILTROS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Backup_automatico();
    
    $objArg = new Generic_Argument();
    
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";    
    
    $nextActions = array("add_backup_automatico"=>I18N::getExpression("Adicionar  backup automático"), 
    					 "list_backup_automatico"=>I18N::getExpression("Listar backups automáticos"));
    
    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);
    
       $obj->setBySession();
        
    }
    
    $obj->setByGet("1");
        
    $obj->formatarParaExibicao();
    					 
    ?>
    
    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="backup_automatico">
    	
        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Backups Automáticos</legend>
        
        <table class="tabela_form">
        
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_hora_base_TIME;
    			$objArg->valor = $obj->getHora_base_TIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoHora_base_TIME($objArg); ?></td>

        		
    			<?
    
    			$objArg->label = $obj->label_status_BOOLEAN;
    			$objArg->labelTrue = "Ativo";
    			$objArg->labelFalse = "Inativo";
    			$objArg->valor = $obj->getStatus_BOOLEAN();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 20;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoStatus_BOOLEAN($objArg); ?></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">
        		
        		<?=Helper::imprimirBotoesList(true, true); ?>
        
        	</td>
        </tr>
	</table>
     
     </fieldset>
    
	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    
    
	<?=$obj->getRodapeFormulario(); ?>
    

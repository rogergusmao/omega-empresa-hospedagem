<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     banco_dados
    * DATA DE GERAÇÃO:    13.12.2009
    * ARQUIVO:            banco_dados.php
    * TABELA MYSQL:       banco_dados
    * BANCO DE DADOS:     DEP_pesquisas_config
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Banco_dados();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_banco_dados"=>I18N::getExpression("Adicionar  backup do banco de dados"),
    					 "list_banco_dados"=>I18N::getExpression("Listar backups do banco de dados"));

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="pages">
        <input type="hidden" name="page" id="tipo" value="restaurar_backup_banco_dados">

    	<? Helper::imprimirCamposHiddenReferentesADeterminadasVariaveisGET(array("analise_retroativa")); ?>

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro"><?=I18N::getExpression("Pesquisar Backups Do Banco De Dados"); ?></legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_descricao;
    			$objArg->valor = $obj->getDescricao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoDescricao($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>


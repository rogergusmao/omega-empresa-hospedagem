<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FILTRO:     db_tipo
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            db_tipo.php
 * TABELA MYSQL:       db_tipo
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Db_tipo();

$objArg = new Generic_Argument();

$class = $obj->nomeClasse;
$action = (Helper::GET("id") ? "edit" : "add");
$postar = "index.php";

$nextActions = array("add_db_tipo" => "Adicionar  tipo de bancos de dados",
    "list_db_tipo" => "Listar tipos de bancos de dados");

if (Helper::SESSION("erro")) {

    unset($_SESSION["erro"]);

    $obj->setBySession();
}

$obj->setByGet("1");

$obj->formatarParaExibicao();
?>

<?= $obj->getCabecalhoFiltro($postar); ?>

<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="tipo" id="tipo" value="lists">
<input type="hidden" name="page" id="tipo" value="db_tipo">

<fieldset class="fieldset_filtro">
    <legend class="legend_filtro"><?=I18N::getExpression("Pesquisar Tipos de Bancos de Dados"); ?></legend>

    <table class="tabela_form">

        <tr class="tr_form">


            <?
            $objArg->label = $obj->label_nome;
            $objArg->valor = $obj->getNome();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->imprimirCampoNome($objArg); ?></td>


            <?
            $objArg->label = $obj->label_porta_padrao_INT;
            $objArg->valor = $obj->getPorta_padrao_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->imprimirCampoPorta_padrao_INT($objArg); ?></td>
        </tr>

        <tr class="tr_form_rodape2">
            <td colspan="4">

                <?= Helper::imprimirBotoesList(true, true); ?>

            </td>
        </tr>
    </table>

</fieldset>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     hospedagem_db
    * DATA DE GERA��O:    19.04.2014
    * ARQUIVO:            hospedagem_db.php
    * TABELA MYSQL:       hospedagem_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Hospedagem_db();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_hospedagem_db"=>I18N::getExpression("Adicionar nova hospedagem do banco de dados"),
    					 "list_hospedagem_db"=>I18N::getExpression("Listar hospedagens do banco de dados"));

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="hospedagem_db">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro"><?=I18N::getExpression("Pesquisar Hospedagens Do Banco De Dados"); ?></legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_hospedagem_id_INT;
    			$objArg->valor = $obj->getHospedagem_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllHospedagem($objArg); ?>
    			</td>


    			<?

    			$objArg->label = $obj->label_nome_db;
    			$objArg->valor = $obj->getNome_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome_db($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_host_db;
    			$objArg->valor = $obj->getHost_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoHost_db($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_porta_db_INT;
    			$objArg->valor = $obj->getPorta_db_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoPorta_db_INT($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_usuario_db;
    			$objArg->valor = $obj->getUsuario_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoUsuario_db($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>


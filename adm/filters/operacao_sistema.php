<?php
    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     operacao_sistema
    * DATA DE GERA��O:    23.10.2009
    * ARQUIVO:            operacao_sistema.php
    * TABELA MYSQL:       operacao_sistema
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
    * GERENCIADOR DE FILTROS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Operacao_sistema();
    
    $objArg = new Generic_Argument();
    
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";    
    
    $nextActions = array("add_operacao_sistema"=>I18N::getExpression("Adicionar nova opera��o no sistema"), 
    					 "list_operacao_sistema"=>I18N::getExpression("Listar opera��es no sistema"));
    
    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);
    
       $obj->setBySession();
        
    }
    
    if(Helper::GET("id")){
        
        $id = Helper::GET("id");
        
        $obj->select($id);
        
    }
    
    
    $obj->formatarParaExibicao();
    					 
    ?>
    
    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="operacao_sistema">
    	
        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro">Pesquisar Opera��es No Sistema</legend>
        
        <table class="tabela_form">
        
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_usuario_id_INT;
    			$objArg->valor = $obj->getUsuario_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllUsuario($objArg); ?>
    			</td>

        		
    			<?
    
    			$objArg->label = $obj->label_tipo_operacao;
    			$objArg->valor = $obj->getTipo_operacao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoTipo_operacao($objArg); ?></td>
			</tr>
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_pagina_operacao;
    			$objArg->valor = $obj->getPagina_operacao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoPagina_operacao($objArg); ?></td>

        		
    			<?
    
    			$objArg->label = $obj->label_entidade_operacao;
    			$objArg->valor = $obj->getEntidade_operacao();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoEntidade_operacao($objArg); ?></td>
			</tr>
			<tr class="tr_form">

        		
    			<?
    
    			$objArg->label = $obj->label_chave_registro_operacao_INT;
    			$objArg->valor = $obj->getChave_registro_operacao_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoChave_registro_operacao_INT($objArg); ?></td>

        		
    			<?
    
    			$objArg->label = $obj->label_data_operacao_DATETIME;
    			$objArg->valor = $obj->getData_operacao_DATETIME();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;
    									
    			?>
    		
    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoData_operacao_DATETIME($objArg); ?></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">
        		
        		<?=Helper::imprimirBotoesList(true, true); ?>
        
        	</td>
        </tr>
	</table>
     
     </fieldset>
    
	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    
    
	<?=$obj->getRodapeFormulario(); ?>
    

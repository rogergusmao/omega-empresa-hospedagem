<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FILTRO:     servico
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            servico.php
 * TABELA MYSQL:       servico
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Servico();

$objArg = new Generic_Argument();

$class = $obj->nomeClasse;
$action = (Helper::GET("id") ? "edit" : "add");
$postar = "index.php";

$nextActions = array("add_servico" => "Adicionar  servi�o",
    "list_servico" => "Listar servi�os");

if (Helper::SESSION("erro")) {

    unset($_SESSION["erro"]);

    $obj->setBySession();
}

$obj->setByGet("1");

$obj->formatarParaExibicao();
?>

<?= $obj->getCabecalhoFiltro($postar); ?>

<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="tipo" id="tipo" value="lists">
<input type="hidden" name="page" id="tipo" value="servico">

<fieldset class="fieldset_filtro">
    <legend class="legend_filtro">Pesquisar Servi�os</legend>

    <table class="tabela_form">

        <tr class="tr_form">

            <?
            $objArg->label = $obj->label_cliente_id_INT;
            $objArg->valor = $obj->getCliente_id_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 500;
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo">
                <?= $obj->getComboBoxAllCliente($objArg); ?>
            </td>

        </tr>
        <tr class="tr_form">
            
            <?
            $objArg->label = $obj->label_data_contratacao_DATE . " - De";
            $objArg->valor = Helper::GET("data_contratacao_DATE_de1");
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->nome = "data_contratacao_DATE_de";
            $objArg->id = $objArg->nome;
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            $objArg->calendario = true;
            $objArg->tipoCalendario = CALENDARIO_INICIAL;
            $objArg->idOutroCalendario = "data_contratacao_DATE_ate1";
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->campoData($objArg); ?></td>
            
            <?
            $objArg->label = $obj->label_data_contratacao_DATE . " - At�";
            $objArg->valor = Helper::GET("data_contratacao_DATE_ate1");
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->nome = "data_contratacao_DATE_ate";
            $objArg->id = $objArg->nome;
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            $objArg->calendario = true;
            $objArg->tipoCalendario = CALENDARIO_FINAL;
            $objArg->idOutroCalendario = "data_contratacao_DATE_de1";
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->campoData($objArg); ?></td>
        
        </tr>
        <tr class="tr_form">


            <?
            $objArg = new Generic_Argument();
            $objArg->label = $obj->label_servico_modalidade_id_INT;
            $objArg->valor = $obj->getServico_modalidade_id_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo">
                <?= $obj->getComboBoxAllServico_modalidade($objArg); ?>
            </td>


            <?
            $objArg->label = $obj->label_dia_mes_vencimento_INT;
            $objArg->valor = $obj->getDia_mes_vencimento_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->imprimirCampoDia_mes_vencimento_INT($objArg); ?></td>
        </tr>
        <tr class="tr_form">

            <?
            $objArg->label = $obj->label_data_vencimento_DATE . " - De";
            $objArg->valor = Helper::GET("data_vencimento_DATE_de1");
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->nome = "data_vencimento_DATE_de";
            $objArg->id = $objArg->nome;
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            $objArg->calendario = true;
            $objArg->tipoCalendario = CALENDARIO_INICIAL;
            $objArg->idOutroCalendario = "data_vencimento_DATE_ate1";
            
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->campoData($objArg); ?></td>

            <?
            $objArg->label = $obj->label_data_vencimento_DATE . " - At�";
            $objArg->valor = Helper::GET("data_vencimento_DATE_ate1");
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->nome = "data_vencimento_DATE_ate";
            $objArg->id = $objArg->nome;
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            $objArg->calendario = true;
            $objArg->tipoCalendario = CALENDARIO_FINAL;
            $objArg->idOutroCalendario = "data_vencimento_DATE_de1";
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->campoData($objArg); ?></td>

        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4">

                <?= Helper::imprimirBotoesList(true, true); ?>

            </td>
        </tr>
    </table>

</fieldset>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


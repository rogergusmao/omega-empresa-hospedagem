<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sistema_db
    * DATA DE GERA��O:    19.04.2014
    * ARQUIVO:            sistema_db.php
    * TABELA MYSQL:       sistema_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_db();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_sistema_db"=>I18N::getExpression("Adicionar  banco do sistema"),
    					 "list_sistema_db"=>I18N::getExpression("Listar bancos do sistema"));

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="sistema_db">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro"><?=I18N::getExpression("Pesquisar Bancos Do Sistema"); ?></legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_assinatura_host_db;
    			$objArg->valor = $obj->getAssinatura_host_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoAssinatura_host_db($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_assinatura_porta_db_INT;
    			$objArg->valor = $obj->getAssinatura_porta_db_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoAssinatura_porta_db_INT($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_assinatura_usuario_db;
    			$objArg->valor = $obj->getAssinatura_usuario_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoAssinatura_usuario_db($objArg); ?></td>


    			<?

    			$objArg->label = $obj->label_assinatura_senha_db;
    			$objArg->valor = $obj->getAssinatura_senha_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoAssinatura_senha_db($objArg); ?></td>
			</tr>
			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_sistema_id_INT;
    			$objArg->valor = $obj->getSistema_id_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
    			    <?=$obj->getComboBoxAllSistema($objArg); ?>
    			</td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>


<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FILTRO:     status_pagamento
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            status_pagamento.php
 * TABELA MYSQL:       status_pagamento
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Status_pagamento();

$objArg = new Generic_Argument();

$class = $obj->nomeClasse;
$action = (Helper::GET("id") ? "edit" : "add");
$postar = "index.php";

$nextActions = array("add_status_pagamento" => "Adicionar  status dos pagamentos",
    "list_status_pagamento" => "Listar status dos pagamentos");

if (Helper::SESSION("erro")) {

    unset($_SESSION["erro"]);

    $obj->setBySession();
}

$obj->setByGet("1");

$obj->formatarParaExibicao();
?>

<?= $obj->getCabecalhoFiltro($postar); ?>

<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="tipo" id="tipo" value="lists">
<input type="hidden" name="page" id="tipo" value="status_pagamento">

<fieldset class="fieldset_filtro">
    <legend class="legend_filtro"><?=I18N::getExpression("Pesquisar Status dos Pagamentos"); ?></legend>

    <table class="tabela_form">

        <tr class="tr_form">


            <?
            $objArg->label = $obj->label_nome;
            $objArg->valor = $obj->getNome();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->imprimirCampoNome($objArg); ?></td>


            <td class="td_form_label"></td>
            <td class="td_form_campo"></td>
        </tr>

        <tr class="tr_form_rodape2">
            <td colspan="4">

                <?= Helper::imprimirBotoesList(true, true); ?>

            </td>
        </tr>
    </table>

</fieldset>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


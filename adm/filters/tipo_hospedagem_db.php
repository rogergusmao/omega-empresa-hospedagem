<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     tipo_hospedagem_db
    * DATA DE GERA��O:    19.04.2014
    * ARQUIVO:            tipo_hospedagem_db.php
    * TABELA MYSQL:       tipo_hospedagem_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Tipo_hospedagem_db();

    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    $nextActions = array("add_tipo_hospedagem_db"=>I18N::getExpression("Adicionar  tipo de hospedagem db"),
    					 "list_tipo_hospedagem_db"=>I18N::getExpression("Listar tipos de hospedagem db"));

    if(Helper::SESSION("erro")){

        unset($_SESSION["erro"]);

       $obj->setBySession();

    }

    $obj->setByGet("1");

    $obj->formatarParaExibicao();

    ?>

    <?=$obj->getCabecalhoFiltro($postar); ?>

    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="tipo" id="tipo" value="lists">
        <input type="hidden" name="page" id="tipo" value="tipo_hospedagem_db">

        <fieldset class="fieldset_filtro">
            <legend class="legend_filtro"><?=I18N::getExpression("Pesquisar Tipos De Hospedagem Db"); ?></legend>

        <table class="tabela_form">

			<tr class="tr_form">


    			<?

    			$objArg->label = $obj->label_nome;
    			$objArg->valor = $obj->getNome();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo"><?=$obj->imprimirCampoNome($objArg); ?></td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>

        <tr class="tr_form_rodape2">
        	<td colspan="4">

        		<?=Helper::imprimirBotoesList(true, true); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>


<?php
//@@NAO_MODIFICAR

/*
 *
 * -------------------------------------------------------
 * NOME DO FILTRO:     venda
 * DATA DE GERA��O:    07.10.2011
 * ARQUIVO:            venda.php
 * TABELA MYSQL:       venda
 * BANCO DE DADOS:     estetica_solution
 * -------------------------------------------------------
 *
 */

$obj = new Generic_DAO();
$objArg = new Generic_Argument();

$postar = "index.php";

?>

<?= $obj->getCabecalhoFiltro($postar); ?>

<input type="hidden" name="tipo" id="tipo" value="reports">
<input type="hidden" name="page" id="tipo" value="fluxo_caixa">
<input type="hidden" name="postado" id="tipo" value="true">

<fieldset class="fieldset_filtro">
    <legend class="legend_filtro">Relat�rio de Fluxo de Caixa</legend>

    <table class="tabela_form">

        <tr class="tr_form">

            <?
            $objArg->label = "Data - De";
            $objArg->valor = Helper::GET("data_de1");
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            $objArg->nome = "data_de";
            $objArg->id = "data_de";
            $objArg->calendario = true;
            $objArg->tipoCalendario = CALENDARIO_INICIAL;
            $objArg->idOutroCalendario = "data_ate1";
            
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->campoData($objArg); ?></td>
        
            <?
            $objArg->label = "Data - At�";
            $objArg->valor = Helper::GET("data_ate1");
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            $objArg->nome = "data_ate";
            $objArg->id = "data_ate";
            $objArg->calendario = true;
            $objArg->tipoCalendario = CALENDARIO_FINAL;
            $objArg->idOutroCalendario = "data_de1";
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo"><?= $obj->campoData($objArg); ?></td>
        
        </tr>
        <tr class="tr_form">

            <?
            $objArg->label = "Tipo de Lan�amento";
            $objArg->valor = Helper::GET("tipo_lancamento1");
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;
            $objArg->nome = "tipo_lancamento";
            $objArg->id = "tipo_lancamento";
            
            $arrOpcoes = array("despesas" => "Despesas",
                               "receitas" => "Receitas");
            
            ?>

            <td class="td_form_label"><?= $objArg->getLabel() ?></td>
            <td class="td_form_campo" colspan="3">
                <?= $obj->getComboBoxArrayValores($objArg, $arrOpcoes); ?>
            </td>
        </tr>

        <tr class="tr_form_rodape2">
            <td colspan="4">

                <?= Helper::imprimirBotoesList(true, true); ?>

            </td>
        </tr>
    </table>

</fieldset>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


<?php
    
    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: acesso
    * DATA DE GERA��O:    08.02.2010
    * ARQUIVO:            acesso.php
    * TABELA MYSQL:       acesso
    * BANCO DE DADOS:     dep_pesquisas
    * -------------------------------------------------------
    * GERENCIADOR DE FORMUL�RIOS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    $obj = new EXTDAO_Acesso();
    
    $objArg = new Generic_Argument();
    
    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";    
    
    $nextActions = array("add_acesso"=>I18N::getExpression("Adicionar novo acesso"), 
                         "list_acesso"=>I18N::getExpression("Listar acesso"));

    ?>
    
    <?=$obj->getCabecalhoFormulario($postar); ?>
    
    <input type="hidden" name="junk" id="junk" value="junk">
    <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    <input type="hidden" name="class" id="class" value="<?=$class; ?>">
    <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    <input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_acesso">

    <? 

    for($cont=1; $cont <= $numeroRegistros; $cont++){ 

        if(Helper::SESSION("erro")){

            unset($_SESSION["erro"]);

           $obj->setBySession();

        }

        if(Helper::GET("id{$cont}")){

            $id = Helper::GET("id{$cont}");

            $obj->select($id);

        }

        $obj->formatarParaExibicao();

    ?>
    	
    <input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    <table class="tabela_form">

        <tr class="tr_form">

            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_usuario_id_INT;
            $objArg->valor = $obj->getUsuario_id_INT();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = true;
            $objArg->largura = 200;

            $obj->addInfoCampos("usuario_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo">
                <?=$obj->getComboBoxAllUsuario($objArg); ?>
            </td>

            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_data_login_DATETIME;
            $objArg->valor = $obj->getData_login_DATETIME();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo"><?=$obj->imprimirCampoData_login_DATETIME($objArg); ?></td>
        </tr>
        <tr class="tr_form">

            <?

            $objArg->numeroDoRegistro = $cont;
            $objArg->label = $obj->label_data_logout_DATETIME;
            $objArg->valor = $obj->getData_logout_DATETIME();
            $objArg->classeCss = "input_text";
            $objArg->classeCssFocus = "focus_text";
            $objArg->obrigatorio = false;
            $objArg->largura = 200;

            ?>

            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
            <td class="td_form_campo"><?=$obj->imprimirCampoData_logout_DATETIME($objArg); ?></td>

            <td class="td_form_label"></td>
            <td class="td_form_campo"></td>

        </tr>

     <? } ?> 

         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?=Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
    </table>

    <?=$obj->getInformacoesDeValidacaoDosCampos(); ?>    

    <?=$obj->getRodapeFormulario(); ?>


<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: cliente
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            cliente.php
 * TABELA MYSQL:       cliente
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Cliente();

$objArg = new Generic_Argument();

$numeroRegistros = 1;
$class = $obj->nomeClasse;
$action = (Helper::GET("id1") ? "edit" : "add");
$postar = "actions.php";

$nextActions = array("add_cliente" => "Adicionar novo cliente",
    "list_cliente" => "Listar clientes");
?>

<?= $obj->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="<?= $action; ?>_cliente">

<?
for ($cont = 1; $cont <= $numeroRegistros; $cont++) {

    if (Helper::SESSION("erro")) {

        unset($_SESSION["erro"]);

        $obj->setBySession();
    }

    if (Helper::GET("id{$cont}")) {

        $id = Helper::GET("id{$cont}");

        $obj->select($id);
        $legend = I18N::getExpression("Atualizar Cliente");
    } else {

        $legend = I18N::getExpression("Cadastrar Cliente");
    }

    $obj->formatarParaExibicao();
    ?>

    <input type="hidden" name="id<?= $cont ?>" id="id<?= $cont ?>" value="<?= $obj->getId(); ?>">

    <fieldset class="fieldset_form">
        <legend class="legend_form"><?= $legend; ?></legend>

        <table class="tabela_form">
            
            <tr class="tr_form">
            
                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_tipo_pessoa_id_INT;
                $objArg->valor = $obj->getTipo_pessoa_id_INT();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;

                $obj->addInfoCampos("tipo_pessoa_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3">
                    <?= $obj->getComboBoxAllTipo_pessoa($objArg); ?>
                </td>
                
            </tr>
            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_razao_social;
                $objArg->valor = $obj->getRazao_social();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 300;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoRazao_social($objArg); ?>

                </td>
                
                 <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_cnpj;
                $objArg->valor = $obj->getCnpj();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3">

                    <?= $obj->imprimirCampoCnpj($objArg); ?>

                </td>
                
            </tr>
            <tr class="tr_form">

                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_nome_fantasia;
                $objArg->valor = $obj->getNome_fantasia();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 300;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3">

                    <?= $obj->imprimirCampoNome_fantasia($objArg); ?>

                </td>
            </tr>            
            <tr class="tr_form">
            
                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_nome;
                $objArg->valor = $obj->getNome();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 300;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoNome($objArg); ?>

                </td>
                
                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_cpf;
                $objArg->valor = $obj->getCpf();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoCpf($objArg); ?>

                </td>
                
            </tr>            
            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_email_cobranca1;
                $objArg->valor = $obj->getEmail_cobranca1();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 300;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoEmail_cobranca1($objArg); ?>

                </td>


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_email_cobranca2;
                $objArg->valor = $obj->getEmail_cobranca2();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 300;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoEmail_cobranca2($objArg); ?>

                </td>
            </tr>
            
        </table>
    </fieldset>
    
    <fieldset class="fieldset_form">
        <legend class="legend_form">Endere�o</legend>

        <table class="tabela_form">
            
            <tr class="tr_form">

                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_endereco_logradouro;
                $objArg->valor = $obj->getEndereco_logradouro();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 400;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoEndereco_logradouro($objArg); ?>

                </td>


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_endereco_numero;
                $objArg->valor = $obj->getEndereco_numero();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 100;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoEndereco_numero($objArg); ?>

                </td>
            </tr>
            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_endereco_complemento;
                $objArg->valor = $obj->getEndereco_complemento();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoEndereco_complemento($objArg); ?>

                </td>


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_endereco_cep;
                $objArg->valor = $obj->getEndereco_cep();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoEndereco_cep($objArg); ?>

                </td>
            </tr>
            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_endereco_cidade;
                $objArg->valor = $obj->getEndereco_cidade();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoEndereco_cidade($objArg); ?>
                    <? Helper::imprimirComandoJavascript("$('#endereco_cidade{$cont}').autocomplete('actions.php?class=EXTDAO_Cliente&action=getListaJSON&campo=endereco_cidade');"); ?>


                </td>


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_uf_id_INT;
                $objArg->valor = $obj->getUf_id_INT();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;

                $obj->addInfoCampos("uf_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">
                    <?= $obj->getComboBoxAllUf($objArg); ?>
                </td>


            </tr>


        <? } ?>

       
    </table>

</fieldset>     

<fieldset class="fieldset_form">
    <legend class="legend_form">Telefones de Contato</legend>

    <?
    $numeroRegistroInterno = 1;

    if (is_numeric($id)) {

        $objBanco->query("SELECT id FROM cliente_telefone
                          WHERE cliente_id_INT={$id} AND excluido_BOOLEAN=0 ORDER BY id");

        for ($numeroRegistroInterno = 1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++) {

            $identificadorRelacionamento = $dados[0];

            echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";

            include('ajax_forms/cliente_telefone_relacionamento_cliente.php');

            echo "</div>";

            unset($identificadorRelacionamento);
        }
    }

//$numeroRegistroInterno++;
    ?>

    <?= Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("cliente_telefone", $numeroRegistroInterno); ?>

    <table class="tabela_form">

        <tr class="tr_form">

            <td class="td_botao_adicionar_novo_bloco">

                <?= Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "cliente_telefone_relacionamento_cliente", "div#cliente_telefone", "Adicionar Telefone") ?>

            </td>

        </tr>

    </table>
    
    <table>
        
         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?= Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?= Helper::getBarraDeBotoesDoFormulario(true, true, $action == "edit" ? true : false); ?>

            </td>
        </tr>
        
    </table>

</fieldset>
<br />

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


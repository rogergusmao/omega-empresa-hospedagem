<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: hospedagem
    * DATA DE GERA��O:    11.09.2014
    * ARQUIVO:            hospedagem.php
    * TABELA MYSQL:       hospedagem
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Hospedagem();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_hospedagem"=>I18N::getExpression("Adicionar nova hospedagem"),
    					 "list_hospedagem"=>I18N::getExpression("Listar hospedagens"));

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_hospedagem">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = I18N::getExpression("Atualizar hospedagem");

            }
            else{

            	$legend = I18N::getExpression("Cadastrar hospedagem");

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        	<input type="hidden" name="excluido_BOOLEAN" value="<?=$obj->getExcluido_BOOLEAN(); ?>" />
	<input type="hidden" name="excluido_DATETIME" value="<?=$obj->getExcluido_DATETIME(); ?>" />
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_dominio;
    			$objArg->valor = $obj->getDominio();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoDominio($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_dominio_webservice;
    			$objArg->valor = $obj->getDominio_webservice();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoDominio_webservice($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_empresa_hosting_id_INT;
                            $objArg->valor = $obj->getEmpresa_hosting_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = true;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("empresa_hosting_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllEmpresa_hosting($objArg); ?>
                            </td>


                            

    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_path;
    			$objArg->valor = $obj->getPath();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoPath($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_dominio_sincronizador;
    			$objArg->valor = $obj->getDominio_sincronizador();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoDominio_sincronizador($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_dominio_sincronizador_webservice;
    			$objArg->valor = $obj->getDominio_sincronizador_webservice();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoDominio_sincronizador_webservice($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_path_sincronizador;
    			$objArg->valor = $obj->getPath_sincronizador();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoPath_sincronizador($objArg); ?>
                            
                        </td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
            <td colspan="4">

                <?=Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

            </td>
        </tr>
	</table>

     </fieldset>     

         <fieldset class="fieldset_form">
            <legend class="legend_form">Hospedagens Do Banco De Dados</legend>

            <?
            
            $numeroRegistroInterno = 1;
            
            if(is_numeric($id)){

                $objBanco->query("SELECT id FROM hospedagem_db
                                    WHERE hospedagem_id_INT={$id} AND excluido_BOOLEAN=0 ORDER BY id");

                for($numeroRegistroInterno=1; $dados = $objBanco->fetchArray(); $numeroRegistroInterno++){

                    $identificadorRelacionamento = $dados[0];

                    echo "<div class=\"container_da_lista\" contador=\"{$numeroRegistroInterno}\">";
                                    
                        include('ajax_forms/hospedagem_db_relacionamento_hospedagem.php');
                    
                    echo "</div>";
                    
                    unset($identificadorRelacionamento);

                }
                                
            }
            
            //$numeroRegistroInterno++;

            ?>
            
            <?=Ajax::getContainerParaCarregamentoDeNovoBlocoEmLista("hospedagem_db", $numeroRegistroInterno); ?>

                <table class="tabela_form">

                    <tr class="tr_form">

                        <td class="td_botao_adicionar_novo_bloco">

                            <?=Ajax::getLinkParaCarregamentoDeNovoBlocoEmLista("ajax_forms", "hospedagem_db_relacionamento_hospedagem", "div#hospedagem_db", "Adicionar Hospedagem Do Banco De Dados") ?>

                        </td>
                                
                    </tr>

    		</table>
            
         </fieldset>
         <br />

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>


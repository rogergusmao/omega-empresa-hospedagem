<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: hospedagem_db
    * DATA DE GERA��O:    19.04.2014
    * ARQUIVO:            hospedagem_db.php
    * TABELA MYSQL:       hospedagem_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Hospedagem_db();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_hospedagem_db"=>I18N::getExpression("Adicionar nova hospedagem do banco de dados"),
    					 "list_hospedagem_db"=>I18N::getExpression("Listar hospedagens do banco de dados"));

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_hospedagem_db">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = I18N::getExpression("Atualizar hospedagem do banco de dados");

            }
            else{

            	$legend = I18N::getExpression("Cadastrar hospedagem do banco de dados");

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        	<input type="hidden" name="excluido_BOOLEAN" value="{$obj->getExcluido_BOOLEAN()}" />
	<input type="hidden" name="excluido_DATETIME" value="{$obj->getExcluido_DATETIME()}" />
			<tr class="tr_form">


                            <?
                            $objArg = new Generic_Argument();
                            $objArg->numeroDoRegistro = $cont;
                            $objArg->label = $obj->label_hospedagem_id_INT;
                            $objArg->valor = $obj->getHospedagem_id_INT();
                            $objArg->classeCss = "input_text";
                            $objArg->classeCssFocus = "focus_text";
                            $objArg->obrigatorio = true;
                            $objArg->largura = 200;

                            $obj->addInfoCampos("hospedagem_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);

                            ?>

                            <td class="td_form_label"><?=$objArg->getLabel() ?></td>
                            <td class="td_form_campo">
                                <?=$obj->getComboBoxAllHospedagem($objArg); ?>
                            </td>


                            

    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_nome_db;
    			$objArg->valor = $obj->getNome_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoNome_db($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_host_db;
    			$objArg->valor = $obj->getHost_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoHost_db($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_porta_db_INT;
    			$objArg->valor = $obj->getPorta_db_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoPorta_db_INT($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_usuario_db;
    			$objArg->valor = $obj->getUsuario_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoUsuario_db($objArg); ?>
                            
                        </td>


            	<td class="td_form_label"></td>
    			<td class="td_form_campo"></td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>


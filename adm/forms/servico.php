<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: servico
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            servico.php
 * TABELA MYSQL:       servico
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Servico();

$objArg = new Generic_Argument();

$numeroRegistros = 1;
$class = $obj->nomeClasse;
$action = (Helper::GET("id1") ? "edit" : "add");
$postar = "actions.php";

$nextActions = array("add_servico" => "Adicionar novo servi�o",
    "list_servico" => "Listar servi�os");
?>

<?= $obj->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="<?= $action; ?>_servico">

<?
for ($cont = 1; $cont <= $numeroRegistros; $cont++) {

    if (Helper::SESSION("erro")) {

        unset($_SESSION["erro"]);

        $obj->setBySession();
    }

    if (Helper::GET("id{$cont}")) {

        $id = Helper::GET("id{$cont}");

        $obj->select($id);
        $legend = I18N::getExpression("Atualizar Servi�o");
    } else {

        $legend = I18N::getExpression("Cadastrar Servi�o");
    }

    $obj->formatarParaExibicao();
    ?>

    <input type="hidden" name="id<?= $cont ?>" id="id<?= $cont ?>" value="<?= $obj->getId(); ?>">

    <fieldset class="fieldset_form">
        <legend class="legend_form"><?= $legend; ?></legend>

        <table class="tabela_form">

            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_cliente_id_INT;
                $objArg->valor = $obj->getCliente_id_INT();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 500;

                $obj->addInfoCampos("cliente_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3">
                    <?= $obj->getComboBoxAllCliente($objArg); ?>
                </td>
               
            </tr>
            
            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_descricao;
                $objArg->valor = $obj->getDescricao();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 500;

                $obj->addInfoCampos("cliente_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3">
                    <?= $obj->imprimirCampoDescricao($objArg); ?>
                </td>
               
            </tr>
            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_servico_modalidade_id_INT;
                $objArg->valor = $obj->getServico_modalidade_id_INT();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;

                $obj->addInfoCampos("servico_modalidade_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">
                    <?= $obj->getComboBoxAllServico_modalidade($objArg); ?>
                </td>

                 <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_data_contratacao_DATE;
                $objArg->valor = $obj->getData_contratacao_DATE();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoData_contratacao_DATE($objArg); ?>

                </td>

            </tr>
            <tr class="tr_form">
            
                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_valor_FLOAT;
                $objArg->valor = $obj->getValor_FLOAT();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoValor_FLOAT($objArg); ?>

                </td>
                
                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_is_ativo_BOOLEAN;
                $objArg->valor = $obj->getIs_ativo_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 80;
                $objArg->labelTrue = "Sim";
                $objArg->labelFalse = "N�o";
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoIs_ativo_BOOLEAN($objArg); ?>

                </td>
            
            </tr>
            <tr class="tr_form">


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_dia_mes_vencimento_INT;
                $objArg->valor = $obj->getDia_mes_vencimento_INT();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoDia_mes_vencimento_INT($objArg); ?>

                </td>


                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_data_vencimento_DATE;
                $objArg->valor = $obj->getData_vencimento_DATE();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">

                    <?= $obj->imprimirCampoData_vencimento_DATE($objArg); ?>

                </td>
            </tr>


        <? } ?>

        <tr class="tr_form_rodape1">
            <td colspan="4">

                <?= Helper::getBarraDaNextAction($nextActions); ?>

            </td>
        </tr>
        <tr class="tr_form_rodape2">
            <td colspan="4" >

                <?= Helper::getBarraDeBotoesDoFormulario(true, true, $action == "edit" ? true : false); ?>

            </td>
        </tr>
    </table>

</fieldset>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


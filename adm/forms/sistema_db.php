<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FORMUL�RIO: sistema_db
    * DATA DE GERA��O:    19.04.2014
    * ARQUIVO:            sistema_db.php
    * TABELA MYSQL:       sistema_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema_db();

    $objArg = new Generic_Argument();

    $numeroRegistros = 1;
    $class = $obj->nomeClasse;
    $action = (Helper::GET("id1")?"edit": "add");
    $postar = "actions.php";

    $nextActions = array("add_sistema_db"=>I18N::getExpression("Adicionar novo banco do sistema"),
    					 "list_sistema_db"=>I18N::getExpression("Listar bancos do sistema"));

    ?>

    <?=$obj->getCabecalhoFormulario($postar); ?>

        <input type="hidden" name="numeroRegs" id="numeroRegs" value="<?=$numeroRegistros; ?>">
    	<input type="hidden" name="class" id="class" value="<?=$class; ?>">
        <input type="hidden" name="action" id="action" value="<?=$action; ?>">
    	<input type="hidden" name="origin_action" id="origin_action" value="<?=$action; ?>_sistema_db">

    	<?

    	for($cont=1; $cont <= $numeroRegistros; $cont++){

            if(Helper::SESSION("erro")){

                unset($_SESSION["erro"]);

               $obj->setBySession();

            }

            if(Helper::GET("id{$cont}")){

                $id = Helper::GET("id{$cont}");

                $obj->select($id);
                $legend = I18N::getExpression("Atualizar banco do sistema");

            }
            else{

            	$legend = I18N::getExpression("Cadastrar banco do sistema");

            }

            $obj->formatarParaExibicao();

    	?>

    	<input type="hidden" name="id<?=$cont ?>" id="id<?=$cont ?>" value="<?=$obj->getId(); ?>">

    	<fieldset class="fieldset_form">
            <legend class="legend_form"><?=$legend; ?></legend>

        <table class="tabela_form">

        	<input type="hidden" name="excluido_BOOLEAN" value="{$obj->getExcluido_BOOLEAN()}" />
	<input type="hidden" name="excluido_DATETIME" value="{$obj->getExcluido_DATETIME()}" />
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_identificador;
    			$objArg->valor = $obj->getIdentificador();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoIdentificador($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_assinatura_host_db;
    			$objArg->valor = $obj->getAssinatura_host_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoAssinatura_host_db($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_assinatura_porta_db_INT;
    			$objArg->valor = $obj->getAssinatura_porta_db_INT();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoAssinatura_porta_db_INT($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_assinatura_usuario_db;
    			$objArg->valor = $obj->getAssinatura_usuario_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoAssinatura_usuario_db($objArg); ?>
                            
                        </td>
			</tr>
			<tr class="tr_form">


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_assinatura_senha_db;
    			$objArg->valor = $obj->getAssinatura_senha_db();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = true;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoAssinatura_senha_db($objArg); ?>
                            
                        </td>


    			<?
                        $objArg = new Generic_Argument();
    			$objArg->numeroDoRegistro = $cont;
    			$objArg->label = $obj->label_path_arquivo_dump;
    			$objArg->valor = $obj->getPath_arquivo_dump();
    			$objArg->classeCss = "input_text";
    			$objArg->classeCssFocus = "focus_text";
    			$objArg->obrigatorio = false;
    			$objArg->largura = 200;

    			?>

    			<td class="td_form_label"><?=$objArg->getLabel() ?></td>
    			<td class="td_form_campo">
                        
                            <?=$obj->imprimirCampoPath_arquivo_dump($objArg); ?>
                            
                        </td>
			</tr>


     	 <? } ?>

         <tr class="tr_form_rodape1">
        	<td colspan="4">

        	    <?=Helper::getBarraDaNextAction($nextActions); ?>

        	</td>
        </tr>
        <tr class="tr_form_rodape2">
        	<td colspan="4" >

        		<?=Helper::getBarraDeBotoesDoFormulario(true, true, $action=="edit"?true:false); ?>

        	</td>
        </tr>
	</table>

     </fieldset>

	<?=$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=$obj->getRodapeFormulario(); ?>


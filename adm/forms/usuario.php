<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMULÁRIO: usuario
 * DATA DE GERAÇÃO:    16.01.2010
 * ARQUIVO:            usuario.php
 * TABELA MYSQL:       usuario
 * BANCO DE DADOS:     dep_pesquisas
 * -------------------------------------------------------
 *
 * GERENCIADOR DE FORMULÁRIOS DO EDUARDO
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Usuario();

$objArg = new Generic_Argument();

$numeroRegistros = 1;
$class = $obj->nomeClasse;
$action = (Helper::GET("id1") ? "edit" : "add");
$postar = "actions.php";

$nextActions = array("add_usuario" => "Adicionar novo usuário",
    "list_usuario" => "Listar usuários");
?>

<?= Helper::carregarArquivoJavascript(1, "recursos/js/", "sistema"); ?>

<?= $obj->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="junk" id="junk" value="junk">
<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="<?= $action; ?>_usuario">

<?
for ($cont = 1; $cont <= $numeroRegistros; $cont++) {

    if (Helper::SESSION("erro")) {

        unset($_SESSION["erro"]);

        $obj->setBySession();
    }

    if (Helper::GET("id{$cont}")) {

        $id = Helper::GET("id{$cont}");
        $obj->select($id);

        $legend = I18N::getExpression("Atualizar Usuário");
    } else {

        $legend = I18N::getExpression("Cadastrar Usuário");
    }

    $obj->formatarParaExibicao();
    ?>

    <input type="hidden" name="id<?= $cont ?>" id="id<?= $cont ?>" value="<?= $obj->getId(); ?>">

    <fieldset class="fieldset_list">
        <legend class="legend_list"><?= $legend ?></legend>

        <table class="tabela_form">

            <tr class="tr_form">


                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_nome;
                $objArg->valor = $obj->getNome();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 300;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoNome($objArg); ?></td>

                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_email;
                $objArg->valor = $obj->getEmail();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 300;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoEmail($objArg); ?></td>
            </tr>
            <tr class="tr_form">


                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_senha;
                $objArg->valor = $obj->getSenha();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $id ? "Já definida" : $obj->imprimirCampoSenha($objArg); ?></td>


                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_usuario_tipo_id_INT;
                $objArg->valor = $obj->getUsuario_tipo_id_INT();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;

                $obj->addInfoCampos("usuario_tipo_id_INT", $objArg->label, "TEXTO", $objArg->obrigatorio);
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo">
                    <?= $obj->getComboBoxAllUsuario_tipo($objArg); ?>
                </td>

            </tr>
            <tr class="tr_form">

                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_pagina_inicial;
                $objArg->valor = $obj->getPagina_inicial();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 500;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3"><?= $obj->imprimirCampoPagina_inicial($objArg); ?></td>
            </tr>
            <tr class="tr_form">

                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_status_BOOLEAN;
                $objArg->labelTrue = "Ativo";
                $objArg->labelFalse = "Inativo";
                $objArg->valor = $obj->getStatus_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 80;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3"><?= $obj->imprimirCampoStatus_BOOLEAN($objArg); ?></td>

            </tr>

        </table>
    </fieldset>    

    <?
    $arrPermissoes = array();

    //edicao
    if ($id) {

        $objBanco->query("SELECT identificador_funcionalidade
                                              FROM usuario_privilegio
                                              WHERE usuario_id_INT={$id}");

        $arrPermissoes = Helper::getResultSetToMatriz($objBanco->getResultSet());
        $arrPermissoes = Helper::getMatrizLinearToArray($arrPermissoes);
    }

    $objSeguranca = new Seguranca();
    $objSeguranca->montarListaDeFuncionalidades();

    $listaDeFuncionalidades = $objSeguranca->getListaDeFuncionalidades();

    $i = 0;
    ?>

 
    <?
    $arrPermissoesMenu = array();

    //edicao
    if ($id) {

        $objBanco->query("SELECT area_menu
                              FROM usuario_menu
                              WHERE usuario_id_INT={$id}");

        $arrPermissoesMenu = Helper::getResultSetToMatriz($objBanco->getResultSet());
        $arrPermissoesMenu = Helper::getMatrizLinearToArray($arrPermissoesMenu);
    }
    ?>

    <fieldset class="fieldset_list">
        <legend class="legend_list"><?=I18N::getExpression("Visibilidade do Menu"); ?></legend>

        <?
        Menu::imprimirTelaDeSelecionarAreasDoMenu($arrPermissoesMenu);
        ?>

    </fieldset>

<? } ?>

<table>
    <tr class="tr_form_rodape1">
        <td colspan="4">

            <?= Helper::getBarraDaNextAction($nextActions); ?>

        </td>
    </tr>
    <tr class="tr_form_rodape2">
        <td colspan="4" >

            <?= Helper::getBarraDeBotoesDoFormulario(true, true, $action == "edit" ? true : false); ?>

        </td>
    </tr>
</table>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


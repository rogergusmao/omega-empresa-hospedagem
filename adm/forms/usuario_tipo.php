<?php
/*
 *
 * -------------------------------------------------------
 * NOME DO FORMUL�RIO: usuario_tipo
 * DATA DE GERA��O:    23.10.2009
 * ARQUIVO:            usuario_tipo.php
 * TABELA MYSQL:       usuario_tipo
 * BANCO DE DADOS:     engenharia
 * -------------------------------------------------------
 * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
 * GERENCIADOR DE FORMUL�RIOS DO EDUARDO
 * -------------------------------------------------------
 *
 */

$obj = new EXTDAO_Usuario_tipo();

$objArg = new Generic_Argument();

$numeroRegistros = 1;
$class = $obj->nomeClasse;
$action = (Helper::GET("id1") ? "edit" : "add");
$postar = "actions.php";

$nextActions = array("add_usuario_tipo" => "Adicionar novo tipo de usu�rio",
    "list_usuario_tipo" => "Listar tipos de usu�rio");
?>

<?= Helper::carregarArquivoJavascript(1, "recursos/js/", "sistema"); ?>

<?= $obj->getCabecalhoFormulario($postar); ?>

<input type="hidden" name="junk" id="junk" value="junk">
<input type="hidden" name="numeroRegs" id="numeroRegs" value="<?= $numeroRegistros; ?>">
<input type="hidden" name="class" id="class" value="<?= $class; ?>">
<input type="hidden" name="action" id="action" value="<?= $action; ?>">
<input type="hidden" name="origin_action" id="origin_action" value="<?= $action; ?>_usuario_tipo">

<?
for ($cont = 1; $cont <= $numeroRegistros; $cont++) {

    if (Helper::SESSION("erro")) {

        unset($_SESSION["erro"]);

        $obj->setBySession();
    }

    if (Helper::GET("id{$cont}")) {

        $id = Helper::GET("id{$cont}");

        $obj->select($id);

        $legend = I18N::getExpression("Atualziar Classe de Usu�rio");
    } else {

        $legend = I18N::getExpression("Cadastrar Classe de Usu�rio");
    }

    $obj->formatarParaExibicao();
    ?>

    <input type="hidden" name="id<?= $cont ?>" id="id<?= $cont ?>" value="<?= $obj->getId(); ?>">

    <fieldset class="fieldset_list">
        <legend class="legend_list"><?= $legend ?></legend>

        <table class="tabela_form">

            <tr class="tr_form">

                <td class="td_form_label" style="font-weight: bold;" colspan="4"><?= $titulo ?></td>

            </tr>

            <tr class="tr_form">

                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_nome;
                $objArg->valor = $obj->getNome();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoNome($objArg); ?></td>

                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_status_BOOLEAN;
                $objArg->labelTrue = "Ativo";
                $objArg->labelFalse = "Inativo";
                $objArg->valor = $obj->getStatus_BOOLEAN();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 80;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoStatus_BOOLEAN($objArg); ?></td>

            </tr>

            <tr class="tr_form">

                <?
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_nome_visivel;
                $objArg->valor = $obj->getNome_visivel();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = true;
                $objArg->largura = 200;
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo"><?= $obj->imprimirCampoNome_visivel($objArg); ?></td>

                <td class="td_form_label"></td>
                <td class="td_form_campo"></td>

            </tr>
            <tr class="tr_form">

                <?
                $objArg = new Generic_Argument();
                $objArg->numeroDoRegistro = $cont;
                $objArg->label = $obj->label_pagina_inicial;
                $objArg->valor = $obj->getPagina_inicial();
                $objArg->classeCss = "input_text";
                $objArg->classeCssFocus = "focus_text";
                $objArg->obrigatorio = false;
                $objArg->largura = 500;
                $objArg->nome = "pagina_inicial";
                $objArg->id = $objArg->nome;

                $arrPaginas = Helper::getArrayDePaginasIniciais();
                ?>

                <td class="td_form_label"><?= $objArg->getLabel() ?></td>
                <td class="td_form_campo" colspan="3">

                    <?= $obj->getComboBoxArrayValores($objArg, $arrPaginas); ?>

                </td>

            </tr>

        </table>
    </fieldset>  

    <?
    $arrPermissoes = array();

    //edicao
    if ($id) {

        $objBanco->query("SELECT identificador_funcionalidade
                              FROM usuario_tipo_privilegio
                              WHERE usuario_tipo_id_INT={$id}");

        $arrPermissoes = Helper::getResultSetToMatriz($objBanco->getResultSet());
        $arrPermissoes = Helper::getMatrizLinearToArray($arrPermissoes);
    }

    $objSeguranca = new Seguranca();
    $objSeguranca->montarListaDeFuncionalidades();

    $listaDeFuncionalidades = $objSeguranca->getListaDeFuncionalidades();

    $i = 0;
    ?>

    <fieldset class="fieldset_list">
        <legend class="legend_list">Permiss�es</legend>

        <table class="tabela_discreta">

            <colgroup>
                <col width="5%" />
                <col width="45%" />
                <col width="5%" />
                <col width="45%" />
            </colgroup>

            <? foreach ($listaDeFuncionalidades as $identificador => $funcionalidade) { ?>

                <? if ($i % 2 == 0) { ?>

                    <tr class="tr_form">

                    <? } ?>

                    <td class="td_form_label">

                        <input type="checkbox" name="funcionalidades[]" value="<?= $identificador ?>" <?= in_array($identificador, $arrPermissoes) ? "checked=\"checked\"" : ""; ?> />

                    </td>

                    <td class="td_form_campo">

                        <?= $funcionalidade->nomeFuncionalidade ?>

                    </td>

                    <? if ($i % 2 == 1) { ?>

                    </tr>

                <? } ?>

            <? } ?>

        </table>

    </fieldset>

    <?
    $arrPermissoesMenu = array();

    //edicao
    if ($id) {

        $objBanco->query("SELECT area_menu
    							  FROM usuario_tipo_menu
    							  WHERE usuario_tipo_id_INT={$id}");

        $arrPermissoesMenu = Helper::getResultSetToMatriz($objBanco->getResultSet());
        $arrPermissoesMenu = Helper::getMatrizLinearToArray($arrPermissoesMenu);
    }
    ?>

    <fieldset class="fieldset_list">
        <legend class="legend_list"><?=I18N::getExpression("Visibilidade do Menu"); ?></legend>

        <?
        Menu::imprimirTelaDeSelecionarAreasDoMenu($arrPermissoesMenu);
        ?>

    </fieldset>

<? } ?>

<table>

    <tr class="tr_form_rodape1">
        <td colspan="4">

            <?= Helper::getBarraDaNextAction($nextActions); ?>

        </td>
    </tr>
    <tr class="tr_form_rodape2">
        <td colspan="4" >

            <?= Helper::getBarraDeBotoesDoFormulario(true, true, $action == "edit" ? true : false); ?>

        </td>
    </tr>
</table>

<?= $obj->getInformacoesDeValidacaoDosCampos(); ?>

<?= $obj->getRodapeFormulario(); ?>


<?php

	Javascript::setRaizDaEstrutura();

    echo Javascript::importarBibliotecaFusionCharts();

    $objBanco = new Database();
    $objEstruturaOrg = new Estrutura_Organizacional();
    $objGrafico = new Grafico("Aporte de Conhecimento Tácito - Contratados");

    $arrComplemento = $objEstruturaOrg->getArrayDePartesParaConsultaSQLRecursiva($objGrafico->arrayNiveisAConsultar, "v.nodo_nivel_estrutura_id_INT", $objGrafico->somenteAreasDaPesquisa, false, false, "nome", $objGrafico->consultarVagas);

    $objConsultas = new ConsultasDosGraficos($arrComplemento);

    $objConsultas->consultaNumeroDeVagasContratadasPorNSPlanejado($objBanco);

    $totalClientesNenhumaContratado = 0;
    $totalClientesBaixaContratado = 0;
    $totalClientesMediaContratado = 0;
    $totalClientesAltaContratado  = 0;

    while($dados = $objBanco->fetchArray()){

        if($dados[1] == "0")
            $totalClientesNenhumaContratado = $dados[0];
        elseif($dados[1] == "1")
            $totalClientesBaixaContratado = $dados[0];
        elseif($dados[1] == "2")
            $totalClientesMediaContratado = $dados[0];
        elseif($dados[1] == "3")
            $totalClientesAltaContratado = $dados[0];

    }

    $totalMobilizado = $totalClientesNenhumaContratado + $totalClientesBaixaContratado +
                       $totalClientesMediaContratado + $totalClientesAltaContratado;



    //NUMERO DE PESSOAS CONTRATADAS (REAL)
    $objConsultas->consultaNumeroDeVagasContratadasPorNSReal($objBanco);

    $totalClientesNenhumaContratadoReal = 0;
    $totalClientesBaixaContratadoReal = 0;
    $totalClientesMediaContratadoReal = 0;
    $totalClientesAltaContratadoReal = 0;

    while($dados = $objBanco->fetchArray()){

        if($dados[1] == "0")
            $totalClientesNenhumaContratadoReal = $dados[0];
        elseif($dados[1] == "1")
            $totalClientesBaixaContratadoReal = $dados[0];
        elseif($dados[1] == "2")
            $totalClientesMediaContratadoReal = $dados[0];
        elseif($dados[1] == "3")
            $totalClientesAltaContratadoReal = $dados[0];

    }

    $totalClassificados = $totalClientesNenhumaContratadoReal + $totalClientesBaixaContratadoReal +
                          $totalClientesMediaContratadoReal + $totalClientesAltaContratadoReal;



    //NUMERO DE VAGAS A CONTRATAR
    $objConsultas->consultaNumeroDeVagasAContratarPorNSPlanejado($objBanco);

    $totalClientesNenhumaAContratar = 0;
    $totalClientesBaixaAContratar = 0;
    $totalClientesMediaAContratar = 0;
    $totalClientesAltaAContratar = 0;

    while($dados = $objBanco->fetchArray()){

        if($dados[1] == "0")
            $totalClientesNenhumaAContratar = $dados[0];
        elseif($dados[1] == "1")
            $totalClientesBaixaAContratar = $dados[0];
        elseif($dados[1] == "2")
            $totalClientesMediaAContratar = $dados[0];
        elseif($dados[1] == "3")
            $totalClientesAltaAContratar = $dados[0];

    }


    //ANOS DE EXPERIENCIA: REAL CONTRATADO

    $objConsultas->consultaTempoTotalDeExperienciaRealContratadosEmMesesNoNivelDeSimilaridade(1, $objBanco);

    $arrBaixaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrBaixaSim = Helper::getMatrizLinearToArray($arrBaixaSim);



    $objConsultas->consultaTempoTotalDeExperienciaRealContratadosEmMesesNoNivelDeSimilaridade(2, $objBanco);

    $arrMediaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrMediaSim = Helper::getMatrizLinearToArray($arrMediaSim);



    $objConsultas->consultaTempoTotalDeExperienciaRealContratadosEmMesesNoNivelDeSimilaridade(3, $objBanco);

    $arrAltaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrAltaSim = Helper::getMatrizLinearToArray($arrAltaSim);


    //calculo do total de anos
    $totalAnosBaixaReal = round(array_sum($arrBaixaSim) / 12, 1);
    $totalAnosMediaReal = round(array_sum($arrMediaSim) / 12, 1);
    $totalAnosAltaReal =  round(array_sum($arrAltaSim)  / 12, 1);

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosBaixaReal;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesBaixaContratadoReal;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosMediaReal;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesMediaContratadoReal;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosAltaReal;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesAltaContratadoReal;



    //ANOS DE EXPERIENCIA: PLANEJADO CONTRATADO

    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaContratadosEmMesesNoNivelDeSimilaridade(1, $objBanco);

    $arrBaixaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrBaixaSim = Helper::getMatrizLinearToArray($arrBaixaSim);



    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaContratadosEmMesesNoNivelDeSimilaridade(2, $objBanco);

    $arrMediaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrMediaSim = Helper::getMatrizLinearToArray($arrMediaSim);



    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaContratadosEmMesesNoNivelDeSimilaridade(3, $objBanco);

    $arrAltaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrAltaSim = Helper::getMatrizLinearToArray($arrAltaSim);



    //calculo do total de anos
    $totalAnosBaixaPlanejado = round(array_sum($arrBaixaSim) /12, 1);
    $totalAnosMediaPlanejado = round(array_sum($arrMediaSim) / 12, 1);
    $totalAnosAltaPlanejado =  round(array_sum($arrAltaSim) / 12, 1);

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosBaixaPlanejado;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesBaixaContratado;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosMediaPlanejado;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesMediaContratado;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosAltaPlanejado;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesAltaContratado;



    //ANOS DE EXPERIENCIA: PLANEJADO A CONTRATAR

    //alta similaridade
    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaAContratarEmMesesNoNivelDeSimilaridade(3, $objBanco);

    $arrAltaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrAltaSim = Helper::getMatrizLinearToArray($arrAltaSim);



    //media similaridade
    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaAContratarEmMesesNoNivelDeSimilaridade(2, $objBanco);

    $arrMediaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrMediaSim = Helper::getMatrizLinearToArray($arrMediaSim);



    //baixa similaridade
    $objConsultas->consultaTempoTotalDeExperienciaPlanejadaAContratarEmMesesNoNivelDeSimilaridade(1, $objBanco);

    $arrBaixaSim = Helper::getResultSetToMatriz($objBanco->result, 0, 1);
    $arrBaixaSim = Helper::getMatrizLinearToArray($arrBaixaSim);



    //calculo do total de anos
    $totalAnosBaixaAContratar = round(array_sum($arrBaixaSim) /12, 1);
    $totalAnosMediaAContratar = round(array_sum($arrMediaSim) / 12, 1);
    $totalAnosAltaAContratar =  round(array_sum($arrAltaSim) / 12, 1);

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosBaixaAContratar;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesBaixaAContratar;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosMediaAContratar;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesMediaAContratar;

    Grafico::$numeroAnosGraficoAporte[]    = $totalAnosAltaAContratar;
    Grafico::$numeroClientesGraficoAporte[] = $totalClientesAltaAContratar;

    $totalAnosBaixaTudo = $totalAnosBaixaPlanejado + $totalAnosBaixaAContratar;
    $totalAnosMediaTudo = $totalAnosMediaPlanejado + $totalAnosMediaAContratar;
    $totalAnosAltaTudo = $totalAnosAltaPlanejado + $totalAnosAltaAContratar;

    $maximoY = max(array($totalAnosAltaTudo, $totalAnosMediaTudo));

    $maximoYNovo = round($maximoY, -2);

    if($maximoYNovo < $maximoY){

    	$maximoYNovo += 100;

    }

    $maximoY = $maximoYNovo;

    $strPathSWF = Helper::acharRaiz() . "recursos/libs/fusion_charts";

    $strXML  = "<chart bgColor='FFFFFF' outCnvBaseFontColor='666666' caption='Aporte de Conhecimento Tácito'  xAxisName='Alta Similaridade' yAxisName='' numberPrefix='' showValues='0'
                    numVDivLines='10' showAlternateVGridColor='1' AlternateVGridColor='e1f5ff' divLineColor='e1f5ff' vdivLineColor='e1f5ff'  baseFontColor='666666' showLegend='0'
                    showLimits='0' showDivLineValue='0' yAxisMinValue='0' yAxisMaxValue='{$maximoY}' toolTipBgColor='F3F3F3' toolTipBorderColor='666666' canvasBorderColor='666666' canvasBorderThickness='1' showPlotBorder='1' plotFillAlpha='80'>
                    <categories>
                            <category label='' />
                            <category label='' />
                    </categories>
                    <dataset seriesName='Atual' color='B1D1DC' plotBorderColor='B1D1DC'>
                            <set value='{$totalAnosAltaReal}' />
                            <set value='{$totalAnosAltaPlanejado}'/>
                    </dataset>
                    <dataset seriesName='Demanda Futura' color='C8A1D1' plotBorderColor='C8A1D1'>
                            <set  />
                            <set value='{$totalAnosAltaAContratar}'/>
                    </dataset>
                    <styles>
                            <definition>
                                    <style type='animation' name='TrendAnim' param='_alpha' duration='1' start='0' />
                            </definition>
                            <application>
                                    <apply toObject='TRENDLINES' styles='TrendAnim' />
                            </application>
                    </styles>
                </chart>";

    //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
    echo renderChart("{$strPathSWF}/StackedBar3D.swf", "", $strXML, "alta", $objGrafico->larguraDoGrafico, 195, false, false);

    echo "<br />";

    $strXML  = "<chart bgColor='FFFFFF' outCnvBaseFontColor='666666' caption=''  xAxisName='Média Similaridade' yAxisName='Anos de Experiência' numberPrefix='' showValues='0'
                    numVDivLines='10' showAlternateVGridColor='1' AlternateVGridColor='e1f5ff' divLineColor='e1f5ff' vdivLineColor='e1f5ff'  baseFontColor='666666' showLegend='0'
                    yAxisMinValue='0' yAxisMaxValue='{$maximoY}' toolTipBgColor='F3F3F3' toolTipBorderColor='666666' canvasBorderColor='666666' canvasBorderThickness='1' showPlotBorder='1' plotFillAlpha='80'>
                    <categories>
                            <category label='' />
                            <category label='' />
                    </categories>
                    <dataset seriesName='Atual' color='B1D1DC' plotBorderColor='B1D1DC'>
                            <set value='{$totalAnosMediaReal}' />
                            <set value='{$totalAnosMediaPlanejado}'/>
                    </dataset>
                    <dataset seriesName='Demanda Futura' color='C8A1D1' plotBorderColor='C8A1D1'>
                            <set  />
                            <set value='{$totalAnosMediaAContratar}'/>
                    </dataset>
                    <styles>
                            <definition>
                                    <style type='animation' name='TrendAnim' param='_alpha' duration='1' start='0' />
                            </definition>
                            <application>
                                    <apply toObject='TRENDLINES' styles='TrendAnim' />
                            </application>
                    </styles>
                </chart>";

    //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
    echo renderChart("{$strPathSWF}/StackedBar3D.swf", "", $strXML, "media", $objGrafico->larguraDoGrafico, 200, false, false);

    echo "<br />";

    $realBaixaNenhumaContratados = $totalClientesBaixaContratadoReal + $totalClientesNenhumaContratadoReal;
    $realNaoClassificados = $totalMobilizado - $totalClassificados;
    $planejadoBaixaNenhumaContratados = $totalClientesBaixaContratado + $totalClientesNenhumaContratado;
    $planejadoAContratar = $totalClientesBaixaAContratar + $totalClientesNenhumaAContratar;

    $strXML  = "<chart bgColor='FFFFFF' outCnvBaseFontColor='666666' caption=''  xAxisName='Baixa / Outros' yAxisName='Número de Clientes' numberPrefix='' showValues='0'
                    numVDivLines='10' showAlternateVGridColor='1' AlternateVGridColor='e1f5ff' divLineColor='e1f5ff' vdivLineColor='e1f5ff'  baseFontColor='666666' showLegend='0'
                    toolTipBgColor='F3F3F3' toolTipBorderColor='666666' canvasBorderColor='666666' canvasBorderThickness='1' showPlotBorder='1' plotFillAlpha='80'>
                    <categories>
                            <category label='' />
                            <category label='' />
                    </categories>
                    <dataset seriesName='Atual' color='B1D1DC' plotBorderColor='B1D1DC'>
                            <set value='{$realBaixaNenhumaContratados}' />
                            <set value='{$planejadoBaixaNenhumaContratados}'/>
                    </dataset>
                    <dataset seriesName='Demanda Futura' color='C8A1D1' plotBorderColor='C8A1D1'>
                            <set  />
                            <set value='{$planejadoAContratar}'/>
                    </dataset>
                    <dataset seriesName='Não Classificados' color='4FADE1' plotBorderColor='C8A1D1'>
                            <set value='{$realNaoClassificados}'/>
                            <set />
                    </dataset>
                    <styles>
                            <definition>
                                    <style type='animation' name='TrendAnim' param='_alpha' duration='1' start='0' />
                            </definition>
                            <application>
                                    <apply toObject='TRENDLINES' styles='TrendAnim' />
                            </application>
                    </styles>
                </chart>";

    //Create the chart - Column 3D Chart with data from strXML variable using dataXML method
    echo renderChart("{$strPathSWF}/StackedBar3D.swf", "", $strXML, "baixa", $objGrafico->larguraDoGrafico, 200, false, false);

?>

<?php
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       acesso
    * NOME DA CLASSE DAO: DAO_Acesso
    * DATA DE GERA��O:    08.02.2010
    * ARQUIVO:            EXTDAO_Acesso.php
    * TABELA MYSQL:       acesso
    * BANCO DE DADOS:     dep_pesquisas
    * -------------------------------------------------------
    *
    * GERENCIADOR DE DATAGRIDS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    
    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
    $acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
    $acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
    $acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");
    
    include("filters/acesso.php");
    
    $registrosPorPagina = REGISTROS_POR_PAGINA;
    
    $registrosPesquisa = 1;
    
    $obj = new EXTDAO_Acesso();    
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();
    
    $strCondicao = array();
    $strGET = array();
    
    
     
         if(!Helper::isNull($obj->getUsuario_id_INT())){
         
            $strCondicao[] = "usuario_id_INT={$obj->getUsuario_id_INT()}";
            $strGET[] = "usuario_id_INT={$obj->getUsuario_id_INT()}";

        }
     
         if(!Helper::isNull($obj->getData_login_DATETIME())){
         
            $strCondicao[] = "data_login_DATETIME={$obj->getData_login_DATETIME()}";
            $strGET[] = "data_login_DATETIME={$obj->getData_login_DATETIME()}";

        }
     
         if(!Helper::isNull($obj->getData_logout_DATETIME())){
         
            $strCondicao[] = "data_logout_DATETIME={$obj->getData_logout_DATETIME()}";
            $strGET[] = "data_logout_DATETIME={$obj->getData_logout_DATETIME()}";

        }
    
    $consulta = "";
    
    for($i=0; $i<count($strCondicao); $i++){
        
        if($i == 0)
            $consulta .= " AND " . $strCondicao[$i];
        else 
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];
        
    }
    
    $consultaNumero = "SELECT COUNT(id) FROM acesso WHERE excluido_BOOLEAN=0 " . $consulta;
    
    $objBanco = new Database();
    
    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);
    
    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);
    
    $consultaRegistros = "SELECT id FROM acesso WHERE excluido_BOOLEAN=0 " . $consulta . " ORDER BY usuario_id_INT LIMIT {$limites[0]},{$limites[1]}";
    
    $objBanco->query($consultaRegistros);
    
    ?>
    
   <fieldset class="fieldset_list">
            <legend class="legend_list"><?=I18N::getExpression("Lista de Acesso"); ?></legend>
    
   <table class="tabela_list">
   		<colgroup>
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_usuario_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_login_DATETIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_data_logout_DATETIME ?></td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){
    
    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();
    	
    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"
    	
    	
    ?>
    
    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                	<? $obj->objUsuario->select($obj->getUsuario_id_INT()) ?>
                    <? $obj->objUsuario->formatarParaExibicao() ?>
                	<?=$obj->objUsuario->valorCampoLabel() ?>
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_login_DATETIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getData_logout_DATETIME() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=acesso&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=acesso&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Acesso&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>

    
    
		</tr>
    
    <? } ?>
    
    </tbody>
    </table>
    
    </fieldset>
    
    <br/>
    <br/>
    
    <?
    
    //Pagina��o
    
    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);
    
    if($numeroPaginas > 1){
    
    ?>
    
    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Pagina��o</legend>
    
	<table class="table_paginacao">
		<tr class="tr_paginacao">
    
	<?
	
	for($i=1; $i <= $numeroPaginas; $i++){
	
		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"
	
	?>
		
		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=acesso&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>
	
	<? } ?>
	
	    </tr>
	</table>
	
	</fieldset>
	
	<? } ?>
	
	
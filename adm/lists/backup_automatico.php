<?php
    
    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       backup_automatico
    * NOME DA CLASSE DAO: DAO_Backup_automatico
    * DATA DE GERA��O:    25.08.2010
    * ARQUIVO:            EXTDAO_Backup_automatico.php
    * TABELA MYSQL:       backup_automatico
    * BANCO DE DADOS:     DEP_pesquisas_config
    * -------------------------------------------------------
    *
    * GERENCIADOR DE DATAGRIDS DO EDUARDO
    * -------------------------------------------------------
    *
    */
    
    
    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
    $acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
    $acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
    $acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");
    
    include("filters/backup_automatico.php");
    
    $registrosPorPagina = REGISTROS_POR_PAGINA;
    
    $registrosPesquisa = 1;
    
    $obj = new EXTDAO_Backup_automatico();    
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();
    
    $strCondicao = array();
    $strGET = array();
    
    
     
         if(!Helper::isNull($obj->getHora_base_TIME())){
         
            $strCondicao[] = "hora_base_TIME LIKE '%{$obj->getHora_base_TIME()}%'";
            $strGET[] = "hora_base_TIME={$obj->getHora_base_TIME()}";

        }
     
         if(!Helper::isNull($obj->getStatus_BOOLEAN())){
         
            $strCondicao[] = "status_BOOLEAN={$obj->getStatus_BOOLEAN()}";
            $strGET[] = "status_BOOLEAN={$obj->getStatus_BOOLEAN()}";

        }
    
    $consulta = "";
    
    for($i=0; $i<count($strCondicao); $i++){
        
        if($i == 0)
            $consulta .= " AND " . $strCondicao[$i];
        else 
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];
        
    }
    
    $consultaNumero = "SELECT COUNT(id) FROM backup_automatico WHERE excluido_BOOLEAN=0 " . $consulta;
    
    $objBanco = new Database(NOME_BANCO_DE_DADOS_DE_CONFIGURACAO);
    
    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);
    
    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);
    
    $consultaRegistros = "SELECT id FROM backup_automatico WHERE excluido_BOOLEAN=0 " . $consulta . " ORDER BY hora_base_TIME LIMIT {$limites[0]},{$limites[1]}";
    
    $objBanco->query($consultaRegistros);
    
    ?>
    
    

   <fieldset class="fieldset_list">
            <legend class="legend_list">Lista de Backups Autom�ticos</legend>
    
   <table class="tabela_list">
   		<colgroup>
			<col width="25%" />
			<col width="25%" />
			<col width="25%" />
			<col width="25%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_hora_base_TIME ?></td>
			<td class="td_list_titulos"><?=$obj->label_status_BOOLEAN ?></td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){
    
    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();
    	
    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"
    	
    	
    ?>
    
    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getHora_base_TIME() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getStatus_BOOLEAN()?"Ativo":"Inativo" ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=backup_automatico&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=backup_automatico&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Backup_automatico&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>

    
    
		</tr>
    
    <? } ?>
    
    </tbody>
    </table>
    
    <br />
    <input type="button" value="Cadastrar Novo Backup Autom�tico" onclick="javascript:document.location.href='index.php?tipo=forms&page=backup_automatico'" class="botoes_form"/>
    
    </fieldset>
    
    <br/>
    <br/>
    
    <?
    
    //Pagina��o
    
    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);
    
    if($numeroPaginas > 1){
    
    ?>
    
    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Pagina��o</legend>
    
	<table class="table_paginacao">
		<tr class="tr_paginacao">
    
	<?
	
	for($i=1; $i <= $numeroPaginas; $i++){
	
		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"
	
	?>
		
		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=backup_automatico&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>
	
	<? } ?>
	
	    </tr>
	</table>
	
	</fieldset>
	
	<? } ?>
	
	
<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       cliente
 * NOME DA CLASSE DAO: DAO_Cliente
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            EXTDAO_Cliente.php
 * TABELA MYSQL:       cliente
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
$acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
$acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
$acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

include("filters/cliente.php");

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Cliente();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getRazao_social())) {

    $strCondicao[] = "razao_social LIKE '%{$obj->getRazao_social()}%'";
    $strGET[] = "razao_social1={$obj->getRazao_social()}";
}

if (!Helper::isNull($obj->getNome_fantasia())) {

    $strCondicao[] = "nome_fantasia LIKE '%{$obj->getNome_fantasia()}%'";
    $strGET[] = "nome_fantasia1={$obj->getNome_fantasia()}";
}

if (!Helper::isNull($obj->getCnpj())) {

    $strCondicao[] = "cnpj1={$obj->getCnpj()}";
    $strGET[] = "cnpj1={$obj->getCnpj()}";
}

if (!Helper::isNull($obj->getNome())) {

    $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
    $strGET[] = "nome1={$obj->getNome()}";
}

if (!Helper::isNull($obj->getCpf())) {

    $strCondicao[] = "cpf1={$obj->getCpf()}";
    $strGET[] = "cpf1={$obj->getCpf()}";
}

if (!Helper::isNull($obj->getTipo_pessoa_id_INT())) {

    $strCondicao[] = "tipo_pessoa_id_INT1={$obj->getTipo_pessoa_id_INT()}";
    $strGET[] = "tipo_pessoa_id_INT1={$obj->getTipo_pessoa_id_INT()}";
}

if (!Helper::isNull($obj->getEndereco_cidade())) {

    $strCondicao[] = "endereco_cidade LIKE '%{$obj->getEndereco_cidade()}%'";
    $strGET[] = "endereco_cidade1={$obj->getEndereco_cidade()}";
}

if (!Helper::isNull($obj->getUf_id_INT())) {

    $strCondicao[] = "uf_id_INT1={$obj->getUf_id_INT()}";
    $strGET[] = "uf_id_INT1={$obj->getUf_id_INT()}";
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++) {

    $consulta .= " AND " . $strCondicao[$i];
}

for ($i = 0; $i < count($strGET); $i++) {

    $varGET .= "&" . $strGET[$i];
}

$consultaNumero = "SELECT COUNT(id) FROM cliente WHERE excluido_BOOLEAN=0 {$consulta}";

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT id FROM cliente WHERE excluido_BOOLEAN=0 {$consulta} ORDER BY razao_social LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);
?>



<fieldset class="fieldset_list">
    <legend class="legend_list"><?=I18N::getExpression("Lista de Clientes"); ?></legend>

    <table class="tabela_list">
        <colgroup>
            <col width="4%" />
            <col width="28%" />
            <col width="12%" />
            <col width="12%" />
            <col width="22%" />
            <col width="10%" />
            <col width="10%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">
                <td class="td_list_titulos"><?="Id" ?></td>
                <td class="td_list_titulos"><?="Raz�o Social / Nome" ?></td>
                <td class="td_list_titulos"><?="CNPJ / CPF"?></td>
                <td class="td_list_titulos"><?= $obj->label_tipo_pessoa_id_INT ?></td>
                <td class="td_list_titulos"><?="Emails de Cobran�a" ?></td>
                <td class="td_list_titulos"><?="Cidade/UF" ?></td>
                <td class="td_list_titulos">A��es</td>

            </tr>
        </thead>
        <tbody>

            <?
            if ($objBanco->rows() == 0) {
                ?>

                <tr class="tr_list_conteudo_impar">
                    <td  colspan="7">
                        <?= Helper::imprimirMensagem("Nenhum cliente foi cadastrado at� o momento.") ?>
                    </td>
                </tr>

                <?
            }

            for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

                $obj->select($regs[0]);
                $obj->formatarParaExibicao();

                $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par"
                ?>

                <tr class="<?= $classTr ?>">
                    
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getId() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getDescricaoDoNomeDoCliente() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getCpfCnpj() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getTipo_pessoa_id_INT())) {

                            $obj->objTipo_pessoa->select($obj->getTipo_pessoa_id_INT());
                            $obj->objTipo_pessoa->formatarParaExibicao();
                            ?>

                            <?= $obj->objTipo_pessoa->valorCampoLabel() ?>

                        <? } ?>

                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getListaDeEmails() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                                                
                        <?
                        if (strlen($obj->getUf_id_INT())) {

                            $obj->objUf->select($obj->getUf_id_INT());
                            $obj->objUf->formatarParaExibicao();
                            ?>

                            <?= $obj->getEndereco_cidade() ?> / <?= $obj->objUf->getSigla() ?>

                        <? } ?>
                            
                    </td>

                    <td class="td_list_conteudo" style="text-align: center;">
                        <img class="icones_list" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=cliente&id1=<?= $obj->getId(); ?>'" onmouseover="javascript:tip('<?= $acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                        <img class="icones_list" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Cliente&action=remove&id=<?= $obj->getId(); ?>','<?= $acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?= $acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                    </td>



                </tr>

            <? } ?>

        </tbody>
    </table>

</fieldset>

<br/>
<br/>

<?
//Pagina��o

$paginaAtual = Helper::GET("pagina") ? Helper::GET("pagina") : "1";
$numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

if ($numeroPaginas > 1) {
    ?>

    <fieldset class="fieldset_paginacao">
        <legend class="legend_paginacao">Pagina��o</legend>

        <table class="table_paginacao">
            <tr class="tr_paginacao">

                <?
                for ($i = 1; $i <= $numeroPaginas; $i++) {

                    $class = ($i == $paginaAtual) ? "td_paginacao_pag_atual" : "td_paginacao"
                    ?>

                    <td class="<?= $class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=cliente&pagina=<?= $i ?><?= $varGET ?>'"><?= $i ?></td>

                <? } ?>

            </tr>
        </table>

    </fieldset>

<? } ?>


<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       cobranca
 * NOME DA CLASSE DAO: DAO_Cobranca
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            EXTDAO_Cobranca.php
 * TABELA MYSQL:       cobranca
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
$acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
$acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
$acoes["tooltip_enviar_email"] = I18N::getExpression("Clique aqui para enviar email com esta cobran�a");

include("filters/cobranca.php");

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Cobranca();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getData_vencimento_DATE())) {

    $strCondicao[] = "co.data_vencimento_DATE1={$obj->getData_vencimento_DATE()}";
    $strGET[] = "data_vencimento_DATE1={$obj->getData_vencimento_DATE()}";
}

if (!Helper::isNull($obj->getValor_FLOAT())) {

    $strCondicao[] = "co.valor_FLOAT1={$obj->getValor_FLOAT()}";
    $strGET[] = "valor_FLOAT1={$obj->getValor_FLOAT()}";
}

if (!Helper::isNull($obj->getStatus_pagamento_id_INT())) {

    $strCondicao[] = "co.status_pagamento_id_INT1={$obj->getStatus_pagamento_id_INT()}";
    $strGET[] = "status_pagamento_id_INT1={$obj->getStatus_pagamento_id_INT()}";
}

if (!Helper::isNull($obj->getMes_ano_referencia())) {

    $strCondicao[] = "co.mes_ano_referencia LIKE '%{$obj->getMes_ano_referencia()}%'";
    $strGET[] = "mes_ano_referencia1={$obj->getMes_ano_referencia()}";
}

if (!Helper::isNull(Helper::GET("cliente_id_INT1"))) {

    $strCondicao[] = "c.id=" . Helper::GET("cliente_id_INT1");
    $strGET[] = "cliente_id_INT1=" . Helper::GET("cliente_id_INT1");
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++) {

    $consulta .= " AND " . $strCondicao[$i];
}

for ($i = 0; $i < count($strGET); $i++) {

    $varGET .= "&" . $strGET[$i];
}

$consultaNumero = "SELECT COUNT(DISTINCT co.id) FROM cobranca AS co, cliente AS c, servico AS s WHERE s.id=co.servico_id_INT AND c.id=s.cliente_id_INT AND co.excluido_BOOLEAN=0 {$consulta}";

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT co.id FROM cobranca AS co, cliente AS c, servico AS s WHERE s.id=co.servico_id_INT AND c.id=s.cliente_id_INT AND co.excluido_BOOLEAN=0 {$consulta} ORDER BY c.id, co.servico_id_INT, co.mes_ano_referencia LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);
?>

<fieldset class="fieldset_list">
    <legend class="legend_list">Lista de Cobran�a</legend>

    <table class="tabela_list">
        <colgroup>
            <col width="5%" />
            <col width="24%" />
            <col width="24%" />
            <col width="8%" />
            <col width="8%" />
            <col width="7%" />
            <col width="15%" />          
            <col width="9%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">

                <td class="td_list_titulos"><?= $obj->label_id ?></td>
                <td class="td_list_titulos"><?= "Cliente" ?></td>
                <td class="td_list_titulos"><?= $obj->label_servico_id_INT ?></td>
                <td class="td_list_titulos"><?= $obj->label_mes_ano_referencia ?></td>
                <td class="td_list_titulos"><?= $obj->label_data_vencimento_DATE ?></td>
                <td class="td_list_titulos"><?= $obj->label_valor_FLOAT ?></td>
                <td class="td_list_titulos"><?= $obj->label_status_pagamento_id_INT ?></td>                    
                <td class="td_list_titulos">A��es</td>

            </tr>
        </thead>
        <tbody>

            <?
            if ($objBanco->rows() == 0) {
                ?>

                <tr class="tr_list_conteudo_impar">
                    <td  colspan="8">
                        <?= Helper::imprimirMensagem("Nenhuma cobran�a foi cadastrada at� o momento.") ?>
                    </td>
                </tr>

                <?
            }

            for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

                $obj->select($regs[0]);
                $obj->formatarParaExibicao();

                $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par"
                ?>

                <tr class="<?= $classTr ?>">

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getId() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getServico_id_INT())) {

                            $obj->objServico->select($obj->getServico_id_INT());
                            $obj->objServico->formatarParaExibicao();
                            ?>

                            <?= $obj->objServico->objCliente->getDescricaoDoNomeDoCliente() ?>

                        <? } ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getServico_id_INT())) {

                            $obj->objServico->select($obj->getServico_id_INT());
                            $obj->objServico->formatarParaExibicao();
                            ?>

                            <?= $obj->objServico->getDescricao() ?>

                        <? } ?>

                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getMes_ano_referencia() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getData_vencimento_DATE() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getValor_FLOAT() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getStatus_pagamento_id_INT())) {

                            $obj->objStatus_pagamento->select($obj->getStatus_pagamento_id_INT());
                            $obj->objStatus_pagamento->formatarParaExibicao();
                            ?>

                            <?= $obj->objStatus_pagamento->valorCampoLabel() ?>

                        <? } ?>

                    </td>

                    <td class="td_list_conteudo" style="text-align: center;">
                        <img class="icones_list" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=cobranca&id1=<?= $obj->getId(); ?>'" onmouseover="javascript:tip('<?= $acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                        
                        <? if($obj->getStatus_pagamento_id_INT() != "3"){ ?>
                        
                            <img class="icones_list" src="imgs/icone_email.png" onclick="javascript:location.href='actions.php?class=EXTDAO_Cobranca_mensagem&action=__actionEnviarMensagem&cobranca_id_INT=<?= $obj->getId(); ?>'" onmouseover="javascript:tip('<?= $acoes['tooltip_enviar_email'] ?>')" onmouseout="javascript:notip()">&nbsp;
                        
                        <? } ?>
                        
                        <img class="icones_list" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Cobranca&action=remove&id=<?= $obj->getId(); ?>','<?= $acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?= $acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                    </td>

                </tr>

            <? } ?>

        </tbody>
    </table>

</fieldset>

<br/>
<br/>

<?
//Pagina��o

$paginaAtual = Helper::GET("pagina") ? Helper::GET("pagina") : "1";
$numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

if ($numeroPaginas > 1) {
    ?>

    <fieldset class="fieldset_paginacao">
        <legend class="legend_paginacao">Pagina��o</legend>

        <table class="table_paginacao">
            <tr class="tr_paginacao">

                <?
                for ($i = 1; $i <= $numeroPaginas; $i++) {

                    $class = ($i == $paginaAtual) ? "td_paginacao_pag_atual" : "td_paginacao"
                    ?>

                    <td class="<?= $class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=cobranca&pagina=<?= $i ?><?= $varGET ?>'"><?= $i ?></td>

                <? } ?>

            </tr>
        </table>

    </fieldset>

<? } ?>


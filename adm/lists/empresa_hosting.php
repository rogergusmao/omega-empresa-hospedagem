<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       empresa_hosting
    * NOME DA CLASSE DAO: DAO_Empresa_hosting
    * DATA DE GERA��O:    14.04.2014
    * ARQUIVO:            EXTDAO_Empresa_hosting.php
    * TABELA MYSQL:       empresa_hosting
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
    $acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
    $acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
    $acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

    include("filters/empresa_hosting.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Empresa_hosting();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getNome())){

            $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
            $strGET[] = "nome1={$obj->getNome()}";

        }

         if(!Helper::isNull($obj->getEndereco_painel_controle())){

            $strCondicao[] = "endereco_painel_controle LIKE '%{$obj->getEndereco_painel_controle()}%'";
            $strGET[] = "endereco_painel_controle1={$obj->getEndereco_painel_controle()}";

        }

         if(!Helper::isNull($obj->getEndereco_cobranca())){

            $strCondicao[] = "endereco_cobranca LIKE '%{$obj->getEndereco_cobranca()}%'";
            $strGET[] = "endereco_cobranca1={$obj->getEndereco_cobranca()}";

        }

    $consulta = "";

    for($i=0; $i < count($strCondicao); $i++){

        $consulta .= " AND " . $strCondicao[$i];

    }
    
    for($i=0; $i < count($strGET); $i++){

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM empresa_hosting WHERE excluido_BOOLEAN=0 {$consulta}";

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM empresa_hosting WHERE excluido_BOOLEAN=0 {$consulta} ORDER BY nome LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list"><?=I18N::getExpression("Lista de Hostings Da Empresa"); ?></legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
			<col width="20%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos"><?=$obj->label_endereco_painel_controle ?></td>
			<td class="td_list_titulos"><?=$obj->label_endereco_cobranca ?></td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? 
    
    if($objBanco->rows() == 0){
    
    ?>
    
    <tr class="tr_list_conteudo_impar">
        <td  colspan="5">
            <?=Helper::imprimirMensagem("Nenhum hosting da empresa foi cadastrado at� o momento.") ?>
        </td>
    </tr>

    <?

    }

    for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getEndereco_painel_controle() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getEndereco_cobranca() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img class="icones_list" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=empresa_hosting&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=empresa_hosting&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Empresa_hosting&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Pagina��o

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Pagina��o</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=empresa_hosting&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       erro
    * NOME DA CLASSE DAO: DAO_Erro
    * DATA DE GERA��O:    04.12.2010
    * ARQUIVO:            EXTDAO_Erro.php
    * TABELA MYSQL:       erro
    * BANCO DE DADOS:     DEP_pesquisas
    * -------------------------------------------------------
    *
    * GERENCIADOR DE DATAGRIDS DO EDUARDO
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
    $acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
    $acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
    $acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

    include("filters/erro.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Erro();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();



         if(!Helper::isNull($obj->getUsuario_id_INT())){

            $strCondicao[] = "usuario_id_INT={$obj->getUsuario_id_INT()}";
            $strGET[] = "usuario_id_INT={$obj->getUsuario_id_INT()}";

        }

         if(!Helper::isNull($obj->getCodigo_erro())){

            $strCondicao[] = "codigo_erro LIKE '%{$obj->getCodigo_erro()}%'";
            $strGET[] = "codigo_erro={$obj->getCodigo_erro()}";

        }

         if(!Helper::isNull($obj->getMensagem_erro())){

            $strCondicao[] = "mensagem_erro LIKE '%{$obj->getMensagem_erro()}%'";
            $strGET[] = "mensagem_erro={$obj->getMensagem_erro()}";

        }

         if(!Helper::isNull($obj->getUrl())){

            $strCondicao[] = "url LIKE '%{$obj->getUrl()}%'";
            $strGET[] = "url={$obj->getUrl()}";

        }

         if(!Helper::isNull($obj->getArquivo_erro())){

            $strCondicao[] = "arquivo_erro LIKE '%{$obj->getArquivo_erro()}%'";
            $strGET[] = "arquivo_erro={$obj->getArquivo_erro()}";

        }

         if(!Helper::isNull($obj->getLinha_erro())){

            $strCondicao[] = "linha_erro LIKE '%{$obj->getLinha_erro()}%'";
            $strGET[] = "linha_erro={$obj->getLinha_erro()}";

        }

         if(!Helper::isNull($obj->getGet())){

            $strCondicao[] = "get LIKE '%{$obj->getGet()}%'";
            $strGET[] = "get={$obj->getGet()}";

        }

         if(!Helper::isNull($obj->getPost())){

            $strCondicao[] = "post LIKE '%{$obj->getPost()}%'";
            $strGET[] = "post={$obj->getPost()}";

        }

         if(!Helper::isNull($obj->getSession())){

            $strCondicao[] = "session LIKE '%{$obj->getSession()}%'";
            $strGET[] = "session={$obj->getSession()}";

        }

         if(!Helper::isNull($obj->getStacktrace())){

            $strCondicao[] = "stacktrace LIKE '%{$obj->getStacktrace()}%'";
            $strGET[] = "stacktrace={$obj->getStacktrace()}";

        }

         if(!Helper::isNull($obj->getDatahora_DATETIME())){

            $strCondicao[] = "datahora_DATETIME={$obj->getDatahora_DATETIME()}";
            $strGET[] = "datahora_DATETIME={$obj->getDatahora_DATETIME()}";

        }

         if(!Helper::isNull($obj->getStatus_BOOLEAN())){

            $strCondicao[] = "status_BOOLEAN={$obj->getStatus_BOOLEAN()}";
            $strGET[] = "status_BOOLEAN={$obj->getStatus_BOOLEAN()}";

        }

    $consulta = "";

    for($i=0; $i<count($strCondicao); $i++){

        if($i == 0)
            $consulta .= "WHERE " . $strCondicao[$i];
        else
            $consulta .= " AND " . $strCondicao[$i];

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM erro WHERE excluido_BOOLEAN=0 " . $consulta;

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM erro WHERE excluido_BOOLEAN=0 " . $consulta . " ORDER BY datahora_DATETIME LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

   <fieldset class="fieldset_list">
            <legend class="legend_list"><?=I18N::getExpression("Lista de Erros"); ?></legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_usuario_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_codigo_erro ?></td>
			<td class="td_list_titulos"><?=$obj->label_arquivo_erro ?></td>
			<td class="td_list_titulos"><?=$obj->label_linha_erro ?></td>
			<td class="td_list_titulos"><?=$obj->label_datahora_DATETIME ?></td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";

    	if(!$obj->getStatus_BOOLEAN()){

    	    $complementoTr = "background-color: #F9FF56;";

    	}
    	else{

    	    $complementoTr = "";

    	}

    ?>

    	<tr class="<?=$classTr ?>" style="<?=$complementoTr ?>">

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

            <td class="td_list_conteudo" style="align: left; padding-left: 5px;">
            	<? $obj->objUsuario->select($obj->getUsuario_id_INT()) ?>
                <? $obj->objUsuario->formatarParaExibicao() ?>
            	<?=$obj->objUsuario->valorCampoLabel() ?>
            </td>

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getCodigo_erro() ?>
    		</td>

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getArquivo_erro() ?>
    		</td>

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getLinha_erro() ?>
    		</td>

    		<td class="td_list_conteudo" style="align: left; padding-left: 5px;">
    			<?=$obj->getDatahora_DATETIME() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img border="0" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=erro&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Erro&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>

		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Pagina��o

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Pagina��o</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=erro&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>


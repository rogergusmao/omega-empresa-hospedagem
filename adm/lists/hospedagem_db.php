<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       hospedagem_db
    * NOME DA CLASSE DAO: DAO_Hospedagem_db
    * DATA DE GERA��O:    19.04.2014
    * ARQUIVO:            EXTDAO_Hospedagem_db.php
    * TABELA MYSQL:       hospedagem_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
    $acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
    $acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
    $acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

    include("filters/hospedagem_db.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Hospedagem_db();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getHospedagem_id_INT())){

            $strCondicao[] = "hospedagem_id_INT1={$obj->getHospedagem_id_INT()}";
            $strGET[] = "hospedagem_id_INT1={$obj->getHospedagem_id_INT()}";

        }

         if(!Helper::isNull($obj->getNome_db())){

            $strCondicao[] = "nome_db LIKE '%{$obj->getNome_db()}%'";
            $strGET[] = "nome_db1={$obj->getNome_db()}";

        }

         if(!Helper::isNull($obj->getHost_db())){

            $strCondicao[] = "host_db LIKE '%{$obj->getHost_db()}%'";
            $strGET[] = "host_db1={$obj->getHost_db()}";

        }

         if(!Helper::isNull($obj->getPorta_db_INT())){

            $strCondicao[] = "porta_db_INT1={$obj->getPorta_db_INT()}";
            $strGET[] = "porta_db_INT1={$obj->getPorta_db_INT()}";

        }

         if(!Helper::isNull($obj->getUsuario_db())){

            $strCondicao[] = "usuario_db LIKE '%{$obj->getUsuario_db()}%'";
            $strGET[] = "usuario_db1={$obj->getUsuario_db()}";

        }

    $consulta = "";

    for($i=0; $i < count($strCondicao); $i++){

        $consulta .= " AND " . $strCondicao[$i];

    }
    
    for($i=0; $i < count($strGET); $i++){

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM hospedagem_db WHERE excluido_BOOLEAN=0 {$consulta}";

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM hospedagem_db WHERE excluido_BOOLEAN=0 {$consulta} ORDER BY nome_db LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list"><?=I18N::getExpression("Lista de Hospedagens Do Banco De Dados"); ?></legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_hospedagem_id_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_nome_db ?></td>
			<td class="td_list_titulos"><?=$obj->label_host_db ?></td>
			<td class="td_list_titulos"><?=$obj->label_porta_db_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_usuario_db ?></td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? 
    
    if($objBanco->rows() == 0){
    
    ?>
    
    <tr class="tr_list_conteudo_impar">
        <td  colspan="7">
            <?=Helper::imprimirMensagem("Nenhuma hospedagem do banco de dados foi cadastrada at� o momento.") ?>
        </td>
    </tr>

    <?

    }

    for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getHospedagem_id_INT())){
                
                        $obj->objHospedagem->select($obj->getHospedagem_id_INT());
                        $obj->objHospedagem->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->objHospedagem->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getNome_db() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getHost_db() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getPorta_db_INT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getUsuario_db() ?>
    		</td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img class="icones_list" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=hospedagem_db&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=hospedagem_db&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Hospedagem_db&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Pagina��o

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Pagina��o</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=hospedagem_db&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       servico
 * NOME DA CLASSE DAO: DAO_Servico
 * DATA DE GERA��O:    17.08.2012
 * ARQUIVO:            EXTDAO_Servico.php
 * TABELA MYSQL:       servico
 * BANCO DE DADOS:     omegasoftware_interno
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
$acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
$acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
$acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

include("filters/servico.php");

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Servico();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();



if (!Helper::isNull($obj->getCliente_id_INT())) {

    $strCondicao[] = "cliente_id_INT1={$obj->getCliente_id_INT()}";
    $strGET[] = "cliente_id_INT1={$obj->getCliente_id_INT()}";
}

if (!Helper::isNull(Helper::GET("data_contratacao_DATE_de1"))) {

    $dataContratacaoDe = Helper::GET("data_contratacao_DATE_de1");
    $dataContratacaoDeSQL = Helper::formatarDataParaComandoSQL($dataContratacaoDe);
    
    $strCondicao[] = "data_contratacao_DATE >={$dataContratacaoDeSQL}";
    $strGET[] = "data_contratacao_DATE_de1={$dataContratacaoDe}";
    
}

if (!Helper::isNull(Helper::GET("data_contratacao_DATE_ate1"))){

    $dataContratacaoAte = Helper::GET("data_contratacao_DATE_ate1");
    $dataContratacaoAteSQL = Helper::formatarDataParaComandoSQL($dataContratacaoAte);
    
    $strCondicao[] = "data_contratacao_DATE <={$dataContratacaoAteSQL}";
    $strGET[] = "data_contratacao_DATE_ate1={$dataContratacaoAte}";
    
}

if (!Helper::isNull($obj->getServico_modalidade_id_INT())) {

    $strCondicao[] = "servico_modalidade_id_INT={$obj->getServico_modalidade_id_INT()}";
    $strGET[] = "servico_modalidade_id_INT1={$obj->getServico_modalidade_id_INT()}";
}

if (!Helper::isNull($obj->getDia_mes_vencimento_INT())) {

    $strCondicao[] = "dia_mes_vencimento_INT={$obj->getDia_mes_vencimento_INT()}";
    $strGET[] = "dia_mes_vencimento_INT1={$obj->getDia_mes_vencimento_INT()}";
}

if (!Helper::isNull(Helper::GET("data_vencimento_DATE_de1"))) {

    $dataVencimentoDe = Helper::GET("data_vencimento_DATE_de1");
    $dataVencimentoDeSQL = Helper::formatarDataParaComandoSQL($dataVencimentoDe);
    
    $strCondicao[] = "data_vencimento_DATE >={$dataVencimentoDeSQL}";
    $strGET[] = "data_vencimento_DATE_de1={$dataVencimentoDe}";
    
}

if (!Helper::isNull(Helper::GET("data_vencimento_DATE_ate1"))){

    $dataVencimentoAte = Helper::GET("data_vencimento_DATE_ate1");
    $dataVencimentoAteSQL = Helper::formatarDataParaComandoSQL($dataVencimentoAte);
    
    $strCondicao[] = "data_vencimento_DATE <={$dataVencimentoAteSQL}";
    $strGET[] = "data_vencimento_DATE_ate1={$dataVencimentoAte}";
    
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++) {

    $consulta .= " AND " . $strCondicao[$i];
}

for ($i = 0; $i < count($strGET); $i++) {

    $varGET .= "&" . $strGET[$i];
}

$consultaNumero = "SELECT COUNT(id) FROM servico WHERE excluido_BOOLEAN=0 {$consulta}";

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT id FROM servico WHERE excluido_BOOLEAN=0 {$consulta} ORDER BY data_contratacao_DATE LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);
?>



<fieldset class="fieldset_list">
    <legend class="legend_list">Lista de Servi�os</legend>

    <table class="tabela_list">
        <colgroup>
            <col width="4%" />
            <col width="20%" />
            <col width="20%" />
            <col width="10%" />
            <col width="10%" />
            <col width="8%" />
            <col width="8%" />
            <col width="9%" />
            <col width="11%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">

                <td class="td_list_titulos"><?= $obj->label_id ?></td>
                <td class="td_list_titulos"><?= $obj->label_cliente_id_INT ?></td>
                <td class="td_list_titulos"><?= $obj->label_descricao ?></td>
                <td class="td_list_titulos"><?= $obj->label_is_ativo_BOOLEAN ?></td>
                <td class="td_list_titulos"><?= $obj->label_data_contratacao_DATE ?></td>
                <td class="td_list_titulos"><?= $obj->label_servico_modalidade_id_INT ?></td>
                <td class="td_list_titulos"><?= $obj->label_valor_FLOAT ?></td>
                <td class="td_list_titulos"><?= "Vencimento" ?></td>
                <td class="td_list_titulos">A��es</td>

            </tr>
        </thead>
        <tbody>

            <?
            if ($objBanco->rows() == 0) {
                ?>

                <tr class="tr_list_conteudo_impar">
                    <td  colspan="8">
                        <?= Helper::imprimirMensagem("Nenhum servi�o foi cadastrado at� o momento.") ?>
                    </td>
                </tr>

                <?
            }

            for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

                $obj->select($regs[0]);
                $obj->formatarParaExibicao();

                $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par"
                ?>

                <tr class="<?= $classTr ?>">

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getId() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getCliente_id_INT())) {

                            $obj->objCliente->select($obj->getCliente_id_INT());
                            $obj->objCliente->formatarParaExibicao();
                            ?>

                            <?= $obj->objCliente->getDescricaoDoNomeDoCliente() ?>

                        <? } ?>

                    </td>
                    
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getDescricao() ?>
                    </td>
                    
                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getIs_ativo_BOOLEAN()?"Sim":"N�o"; ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getData_contratacao_DATE() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">

                        <?
                        if (strlen($obj->getServico_modalidade_id_INT())) {

                            $obj->objServico_modalidade->select($obj->getServico_modalidade_id_INT());
                            $obj->objServico_modalidade->formatarParaExibicao();
                            ?>

                            <?= $obj->objServico_modalidade->valorCampoLabel() ?>

                        <? } ?>

                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        R$ <?= $obj->getValor_FLOAT() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getDescricaoDoVencimento() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: center;">
                        <img class="icones_list" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=servico&id1=<?= $obj->getId(); ?>'" onmouseover="javascript:tip('<?= $acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                        <img class="icones_list" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Servico&action=remove&id=<?= $obj->getId(); ?>','<?= $acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?= $acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                    </td>



                </tr>

            <? } ?>

        </tbody>
    </table>

</fieldset>

<br/>
<br/>

<?
//Pagina��o

$paginaAtual = Helper::GET("pagina") ? Helper::GET("pagina") : "1";
$numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

if ($numeroPaginas > 1) {
    ?>

    <fieldset class="fieldset_paginacao">
        <legend class="legend_paginacao">Pagina��o</legend>

        <table class="table_paginacao">
            <tr class="tr_paginacao">

                <?
                for ($i = 1; $i <= $numeroPaginas; $i++) {

                    $class = ($i == $paginaAtual) ? "td_paginacao_pag_atual" : "td_paginacao"
                    ?>

                    <td class="<?= $class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=servico&pagina=<?= $i ?><?= $varGET ?>'"><?= $i ?></td>

                <? } ?>

            </tr>
        </table>

    </fieldset>

<? } ?>


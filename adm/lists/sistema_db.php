<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       sistema_db
    * NOME DA CLASSE DAO: DAO_Sistema_db
    * DATA DE GERA��O:    19.04.2014
    * ARQUIVO:            EXTDAO_Sistema_db.php
    * TABELA MYSQL:       sistema_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    $acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
    $acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
    $acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
    $acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

    include("filters/sistema_db.php");

    $registrosPorPagina = REGISTROS_POR_PAGINA;

    $registrosPesquisa = 1;

    $obj = new EXTDAO_Sistema_db();
    $obj->setByGet($registrosPesquisa);
    $obj->formatarParaSQL();

    $strCondicao = array();
    $strGET = array();

    

         if(!Helper::isNull($obj->getAssinatura_host_db())){

            $strCondicao[] = "assinatura_host_db LIKE '%{$obj->getAssinatura_host_db()}%'";
            $strGET[] = "assinatura_host_db1={$obj->getAssinatura_host_db()}";

        }

         if(!Helper::isNull($obj->getAssinatura_porta_db_INT())){

            $strCondicao[] = "assinatura_porta_db_INT1={$obj->getAssinatura_porta_db_INT()}";
            $strGET[] = "assinatura_porta_db_INT1={$obj->getAssinatura_porta_db_INT()}";

        }

         if(!Helper::isNull($obj->getAssinatura_usuario_db())){

            $strCondicao[] = "assinatura_usuario_db LIKE '%{$obj->getAssinatura_usuario_db()}%'";
            $strGET[] = "assinatura_usuario_db1={$obj->getAssinatura_usuario_db()}";

        }

         if(!Helper::isNull($obj->getAssinatura_senha_db())){

            $strCondicao[] = "assinatura_senha_db LIKE '%{$obj->getAssinatura_senha_db()}%'";
            $strGET[] = "assinatura_senha_db1={$obj->getAssinatura_senha_db()}";

        }

         if(!Helper::isNull($obj->getSistema_id_INT())){

            $strCondicao[] = "sistema_id_INT1={$obj->getSistema_id_INT()}";
            $strGET[] = "sistema_id_INT1={$obj->getSistema_id_INT()}";

        }

    $consulta = "";

    for($i=0; $i < count($strCondicao); $i++){

        $consulta .= " AND " . $strCondicao[$i];

    }
    
    for($i=0; $i < count($strGET); $i++){

        $varGET .= "&" . $strGET[$i];

    }

    $consultaNumero = "SELECT COUNT(id) FROM sistema_db WHERE excluido_BOOLEAN=0 {$consulta}";

    $objBanco = new Database();

    $objBanco->query($consultaNumero);
    $numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

    $limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

    $consultaRegistros = "SELECT id FROM sistema_db WHERE excluido_BOOLEAN=0 {$consulta} ORDER BY id LIMIT {$limites[0]},{$limites[1]}";

    $objBanco->query($consultaRegistros);

    ?>

    

   <fieldset class="fieldset_list">
            <legend class="legend_list"><?=I18N::getExpression("Lista de Bancos Do Sistema"); ?></legend>

   <table class="tabela_list">
   		<colgroup>
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
			<col width="14%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_id ?></td>
			<td class="td_list_titulos"><?=$obj->label_assinatura_host_db ?></td>
			<td class="td_list_titulos"><?=$obj->label_assinatura_porta_db_INT ?></td>
			<td class="td_list_titulos"><?=$obj->label_assinatura_usuario_db ?></td>
			<td class="td_list_titulos"><?=$obj->label_assinatura_senha_db ?></td>
			<td class="td_list_titulos"><?=$obj->label_sistema_id_INT ?></td>
			<td class="td_list_titulos">A��es</td>

		</tr>
		</thead>
    	<tbody>

    <? 
    
    if($objBanco->rows() == 0){
    
    ?>
    
    <tr class="tr_list_conteudo_impar">
        <td  colspan="7">
            <?=Helper::imprimirMensagem("Nenhum banco do sistema foi cadastrado at� o momento.") ?>
        </td>
    </tr>

    <?

    }

    for($i=1; $regs = $objBanco->fetchArray(); $i++){

    	$obj->select($regs[0]);
    	$obj->formatarParaExibicao();

    	$classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par"


    ?>

    	<tr class="<?=$classTr ?>">

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getId() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getAssinatura_host_db() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getAssinatura_porta_db_INT() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getAssinatura_usuario_db() ?>
    		</td>

    		<td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
    			<?=$obj->getAssinatura_senha_db() ?>
    		</td>

                <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                    
                    <? if(strlen($obj->getSistema_id_INT())){
                
                        $obj->objSistema->select($obj->getSistema_id_INT());
                        $obj->objSistema->formatarParaExibicao();
                        
                    ?>
                        
                        <?=$obj->objSistema->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>

			<td class="td_list_conteudo" style="text-align: center;">
				<img class="icones_list" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_db&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_detalhes.png" onclick="javascript:location.href='index.php?tipo=forms&page=sistema_db&id1=<?=$obj->getId(); ?>'" onmouseover="javascript:tip('<?=$acoes['tooltip_visualizacao'] ?>')" onmouseout="javascript:notip()">&nbsp;
				<img class="icones_list" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Sistema_db&action=remove&id=<?=$obj->getId(); ?>','<?=$acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?=$acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
			</td>


    
		</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Pagina��o

    $paginaAtual = Helper::GET("pagina")?Helper::GET("pagina"):"1";
    $numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

    if($numeroPaginas > 1){

    ?>

    <fieldset class="fieldset_paginacao">
            <legend class="legend_paginacao">Pagina��o</legend>

	<table class="table_paginacao">
		<tr class="tr_paginacao">

	<?

	for($i=1; $i <= $numeroPaginas; $i++){

		$class = ($i==$paginaAtual)?"td_paginacao_pag_atual":"td_paginacao"

	?>

		<td class="<?=$class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=sistema_db&pagina=<?=$i ?><?=$varGET ?>'"><?=$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	
<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       usuario
 * NOME DA CLASSE DAO: DAO_Usuario
 * DATA DE GERA��O:    16.01.2010
 * ARQUIVO:            EXTDAO_Usuario.php
 * TABELA MYSQL:       usuario
 * BANCO DE DADOS:     dep_pesquisas
 * -------------------------------------------------------
 *
 * GERENCIADOR DE DATAGRIDS DO EDUARDO
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
$acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
$acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
$acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

include("filters/usuario.php");

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Usuario();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();



if (!Helper::isNull($obj->getNome())) {

    $strCondicao[] = "nome LIKE '%{$obj->getNome()}%'";
    $strGET[] = "nome={$obj->getNome()}";
}

if (!Helper::isNull($obj->getEmail())) {

    $strCondicao[] = "email={$obj->getEmail()}";
    $strGET[] = "email={$obj->getEmail()}";
}

if (!Helper::isNull($obj->getUsuario_tipo_id_INT())) {

    $strCondicao[] = "usuario_tipo_id_INT={$obj->getUsuario_tipo_id_INT()}";
    $strGET[] = "usuario_tipo_id_INT={$obj->getUsuario_tipo_id_INT()}";
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++) {

    if ($i == 0)
        $consulta .= " AND " . $strCondicao[$i];
    else
        $consulta .= " AND " . $strCondicao[$i];

    $varGET .= "&" . $strGET[$i];
}

$consultaNumero = "SELECT COUNT(id) FROM usuario WHERE excluido_BOOLEAN=0 " . $consulta . " ";

$objBanco = new Database();

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT id FROM usuario WHERE excluido_BOOLEAN=0 " . $consulta . " ORDER BY nome LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);
?>



<fieldset class="fieldset_list">
    <legend class="legend_list">Lista de Usu�rios</legend>

    <table class="tabela_list">
        <colgroup>
            <col width="7%" />
            <col width="23%" />
            <col width="23%" />
            <col width="27%" />
            <col width="10%" />
            <col width="10%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">

                <td class="td_list_titulos"><?= $obj->label_id ?></td>
                <td class="td_list_titulos"><?= $obj->label_nome ?></td>
                <td class="td_list_titulos"><?= $obj->label_email ?></td>
                <td class="td_list_titulos"><?= $obj->label_usuario_tipo_id_INT ?></td>
                <td class="td_list_titulos"><?= $obj->label_status_BOOLEAN ?></td>
                <td class="td_list_titulos">A��es</td>

            </tr>
        </thead>
        <tbody>

            <?
            for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

                $obj->select($regs[0]);
                $obj->formatarParaExibicao();

                $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par"
                ?>

                <tr class="<?= $classTr ?>">

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getId() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getNome() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getEmail() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <? $obj->objUsuario_tipo->select($obj->getUsuario_tipo_id_INT()) ?>
                        <? $obj->objUsuario_tipo->formatarParaExibicao() ?>
                        <?= $obj->objUsuario_tipo->valorCampoLabel() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getStatus_BOOLEAN() ? "Ativo" : "<font color=\"#ff0000\">Inativo</font>" ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: center;">
                        <img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=usuario&id1=<?= $obj->getId(); ?>'" onmouseover="javascript:tip('<?= $acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                        <img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Usuario&action=remove&id=<?= $obj->getId(); ?>','<?= $acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?= $acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                    </td>

                </tr>

            <? } ?>

        </tbody>
    </table>

</fieldset>

<br/>
<br/>

<?
//Pagina��o

$paginaAtual = Helper::GET("pagina") ? Helper::GET("pagina") : "1";
$numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

if ($numeroPaginas > 1) {
    ?>

    <fieldset class="fieldset_paginacao">
        <legend class="legend_paginacao">Pagina��o</legend>

        <table class="table_paginacao">
            <tr class="tr_paginacao">

                <?
                for ($i = 1; $i <= $numeroPaginas; $i++) {

                    $class = ($i == $paginaAtual) ? "td_paginacao_pag_atual" : "td_paginacao"
                    ?>

                    <td class="<?= $class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=usuario&pagina=<?= $i ?><?= $varGET ?>'"><?= $i ?></td>

                <? } ?>

            </tr>
        </table>

    </fieldset>

<? } ?>
	

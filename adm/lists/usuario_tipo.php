<?php
/*
 *
 * -------------------------------------------------------
 * NOME DA LIST:       usuario_tipo
 * NOME DA CLASSE DAO: DAO_Usuario_tipo
 * DATA DE GERA��O:    23.10.2009
 * ARQUIVO:            EXTDAO_Usuario_tipo.php
 * TABELA MYSQL:       usuario_tipo
 * BANCO DE DADOS:     engenharia
 * -------------------------------------------------------
 * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
 * GERENCIADOR DE DATAGRIDS DO EDUARDO
 * -------------------------------------------------------
 *
 */


//Mensagens e Textos dos Tooltips
$acoes["mensagem_exclusao"] = I18N::getExpression("Tem certeza que deseja excluir este registro?");
$acoes["tooltip_exclusao"] = I18N::getExpression("Clique aqui para excluir este registro");
$acoes["tooltip_edicao"] = I18N::getExpression("Clique aqui para editar este registro");
$acoes["tooltip_visualizacao"] = I18N::getExpression("Clique aqui para visualizar este registro");

include("filters/usuario_tipo.php");

$registrosPorPagina = REGISTROS_POR_PAGINA;

$registrosPesquisa = 1;

$obj = new EXTDAO_Usuario_tipo();
$obj->setByGet($registrosPesquisa);
$obj->formatarParaSQL();

$strCondicao = array();
$strGET = array();

if (!Helper::isNull($obj->getNome())) {

    $strCondicao[] = "ut.nome LIKE '%{$obj->getNome()}%'";
    $strGET[] = "nome={$obj->getNome()}";
}

$consulta = "";

for ($i = 0; $i < count($strCondicao); $i++) {

    if ($i == 0)
        $consulta .= " AND " . $strCondicao[$i];
    else
        $consulta .= " AND " . $strCondicao[$i];

    $varGET .= "&" . $strGET[$i];
}

$objBanco = new Database();

$objBanco->query("SELECT COUNT(u.id) FROM usuario AS u, usuario_tipo AS ut WHERE u.excluido_BOOLEAN=0 AND u.status_BOOLEAN=1 AND u.usuario_tipo_id_INT=ut.id");
$numeroTotalDeUsuarios = $objBanco->getPrimeiraTuplaDoResultSet(0);

$consultaNumero = "SELECT COUNT(ut.id) FROM usuario_tipo AS ut
                   LEFT OUTER JOIN usuario AS u ON u.usuario_tipo_id_INT=ut.id AND u.status_BOOLEAN=1 AND u.excluido_BOOLEAN=0
                   WHERE ut.excluido_BOOLEAN=0
                   {$consulta}";

$objBanco->query($consultaNumero);
$numeroRegistros = $objBanco->getPrimeiraTuplaDoResultSet(0);

$limites = Helper::getLimitesRegsPaginacao($registrosPorPagina, $numeroRegistros);

$consultaRegistros = "SELECT ut.id AS identificador, COUNT(u.id) AS contagem_usuario
                          FROM usuario_tipo AS ut
                          LEFT OUTER JOIN usuario AS u ON u.usuario_tipo_id_INT=ut.id AND u.status_BOOLEAN=1 AND u.excluido_BOOLEAN=0
                          WHERE ut.excluido_BOOLEAN=0
                          {$consulta}
                          GROUP BY identificador
                          ORDER BY ut.nome
                          LIMIT {$limites[0]},{$limites[1]}";

$objBanco->query($consultaRegistros);
?>

<fieldset class="fieldset_list">
    <legend class="legend_list">Lista de Classes de Usu�rio</legend>

    <table class="tabela_list">
        <colgroup>
            <col width="10%" />
            <col width="50%" />
            <col width="15%" />
            <col width="15%" />
            <col width="10%" />
        </colgroup>
        <thead>
            <tr class="tr_list_titulos">

                <td class="td_list_titulos"><?= $obj->label_id ?></td>
                <td class="td_list_titulos"><?= $obj->label_nome ?></td>
                <td class="td_list_titulos">N� de Usu�rios</td>
                <td class="td_list_titulos">Representa��o</td>
                <td class="td_list_titulos">A��es</td>

            </tr>
        </thead>
        <tbody>

<?
for ($i = 1; $regs = $objBanco->fetchArray(); $i++) {

    $obj->select($regs[0]);
    $obj->formatarParaExibicao();

    $classTr = ($i % 2) ? "tr_list_conteudo_impar" : "tr_list_conteudo_par";
    ?>

                <tr class="<?= $classTr ?>">

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                <?= $obj->getId() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $obj->getNome() ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= $regs[1] ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
                        <?= Helper::getPorcentagem($regs[1], $numeroTotalDeUsuarios, 0); ?>
                    </td>

                    <td class="td_list_conteudo" style="text-align: center;">
                        <img border="0" src="imgs/icone_editar.png" onclick="javascript:location.href='index.php?tipo=forms&page=usuario_tipo&id1=<?= $obj->getId(); ?>'" onmouseover="javascript:tip('<?= $acoes['tooltip_edicao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                        <img border="0" src="imgs/icone_excluir.png" onclick="javascript:confirmarExclusao('actions.php?class=EXTDAO_Usuario_tipo&action=remove&id=<?= $obj->getId(); ?>','<?= $acoes['mensagem_exclusao'] ?>')" onmouseover="javascript:tip('<?= $acoes['tooltip_exclusao'] ?>')" onmouseout="javascript:notip()">&nbsp;
                    </td>

                </tr>

<? } ?>

        </tbody>
    </table>

</fieldset>

<br />
<br />

<?
//Pagina��o

$paginaAtual = Helper::GET("pagina") ? Helper::GET("pagina") : "1";
$numeroPaginas = Helper::getNumeroPaginas($registrosPorPagina, $numeroRegistros);

if ($numeroPaginas > 1) {
    ?>

    <fieldset class="fieldset_paginacao">
        <legend class="legend_paginacao">Pagina��o</legend>

        <table class="table_paginacao">
            <tr class="tr_paginacao">

    <?
    for ($i = 1; $i <= $numeroPaginas; $i++) {

        $class = ($i == $paginaAtual) ? "td_paginacao_pag_atual" : "td_paginacao"
        ?>

                    <td class="<?= $class ?>" onclick="javascript:location.href='index.php?tipo=lists&page=usuario_tipo&pagina=<?= $i ?><?= $varGET ?>'"><?= $i ?></td>

                <? } ?>

            </tr>
        </table>

    </fieldset>

<? } ?>


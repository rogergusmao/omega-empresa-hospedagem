<?php

$relatorio = Helper::GET("relatorio");

if(!Helper::isNull(Helper::GET("largura")) && !Helper::isNull(Helper::GET("altura"))){

	$largura = Helper::GET("largura");
	$altura = Helper::GET("altura");

}
else{

	$largura = LARGURA_PADRAO_RELATORIO;
	$altura = ALTURA_PADRAO_RELATORIO;

}

if(!Helper::isNull(Helper::GET("filtro"))){

	if(file_exists("filters_reports/" . Helper::GET("filtro") . ".php")){

		include "filters_reports/" . Helper::GET("filtro") . ".php";

	}
	elseif(file_exists("filters/" . Helper::GET("filtro") . ".php")){

		include "filters/" . Helper::GET("filtro") . ".php";

	}

}

if((!Helper::isNull(Helper::GET("filtro")) && !Helper::isNull(Helper::GET("postado"))) ||
	Helper::isNull(Helper::GET("filtro"))){

	if(!Helper::isNull(Helper::GET("postado"))){

		$variaveis = Helper::getStringDasVariaveisDaUrlComExcessoes(array("page", "tipo"));

	}

?>

<script language="javascript">

popupModoDeImpressaoDeRelatorios("popup.php?<?=$variaveis ?>&tipo=reports&page=<?=$relatorio ?>", <?=$largura ?>, <?=$altura ?>);

</script>

<?

Helper::imprimirMensagem(MENSAGEM_AVISO_RELATORIO_ABERTO_EM_POPUP, MENSAGEM_INFO);

?>

<? } ?>

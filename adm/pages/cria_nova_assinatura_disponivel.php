<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO FILTRO:     sistema
    * DATA DE GERA��O:    08.11.2013
    * ARQUIVO:            sistema.php
    * TABELA MYSQL:       sistema
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    $obj = new EXTDAO_Sistema();
    $objArg = new Generic_Argument();

    $class = $obj->nomeClasse;
    $action = (Helper::GET("id")?"edit": "add");
    $postar = "index.php";

    
    $q = "SELECT id"
        . " FROM sistema"
        . " WHERE excluido_BOOLEAN = 0";
    
    $db = new Database();
    $db->query($q);
    $ids = Helper::getResultSetToArrayDeUmCampo($db->result);
    
    ?>
     
        <table>
          
        <table class="tabela_list">
   		<colgroup>
			<col width="20%" />
			<col width="20%" />
                        <col width="20%" />
		</colgroup>
        <thead>
		<tr class="tr_list_titulos">

			<td class="td_list_titulos"><?=$obj->label_nome ?></td>
			<td class="td_list_titulos">Total de assinaturas dispon�veis</td>
            <td class="td_list_titulos">Total de assinaturas reservadas</td>
                        <td class="td_list_titulos">Total de assinaturas ocupadas</td>
            <td class="td_list_titulos">Disponivel para migra��o</td>
            <td class="td_list_titulos">Em processo de migra��o</td>

			<td class="td_list_titulos">Reservar</td>

		</tr>
		</thead>
    	<tbody>
        <?php
        $objSistema = new EXTDAO_Sistema($db);
        for($i = 0 ; $i < count($ids); $i++){
            $idSistema= $ids[$i];
            
            $objSistema->select($idSistema);
            
            $objSistema->getNome();
            $classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";
            $livre= EXTDAO_Assinatura::getNumeroDeAssinaturaLivreDoSistema(
                $idSistema, $db);
            $reservada = EXTDAO_Assinatura::getNumeroDeAssinaturaNoEstado(
                $idSistema, EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA, $db);
            $ocupada = EXTDAO_Assinatura::getNumeroDeAssinaturaNoEstado(
                $idSistema, EXTDAO_Estado_assinatura::OCUPADA, $db);
            $disponivelParaMigracao = EXTDAO_Assinatura::getNumeroDeAssinaturaNoEstado(
                $idSistema, EXTDAO_Estado_assinatura::DISPONIVEL_PARA_MIGRACAO, $db);

            $emMigracao = EXTDAO_Assinatura::getNumeroDeAssinaturaNoEstado(
                $idSistema, EXTDAO_Estado_assinatura::EM_MIGRACAO, $db);
            $objLink = new Link();
            $objLink->alturaGreyBox = null;
            $objLink->larguraGreyBox = null;
            
            $objLink->url = "popup.php?page=processo_cria_nova_assinatura_disponivel&tipo=pages&id_sistema=".$ids[$i];
            $objLink->demaisAtributos = array(
                "onmouseover" => "tip('Clique aqui para criar uma nova assinatura disponivel.', this);", 
                "onmouseout" => "notip();",
                "style" => "color: 000000;");
            $objLink->cssClass = "link_topo";
            $objLink->label = "CRIAR NOVA ASSINATURA";
            $objLink->numeroNiveisPai = 0;

            ?>
            <tr class="<?=$classTr?>">  
                <td><?=$objSistema->getNome();?></td>
                <td><?=$livre;?></td>
                <td><?=$reservada;?></td>

                <td><?=$ocupada;?></td>
                <td><?=$disponivelParaMigracao;?></td>
                <td><?=$emMigracao;?></td>


                <td>
                    <?echo $objLink->montarLink(); ?>
                </td>
            </tr>    
                
            <?php
              
            
        }
    ?>
        </tbody>
    </table>
            
            
    

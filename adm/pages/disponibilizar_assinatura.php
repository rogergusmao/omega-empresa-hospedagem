<?php

$fluxoFinalizado = Helper::POSTGET("fluxo_finalizado");
$idAssinatura = Helper::POSTGET("id_assinatura");
if(!strlen($idAssinatura)){
    
    Helper::imprimirMensagem("Parâmentros inválidos", MENSAGEM_ERRO);
    echo Helper::getComandoJavascriptComTimer("", 5);
}
else if(!strlen($fluxoFinalizado)){
    Helper::imprimirMensagem("Disponibilizando a assinatura", MENSAGEM_INFO);
    Helper::mudarLocation("popup.php?tipo=pages&page=disponibilizar_assinatura&fluxo_finalizado=true&id_assinatura=$idAssinatura");
} else{
    EXTDAO_Assinatura::disponibilizarAssinatura($idAssinatura);
    Helper::imprimirMensagem("Assinatura disponibilizada com sucesso", MENSAGEM_OK);
    
    echo Helper::getComandoJavascriptComTimer("", 5);
}


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


<?php

include '../../recursos/languages/pt-br.php';
include '../../recursos/php/funcoes.php';
include '../../recursos/php/constants.php';
include '../../recursos/php/database_config.php';


$nomeScript = Helper::getNomeDoScriptAtual();

if($nomeScript == "index.php"){

	echo "<center>";
	Helper::imprimirMensagem("A janela de download abrirá em alguns segundos.\nCaso não abra automaticamente, <a class='link_padrao' href='forms/exportar_celulares.php'>clique aqui</a>.");
	echo "</center>";

	Helper::mudarLocation("pages/exportar_celulares.php");
	exit();

}

$objBanco = new Database();
$objBanco->Query("SELECT DISTINCT celular FROM cliente WHERE celular IS NOT NULL");

$stringRetorno = "";
while($dados = $objBanco->fetchArray()){
    
    $celular = $dados[0];
    $celularFormatado = "";
    
    for($i=0; $i < strlen($celular); $i++){

        $posicao = substr($celular, $i, 1);

        if(is_numeric($posicao)){

            $celularFormatado .= $posicao;

        }            

    }
    
    if(strlen($celularFormatado) == 10){
        
        $stringRetorno .= "{$celularFormatado}\r\n";
        
    }
    
    
}

$objDownload = new Download("celulares.txt");
print $objDownload->ds_download($stringRetorno);

?>




<?php

$cobrancasUnicas = EXTDAO_Cobranca::criarCobrancasEMensagensDeCobrancasUnicas();
$cobrancasMensais = EXTDAO_Cobranca::criarCobrancasEMensagensDeCobrancasMensaisDoMesSeguinte();
$strMesEAnoDoMesSeguinte = date("m/Y", strtotime("+1 months"));

Helper::imprimirMensagem("Geração de Cobranças em " . Helper::getDiaEHoraAtual(), MENSAGEM_INFO);
Helper::imprimirMensagem("Cobranças Únicas Geradas: {$cobrancasUnicas}", MENSAGEM_OK);
Helper::imprimirMensagem("Cobranças Mensais Geradas para o Mês {$strMesEAnoDoMesSeguinte}: {$cobrancasMensais}", MENSAGEM_OK);

?>

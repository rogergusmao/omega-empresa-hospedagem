<style type="text/css">

    .header{

        border-bottom: 3px solid #009AD9;
        line-height: normal;
        vertical-align: middle;
        padding-top: 40px;
        padding-bottom: 40px;
        font-size: 30px;
        font-weight: bold;
        letter-spacing: -2px;
        line-height: 38px;
        font-family: arial,Helvetica,sans-serif;


    }

    .footer{

        height: 150px;
        border-top: 3px solid #009AD9;
        line-height: normal;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        font-family: arial,Helvetica,sans-serif;
        font-size: 14px;
        font-weight: bold;


    }

    .footer table td{

        text-align: center;
        width: 500px;

    }

    .footer img{

        height: 130px;
        width: 130px;

    }

    .container {
        height: 650px;
        width: 900px;
        position: static;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }
    ul.thumb {
        float: left;
        list-style: none;
        padding: 10px;                    
        width: 900px;

    }

    ul.thumb li > a{

        display: block;
        margin-bottom: 20px;
        min-height: 200px;

    }

    ul.thumb li {
        margin: 15px;
        margin-left: 0px;
        padding: 0px;
        padding-bottom: 5px;
        float: left;
        position: relative;
        width: 210px;
        height: 210px;
        margin-bottom: 50px;
    }
    ul.thumb li img {
        width: 200px; height: 200px;
        border: 1px solid #ddd;
        padding: 5px;
        background: #f0f0f0;
        position: absolute;
        left: 0; top: 0;
        -ms-interpolation-mode: bicubic; 
    }
    ul.thumb li img.hover {
        background:url(thumb_bg.png) no-repeat center center;
        border: none;
    }
    #main_view {
        float: left;
        padding: 9px 0;
        margin-left: -10px;
    }

    ul.thumb li h2 {
        font-size: 1em;
        font: normal 10px Verdana, Arial, Helvetica, sans-serif;
        font-weight: normal;
        text-transform: uppercase;
        padding: 10px;
        margin-top: 20px;
        background: #f0f0f0;
        width: 190px;
        min-width: 190px;
        border: 1px solid #ddd;
        text-align: center;
        height: 100px;
        min-height: 100px;
        vertical-align: middle;
        display: table-cell;

    }

    ul.thumb li h2 a 
    {
        height: 100%; 
        text-decoration: none; 
        color: #777; 
        vertical-align: middle;
        font-size: 13px;
        font-weight: bold;
    }

    ul.thumb li span { /*--Used to crop image--*/
        width: 202px;
        height: 202px;
        overflow: hidden;
        display: block;
    }

</style>

<script type="text/javascript"> 

    $(document).ready(function(){

        //Larger thumbnail preview 

        $("ul.thumb li").hover(function() {
            $(this).css({'z-index' : '10'});
            $(this).find('img').addClass("hover").stop()
            .animate({
                marginTop: '-150px', 
                marginLeft: '-150px', 
                top: '50%', 
                left: '50%', 
                width: '256px', 
                height: '256px',
                padding: '20px' 
            }, 200);

        } , function() {
            $(this).css({'z-index' : '0'});
            $(this).find('img').removeClass("hover").stop()
            .animate({
                marginTop: '0', 
                marginLeft: '0',
                top: '0', 
                left: '0', 
                width: '200px', 
                height: '200px', 
                padding: '5px'
            }, 400);
        });

        //Swap Image on Click
        $("ul.thumb li a").click(function() {

            //var mainImage = $(this).attr("href"); //Find Image Name
            //$("#main_view img").attr({ src: mainImage });
            //return false;		

        });

    });



</script> 

<?php



?>

<div class="container">

    <ul class="thumb">
        <li style="z-index: 0;">
            <a href="index.php?tipo=forms&page=mensagem_email"><span><img style="margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px; overflow: hidden;" class="" src="imgs/especifico/email.png" alt=""></span></a>
            <h2><a href="index.php?tipo=forms&page=mensagem_email">Cadastrar Nova Mensagem de Email</a></h2>
        </li>
        <li style="z-index: 0;">
            <a href="index.php?tipo=lists&page=mensagem_email"><span><img style="margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px;" class="" src="imgs/especifico/lista_de_emails.png" alt=""></span></a>
            <h2><a href="index.php?tipo=lists&page=mensagem_email">Visualizar Mensagens Existentes</a></h2>
        </li>
        <li style="z-index: 0;">
            <a href="index.php?tipo=forms&page=cliente"><span><img style="margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px; overflow: hidden;" class="" src="imgs/especifico/cliente.png" alt=""></span></a>
            <h2><a href="#">Cadastrar Cliente</a></h2>
        </li>
        <li style="z-index: 0;">
            <a href="index.php?tipo=lists&page=cliente"><span><img style="overflow: hidden; margin-top: 0px; margin-left: 0px; top: 0px; left: 0px; width: 200px; height: 200px; padding: 5px;" class="" src="imgs/especifico/lista_de_clientes.png" alt=""></span></a>
            <h2><a href="index.php?tipo=lists&page=cliente">Visualizar Lista de Clientes</a></h2>
        </li>

    </ul>
</div>

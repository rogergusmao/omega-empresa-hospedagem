<?php
    
    $idSistema = Helper::POSTGET("id_sistema");
    $url = "popup.php?tipo=pages&page=processo_cria_nova_assinatura_disponivel&id_sistema=$idSistema";
    $fluxoFinalizado = Helper::POSTGET("fluxo_finalizado");
    

    $objSistema = new EXTDAO_Sistema();
    $objSistema->select($idSistema);
    $nomeSistema =  $objSistema->getNome();
    if(strlen($fluxoFinalizado) && $fluxoFinalizado == "true"){
        $msgSucesso =Helper::POSTGET("msgSucesso");
        if(strlen($msgSucesso)){
            Helper::imprimirMensagem(urldecode($msgSucesso), MENSAGEM_OK);
        } else{
            $msgErro = Helper::POSTGET("msgErro");    
            Helper::imprimirMensagem(urldecode($msgErro), MENSAGEM_ERRO);
        }
    } else if(strlen($fluxoFinalizado) && $fluxoFinalizado == "false"){
    
        $idNovaAssinatura = EXTDAO_Assinatura::criaNovaAssinaturas($idSistema);
        if(!empty($idNovaAssinatura)){
            $msgSuc = urlencode("Assinatura do sistema '$nomeSistema' criada com sucesso");
            Helper::mudarLocation($url."&fluxo_finalizado=true&msgSucesso=$msgSuc");
        }else{
            $msgErro = urlencode("Falha ao criar uma nova assinatura do sistema '$nomeSistema'");
            Helper::mudarLocation($url."&fluxo_finalizado=true&msgErro=$msgErro");
        }
    } else{
        Helper::imprimirMensagem("Criando uma nova assinatura do sistema: $nomeSistema", MENSAGEM_INFO);
        Helper::mudarLocation($url."&fluxo_finalizado=false");
        
    }

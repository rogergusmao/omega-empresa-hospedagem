    <?php

    if(Helper::getNomeDoScriptAtual() != "pdf.php"){
    
        include_once 'filters_reports/fluxo_caixa.php';
    
    }
    
    $objBanco = new Database();
    
    $dataDe = Helper::GET("data_de1");
    $dataAte = Helper::GET("data_ate1");
    
    $tipoLancamento = Helper::GET("tipo_lancamento1");
    
    $complementoReceitas = "";
    $complementoDespesas = "";
    
    if(strlen($dataDe)){
        
        $dataDeSQL = Helper::formatarDataParaComandoSQL($dataDe);
        
        $complementoReceitas .= "AND v.data_pagamento_DATETIME >= {$dataDeSQL} ";
        $complementoDespesas .= "AND cp.data_pagamento_DATE >= {$dataDeSQL} ";
        
    }
    
    if(strlen($dataAte)){
        
        $dataAteSQL = Helper::formatarDataParaComandoSQL($dataAte);
        
        $complementoReceitas .= "AND v.data_pagamento_DATETIME <= {$dataAteSQL} ";
        $complementoDespesas .= "AND cp.data_pagamento_DATE <= {$dataAteSQL} ";
        
    }
    
    $queryReceitas = "SELECT 'r' AS tipo, 
                      DATE_FORMAT(v.data_pagamento_DATETIME, '%Y-%m-%d') AS data, 
                      CONCAT('Venda ', v.id) AS titulo, 
                      CONCAT('Cliente: ', c.nome_completo) AS descricao, 
                      v.valor_liquido_FLOAT AS valor,
                      IF(v.venda_status_id_INT = 5, 'a', 'c') AS status
                      FROM venda AS v,
                      cliente AS c
                      WHERE c.id=v.cliente_id_INT
                      AND v.venda_status_id_INT > 1
                      AND v.excluido_BOOLEAN=0
                      {$complementoReceitas}
                      ORDER BY data ASC";
    
    $queryDespesas = "SELECT 'd' AS tipo, 
                      cp.data_pagamento_DATE AS data, 
                      cp.titulo_conta AS titulo, 
                      CONCAT('Cedente: ', c.nome) AS descricao, 
                      cp.valor_pago_FLOAT AS valor,
                      'a' AS status
                      FROM conta_a_pagar AS cp,
                      cedente AS c
                      WHERE c.id=cp.cedente_id_INT
                      AND cp.foi_paga_BOOLEAN=1
                      AND cp.excluido_BOOLEAN=0
                      {$complementoDespesas}
                      ORDER BY data ASC";
    
    if($tipoLancamento == "receitas"){
        
        $query = $queryReceitas;
        
        
    }
    elseif($tipoLancamento == "despesas"){
        
        $query = $queryDespesas;
        
    }
    else{
        
        $query = "({$queryReceitas})
                  UNION
                  ({$queryDespesas})
                  ORDER BY data ASC";
        
        
    }
    
    if($dataDe && $dataAte){
        
        $strPeriodo = "De {$dataDe} a {$dataAte}";
        
    }
    elseif($dataDe){
        
        $strPeriodo = "A partir de {$dataDe}";
        
    }
    elseif($dataAte){
        
        $strPeriodo = "Até {$dataAte}";
        
    }
    else{
        
        $strPeriodo = "Período: Todas as datas";
        
    }
    
    if($tipoLancamento == "receitas"){
        
        $strDescricao =  "Receitas";
        
    }
    elseif($tipoLancamento == "despesas"){
        
        $strDescricao =  "Despesas";
        
    }
    else{
        
        $strDescricao = "Receitas e Despesas";
        
    }

    if(Helper::GET("postado") == "true"){
    
?>

<fieldset class="fieldset_form">
    <legend class="legend_form">Relatório de Fluxo de Caixa (<?=$strDescricao ?>)</legend>

    <table class="tabela_form" style="border-width: 0px;">

    	<colgroup>
            <col width="5%" />
            <col width="5%" />
            <col width="10%" />
            <col width="30%" />
            <col width="30%" />
            <col width="10%" />
            <col width="10%" />
    	</colgroup>
    	<tr class="tr_list_conteudo_impar">
            <td class="td_list_conteudo" style="text-align: left;" colspan="7"><?=$strPeriodo ?></td>
    	</tr>
    	<tr class="tr_list_titulos">
            <td class="td_list_titulos" style="text-align: center;">Nº</td>
            <td class="td_list_titulos" style="text-align: center;">Tipo</td>
            <td class="td_list_titulos" style="text-align: center;">Data</td>
            <td class="td_list_titulos" style="text-align: left; padding-left: 5px;">Título</td>
            <td class="td_list_titulos" style="text-align: left; padding-left: 5px;">Descrição</td>
            <td class="td_list_titulos" style="text-align: center;">Status</td>
            <td class="td_list_titulos" style="text-align: center;">Valor</td>
    	</tr>

     <?
     
     $objBanco->query($query);
     
     $valorReceitas = "0";
     $valorDespesas = "0";
     
     if($objBanco->rows() == 0){
     
     ?>
       
        <tr class="tr_list_conteudo_impar">
            <td class="td_list_conteudo" style="text-align: left;" colspan="7">
                
                <?=Helper::imprimirMensagem("Nenhum lançamento encontrado.") ?>
            
            </td>
    	</tr>
        
        
     <?    
         
     }
     
     for($i=1; $dados = $objBanco->fetchArray(); $i++){
         
         $numero = $i;
         $tipo = $dados["tipo"];
         $data = $dados["data"];
         $titulo = $dados["titulo"];
         $descricao = $dados["descricao"];
         $status = $dados["status"];
         $valor = $dados["valor"];
         
         $classTr = ($i%2)?"tr_list_conteudo_impar":"tr_list_conteudo_par";

         $data = Helper::formatarDataParaExibicao($data);
         
         if($tipo == "r" && $status == "a"){
             
             $status = "RECEBIDA";
             $valorReceitas += $valor;
             
         }
         elseif($tipo == "r" && $status == "c"){
             
             $status = "<font color=\"#FF0000\">CANCELADA</font>";
             
         }
         elseif($tipo == "d" && $status == "a"){
             
             $status = "PAGA";
             $valorDespesas += $valor;
             
         }
         
         if($tipo == "r"){
             
             $tipo = "REC";
             
         }
         elseif($tipo == "d"){
             
             $tipo = "DESP";
             
         }
         
         $valor = "R$ " . Helper::formatarFloatParaExibicao($valor);
         
         ?>

        <tr class="<?=$classTr ?>">

            <td class="td_list_conteudo" style="text-align: center;">
               <?=$numero ?>
            </td>
            <td class="td_list_conteudo" style="text-align: center;">
               <?=$tipo ?>
            </td>
            <td class="td_list_conteudo" style="text-align: center;">
               <?=$data ?>
            </td>            
            <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
               <?=$titulo ?>
            </td>
            <td class="td_list_conteudo" style="text-align: left; padding-left: 5px;">
               <?=$descricao ?>
            </td>
            <td class="td_list_conteudo" style="text-align: center;">
               <?=$status ?>
            </td> 
            <td class="td_list_conteudo" style="text-align: center;">
               <?=$valor ?>
            </td> 

        </tr>
                
  <? } ?>
        
  <?
  
  $valorDespesas = "R$ " . Helper::formatarFloatParaExibicao($valorDespesas);
  $valorReceitas = "R$ " . Helper::formatarFloatParaExibicao($valorReceitas);
  
  ?>
        
        <tr class="<?=$classTr ?>">

            <td colspan="6" class="td_relatorio_cinza" style="text-align: left; padding-left: 5px;">
            
                Valor das Despesas:
            
            </td>           
            <td class="td_relatorio_cinza" style="text-align: center;">
               <?=$valorDespesas ?>
            </td> 

        </tr>
        <tr class="<?=$classTr ?>">

            <td colspan="6" class="td_relatorio_cinza" style="text-align: left; padding-left: 5px;">
            
                Valor das Receitas:
            
            </td>           
            <td class="td_relatorio_cinza" style="text-align: center;">
               <?=$valorReceitas ?>
            </td> 

        </tr>
    	
     </table>
    
</fieldset>
<br>

<? if(Helper::getNomeDoScriptAtual() != "pdf.php"){ ?>

<fieldset class="fieldset_form">
    <legend class="legend_form">Impressão</legend>
    
    <table class="tabela_form">
        <tr>
            <td>
                <?
                
                $strGET = Helper::getStringDasVariaveisDaUrlComExcessoes(array("tipo", "page"));
                
                $scriptComParametros = urlencode("index.php?tipo=reports&page=fluxo_caixa&{$strGET}");
                
                ?>
                <input type="button" class="botoes_form" value="Gerar versão para impressão" onclick="javascript:popupModoDeImpressaoDeScriptEmPDF('<?=$scriptComParametros ?>', 'fluxo_de_caixa')" />
            </td>            
        </tr>
    </table>
    
</fieldset>
    
<? } ?>

<?

}
else{

    Helper::imprimirMensagem("Escolha os parâmetros de busca e clique em Pesquisar.", MENSAGEM_INFO);

}    
    
?>

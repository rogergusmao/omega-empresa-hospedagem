<?php

/**
 * Created by PhpStorm.
 * User: W10
 * Date: 14/08/2017
 * Time: 21:16
 */
class ProcessoRotinas
{

    public $categoria;

    public function __construct($categoria = "1"){
        $this->categoria = $categoria;
    }

    public function run()
    {

        $first = true;
        while (true)
        {
            if (!$first)
            {
                sleep(60);
            }
            else
            {
                $first = false;
            }
            $boAM = new BO_Assinatura_migracao();
            $boA = new BO_Assinatura();

            HelperLog::logRotinas("migrarProximaAssinaturaDaFila ...");
            $msg = $boAM->migrarProximaAssinaturaDaFila();
            HelperLog::logRotinas("migrarProximaAssinaturaDaFila: " . print_r($msg, true) . "\n\n");
            Database::closeAll();

            sleep(5);
            HelperLog::logRotinas("continuaMigracaoDasQueJaEstaoNoEstadoEmManutencao... ");
            $msg = $boAM->continuaMigracaoDasQueJaEstaoNoEstadoEmManutencao();
            HelperLog::logRotinas("continuaMigracaoDasQueJaEstaoNoEstadoEmManutencao: " . print_r($msg, true) . "\n\n");
            Database::closeAll();

            sleep(5);
            HelperLog::logRotinas("disponibilizarProximaAssinaturaMigrada... ");
            $msg = $boAM->disponibilizarProximaAssinaturaMigrada();
            HelperLog::logRotinas("disponibilizarProximaAssinaturaMigrada: " . print_r($msg, true) . "\n\n");
            Database::closeAll();

            sleep(5);
            HelperLog::logRotinas("criarNovasAssinaturas... ");
            $msg = $boA->criarNovasAssinaturas();
            HelperLog::logRotinas("criarNovasAssinaturas: " . print_r($msg, true) . "\n\n");
            Database::closeAll();

            sleep(5);
            HelperLog::logRotinas("criarNovasAssinaturasParaMigracao... ");
            $msg = $boA->criarNovasAssinaturasParaMigracao();
            HelperLog::logRotinas("criarNovasAssinaturasParaMigracao: " . print_r($msg, true) . "\n\n");
            Database::closeAll();
        }
    }


    public static function factory($categoria = "1"){
        return new ProcessoRotinas($categoria);
    }
}
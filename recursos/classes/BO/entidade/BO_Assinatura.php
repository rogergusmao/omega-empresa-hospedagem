<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of BO_Assinatura
     *
     * @author home
     */
    class BO_Assinatura
    {
        public static function cadastraAssinaturaDoClienteReservado(
            $novaCorporacao, $idAssinatura, $idCorporacao)
        {
            try
            {
                $db = new Database();
                $objAssinatura = new EXTDAO_Assinatura($db);
                $msg = $objAssinatura->select($idAssinatura);
                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }
                $idSistema = $objAssinatura->getSistema_id_INT();
                $corporacaoAntiga = $objAssinatura->getNome_site();
                $msg = $db->queryMensagem(
                    "UPDATE assinatura
                SET nome_site = '$novaCorporacao', 
                  estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::OCUPADA . ",
                   id_corporacao_INT = $idCorporacao
                WHERE id = $idAssinatura");

                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }

                switch ($idSistema)
                {
                    case EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO:
                    case EXTDAO_Sistema::OMEGA_EMPRESA:

                        $objHospedagem = $objAssinatura->getFkObjHospedagem();
                        $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());
                        $pathHospedagemSincronizador = Helper::formatarPath($objHospedagem->getPath_sincronizador());

                        $pathDatabaseConfig = $pathHospedagem . "/recursos/database_config_corporacao/";
                        $pathDatabaseSincronizadorConfig = $pathHospedagemSincronizador . "/recursos/database_config_corporacao/";
                        if (file_exists($pathDatabaseConfig . "/database_config_{$corporacaoAntiga}.json"))
                        {
                            unlink($pathDatabaseConfig . "/database_config_{$corporacaoAntiga}.json");
                        }
                        if (file_exists($pathDatabaseSincronizadorConfig . "/database_config_{$corporacaoAntiga}.json"))
                        {
                            unlink($pathDatabaseSincronizadorConfig . "/database_config_{$corporacaoAntiga}.json");
                        }

                        BO_Assinatura::geraArquivosConfiguracaoSistemaCompartilhado(
                            $idAssinatura,
                            $db);

                        break;
                    default:
                        throw new NotImplementedException("Sistema $idSistema");
                }

                return $msg == null ? new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) : $msg;
            }
            catch (Exception $ex)
            {
                return new Mensagem(null, null, $ex);
            }
        }

        public static function disponibilizaAssinaturaReservada($idAssinatura, $corporacaoReserva)
        {
            try
            {
                $db = new Database();
                $objAssinatura = new EXTDAO_Assinatura($db);
                $objAssinatura->select($idAssinatura);
                $idSistema = $objAssinatura->getSistema_id_INT();
                $corporacaoNova = $objAssinatura->getNome_site();
                $msg = $db->queryMensagem(
                    "UPDATE assinatura
                SET nome_site = '$corporacaoReserva' 
                WHERE id = $idAssinatura");

                if ($msg != null && !$msg->ok())
                {
                    return $msg;
                }

                switch ($idSistema)
                {
                    case EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO:
                    case EXTDAO_Sistema::OMEGA_EMPRESA:

                        $objHospedagem = $objAssinatura->getFkObjHospedagem();
                        $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());
                        $pathHospedagemSincronizador = Helper::formatarPath($objHospedagem->getPath_sincronizador());

                        $pathDatabaseConfig = $pathHospedagem . "/recursos/database_config_corporacao/";
                        $pathDatabaseSincronizadorConfig = $pathHospedagemSincronizador . "/recursos/database_config_corporacao/";
                        if (file_exists($pathDatabaseConfig . "/database_config_{$corporacaoNova}.json"))
                        {
                            unlink($pathDatabaseConfig . "/database_config_{$corporacaoNova}.json");
                        }
                        if (file_exists($pathDatabaseSincronizadorConfig . "/database_config_{$corporacaoNova}.json"))
                        {
                            unlink($pathDatabaseSincronizadorConfig . "/database_config_{$corporacaoNova}.json");
                        }
                        BO_Assinatura::geraArquivosConfiguracaoSistemaCompartilhado(
                            $idAssinatura,
                            $db);
                        break;
                    default:
                        throw new NotImplementedException("Sistema $idSistema");
                }

                return $msg == null ? new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO) : $msg;
            }
            catch (Exception $ex)
            {
                return new Mensagem(null, null, $ex);
            }
        }

        public static function factory()
        {
            return new BO_Assinatura();
        }

        public function rotinas()
        {
            $boAM = new BO_Assinatura_migracao();
            $boA = new BO_Assinatura();
            $first = true;
            while (true)
            {
                if (!$first)
                {
                    sleep(60);
                }
                else
                {
                    $first = false;
                }

                HelperLog::logRotinas("migrarProximaAssinaturaDaFila ...");
                $msg = $boAM->migrarProximaAssinaturaDaFila();
                HelperLog::logRotinas("migrarProximaAssinaturaDaFila: " . print_r($msg, true) . "\n\n");
                Database::closeAll();

                sleep(5);
                HelperLog::logRotinas("continuaMigracaoDasQueJaEstaoNoEstadoEmManutencao... ");
                $msg = $boAM->continuaMigracaoDasQueJaEstaoNoEstadoEmManutencao();
                HelperLog::logRotinas("continuaMigracaoDasQueJaEstaoNoEstadoEmManutencao: " . print_r($msg, true) . "\n\n");
                Database::closeAll();

                sleep(5);
                HelperLog::logRotinas("disponibilizarProximaAssinaturaMigrada... ");
                $msg = $boAM->disponibilizarProximaAssinaturaMigrada();
                HelperLog::logRotinas("disponibilizarProximaAssinaturaMigrada: " . print_r($msg, true) . "\n\n");
                Database::closeAll();

                sleep(5);
                HelperLog::logRotinas("criarNovasAssinaturas... ");
                $msg = $boA->criarNovasAssinaturas();
                HelperLog::logRotinas("criarNovasAssinaturas: " . print_r($msg, true) . "\n\n");
                Database::closeAll();

                sleep(5);
                HelperLog::logRotinas("criarNovasAssinaturasParaMigracao... ");
                $msg = $boA->criarNovasAssinaturasParaMigracao();
                HelperLog::logRotinas("criarNovasAssinaturasParaMigracao: " . print_r($msg, true) . "\n\n");
                Database::closeAll();
            }
        }

        public function criarNovasAssinaturas()
        {
            $db = new Database();
            $livreE = EXTDAO_Assinatura::getNumeroDeAssinaturaReservadasDoSistema(
                EXTDAO_Sistema::OMEGA_EMPRESA, $db);
            $livreE2 = EXTDAO_Assinatura::getNumeroDeAssinaturaLivreDoSistema(
                EXTDAO_Sistema::OMEGA_EMPRESA, $db);
            if ($livreE + $livreE2 < MAXIMO_ASSINATURA_OMEGA_EMPRESA_DISPONIVEL)
            {
                EXTDAO_Assinatura::criaNovaAssinaturas(EXTDAO_Sistema::OMEGA_EMPRESA);
            }

            $livre = EXTDAO_Assinatura::getNumeroDeAssinaturaReservadasDoSistema(
                EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO, $db);
            $livre2 = EXTDAO_Assinatura::getNumeroDeAssinaturaLivreDoSistema(
                EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO, $db);
            if ($livre + $livre2 < MAXIMO_ASSINATURA_OMEGA_EMPRESA_CORPORACAO_DISPONIVEL)
            {
                EXTDAO_Assinatura::criaNovaAssinaturas(EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO);
            }

            $this->procedimentoReservaAssinaturaEGeraBancoSqlite($db, EXTDAO_Sistema::OMEGA_EMPRESA);

            $this->procedimentoReservaAssinaturaEGeraBancoSqlite($db, EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO);

            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                "Temos $livreE assinaturas livres do sistema OMEGA_EMPRESA. "
                                . "Temos $livre2 assinaturas do sistema OMEGA_EMPRESA_CORPORACAO. ");
        }

        public function procedimentoReservaAssinaturaEGeraBancoSqlite(Database $db, $idSistema)
        {
            $totalLivreOmegaEmpresa = EXTDAO_Assinatura::getNumeroDeAssinaturaLivreDoSistema(
                $idSistema, $db);

            for ($i = 0; $i < $totalLivreOmegaEmpresa; $i++)
            {
                $msg = BO_SICOB::reservaCorporacaoEClienteAssinatura($idSistema);
                if (Interface_mensagem::checkOk($msg))
                {
                    $corporacao = $msg->mObj->corporacao;
                    $idCorporacao = $msg->mObj->idCorporacao;
                    $idHospedagem = EXTDAO_Assinatura::getIdHospedagem($msg->mObj->idAssinatura, $db);

                    $boSincw = new BO_SINCW($db, $idHospedagem);
                    $msg = $boSincw->procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao(
                        $idCorporacao, $corporacao);
                    if ($msg == null)
                    {
                        HelperLog::logErro(null, null, "Retorno nulo de procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao($idCorporacao, $corporacao)");
                    }
                    else
                    {
                        if (!$msg->ok())
                        {
                            HelperLog::logErro(null, $msg);
                        }
                    }
                } else {
                    return $msg;
                }
            }
        }

        public function criarNovasAssinaturasParaMigracao()
        {
            $db = new Database();

            $livreE = EXTDAO_Assinatura::getNumeroDeAssinaturaDisponivelParaMigracaoDoSistema(
                EXTDAO_Sistema::OMEGA_EMPRESA, $db);
            if ($livreE < MAXIMO_ASSINATURA_OMEGA_EMPRESA_PARA_MIGRACAO_DISPONIVEL)
            {
                EXTDAO_Assinatura::criaNovaAssinaturas(
                    EXTDAO_Sistema::OMEGA_EMPRESA,
                    EXTDAO_Estado_assinatura::DISPONIVEL_PARA_MIGRACAO);
            }

            $livreE2 = EXTDAO_Assinatura::getNumeroDeAssinaturaDisponivelParaMigracaoDoSistema(
                EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO, $db);
            if ($livreE2 < MAXIMO_ASSINATURA_OMEGA_EMPRESA_PARA_MIGRACAO_CORPORACAO_DISPONIVEL)
            {
                EXTDAO_Assinatura::criaNovaAssinaturas(
                    EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO,
                    EXTDAO_Estado_assinatura::DISPONIVEL_PARA_MIGRACAO);
            }

            return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                                "Temos $livreE assinaturas livres do sistema OMEGA_EMPRESA. "
                                . "Temos $livreE2 assinaturas do sistema OMEGA_EMPRESA_CORPORACAO. ");
        }

        public function getTotalUsuariosDaAssinatura($idAssinatura, $idCorporacao, $corporacao)
        {
            $db = new Database();

            $obj = new EXTDAO_Assinatura($db);

            $msg = $obj->select($idAssinatura);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return BO_Web_service_hospedagem::getTotalDeUsuariosDaHospedagem(
                $idCorporacao, $corporacao, $obj->getHospedagem_id_INT(), $db);
        }

        public function getHospedagensDoUsuario($idAssinatura, $idCorporacao, $corporacao)
        {
            $db = new Database();
            $obj = new EXTDAO_Assinatura($db);

            $msg = $obj->select($idAssinatura);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $totalDeUsuarios = BO_Web_service_hospedagem::getTotalDeUsuariosDaHospedagem(
                $idCorporacao, $corporacao, $obj->getHospedagem_id_INT(), $db);

            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $totalDeUsuarios);
        }

        public function getEspacoEmMBOcupadoPelaAssinatura($idAssinatura)
        {
            $obj = new EXTDAO_Assinatura();
            $msg = $obj->select($idAssinatura);
//        echo "asdf";
//        print_r($msg);
//        exit();
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $espacoBanco = BO_Web_service_hospedagem::getEspacoOcupadoPelaBancoDeDados($obj->getHospedagem_id_INT());

            $espacoDosArquivos = $obj->getFkObjHospedagem()->getEspacoOcupadoPelosArquivosDaHospedagemEmMB();

            $espacoTotal = 0;
            if (is_numeric($espacoBanco))
            {
                $espacoTotal += $espacoBanco;
            }

            if (is_numeric($espacoDosArquivos))
            {
                $espacoTotal += $espacoDosArquivos;
            }

            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $espacoTotal);
        }

        public function getConfiguracoesDeBancoDaAssinatura($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $configuracoesBanco = EXTDAO_Assinatura::getConfiguracoesDeBancoDaAssinatura($idAssinatura);

                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $configuracoesBanco);
                }

                return new Mensagem_token(

                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getConfiguracoesDeBancoDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )

                );
            }
            catch (Exception $exc)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function getDominioDaAssinatura($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $msg = EXTDAO_Assinatura::getDominioDaAssinatura($idAssinatura);
                    if ($msg->resultadoVazio())
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Assinatura indefinida: $idAssinatura");
                    }

                    return $msg;
                }

                return new Mensagem_token(

                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getDominioDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )

                );
            }
            catch (Exception $exc)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function getDominioWebServiceDaAssinatura($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $msg = EXTDAO_Assinatura::getDominioDoWebServiceDaAssinatura($idAssinatura);
                    if ($msg->resultadoVazio())
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Assinatura indefinida: $idAssinatura");
                    }

                    return $msg;
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getDominioWebServiceDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )
                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function getDominioDoSincronizadorWeb($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $msg = EXTDAO_Assinatura::getDominioDoWebServiceDoSincronizadorWebAssinatura($idAssinatura);
                    if ($msg->resultadoVazio())
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Assinatura indefinida: $idAssinatura");
                    }

                    return $msg;
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getDominioWebServiceDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )
                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function deletarAssinatura($idAssinatura)
        {
            if (strlen($idAssinatura))
            {
                $objAssinatura = new EXTDAO_Assinatura();
                $msg = $objAssinatura->select($idAssinatura);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                else
                {
                    if ($msg != null && $msg->resultadoVazio())
                    {
                        return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                             I18N::getExpression("A assinatura {0} já não existia.", $idAssinatura));
                    }
                }
                if (strlen($objAssinatura->getExcluido_DATETIME()))
                {
                    return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                         "A assinatura foi excluída com sucesso.");
                }

                if ($objAssinatura->getEstado_assinatura_id_INT() == EXTDAO_Estado_assinatura::DISPONIVEL)
                {
                    return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                         "Assinatura está disponível novamente.");
                }
                else
                {
                    try
                    {
                        $msg = EXTDAO_Assinatura::deletarAssinatura($idAssinatura);
                        return $msg;
                    }
                    catch (Exception $exc)
                    {
                        HelperLog::logException($exc, true);
                        return new Mensagem(null, null, $exc);
                    }
                }
            }
            else
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    I18N::getExpression("Parametros inválidos.")
                );
            }
        }

        public function disponibilizaAssinatura($idAssinatura)
        {
            if (strlen($idAssinatura))
            {
                $objAssinatura = new EXTDAO_Assinatura();
                $objAssinatura->select($idAssinatura);

                if ($objAssinatura->getEstado_assinatura_id_INT() == EXTDAO_Estado_assinatura::DISPONIVEL)
                {
                    return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                         I18N::getExpression("Assinatura está disponível novamente."));
                }
                else
                {
                    try
                    {
                        EXTDAO_Assinatura::disponibilizarAssinatura($idAssinatura);

                        return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                             I18N::getExpression("Assinatura está disponível novamente."));
                    }
                    catch (Exception $exc)
                    {
                        HelperLog::logException($exc, true);
                        return new Mensagem(null, null, $exc);
                    }
                }
            }
            else
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    I18N::getExpression("Parâmetros inválidos.")
                );
            }
        }

        public function cadastraAssinaturaDoClienteParaSistemaCompartilhadoEBancoCompartilhado(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorDominio,
            Database $db = null)
        {
            if (strlen($idSistema)
                && strlen($idClienteAssinatura)
                && strlen($nomeSite)
                && strlen($identificadorDominio))
            {
                $idAssinatura = EXTDAO_Assinatura::getIdAssinaturaLivreDoSistema(
                    $idSistema, $db);

                if (!strlen($idAssinatura))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                        I18N::getExpression("Não existe nenhuma assinatura disponível para hospedagem, tente novamente daqui a 5 minutos. Desculpe o transtorno.")
                    );
                }
                else
                {
                    if ($db == null)
                    {
                        $db = new Database();
                    }

                    $obj = new EXTDAO_Assinatura($db);
                    $obj->select($idAssinatura);
                    $obj->setSicob_cliente_assinatura_INT($idClienteAssinatura);
                    $obj->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::OCUPADA);
                    $idHospedagem = $obj->getHospedagem_id_INT();
                    $objHospedagem = new EXTDAO_Hospedagem($db);
                    $objHospedagem->select($idHospedagem);

                    $objSistema = new EXTDAO_Sistema($db);
                    $objSistema->select($idSistema);

                    $dominio = $objSistema->getDominio_raiz_assinatura();
                    $dominioSincronizador = $objSistema->getDominio_raiz_sincronizador();
                    $dominioDaAssinatura = Helper::getPathComBarra($dominio);

                    $objHospedagem->setDominio_sincronizador($dominioSincronizador);
                    $objHospedagem->setDominio($dominioDaAssinatura);

                    $obj = new EXTDAO_Assinatura($db);
                    $obj->select($idAssinatura);
                    $obj->setSicob_cliente_assinatura_INT($idClienteAssinatura);
                    $obj->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::OCUPADA);

                    $objHospedagem->setDominio_webservice(Helper::getPathComBarra($dominioDaAssinatura) . "adm/webservice.php?class=Servicos_web_sihop&action=");

                    $objHospedagem->formatarParaSQL();
                    $objHospedagem->update($idHospedagem);

                    $obj->setNome_site($nomeSite);
                    $obj->formatarParaSQL();
                    $obj->update($idAssinatura);

                    $this->geraArquivosConfiguracaoSistemaCompartilhado(
                        $idAssinatura,
                        $db);

                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                              null, $idAssinatura);
                }
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "criaHospedagem",
                        array($idSistema, $idClienteAssinatura, $nomeSite, $identificadorDominio)
                    ),
                    array()
                )
            );
        }

        public static function geraArquivosConfiguracaoSistemaCompartilhado(
            $idAssinatura,
            Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $obj = new EXTDAO_Assinatura($db);
            $obj->select($idAssinatura);

            $idSistema = $obj->getSistema_id_INT();
            $nomeSite = $obj->getNome_site();
            $identificadorDominio = $obj->getNome_site();

            $idHospedagem = $obj->getHospedagem_id_INT();
            $objHospedagem = new EXTDAO_Hospedagem($db);
            $objHospedagem->select($idHospedagem);

            $objSistema = new EXTDAO_Sistema($db);
            $objSistema->select($idSistema);

            $dominio = $objSistema->getDominio_raiz_assinatura();
            $dominioSincronizador = $objSistema->getDominio_raiz_sincronizador();
            $dominioDaAssinatura = Helper::getPathComBarra($dominio);

            $objHospedagem->setDominio_sincronizador($dominioSincronizador);
            $objHospedagem->setDominio($dominioDaAssinatura);

            $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());

            $pathHospedagemSincronizador = Helper::formatarPath($objHospedagem->getPath_sincronizador());

            $idHDBPrincipal = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                $idHospedagem,
                EXTDAO_Tipo_hospedagem_db::PRINCIPAL);

            $idHDBParametros = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                $idHospedagem,
                EXTDAO_Tipo_hospedagem_db::PARAMETROS);

            $idHDBSincronizador = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                $idHospedagem,
                EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR);

            $objHDBPrincipal = new EXTDAO_Hospedagem_db($db);
            $objHDBPrincipal->select($idHDBPrincipal);

            $objHDBParametros = new EXTDAO_Hospedagem_db($db);
            $objHDBParametros->select($idHDBParametros);

            $objHDBSincronizador = new EXTDAO_Hospedagem_db($db);
            $objHDBSincronizador->select($idHDBSincronizador);

            $jsonDatabaseConfig = $obj->getStringArquivoJsonDatabaseConfigFactu(
                $nomeSite,
                $objHDBPrincipal,
                $objHDBParametros,
                $identificadorDominio,
                $objHDBSincronizador);

            $pathPhpCorporacao = $pathHospedagem . "/recursos/database_config_corporacao/";
            if (!file_exists($pathPhpCorporacao))
            {
                Helper::mkdir($pathPhpCorporacao, 0777, true);
            }

            $pathDatabaseConfigSincronizador = $pathHospedagemSincronizador . "/recursos/database_config_corporacao/";
            if (!file_exists($pathDatabaseConfigSincronizador))
            {
                Helper::mkdir($pathDatabaseConfigSincronizador, 0777, true);
            }

            $pathJsonDatabaseConfig = $pathPhpCorporacao . "/database_config_{$identificadorDominio}.json";
            Helper::criaArquivoComOConteudo($pathJsonDatabaseConfig, $jsonDatabaseConfig);
        }

        public function apagaArquivosDeConfiguracaoDaAssinatura($idAssinatura)
        {
            $obj = new EXTDAO_Assinatura();
            $obj->select($idAssinatura);

            $objHospedagem = $obj->getFkObjHospedagem();

            $identificadorCorporacao = $obj->getNome_site();

            $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());

            $pathHospedagemSincronizador = Helper::formatarPath($objHospedagem->getPath_sincronizador());
            $pathDatabaseConfigSincronizador = $pathHospedagemSincronizador . "/recursos/database_config_corporacao/";
            $pathDatabaseSincronizador = $pathDatabaseConfigSincronizador . "database_config_{$identificadorCorporacao}.php";
            $pathJsonDatabaseSincronizador = $pathDatabaseConfigSincronizador . "database_config_{$identificadorCorporacao}.json";

            if (file_exists($pathDatabaseSincronizador))
            {
                unlink($pathDatabaseSincronizador);
            }
            if (file_exists($pathJsonDatabaseSincronizador))
            {
                unlink($pathJsonDatabaseSincronizador);
            }
            $pathPhpCorporacaoSincronizador = $pathHospedagemSincronizador . "recursos/php_corporacao/";
            $nomClasse = $obj->getNomeClasseConstantDatabaseConfigCorporacao($identificadorCorporacao);

            $pathDatabaseSincronizador = $pathPhpCorporacaoSincronizador . "$nomClasse.php";
            if (file_exists($pathDatabaseSincronizador))
            {
                unlink($pathDatabaseSincronizador);
            }

            $pathPhpCorporacao = $pathHospedagem . "recursos/database_config_corporacao/";
            $pathDatabaseConfig = $pathPhpCorporacao . "database_config_$identificadorCorporacao.php";
            $pathJsonDatabaseConfig = $pathPhpCorporacao . "database_config_$identificadorCorporacao.json";
            if (file_exists($pathDatabaseConfig))
            {
                unlink($pathDatabaseConfig);
            }
            if (file_exists($pathJsonDatabaseConfig))
            {
                unlink($pathJsonDatabaseConfig);
            }
        }

        public function iniciarMigracaoAssinatura($idAssinaturaAntiga, $idSistema, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $msg = $db->queryMensagem("SET @updateId = -1");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $msg = $db->queryMensagem("UPDATE assinatura "
                                          . "SET estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::EM_MIGRACAO
                                          . " , id = (SELECT @updateId := id)  "
                                          . " WHERE sistema_id_INT = {$idSistema} "
                                          . " AND estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::DISPONIVEL_PARA_MIGRACAO
                                          . " AND excluido_DATETIME IS NULL "
                                          . " LIMIT 1");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $msg = $db->queryMensagem("SELECT  @updateId");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $idAssinatura = $db->getPrimeiraTuplaDoResultSet(0);
            HelperLog::verbose("iniciarMigracaoAssinatura: $idAssinatura");
            if ($idAssinatura < 0)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    I18N::getExpression("Não foi encontrada nenhuma assinatura disponivel para o sistema: {0}", $idSistema));
            }
            else
            {
                $msg = $db->queryMensagem("UPDATE assinatura "
                                          . "SET estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::EM_MANUTENCAO
                                          . " WHERE id = $idAssinaturaAntiga ");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                return new Mensagem_token(null, null, $idAssinatura);
            }
        }

        public function finalizarMigracaoAssinatura(
            $idAssinaturaAntiga,
            $idAssinaturaNova = null,
            Database $db = null,
            $estadoAssinatura = EXTDAO_Estado_assinatura::ASSINATURA_FOI_MIGRADA
        )
        {
            if ($db == null)
            {
                $db = new Database();
            }
            if ($idAssinaturaNova != null)
            {
                $msg = $db->queryMensagem("UPDATE assinatura "
                                          . "SET estado_assinatura_id_INT = $estadoAssinatura, "
                                          . " nome_site = null, sicob_cliente_assinatura_INT = null "
                                          . " WHERE id =  " . EXTDAO_Estado_assinatura::OCUPADA);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
            }

            $msg = $db->queryMensagem("UPDATE assinatura "
                                      . "SET estado_assinatura_id_INT = " . $estadoAssinatura
                                      . " WHERE id = $idAssinaturaAntiga ");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return $msg;
        }

        public function cadastraAssinaturaDoCliente(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorCorporacao,
            $idAssinatura = null,
            $novoEstado = EXTDAO_Estado_assinatura::OCUPADA,
            $idCorporacao = null)
        {
            try
            {
                if (strlen($idSistema)
                    && strlen($idClienteAssinatura)
                    && strlen($nomeSite)
                    && strlen($identificadorCorporacao))
                {
                    $db = new Database();
                    if ($idAssinatura == null)
                    {
                        HelperLog::verbose("cadastraAssinaturaDoCliente: Buscando assinatura livre do sistema $idSistema ...");
                        $idAssinatura = EXTDAO_Assinatura::getIdAssinaturaLivreDoSistema($idSistema, $db);
                    }
                    if (!strlen($idAssinatura))
                    {
                        HelperLog::verbose("cadastraAssinaturaDoCliente: N?o encontrou assinatura livre");

                        return new Mensagem(
                            PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                            "[gert54]N?o existe nenhuma assinatura dispon?vel para hospedagem, tente novamente daqui a 5 minutos. Desculpe o transtorno"
                        );
                    }
                    else
                    {
                        HelperLog::verbose("cadastraAssinaturaDoCliente: $idAssinatura");
                        $objSistema = new EXTDAO_Sistema($db);
                        $objSistema->select($idSistema);
                        switch ($objSistema->getSistema_tipo_id_INT())
                        {
                            case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA :
                            case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS:
                                return $this->cadastraAssinaturaDoClienteParaSistemaCompartilhado(
                                    $idSistema,
                                    $idClienteAssinatura,
                                    $nomeSite,
                                    $identificadorCorporacao,
                                    $idAssinatura,
                                    $novoEstado,
                                    $idCorporacao,
                                    $db);
                            case EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                                return $this->cadastraAssinaturaDoClienteParaSistemaIsolado(
                                    $idSistema,
                                    $idClienteAssinatura,
                                    $nomeSite,
                                    $identificadorCorporacao,
                                    $idAssinatura,
                                    $novoEstado,
                                    $idCorporacao);
                            default:
                                throw new Exception("Tipo de sistema nao mapeado", "0", null);
                        }

                        return new Mensagem(null, "Migracao realizada com sucesso");
                    }
                }
                else
                {
                    return new Mensagem_token(
                        PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        HelperMensagem::GERAL_FALHA_CADASTRO(
                            HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                                "criaHospedagem",
                                array($idSistema, $idClienteAssinatura, $nomeSite, $identificadorCorporacao)
                            ),
                            array()
                        )
                    );
                }
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        private function cadastraAssinaturaDoClienteParaSistemaCompartilhado(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorCorporacao,
            $idAssinatura,
            $novoEstado = EXTDAO_Estado_assinatura::OCUPADA,
            $idCorporacao = null,
            $db = null)
        {
            if (strlen($idSistema)
                && strlen($idClienteAssinatura)
                && strlen($nomeSite)
                && strlen($identificadorCorporacao)
                && strlen($idAssinatura))
            {
                $identificadorCorporacao = str_replace(' ', "_", $identificadorCorporacao);
                if($db ==null)$db = new Database();
                $obj = new EXTDAO_Assinatura($db);

                $obj->select($idAssinatura);
                $obj->setSicob_cliente_assinatura_INT($idClienteAssinatura);
                $obj->setEstado_assinatura_id_INT($novoEstado);
                $idHospedagem = $obj->getHospedagem_id_INT();

                $objHospedagem = new EXTDAO_Hospedagem($db);
                $objHospedagem->select($idHospedagem);

                $objSistema = new EXTDAO_Sistema($db);
                $msg=$objSistema->select($idSistema);
                if(Interface_mensagem::checkErro($msg))return $msg;

                $dominio = $objSistema->getDominio_raiz_assinatura();
                $dominioSincronizador = $objSistema->getDominio_raiz_sincronizador();
                $pathRaizConteudo = Helper::formatarPath($objSistema->getPath_raiz_conteudo());

                $dominioDaAssinatura = Helper::getPathComBarra($dominio);

                $objHospedagem->setDominio_sincronizador($dominioSincronizador);
                $objHospedagem->setDominio($dominioDaAssinatura);

                $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());

                $pathHospedagemSincronizador = Helper::formatarPath($objHospedagem->getPath_sincronizador());

                $idHDBPrincipal = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                    $idHospedagem,
                    EXTDAO_Tipo_hospedagem_db::PRINCIPAL);

                $idHDBParametros = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                    $idHospedagem,
                    EXTDAO_Tipo_hospedagem_db::PARAMETROS);

                $idHDBSincronizador = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                    $idHospedagem,
                    EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR);

                $objHDBPrincipal = new EXTDAO_Hospedagem_db($db);
                $msg=$objHDBPrincipal->select($idHDBPrincipal);
                if(Interface_mensagem::checkErro($msg))return $msg;

                $objHDBParametros = new EXTDAO_Hospedagem_db($db);
                $msg=$objHDBParametros->select($idHDBParametros);
                if(Interface_mensagem::checkErro($msg))return $msg;

//                         echo "passou: $idHDBSincronizador";
                $objHDBSincronizador = new EXTDAO_Hospedagem_db($db);
                $msg=$objHDBSincronizador->select($idHDBSincronizador);
                if(Interface_mensagem::checkErro($msg))return $msg;

                $jsonDatabaseConfig = $obj->getStringArquivoJsonDatabaseConfigFactu(
                    $nomeSite,
                    $objHDBPrincipal,
                    $objHDBParametros,
                    $identificadorCorporacao,
                    $objHDBSincronizador);

                $pathDatabaseConfigSincronizador = $pathHospedagemSincronizador . "/recursos/database_config_corporacao/";
                if (!file_exists($pathDatabaseConfigSincronizador))
                {
                    Helper::mkdir($pathDatabaseConfigSincronizador, 0777, true);
                }

                $pathPhpCorporacao = $pathHospedagem . "/recursos/database_config_corporacao/";
                if (!file_exists($pathPhpCorporacao))
                {
                    Helper::mkdir($pathPhpCorporacao, 0777, true);
                }
                $pathJsonDatabaseConfig = $pathPhpCorporacao . "/database_config_{$identificadorCorporacao}.json";

                Helper::criaArquivoComOConteudo($pathJsonDatabaseConfig, $jsonDatabaseConfig);

                $objHospedagem->setDominio_webservice(Helper::getPathComBarra($dominioDaAssinatura) . "adm/webservice.php?class=Servicos_web_sihop&action=");
                $objHospedagem->setPath_raiz_conteudo(Helper::getPathComBarraDiretorio($pathRaizConteudo));

                $objHospedagem->formatarParaSQL();
                $msg=$objHospedagem->update($idHospedagem);
                if(Interface_mensagem::checkErro($msg))return $msg;

                $obj->setId_corporacao_INT($idCorporacao);

                $obj->setNome_site($nomeSite);
                $obj->formatarParaSQL();

                $msg = $obj->update($idAssinatura);
                if(Interface_mensagem::checkErro($msg))return $msg;

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    null,
                    $idAssinatura);
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "criaHospedagem",
                        array($idSistema, $idClienteAssinatura, $nomeSite, $identificadorCorporacao)
                    ),
                    array()
                )
            );
        }

        private function cadastraAssinaturaDoClienteParaSistemaIsolado(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorCorporacao,
            $idAssinatura,
            $novoEstado = EXTDAO_Estado_assinatura::OCUPADA,
            $idCorporacao = null)
        {
            $identificadorCorporacao = str_replace(' ', "_", $identificadorCorporacao);
            $obj = new EXTDAO_Assinatura();
            $obj->select($idAssinatura);
            $obj->setSicob_cliente_assinatura_INT($idClienteAssinatura);
            $obj->setEstado_assinatura_id_INT($novoEstado);

            $idHospedagem = $obj->getHospedagem_id_INT();
            $objHospedagem = new EXTDAO_Hospedagem();
            $objHospedagem->select($idHospedagem);

            $objSistema = new EXTDAO_Sistema();
            $objSistema->select($idSistema);
            $pathRaizAssinatura = $objSistema->getPath_raiz_assinatura();
            $dominio = $objSistema->getDominio_raiz_assinatura();

            $dominioDaAssinatura = Helper::getPathComBarra($dominio) . $identificadorCorporacao . "/";

            $objHospedagem->setDominio($dominioDaAssinatura);

            $pathHospedagemAntiga = Helper::formatarPath($objHospedagem->getPath());
            $pathHospedagem = Helper::formatarPath(Helper::getPathComBarra($pathRaizAssinatura) . $identificadorCorporacao . "\\");
//                echo $pathHospedagem;
            $idHDBPrincipal = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                $idHospedagem,
                EXTDAO_Tipo_hospedagem_db::PRINCIPAL);

            $idHDBParametros = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                $idHospedagem,
                EXTDAO_Tipo_hospedagem_db::PARAMETROS);

            $objHDBPrincipal = new EXTDAO_Hospedagem_db();
            $objHDBPrincipal->select($idHDBPrincipal);

            $objHDBParametros = new EXTDAO_Hospedagem_db();
            $objHDBParametros->select($idHDBParametros);

            $databaseConfig = $obj->getStringArquivoDatabaseConfigFactu(
                $nomeSite,
                $objHDBPrincipal,
                $objHDBParametros,
                $identificadorCorporacao);

            $jsonDatabaseConfig = $obj->getStringArquivoJsonDatabaseConfigFactu(
                $nomeSite,
                $objHDBPrincipal,
                $objHDBParametros,
                $identificadorCorporacao);

            $constants = $obj->getStringArquivoConstantsFactu(
                $objHospedagem->getId(),
                $nomeSite,
                $dominioDaAssinatura,
                $identificadorCorporacao);

            $pathDatabaseConfig = $pathHospedagemAntiga . "/recursos/php/database_config.php";
            $pathJsonDatabaseConfig = $pathHospedagemAntiga . "/recursos/php/database_config.json";
            Helper::criaArquivoComOConteudo($pathDatabaseConfig, $databaseConfig);
            Helper::criaArquivoComOConteudo($pathJsonDatabaseConfig, $jsonDatabaseConfig);

            $pathDatabaseConstants = $pathHospedagemAntiga . "/recursos/php/constants.php";
            Helper::criaArquivoComOConteudo($pathDatabaseConstants, $constants);

            $objHospedagem->setDominio_webservice(Helper::getPathComBarra($dominioDaAssinatura) . "adm/webservice.php?class=Servicos_web_sihop&action=");
            if (file_exists($pathHospedagem))
            {
                Helper::deletaDiretorio($pathHospedagem);
            }
            if (!Helper::renomearDiretorio($pathHospedagemAntiga, $pathHospedagem))
            {
                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    "Ocorreu um erro durante a execu??o, tente novamente mais tarde. Mas j? estamos analisando o problema, desculpe o transtorno.");
            }
            else
            {
                $objHospedagem->setPath($pathHospedagem);
                $objHospedagem->formatarParaSQL();
                $msg =$objHospedagem->update($idHospedagem);
                if(Interface_mensagem::checkErro($msg)) return $msg;
                $obj->setId_corporacao_INT($idCorporacao);
                $obj->setNome_site($nomeSite);
                $obj->formatarParaSQL();
                $msg = $obj->update($idAssinatura);
                if(Interface_mensagem::checkErro($msg)) return $msg;

                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                          null, $idAssinatura);
            }
        }

        public function criaNovasAssinaturasDeTodosOsSistemas()
        {
            try
            {
                $sistemas = EXTDAO_Sistema::getSistemas();
                for ($i = 0; $i < count($sistemas); $i++)
                {
                    $idSistema = $sistemas[ $i ];
                    $objSistema = new EXTDAO_Sistema();
                    $msg=$objSistema->select($idSistema);
                    if(Interface_mensagem::checkErro($msg)) return $msg;
                    $numeroAssinaturas = EXTDAO_Assinatura::getNumeroDeAssinaturaLivreDoSistema($idSistema);
                    if ($numeroAssinaturas != null)
                    {
                        for ($j = $numeroAssinaturas; $j < 100; $j++)
                        {
                            $this->criaNovaAssinatura($idSistema);

                            return new Mensagem(
                                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                null,
                                null);
                        }
                    }
                }

                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, null);
            }
            catch (Exception $exc)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function criaNovaAssinatura($idSistema)
        {
            try
            {
                if (strlen($idSistema))
                {
                    $idNovaAssinatura = EXTDAO_Assinatura::criaNovaAssinaturas($idSistema);

                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                              null,
                                              $idNovaAssinatura);
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "criaHospedagem",
                            array()
                        ),
                        array($idSistema)
                    )
                );
            }
            catch (Exception $exc)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function atualizaAssinaturaDoCliente(
            $idAssinatura,
            $idClienteAssinaturaAntigo,
            $idClienteAssinaturaNovo,
            $idSistemaAntigo,
            $idSistemaNovo)
        {
            if (strlen($idAssinatura)
                && strlen($idClienteAssinaturaAntigo)
                && strlen($idClienteAssinaturaNovo))
            {
                $db = new Database();
                if ($idSistemaAntigo != $idSistemaNovo)
                {
                    $migrar = false;
                    switch ($idSistemaAntigo)
                    {
                        case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                            if ($idSistemaNovo == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS
                                || $idSistemaNovo == EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
                            {
                                $migrar = true;
                            }

                            break;
                        case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS:
                            if ($idSistemaNovo == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
                            {
                                $migrar = true;
                            }

                            break;
                        case EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                            if ($idSistemaNovo == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
                            {
                                $migrar = true;
                            }

                            break;
                    }
                    if ($migrar)
                    {
                        HelperLog::verbose("Migrando o sistema");
                        $objUltimaMigracao = EXTDAO_Assinatura_migracao::getIdUltimaMigracaoPendenteOuEmAndamento(
                            $idClienteAssinaturaAntigo, $db);
                        $objM = new EXTDAO_Assinatura_migracao($db);
                        if ($objUltimaMigracao != null
                            && $objUltimaMigracao['estado'] == EXTDAO_Estado_assinatura_migracao::AGUARDANDO_MIGRACAO)
                        {
                            HelperLog::verbose("Cancelando migra??o {$objUltimaMigracao['id']} pendente.. ");
                            //CANCELA A ULTIMA MIGRACAO PENDENTE

                            $msg =EXTDAO_Assinatura_migracao::cancelaTodaAssinaturaPendente($idAssinatura,$db);
                            if(Interface_mensagem::checkErro($msg))return $msg;
                        }
                        else
                        {
                            HelperLog::verbose("Não existe migração pendente da assinatura SICOB $idClienteAssinaturaAntigo .");
                        }

                        $objM->setAntigo_sicob_cliente_assinatura_INT($idClienteAssinaturaAntigo);
                        $objM->setAntigo_sistema_id_INT($idSistemaAntigo);
                        $objM->setAssinatura_id_INT($idAssinatura);
                        $objM->setData_cadastro_DATETIME(Helper::getDiaEHoraAtualSQL());
                        $objM->setNovo_sicob_cliente_assinatura_INT($idClienteAssinaturaNovo);
                        $objM->setNovo_sistema_id_INT($idSistemaNovo);
                        $objM->setEstado_assinatura_migracao_id_INT(EXTDAO_Estado_assinatura_migracao::AGUARDANDO_MIGRACAO);
                        $objM->formatarParaSQL();

                        $msg=$objM->insert();
                        if(Interface_mensagem::checkErro($msg))return $msg;
                        $idMigracao = $objM->getUltimoIdInserido();
                        HelperLog::verbose("Migracao $idMigracao empilhada!");
                    }
                    else
                    {
                        HelperLog::verbose("[1]N?o ser? necess?rio realizar migra??o. Id Sistema Antigo: $idSistemaAntigo. Id Sistema Novo: $idSistemaNovo.");
                    }
                }
                else
                {
                    HelperLog::verbose("[2]N?o ser? necess?rio realizar migra??o. Id Sistema Antigo: $idSistemaAntigo. Id Sistema Novo: $idSistemaNovo.");
                }

                $obj = new EXTDAO_Assinatura($db);
                $obj->select($idAssinatura);
                $obj->setSicob_cliente_assinatura_INT($idClienteAssinaturaNovo);
                $obj->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::OCUPADA);

                $obj->formatarParaSQL();
                $msg=$obj->update($idAssinatura);
                if(Interface_mensagem::checkErro($msg))return $msg;
                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $obj->getId());
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "atualizaAssinaturaDoCliente",
                        array($idAssinatura, $idClienteAssinaturaAntigo, $idClienteAssinaturaNovo)
                    ),
                    array()
                )
            );
        }

        public function finalizaDisponibilizacaoAssinatura(
            $idAssinatura,
            Database $db = null
        )
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $msg = $db->queryMensagem("UPDATE assinatura "
                                      . "SET estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::DISPONIVEL
                                      . ", nome_site = null "
                                      . ", sicob_cliente_assinatura_INT = null "
                                      . " WHERE id = $idAssinatura ");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return $msg;
        }

    }

<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    class BO_Assinatura_migracao
    {

        public static function factory()
        {
            return new BO_Assinatura_migracao();
        }

        //php C:\wamp\www\Hospedagem\10002Corporacao\adm\executavel.php BO_Assinatura_migracao migrarProximaAssinaturaDaFila
        public function migrarProximaAssinaturaDaFila()
        {
            $idAM = null;
            $corporacao = null;
            $idCorporacao = null;
            try
            {
                //valida horairo de funcionamento da migracao

                if(!Helper::validaHorarioFuncionamento(
                    HORA_INICIO_FUNCIONAMENTO_MIGRACAO_DE_ASSINATURAS,
                        HORA_FIM_FUNCIONAMENTO_MIGRACAO_DE_ASSINATURAS))
                    return new Mensagem(PROTOCOLO_SISTEMA::FORA_HORARIO_FUNCIONAMENTO, "O servico funciona das 00 as 04 GMT Brasilia!");

                $db = new Database();
                Registry::add($db, 'Database');
                $msg = EXTDAO_Assinatura_migracao::getProximoIdParaMigracao($db);
                if (!$msg->ok())
                {
                    return $msg;
                }

                $idAM = $msg->mValor;
                $boAssinatura = new BO_Assinatura();
                $objAM = new EXTDAO_Assinatura_migracao($db);
                $objAM->select($idAM);
                
                HelperLog::verbose("migrarProximaAssinaturaDaFila - $idAM id_assinatura_migracao", null, true);

                $idAssinatura = $objAM->getAssinatura_id_INT();
                $objCorporacao = EXTDAO_Assinatura::getIdCorporacaoPorNome($idAssinatura, $db);
                $corporacao = $objCorporacao["corporacao"];
                $idCorporacao = $objCorporacao["idCorporacao"];
                $objAssinaturaAntiga = new EXTDAO_Assinatura($db);
                $objAssinaturaAntiga->select($idAssinatura);

                $msg = $boAssinatura->iniciarMigracaoAssinatura(
                    $idAssinatura,
                    $objAM->getNovo_sistema_id_INT(),
                    $db);

                HelperLog::verbose("iniciarMigracaoAssinatura[$idAssinatura]: " . print_r($msg, true), null, true);

                //iremos utilizar alguma assinatura disponivel do novo sistema,
                //deletar a antiga assinatura
                //ocupar a nova assinatura
                if ($msg != null && !$msg->ok())
                {
                    if ($msg->resultadoVazio())
                    {
                        $msg2 = $boAssinatura->finalizarMigracaoAssinatura(
                            $idAssinatura,
                            null,
                            $db,
                            EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA);
                        HelperLog::verbose("finalizarMigracaoAssinatura 1234- [$idAssinatura]: " . print_r($msg2, true), null, true);
                    }

                    $erro = Helper::substring(Helper::jsonEncode($msg), 256);
                    $objAM->setData_fim_migracao_DATETIME(Helper::getDiaEHoraAtualSQL());
                    $objAM->setErro(Helper::substring($erro, 255));
                    $objAM->setEstado_assinatura_migracao_id_INT(EXTDAO_Estado_assinatura_migracao::FALTA_ASSINATURA_DISPONIVEL);
                    $objAM->formatarParaSQL();
                    $objAM->update($objAM->getId());
                    HelperLog::verbose("FIM[$idAssinatura]: " . print_r($msg, true), null, true);

                    return $msg;
                }
                $idNovaAssinatura = $msg->mValor;
                Manutencao::setAssinaturaDaCorporacaoEmManutencao($idCorporacao);

                if (!Manutencao::checkIfAssinaturaDaCorporacaoEstaEmManutencao(
                    false, $idCorporacao, false))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                        I18N::getExpression("Falha ao setar o modo manutenção da corporação {0}", $corporacao));
                }
                EXTDAO_Assinatura_migracao::iniciaEsperaParaEstadoManutencao($idAM, $idNovaAssinatura, $db);
                Database::closeAll();
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);
                if ($corporacao != null)
                {
                    Manutencao::setAssinaturaDaCorporacaoEmManutencao($idCorporacao, false);
                }
                $msg = new Mensagem(null, null, $ex);
            }

            return $msg;
        }

        public function continuaMigracaoDasQueJaEstaoNoEstadoEmManutencao()
        {
            $corporacao = null;
            $idCorporacao = null;
            $idUltimaSincronizacao = 1;
            $idAM = null;
            try
            {
                Registry::remove('Database');
                $db = new Database();

                Registry::add($db, 'Database');

                $msg = EXTDAO_Assinatura_migracao::getProximoIdParaContinuarMigracao($db);
                if ($msg == null || !$msg->ok())
                {
                    return $msg;
                }
                $idAM = $msg->mValor;
                if (!strlen($idAM))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                        "Não existe ids migração pendente.");
                }
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);
                return new Mensagem(null, null, $ex);
            }

            try
            {
                $objAM = new EXTDAO_Assinatura_migracao($db);
                $objAM->select($idAM);
                $idNovaAssinatura = $objAM->getNova_assinatura_id_INT();
                $idAssinatura = $objAM->getAssinatura_id_INT();

                $objCorporacao = EXTDAO_Assinatura::getIdCorporacaoPorNome($idAssinatura, $db);
                $corporacao = $objCorporacao["corporacao"];
                $idCorporacao = $objCorporacao["idCorporacao"];

                $objAssinaturaAntiga = new EXTDAO_Assinatura($db);
                $objAssinaturaAntiga->select($idAssinatura);
                $idSICOBClienteAssinaturaAntiga = $objAssinaturaAntiga->getSicob_cliente_assinatura_INT();

                $msg = null;
                try
                {
                    //BANCO INDIVIDUAL
                    if ($objAM->getNovo_sistema_id_INT() == EXTDAO_Sistema::OMEGA_EMPRESA)
                    {
                        $msg = $this->migrarSistemaOmegaEmpresa(
                            $idNovaAssinatura,
                            $idAssinatura,
                            $objAM,
                            $db,
                            $idCorporacao);
                    }
                    else
                    {
                        if ($objAM->getNovo_sistema_id_INT() == EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO)
                        {
                            $msg = $this->migrarSistemaOmegaEmpresaCorporacao(
                                $idNovaAssinatura,
                                $idAssinatura,
                                $objAM,
                                $db,
                                $idCorporacao);
                        }
                        else
                        {
                            $msg = new Mensagem(
                                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                                "Sistema não programado: " . $objAM->getNovo_sistema_id_INT());
                        }
                    }
                    if ($msg != null && $msg->ok())
                    {
                        $idUltimaSincronizacao = $msg->mValor;
                    }
                }
                catch (Exception $ex)
                {
                    HelperLog::logException($ex, true);
                    $msg = new Mensagem(null, null, $ex);
                }

                if ($msg == null || !$msg->erro())
                {
                    $msg = EXTDAO_Assinatura::limpaDadosDaAssinatura($idAssinatura, $db);

                    if ($msg == null || !$msg->erro())
                    {
                        $boAssinatura = new BO_Assinatura();
                        $boAssinatura->apagaArquivosDeConfiguracaoDaAssinatura($idAssinatura);

                        $msg = $boAssinatura->cadastraAssinaturaDoCliente(
                            $objAM->getNovo_sistema_id_INT(),
                            $objAM->getNovo_sicob_cliente_assinatura_INT(),
                            $corporacao,
                            $corporacao,
                            $idNovaAssinatura,
                            EXTDAO_Estado_assinatura::OCUPADA,
                            $idCorporacao);
                        $boWSH = null;
                        $boSINCW = null;
                        if ($msg == null || $msg->ok())
                        {
                            $idNovaHospedagem = EXTDAO_Assinatura::getHospedagemDaAssinatura($idNovaAssinatura, $db);
                            $boWSH = new BO_Web_service_hospedagem($db, $idNovaHospedagem);

                            $msg = $boWSH->assinaturaMigrada($idCorporacao, $corporacao, $idUltimaSincronizacao);
                        }

                        if ($msg != null && !$msg->ok())
                        {
                            $boAssinatura->apagaArquivosDeConfiguracaoDaAssinatura($idNovaAssinatura);

                            $boAssinatura->cadastraAssinaturaDoCliente(
                                $objAM->getAntigo_sistema_id_INT(),
                                $objAM->getAntigo_sicob_cliente_assinatura_INT(),
                                $corporacao,
                                $corporacao,
                                $idAssinatura,
                                EXTDAO_Estado_assinatura::OCUPADA,
                                $idCorporacao);

                            $msg = $this->finalizaMigracaoComErro(
                                $boAssinatura,
                                $idAssinatura,
                                $db,
                                $msg,
                                $objAM,
                                $idNovaAssinatura,
                                $corporacao,
                                $idSICOBClienteAssinaturaAntiga,
                                $idCorporacao
                            );

                            return $msg;
                        }

                        if ($msg == null || !$msg->erro())
                        {
                            $msg = BO_SICOB::updateIdSihopAssinatura(
                                $objAM->getNovo_sicob_cliente_assinatura_INT(),
                                $idNovaAssinatura);
                        }

                        if ($msg == null || !$msg->erro())
                        {
                            $msg = $boWSH->clearCacheBanco($idCorporacao, $corporacao);
                        }

                        if ($msg == null || !$msg->erro())
                        {
                            $boSINCW = new BO_SINCW($db, $idNovaHospedagem);
                            $msg = $boSINCW->clearCacheBancoEEmpilhaPedidoSqlite(
                                $idCorporacao,
                                $corporacao,
                                $objAM->getNovo_sistema_id_INT());
                        }

                        if ($msg == null || !$msg->erro())
                        {
                            $msg = $boAssinatura->finalizarMigracaoAssinatura(
                                $idAssinatura,
                                $idNovaAssinatura,
                                $db);
                        }

                        if ($msg == null || !$msg->erro())
                        {
                            $objAM->setData_fim_migracao_DATETIME(Helper::getDiaEHoraAtualSQL());
                            $objAM->setNova_assinatura_id_INT($idNovaAssinatura);
                            $objAM->setEstado_assinatura_migracao_id_INT(EXTDAO_Estado_assinatura_migracao::MIGRADO_COM_SUCESSO);
                            $objAM->setId_corporacao_INT($idCorporacao);
                            $objAM->formatarParaSQL();
                            $msg = $objAM->update($objAM->getId());
                        }

                    }

                }

                if ($msg != null && $msg->erro())
                {
                    $msg = $this->finalizaMigracaoComErro(
                        $boAssinatura,
                        $idAssinatura,
                        $db,
                        $msg,
                        $objAM,
                        $idNovaAssinatura,
                        $corporacao,
                        $idSICOBClienteAssinaturaAntiga,
                        $idCorporacao
                    );

                    return $msg;
                }

                Manutencao::setAssinaturaDaCorporacaoEmManutencao($idCorporacao, false);

                return new Mensagem(
                    null,
                    "Migracao realizada com sucesso da assinatura $idAssinatura para $idNovaAssinatura");

            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                $msg = new Mensagem(null, null, $exc);
                $this->finalizaMigracaoComErro(
                    $boAssinatura,
                    $idAssinatura,
                    $db,
                    $msg,
                    $objAM,
                    $idNovaAssinatura,
                    $corporacao,
                    $idSICOBClienteAssinaturaAntiga,
                    $idCorporacao
                );
                if ($corporacao != null)
                {
                    Manutencao::setAssinaturaDaCorporacaoEmManutencao($idCorporacao, false);
                }

                return $msg;
            }
        }

        private function finalizaMigracaoComErro(
            $boAssinatura,
            $idAssinatura,
            $db,
            $msg,
            $objAM,
            $idNovaAssinatura,
            $corporacao,
            $idSicobClienteAssinatura,
            $idCorporacao
        )
        {
            try
            {
                if ($boAssinatura == null)
                {
                    $boAssinatura = new BO_Assinatura();
                }
                $boAssinatura->finalizarMigracaoAssinatura(
                    $idAssinatura,
                    null,
                    $db,
                    EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA);
                $erro = Helper::substring(Helper::jsonEncode($msg), 256);
                $objAM->setData_fim_migracao_DATETIME(Helper::getDiaEHoraAtualSQL());
                $objAM->setEstado_assinatura_migracao_id_INT(EXTDAO_Estado_assinatura_migracao::ERRO);
                $objAM->setErro(Helper::substring($erro, 255));
                $objAM->formatarParaSQL();
                $objAM->update($objAM->getId());

                $this->disponibilizaAssinatura($idNovaAssinatura);

                EXTDAO_Assinatura::setaDadosDaAssinatura(
                    $idCorporacao,
                    $idAssinatura,
                    $corporacao,
                    $idSicobClienteAssinatura,
                    $db);

                Manutencao::setAssinaturaDaCorporacaoEmManutencao($idCorporacao, false);

                return $msg;
            }
            catch (Exception $ex)
            {
                HelperLog::logException($ex, true);
                return new Mensagem(null, null, $ex);
            }
        }

        private function finalizaDisponibilizacaoComErro(
            $msg,
            EXTDAO_Assinatura_migracao $objAM
        )
        {
            $json = Helper::jsonEncode($msg);
            HelperLog::logDisponibilizaHospedagem("Erro durante a disponibilizacao da corporacao. Erro: $json", true);
            $erro = Helper::substring($json, 256);
            $objAM->setData_fim_disponibilizacao_assinatura_DATETIME(Helper::getDiaEHoraAtualSQL());
            $objAM->setEstado_assinatura_migracao_id_INT(EXTDAO_Estado_assinatura_migracao::ERRO_DURANTE_DISPONIBILIZACAO);
            $objAM->setErro(Helper::substring($erro, 255));
            $objAM->formatarParaSQL();
            $objAM->update($objAM->getId());

            return $msg;
        }

        //migra uma assinatura paga(banco com uma unica corporacao) para uma free(banco com varias corporacoes)
        public function migrarSistemaOmegaEmpresaCorporacao(
            $idAssinaturaNova,
            $idAssinaturaAntiga,
            EXTDAO_Assinatura_migracao $objAM,
            Database $db,
            $idCorporacao)
        {
            $msg = $objAM->getIdAssinaturaGratuitaPendenteDeDisponibilizacao($idAssinaturaNova, $idCorporacao);
            if ($msg != null && $msg->ok())
            {
                $idAMASerDisponibilizada = $msg->mValor;
                $msg = $this->disponibilizaAssinaturaMigracao($idAMASerDisponibilizada, $db);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
            }

            $objAssinaturaAntiga = new EXTDAO_Assinatura($db);
            $objAssinaturaAntiga->select($idAssinaturaAntiga);
            $objAssinaturaNova = new EXTDAO_Assinatura($db);
            $objAssinaturaNova->select($idAssinaturaNova);
            set_time_limit(0);

            $antigasHospedagensDb = EXTDAO_Assinatura::getConfiguracoesDeTodosOsBancosDaAssinatura(
                $idAssinaturaAntiga,
                $db);

            $novasHospedagensDb = EXTDAO_Assinatura::getConfiguracoesDeTodosOsBancosDaAssinatura(
                $idAssinaturaNova,
                $db);
            //VERIFICAR SE DE FATO A ARQVORE DEVE SER CONSULTADA DA HOSPEDAGEM NOVA OU DA ANTIGA!!!
            //NO CASO DA NOVA, ESTA RETORNANDO NULO APOS A OCORRENCIA DE UMA MIGRACAO
            $dadosArvore = BO_BLNUV::getArvoreBancoOmegaEmpresa(
                $objAM->getFkObjNovo_sistema()->getSistema_biblioteca_nuvem_INT()
            );

            $corporacao = $objAssinaturaAntiga->getNome_site();
            if (!strlen($corporacao))
            {
                throw new Exception("Corporacao indefinida para a assinatura[$idAssinaturaAntiga]");
            }
            $dados = new stdClass();
            $dados->configDatabasePrincipal = $antigasHospedagensDb[ EXTDAO_Tipo_hospedagem_db::PRINCIPAL ];
            $dados->configDatabaseConfiguracao = $antigasHospedagensDb[ EXTDAO_Tipo_hospedagem_db::PARAMETROS ];
            $dados->configDatabaseSincronizador = $antigasHospedagensDb[ EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR ];

            $dbSincronizador = new Database($dados->configDatabaseSincronizador);
            $dbSincronizadorNovo = new Database($novasHospedagensDb[ EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR ]);

            $msg = $dbSincronizador->queryMensagem("SELECT MAX(id) FROM sincronizacao WHERE corporacao_id_INT = $idCorporacao ");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $idUltimaSincronizacao = Helper::getResultSetToPrimeiroResultado($dbSincronizador->result);

            $msg = $dbSincronizadorNovo->queryMensagem("SELECT 1 FROM corporacao WHERE id = $idCorporacao");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $idCorporacaoInserida = $dbSincronizadorNovo->getPrimeiraTuplaDoResultSet(0);
            if ($idCorporacaoInserida != 1)
            {
                $dbSincronizadorNovo->queryMensagem(
                    "INSERT INTO corporacao (id, nome, nome_normalizado, ultima_migracao_DATETIME) 
              VALUES ('$idCorporacao', '$corporacao', '$corporacao', '" . Helper::getDiaEHoraAtualSQL() . "' )");
            }

            $dbNovo = new Database($novasHospedagensDb[ EXTDAO_Tipo_hospedagem_db::PRINCIPAL ]);
            $dbNovo->query("SET foreign_key_checks = 0");
            $dbAntigo = new Database($antigasHospedagensDb[ EXTDAO_Tipo_hospedagem_db::PRINCIPAL ]);

            HelperLog::logTemp("Trasferindo dados do "
                               . $dbAntigo->getIdentificador() . " para " . $dbNovo->getIdentificador(), true);

            $tabelasNovo = $dbNovo->getListaDosNomesDasTabelas();
            $tabelasAntigo = $dbAntigo->getListaDosNomesDasTabelas();
            $TOT_IT = 1000;
            $boWH = new  BO_Web_service_hospedagem($db, $objAssinaturaAntiga->getHospedagem_id_INT());
            $idTabelaPorLru = array();
            $cabecalhoDipara = "INSERT INTO assinatura_migracao_dipara ( id_sistema_tabela_INT, id_antigo_INT, id_novo_INT, assinatura_migracao_id_INT ) VALUES ";

            foreach ($dadosArvore as $objTabela)
            {
                $totalColunas = -1;
                $where = "";
                $join = "";
                if ($dbAntigo->existeAtributo($objTabela->nome, "corporacao_id_INT"))
                {
                    $where .= " WHERE t.corporacao_id_INT = $idCorporacao";
                }
                else
                {
                    if (!in_array($objTabela->nome, $tabelasNovo))
                    {
                        throw new Exception ("Tabela {$objTabela->nome} nao foi encontrado"
                                             . " no novo banco: {$dbNovo->getIdentificador()}."
                                             . " Durante a migracao do banco {$dbAntigo->getIdentificador()}");
                    }
                    else
                    {
                        if (!in_array($objTabela->nome, $tabelasAntigo))
                        {
                            throw new Exception ("Tabela {$objTabela->nome} nao foi encontrado"
                                                 . " no novo banco: {$dbNovo->getIdentificador()}."
                                                 . " Durante a migracao do banco {$dbAntigo->getIdentificador()}");
                        }
                        else
                        {
                            if ($objTabela->nome == "corporacao")
                            {
                                $where .= " WHERE t.id = $idCorporacao";
                            }
                            else
                            {
                                if ($objTabela->nome == "usuario")
                                {
                                    $join .= " JOIN usuario_corporacao uc ON t.id = uc.usuario_id_INT ";
                                    $where .= " WHERE uc.corporacao_id_INT = $idCorporacao";
                                }
                                else
                                {
                                    if (!in_array($objTabela->nome, $tabelasNovo))
                                    {
                                        throw new Exception ("Tabela {$objTabela->nome} nao encontrada"
                                                             . " no novo banco: {$dbNovo->getIdentificador()}."
                                                             . " Durante a migracao do banco {$dbAntigo->getIdentificador()}");
                                    }
                                    else
                                    {
                                        HelperLog::logTemp("Tabela constante, logo não é migrada: {$objTabela->nome}", true);
                                        continue;
                                    }

                                }

                            }

                        }

                    }

                }

                $qCount = "SELECT COUNT(t.id) "
                            . " FROM {$objTabela->nome} t {$join} "
                            . " {$where} ";

                HelperLog::logTemp($qCount, true);
                $dbAntigo->query($qCount);
                $totGerar = $dbAntigo->getPrimeiraTuplaDoResultSet(0);
                if ($totGerar == null || $totGerar <= 0)
                {
                    continue;
                }
                if ($objTabela->nome == "corporacao")
                {
                    $idIniGerado = $idCorporacao;
                }
                else
                {
                    //reserva a faixa inteira [$idIniGerado, $idIniGerado+ $totGerar]
                    $idIniGerado = $boWH->gerarIdInterna(
                        $db,
                        $objAssinaturaNova->getHospedagem_id_INT(),
                        $idCorporacao,
                        $corporacao,
                        $objTabela->nome,
                        $totGerar);
                }

                HelperLog::logTemp("idIniGerado[$objTabela->nome]: $idIniGerado", true);

                $q = "SELECT t.*"
                    . " FROM {$objTabela->nome} t $join "
                    . " $where ";

                HelperLog::logTemp($q, true);

                $iLimit = 0;
                $i = $TOT_IT;
                $indicesFksAttr = null;
                $fks = null;
                $indiceId = 0;
                $cabecalho = "";

                while ($i == $TOT_IT)
                {
                    $i = 0;
                    $dbAntigo->query($q . " LIMIT $iLimit, $TOT_IT");
                    $iLimit += $TOT_IT;
                    if (mysqli_num_rows($dbAntigo->result) > 0)
                    {
//                    mysqli_data_seek($dbAntigo->result, 0);

                        if ($totalColunas < 0)
                        {
                            $totalColunas = mysqli_num_fields($dbAntigo->result);
                        }
//                    $insert = "( ";

                        if ($indicesFksAttr == null)
                        {
                            $cabecalho = "INSERT INTO {$objTabela->nome} (";
                            $indicesFksAttr = array();
                            $fks = array();
//                            atributosFks
//                            $attr->id = $obj[2];
//                            $attr->nome = $obj[3];
//                            $attr->idTabelaFk = $obj[4];
                            $numFields = mysqli_num_fields($dbAntigo->result);
                            $fields = $dbAntigo->result->fetch_fields();

                            HelperLog::logTemp("Total Fields: " . print_r($numFields, true), true);
                            HelperLog::logTemp("Fields: " . print_r($fields, true), true);

                            for ($k = 0; $k < $numFields; $k++)
                            {
                                $field_info = $fields [ $k ];
                                if ($field_info->name == "id")
                                {
                                    $indiceId = $k;
                                }
                                if ($k > 0)
                                {
                                    $cabecalho .= ", ";
                                }
                                $cabecalho .= $field_info->name;
                                foreach ($objTabela->atributosFks as $attrFk)
                                {
                                    if ($attrFk->nome == $field_info->name)
                                    {
                                        $indicesFksAttr[ count($indicesFksAttr) ] = $k;
                                        $fks[ count($fks) ] = $attrFk;
                                        break;
                                    }
                                }
                            }
                            $cabecalho .= ") VALUES ";
                            HelperLog::logTemp("indicesFksAttr: " . print_r($indicesFksAttr, true), true);
                            HelperLog::logTemp("Fks: " . print_r($fks, true), true);
                        }
                        $insert = "";
                        $inserts = array();
                        $idsAntigos = array();

                        $insertDipara = "";
                        $insertsDipara = array();
                        while ($registro = mysqli_fetch_array($dbAntigo->result, MYSQL_NUM))
                        {
                            $iIndiceFks = 0;
                            if (!empty($insert))
                            {
                                $insert .= ", ";
                                $insertDipara .= ", ";
                            }
                            $qInsert = "( ";
                            $idAntigo = null;
                            for ($j = 0; $j < $totalColunas; $j++)
                            {
                                if ($j > 0)
                                {
                                    $qInsert .= ", ";
                                }

                                if ($j == $indiceId)
                                {
                                    $idNovo = $idIniGerado++;
                                    $qInsert .= $idNovo;

                                    $idAntigo = $registro[ $j ];
                                    $idsAntigos[] = $idAntigo;
                                    //id_sistema_tabela_INT, id_antigo_INT, id_novo_INT, assinatura_migracao_id_INT
                                    $temp2 = "({$objTabela->id}, $idAntigo, $idNovo, {$objAM->getId()})";
                                    $insertDipara .= $temp2;
                                    $insertsDipara[] = $temp2;
                                    continue;
                                }

                                if (!isset($registro[ $j ]))
                                {
                                    $qInsert .= "null ";
                                }
                                else
                                {
                                    if (!empty($indicesFksAttr)
                                        && $iIndiceFks < count($indicesFksAttr)
                                        && $indicesFksAttr[ $iIndiceFks ] == $j)
                                    {
                                        if (isset($idTabelaPorLru[ $fks[ $iIndiceFks ]->idTabelaFk ]))
                                        {
                                            $idNovo = $idTabelaPorLru[ $fks[ $iIndiceFks ]->idTabelaFk ]->get($registro[ $j ]);
                                        }
                                        if ($idNovo != null)
                                        {
                                            $q2 = "SELECT id_novo_INT"
                                                . " FROM assinatura_migracao_dipara"
                                                . " WHERE id_antigo_INT = {$registro[$j]}"
                                                . " AND id_sistema_tabela_INT = {$fks[$iIndiceFks]->idTabelaFk}";
                                            HelperLog::logTemp($q2, true);

                                            $db->query($q2);
                                            if ($db->rows() > 0)
                                            {
                                                $idNovo = $db->getPrimeiraTuplaDoResultSet(0);
                                            }
                                        }
                                        if (isset($idNovo))
                                        {
                                            $qInsert .= $idNovo;
                                        }
                                        else
                                        {
                                            $qInsert .= "null ";
                                        }
                                        $iIndiceFks++;
                                    }
                                    else
                                    {
                                        $qInsert .= "'" . $registro[ $j ] . "' ";
                                    }
                                }
                            }
                            $qInsert .= ") ";
                            $inserts[] = $qInsert;
                            $insert .= $qInsert;
                        }

                        $qDipara = $cabecalhoDipara . $insertDipara;
                        HelperLog::logTemp($qDipara, true);
                        try
                        {
                            $db->query($qDipara);
                        }
                        catch (QueryException $ex)
                        {
                            $codigo = $ex->getCode();
                            if ($codigo == 1062)
                            {
                                HelperLog::logTemp("Tratamento $codigo: " . print_r($insertsDipara, true), true);
                                foreach ($insertsDipara as $temp5)
                                {
                                    try
                                    {
                                        $db->query($temp5);
                                    }
                                    catch (QueryException $ex)
                                    {
                                        if ($codigo != 1062)
                                        {
                                            throw $ex;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                throw $ex;
                            }
                        }
                        $qFinal = $cabecalho . $insert;
                        HelperLog::logTemp($qFinal, true);
                        try
                        {
                            $dbNovo->query($qFinal);
                            $idLast = $dbNovo->getLastInsertId();
                            if (!isset($idTabelaPorLru[ $objTabela->id ]))
                            {
                                $idTabelaPorLru[ $objTabela->id ] = new CacheLRU(10);
                            }
                            $idTabelaPorLru[ $objTabela->id ]->set($idAntigo, $idLast);
                        }
                        catch (QueryException $exc)
                        {
                            HelperLog::logException($exc, true);
                            $codigo = $exc->getCode();
                            if ($codigo == 1064 || $codigo == 1062)
                            {
                                HelperLog::logTemp("Tratamento: " . print_r($idsAntigos, true), true);
                                for ($k = 0; $k < count($idsAntigos); $k++)
                                {
                                    $idAntigo = $idsAntigos[ $k ];
                                    $sql = $inserts[ $k ];
                                    try
                                    {
                                        $dbNovo->query($cabecalho . $sql);
                                        if (!isset($idTabelaPorLru[ $objTabela->id ]))
                                        {
                                            $idTabelaPorLru[ $objTabela->id ] = new CacheLRU(10);
                                        }
                                        $idTabelaPorLru[ $objTabela->id ]->set($idAntigo, $idLast);
                                    }
                                    catch (QueryException $exc)
                                    {
                                        $codigo = $exc->getCode();
                                        if ($codigo != 1064 && $codigo != 1062)
                                        {
                                            throw $exc;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                throw $exc;
                            }
                        }
                    }
                }
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                null,
                $idUltimaSincronizacao);
        }

        private function findObjTabela(&$arvore, $tabela)
        {
            foreach ($arvore as $objTabela)
            {
                if ($objTabela->nome == $tabela)
                {
                    return $objTabela;
                }
            }

            return null;
        }
        //Migra a assinatura de uma assinatura free (banco compartilhado entre varias corporacoes) para um banco pago(banco
        // com uma unica corporacao)
        public function migrarSistemaOmegaEmpresa(
            $idNovaAssinatura,
            $idAssinaturaAntiga,
            EXTDAO_Assinatura_migracao $objAM,
            Database $db,
            $idCorporacao)
        {
            $objAssinaturaAntiga = new EXTDAO_Assinatura($db);
            $objAssinaturaAntiga->select($idAssinaturaAntiga);
            $objAssinaturaNova = new EXTDAO_Assinatura($db);
            $objAssinaturaNova->select($idNovaAssinatura);

            $arvore = BO_BLNUV::getArvoreBancoOmegaEmpresa($objAM->getFkObjNovo_sistema()->getSistema_biblioteca_nuvem_INT());

            $antigasHospedagensDb = EXTDAO_Assinatura::getConfiguracoesDeTodosOsBancosDaAssinatura(
                $idAssinaturaAntiga, $db);

            $dadosConfigDatabaseSincronizador = $antigasHospedagensDb[ EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR ];

            $dbSincronizador = new Database($dadosConfigDatabaseSincronizador);

            $msg = $dbSincronizador->queryMensagem("SELECT MAX(id) FROM sincronizacao WHERE corporacao_id_INT = $idCorporacao ");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $idUltimaSincronizacao = Helper::getResultSetToPrimeiroResultado($dbSincronizador->result);

            $novasHospedagensDb = EXTDAO_Assinatura::getConfiguracoesDeTodosOsBancosDaAssinatura(
                $idNovaAssinatura, $db);

            foreach ($antigasHospedagensDb as $tipo => $configHospdegamDb)
            {
                $dbNovo = new Database($novasHospedagensDb[ $tipo ]);
                $dbNovo->query("SET FOREIGN_KEY_CHECKS = 0");
                $dbAntigo = new Database($configHospdegamDb);
                $bancoPrincipal = $configHospdegamDb ["tipo"] == EXTDAO_Tipo_hospedagem_db::PRINCIPAL;

                $tabelasNovo = $dbNovo->getListaDosNomesDasTabelas();
                $tabelasAntigo = $dbAntigo->getListaDosNomesDasTabelas();
                $TOT_IT = 1000;

                foreach ($tabelasAntigo as $tabela)
                {
                    $indicesFksAttr = null;
                    $cabecalho = "";
                    $totalColunas = -1;
                    $where = "";
                    $join = "";
                    if ($dbAntigo->existeAtributo($tabela, "corporacao_id_INT"))
                    {
                        $where .= " WHERE t.corporacao_id_INT = $idCorporacao";
                    }
                    else
                    {
                        if ($tabela == "corporacao")
                        {
                            $where .= " WHERE t.id = $idCorporacao";
                        }
                        else
                        {
                            if ($bancoPrincipal && $tabela == "usuario")
                            {
                                $join .= " JOIN usuario_corporacao uc ON t.id = uc.usuario_id_INT ";
                                $where .= " WHERE uc.corporacao_id_INT = $idCorporacao";
                            }
                            else
                            {
                                if (!in_array($tabela, $tabelasNovo))
                                {
                                    throw new Exception ("Tabela $tabela n?o encontrada"
                                                         . " no novo banco: {$dbNovo->getIdentificador()}."
                                                         . " Durante a migracao do banco {$dbAntigo->getIdentificador()}");
                                }
                                else
                                {
                                    HelperLog::logTemp("Tabela constante, logo não é migrada: {$tabela}", true);
                                    continue;
                                }
                            }
                        }
                    }
                    $q = "SELECT t.*"
                        . " FROM $tabela t $join "
                        . " $where ";
                    $iLimit = 0;
                    $i = $TOT_IT;
                    $objTabela = $this->findObjTabela($arvore, $tabela);
                    while ($i == $TOT_IT)
                    {
                        $i = 0;
                        $dbAntigo->query($q . " LIMIT $iLimit, $TOT_IT");
                        $iLimit += $TOT_IT;
                        if (mysqli_num_rows($dbAntigo->result) > 0)
                        {
                            if ($totalColunas < 0)
                            {
                                $totalColunas = mysqli_num_fields($dbAntigo->result);
                            }

                            if ($indicesFksAttr == null)
                            {
                                $cabecalho = "INSERT INTO {$tabela} (";
                                $indicesFksAttr = array();
                                $fks = array();
                                $numFields = mysqli_num_fields($dbAntigo->result);
                                $fields = $dbAntigo->result->fetch_fields();

                                HelperLog::logTemp("Total Fields: " . print_r($numFields, true), true);
                                HelperLog::logTemp("Fields: " . print_r($fields, true), true);

                                for ($k = 0; $k < $numFields; $k++)
                                {
                                    $field_info = $fields [ $k ];
                                    if ($field_info->name == "id")
                                    {
                                        $indiceId = $k;
                                    }
                                    if ($k > 0)
                                    {
                                        $cabecalho .= ", ";
                                    }
                                    $cabecalho .= $field_info->name;
                                    foreach ($objTabela->atributosFks as $attrFk)
                                    {
                                        if ($attrFk->nome == $field_info->name)
                                        {
                                            $indicesFksAttr[ count($indicesFksAttr) ] = $k;
                                            $fks[ count($fks) ] = $attrFk;
                                            break;
                                        }
                                    }
                                }
                                $cabecalho .= ") VALUES ";
                            }
                            $inserts = array();
                            $insert = "";
                            for (;
                                $registro = mysqli_fetch_array($dbAntigo->result, MYSQL_NUM);
                                $i++)
                            {
                                if ($i > 0)
                                {
                                    $insert .= ", ";
                                }
                                $auxInsert = "( ";
                                for ($j = 0; $j < $totalColunas; $j++)
                                {
                                    if ($j > 0)
                                    {
                                        $auxInsert .= ", ";
                                    }
                                    if (!isset($registro[ $j ]))
                                    {
                                        $auxInsert .= "null ";
                                    }
                                    else
                                    {
                                        $auxInsert .= "'{$registro[$j]}'";
                                    }
                                }
                                $auxInsert .= ") ";
                                $inserts[] = $auxInsert;
                                $insert .= $auxInsert;
                            }

                            $msg = $dbNovo->queryMensagem($cabecalho . $insert);
                            if ($msg != null && $msg->erro())
                            {
                                if ($msg->erroConsultaSql()
                                    && $msg->mValor == Database::CHAVE_DUPLICADA)
                                {
                                    foreach ($inserts as $sqlI)
                                    {
                                        $msg = $dbNovo->queryMensagem($cabecalho . $sqlI);
                                        if ($msg != null
                                            && $msg->erro())
                                        {
                                            if ($msg->erroConsultaSql()
                                                && $msg->mValor == Database::CHAVE_DUPLICADA)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                return $msg;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    return $msg;
                                }
                            }
                        }
                    }
                }
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                null,
                $idUltimaSincronizacao);
        }

        public function getHospedagensDoUsuario($idAssinatura, $idCorporacao, $corporacao)
        {
            $db = new Database();
            $obj = new EXTDAO_Assinatura($db);

            $msg = $obj->select($idAssinatura);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $totalDeUsuarios = BO_Web_service_hospedagem::getTotalDeUsuariosDaHospedagem(
                $idCorporacao, $corporacao, $obj->getHospedagem_id_INT(), $db);

            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $totalDeUsuarios);
        }

        public function getEspacoEmMBOcupadoPelaAssinatura($idAssinatura)
        {
            $obj = new EXTDAO_Assinatura();
            $msg = $obj->select($idAssinatura);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $espacoBanco = BO_Web_service_hospedagem::getEspacoOcupadoPelaBancoDeDados($obj->getHospedagem_id_INT());

            $espacoDosArquivos = $obj->getFkObjHospedagem()->getEspacoOcupadoPelosArquivosDaHospedagemEmMB();

            $espacoTotal = 0;
            if (is_numeric($espacoBanco))
            {
                $espacoTotal += $espacoBanco;
            }

            if (is_numeric($espacoDosArquivos))
            {
                $espacoTotal += $espacoDosArquivos;
            }

            return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $espacoTotal);
        }

        public function getConfiguracoesDeBancoDaAssinatura($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $configuracoesBanco = EXTDAO_Assinatura::getConfiguracoesDeBancoDaAssinatura($idAssinatura);

                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $configuracoesBanco);
                }

                return new Mensagem_token(

                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getConfiguracoesDeBancoDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )

                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function getDominioDaAssinatura($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $msg = EXTDAO_Assinatura::getDominioDaAssinatura($idAssinatura);
                    if ($msg->resultadoVazio())
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Assinatura indefinida: $idAssinatura");
                    }

                    return $msg;
                }

                return new Mensagem_token(

                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getDominioDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )

                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function getDominioWebServiceDaAssinatura($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $msg = EXTDAO_Assinatura::getDominioDoWebServiceDaAssinatura($idAssinatura);
                    if ($msg->resultadoVazio())
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Assinatura indefinida: $idAssinatura");
                    }

                    return $msg;
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getDominioWebServiceDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )
                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function getDominioDoSincronizadorWeb($idAssinatura)
        {
            try
            {
                if (strlen($idAssinatura))
                {
                    $msg = EXTDAO_Assinatura::getDominioDoWebServiceDoSincronizadorWebAssinatura($idAssinatura);
                    if ($msg->resultadoVazio())
                    {
                        return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, "Assinatura indefinida: $idAssinatura");
                    }

                    return $msg;
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getDominioWebServiceDaAssinatura",
                            array()
                        ),
                        array($idAssinatura)
                    )
                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function deletarAssinatura($idAssinatura)
        {
            if (strlen($idAssinatura))
            {
                $objAssinatura = new EXTDAO_Assinatura();
                $msg = $objAssinatura->select($idAssinatura);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                else
                {
                    if ($msg != null && $msg->resultadoVazio())
                    {
                        return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                             "[wrgerg]A assinatura $idAssinatura j? n?o existia.");
                    }
                }
                if (strlen($objAssinatura->getExcluido_DATETIME()))
                {
                    return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                         "A assinatura foi deletada com sucesso.");
                }

                if ($objAssinatura->getEstado_assinatura_id_INT() == EXTDAO_Estado_assinatura::DISPONIVEL)
                {
                    return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                         "Assinatura est? dispon?vel novamente.");
                }
                else
                {
                    try
                    {
                        $msg = EXTDAO_Assinatura::deletarAssinatura($idAssinatura);
                        return $msg;
                    }
                    catch (Exception $exc)
                    {
                        HelperLog::logException($exc, true);
                        return new Mensagem(null, null, $exc);
                    }
                }
            }
            else
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    "Parametros inv?lidos."
                );
            }
        }

        public function disponibilizaAssinatura($idAssinatura)
        {
            if (strlen($idAssinatura))
            {
                $objAssinatura = new EXTDAO_Assinatura();
                $objAssinatura->select($idAssinatura);

                if ($objAssinatura->getEstado_assinatura_id_INT() == EXTDAO_Estado_assinatura::DISPONIVEL)
                {
                    return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                         "Assinatura est? dispon?vel novamente.");
                }
                else
                {
                    try
                    {
                        EXTDAO_Assinatura::disponibilizarAssinatura($idAssinatura);

                        return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                             "Assinatura est? dispon?vel novamente.");
                    }
                    catch (Exception $exc)
                    {
                        HelperLog::logException($exc, true);
                        return new Mensagem(null, null, $exc);
                    }
                }
            }
            else
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    "Parametros inv?lidos."
                );
            }
        }

        public function cadastraAssinaturaDoClienteParaSistemaCompartilhadoEBancoCompartilhado(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorDominio,
            Database $db = null,
            $idAssinatura = null)
        {
            if (strlen($idSistema)
                && strlen($idClienteAssinatura)
                && strlen($nomeSite)
                && strlen($identificadorDominio))
            {
                if ($db == null)
                {
                    $db = new Database();
                }
                if ($idAssinatura == null)
                {
                    $idAssinatura = EXTDAO_Assinatura::getIdAssinaturaLivreDoSistema(
                        $idSistema,
                        $db);
                }

                if (!strlen($idAssinatura))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                        "[e45er]N?o existe nenhuma assinatura dispon?vel para hospedagem, tente novamente daqui a 5 minutos. Desculpe o transtorno"
                    );
                }
                else
                {
                    $identificadorDominio = str_replace(' ', "_", $identificadorDominio);
                    $obj = new EXTDAO_Assinatura($db);
                    $obj->select($idAssinatura);
                    $obj->setSicob_cliente_assinatura_INT($idClienteAssinatura);
                    $obj->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA);
                    $idHospedagem = $obj->getHospedagem_id_INT();
                    $objHospedagem = new EXTDAO_Hospedagem($db);
                    $objHospedagem->select($idHospedagem);

                    $objSistema = new EXTDAO_Sistema($db);
                    $objSistema->select($idSistema);
                    $pathRaizAssinatura = $objSistema->getPath_raiz_assinatura();
                    $dominio = $objSistema->getDominio_raiz_assinatura();
                    $dominioSincronizador = $objSistema->getDominio_raiz_sincronizador();
                    $dominioDaAssinatura = Helper::getPathComBarra($dominio);

                    $objHospedagem->setDominio_sincronizador($dominioSincronizador);
                    $objHospedagem->setDominio($dominioDaAssinatura);

                    $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());

                    $pathHospedagemSincronizador = Helper::formatarPath($objHospedagem->getPath_sincronizador());

                    $idHDBPrincipal = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::PRINCIPAL,
                        $db);

                    $idHDBParametros = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::PARAMETROS,
                        $db);

                    $idHDBSincronizador = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR,
                        $db);

                    $objHDBPrincipal = new EXTDAO_Hospedagem_db($db);
                    $objHDBPrincipal->select($idHDBPrincipal);

                    $objHDBParametros = new EXTDAO_Hospedagem_db($db);
                    $objHDBParametros->select($idHDBParametros);
//                         echo "passou: $idHDBSincronizador"; 
                    $objHDBSincronizador = new EXTDAO_Hospedagem_db($db);
                    $objHDBSincronizador->select($idHDBSincronizador);

                    $databaseConfig = $obj->getStringArquivoDatabaseConfigFactu(
                        $nomeSite,
                        $objHDBPrincipal,
                        $objHDBParametros,
                        $identificadorDominio,
                        $objHDBSincronizador);

                    $databaseSincronizador = $obj->getStringArquivoDatabaseConfigSincronizador(
                        $nomeSite,
                        $objHDBPrincipal,
                        $objHDBSincronizador);

                    $pathDatabaseConfigSincronizador = $pathHospedagemSincronizador
                        . "/recursos/database_config_corporacao/";
                    if (!file_exists($pathDatabaseConfigSincronizador))
                    {
                        Helper::mkdir($pathDatabaseConfigSincronizador, 0777, true);
                    }
                    $pathDatabaseSincronizador = $pathDatabaseConfigSincronizador
                        . "database_config_$identificadorDominio.php";
                    Helper::criaArquivoComOConteudo($pathDatabaseSincronizador, $databaseSincronizador);

                    $classeDatabaseSincronizador = $obj->getStringArquivoObjetoDatabaseConfigSincronizador(
                        $identificadorDominio,
                        $nomeSite,
                        $objHDBPrincipal,
                        $objHDBSincronizador);

                    $pathPhpCorporacaoSincronizador = $pathHospedagemSincronizador
                        . "recursos/php_corporacao/";
                    if (!file_exists($pathPhpCorporacaoSincronizador))
                    {
                        Helper::mkdir($pathPhpCorporacaoSincronizador, 0777, true);
                    }

                    $nomClasse = $obj->getNomeClasseConstantDatabaseConfigCorporacao($identificadorDominio);
                    $pathDatabaseSincronizador = $pathPhpCorporacaoSincronizador
                        . "$nomClasse.php";
                    Helper::criaArquivoComOConteudo($pathDatabaseSincronizador, $classeDatabaseSincronizador);

                    $pathPhpCorporacao = $pathHospedagem . "recursos/database_config_corporacao/";
                    if (!file_exists($pathPhpCorporacao))
                    {
                        Helper::mkdir($pathPhpCorporacao, 0777, true);
                    }
                    $pathDatabaseConfig = $pathPhpCorporacao
                        . "database_config_$identificadorDominio.php";
                    Helper::criaArquivoComOConteudo($pathDatabaseConfig, $databaseConfig);

                    $objHospedagem->setDominio_webservice(Helper::getPathComBarra($dominioDaAssinatura) . "adm/webservice.php?class=Servicos_web_sihop&action=");

                    $objHospedagem->formatarParaSQL();
                    $objHospedagem->update($idHospedagem);

                    $obj->setNome_site($nomeSite);
                    $obj->formatarParaSQL();
                    $obj->update($idAssinatura);

                    return new Mensagem_token(
                        PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                        null,
                        $idAssinatura);
                }
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "criaHospedagem",
                        array($idSistema, $idClienteAssinatura, $nomeSite, $identificadorDominio)
                    ),
                    array()
                )
            );
        }

        public function cadastraAssinaturaDoClienteParaSistemaCompartilhado(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorDominio,
            $idCorporacao)
        {
            if (strlen($idSistema)
                && strlen($idClienteAssinatura)
                && strlen($nomeSite)
                && strlen($identificadorDominio))
            {
                $idAssinatura = EXTDAO_Assinatura::getIdAssinaturaLivreDoSistema($idSistema);

                if (!strlen($idAssinatura))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                        "[defg4]N?o existe nenhuma assinatura dispon?vel para hospedagem, tente novamente daqui a 5 minutos. Desculpe o transtorno"
                    );
                }
                else
                {
                    $identificadorDominio = str_replace(' ', "_", $identificadorDominio);
                    $obj = new EXTDAO_Assinatura();
                    $obj->select($idAssinatura);
                    $obj->setSicob_cliente_assinatura_INT($idClienteAssinatura);
                    $obj->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA);
                    $obj->setId_corporacao_INT($idCorporacao);
                    $idHospedagem = $obj->getHospedagem_id_INT();
                    $objHospedagem = new EXTDAO_Hospedagem();
                    $objHospedagem->select($idHospedagem);

                    $objSistema = new EXTDAO_Sistema();
                    $objSistema->select($idSistema);
                    $pathRaizAssinatura = $objSistema->getPath_raiz_assinatura();
                    $dominio = $objSistema->getDominio_raiz_assinatura();
                    $dominioSincronizador = $objSistema->getDominio_raiz_sincronizador();
                    $dominioDaAssinatura = Helper::getPathComBarra($dominio);

                    $objHospedagem->setDominio_sincronizador($dominioSincronizador);
                    $objHospedagem->setDominio($dominioDaAssinatura);

                    $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());

                    $pathHospedagemSincronizador = Helper::formatarPath($objHospedagem->getPath_sincronizador());

                    $idHDBPrincipal = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::PRINCIPAL);

                    $idHDBParametros = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::PARAMETROS);

                    $idHDBSincronizador = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR);

                    $objHDBPrincipal = new EXTDAO_Hospedagem_db();
                    $objHDBPrincipal->select($idHDBPrincipal);

                    $objHDBParametros = new EXTDAO_Hospedagem_db();
                    $objHDBParametros->select($idHDBParametros);

                    $objHDBSincronizador = new EXTDAO_Hospedagem_db();
                    $objHDBSincronizador->select($idHDBSincronizador);

                    $databaseConfig = $obj->getStringArquivoDatabaseConfigFactu(
                        $nomeSite,
                        $objHDBPrincipal,
                        $objHDBParametros,
                        $identificadorDominio,
                        $objHDBSincronizador);

                    $pathDatabaseConfigSincronizador = $pathHospedagemSincronizador . "/recursos/database_config_corporacao/";
                    if (!file_exists($pathDatabaseConfigSincronizador))
                    {
                        Helper::mkdir($pathDatabaseConfigSincronizador, 0777, true);
                    }

                    $classeDatabaseSincronizador = $obj->getStringArquivoObjetoDatabaseConfigSincronizador(
                        $identificadorDominio,
                        $nomeSite,
                        $objHDBPrincipal,
                        $objHDBSincronizador);

                    $pathPhpCorporacaoSincronizador = $pathHospedagemSincronizador . "/recursos/php_corporacao/";
                    if (!file_exists($pathPhpCorporacaoSincronizador))
                    {
                        Helper::mkdir($pathPhpCorporacaoSincronizador, 0777, true);
                    }

                    $nomClasse = $obj->getNomeClasseConstantDatabaseConfigCorporacao($identificadorDominio);

                    $pathDatabaseSincronizador = $pathPhpCorporacaoSincronizador . "/$nomClasse.php";

                    Helper::criaArquivoComOConteudo($pathDatabaseSincronizador, $classeDatabaseSincronizador);

                    $pathPhpCorporacao = $pathHospedagem . "/recursos/database_config_corporacao/";
                    if (!file_exists($pathPhpCorporacao))
                    {
                        Helper::mkdir($pathPhpCorporacao, 0777, true);
                    }
                    $pathDatabaseConfig = $pathPhpCorporacao . "/database_config_{$identificadorDominio}.php";
                    Helper::criaArquivoComOConteudo($pathDatabaseConfig, $databaseConfig);

                    $objHospedagem->setDominio_webservice(Helper::getPathComBarra($dominioDaAssinatura) . "adm/webservice.php?class=Servicos_web_sihop&action=");

                    $objHospedagem->formatarParaSQL();
                    $objHospedagem->update($idHospedagem);

                    $obj->setNome_site($nomeSite);
                    $obj->formatarParaSQL();
                    $obj->update($idAssinatura);

                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                              null, $idAssinatura);
                }
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "criaHospedagem",
                        array($idSistema, $idClienteAssinatura, $nomeSite, $identificadorDominio)
                    ),
                    array()
                )
            );
        }

        public function cadastraAssinaturaDoCliente(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorCorporacao,
            $idCorporacao)
        {
            try
            {
                $objSistema = new EXTDAO_Sistema();
                $objSistema->select($idSistema);

                switch ($objSistema->getSistema_tipo_id_INT())
                {
                    case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA :
                    case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS:
                        return $this->cadastraAssinaturaDoClienteParaSistemaCompartilhado(
                            $idSistema,
                            $idClienteAssinatura,
                            $nomeSite,
                            $identificadorCorporacao,
                            $idCorporacao);

                    case EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                        return $this->cadastraAssinaturaDoClienteParaSistemaIsolado(
                            $idSistema,
                            $idClienteAssinatura,
                            $nomeSite,
                            $identificadorCorporacao,
                            $idCorporacao);

                    default:
                        throw new Exception("Tipo de sistema não mapeado", "0", null);

                }
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function cadastraAssinaturaDoClienteParaSistemaIsolado(
            $idSistema,
            $idClienteAssinatura,
            $nomeSite,
            $identificadorDominio,
            $idCorporacao)
        {
            if (strlen($idSistema)
                && strlen($idClienteAssinatura)
                && strlen($nomeSite)
                && strlen($identificadorDominio))
            {
                $idAssinatura = EXTDAO_Assinatura::getIdAssinaturaLivreDoSistema($idSistema);
                $identificadorDominio = str_replace(' ', "_", $identificadorDominio);
                if (!strlen($idAssinatura))
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                        "[gert54]N?o existe nenhuma assinatura dispon?vel para hospedagem, tente novamente daqui a 5 minutos. Desculpe o transtorno"
                    );
                }
                else
                {
                    $obj = new EXTDAO_Assinatura();
                    $obj->select($idAssinatura);
                    $obj->setSicob_cliente_assinatura_INT($idClienteAssinatura);
                    $obj->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA);
                    $obj->setId_corporacao_INT($idCorporacao);
                    $idHospedagem = $obj->getHospedagem_id_INT();
                    $objHospedagem = new EXTDAO_Hospedagem();
                    $objHospedagem->select($idHospedagem);

                    $objSistema = new EXTDAO_Sistema();
                    $objSistema->select($idSistema);
                    $pathRaizAssinatura = $objSistema->getPath_raiz_assinatura();
                    $dominio = $objSistema->getDominio_raiz_assinatura();

                    $dominioDaAssinatura = Helper::getPathComBarra($dominio) . $identificadorDominio . "/";

                    $objHospedagem->setDominio($dominioDaAssinatura);

                    $pathHospedagemAntiga = Helper::formatarPath($objHospedagem->getPath());
                    $pathHospedagem = Helper::formatarPath(Helper::getPathComBarra($pathRaizAssinatura) . $identificadorDominio . "\\");

                    $idHDBPrincipal = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::PRINCIPAL);

                    $idHDBParametros = EXTDAO_Hospedagem_db::getIdHospedagemDbDoTipo(
                        $idHospedagem,
                        EXTDAO_Tipo_hospedagem_db::PARAMETROS);

                    $objHDBPrincipal = new EXTDAO_Hospedagem_db();
                    $objHDBPrincipal->select($idHDBPrincipal);

                    $objHDBParametros = new EXTDAO_Hospedagem_db();
                    $objHDBParametros->select($idHDBParametros);

                    $databaseConfig = $obj->getStringArquivoDatabaseConfigFactu(
                        $nomeSite,
                        $objHDBPrincipal,
                        $objHDBParametros,
                        $identificadorDominio);

                    $constants = $obj->getStringArquivoConstantsFactu(
                        $objHospedagem->getId(),
                        $nomeSite,
                        $dominioDaAssinatura,
                        $identificadorDominio);

                    $pathDatabaseConfig = $pathHospedagemAntiga . "recursos/php/database_config.php";
                    Helper::criaArquivoComOConteudo($pathDatabaseConfig, $databaseConfig);

                    $pathDatabaseConstants = $pathHospedagemAntiga . "recursos/php/constants.php";
                    Helper::criaArquivoComOConteudo($pathDatabaseConstants, $constants);

                    $objHospedagem->setDominio_webservice(Helper::getPathComBarra($dominioDaAssinatura) . "adm/webservice.php?class=Servicos_web_sihop&action=");
                    if (file_exists($pathHospedagem))
                    {
                        Helper::deletaDiretorio($pathHospedagem);
                    }
                    if (!Helper::renomearDiretorio($pathHospedagemAntiga, $pathHospedagem))
                    {
                        return new Mensagem_token(
                            PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                            "Ocorreu um erro durante a execu??o, tente novamente mais tarde. Mas j? estamos analisando o problema, desculpe o transtorno.");
                    }
                    else
                    {
                        $objHospedagem->setPath($pathHospedagem);
                        $objHospedagem->formatarParaSQL();
                        $objHospedagem->update($idHospedagem);

                        $obj->setNome_site($nomeSite);
                        $obj->formatarParaSQL();
                        $obj->update($idAssinatura);

                        return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                                  null, $idAssinatura);
                    }
                }
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "criaHospedagem",
                        array($idSistema, $idClienteAssinatura, $nomeSite, $identificadorDominio)
                    ),
                    array()
                )
            );
        }

        public function criaNovaAssinatura($idSistema)
        {
            try
            {
                if (strlen($idSistema))
                {
                    $idNovaAssinatura = EXTDAO_Assinatura::criaNovaAssinaturas($idSistema);

                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                              null,
                                              $idNovaAssinatura);
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "criaHospedagem",
                            array()
                        ),
                        array($idSistema)
                    )
                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function criaNovasAssinaturasDeTodosOsSistemas()
        {
            try
            {
                $sistemas = EXTDAO_Sistema::getSistemas();
                for ($i = 0; $i < count($sistemas); $i++)
                {
                    $idSistema = $sistemas[ $i ];
                    $objSistema = new EXTDAO_Sistema();
                    $objSistema->select($idSistema);

                    $numeroAssinaturas = EXTDAO_Assinatura::getNumeroDeAssinaturaLivreDoSistema($idSistema);
                    if ($numeroAssinaturas != null)
                    {
                        for ($j = $numeroAssinaturas; $j < 100; $j++)
                        {
                            $this->criaNovaAssinatura($idSistema);

                            return new Mensagem(
                                PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                null,
                                null);
                        }
                    }
                }

                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, null);
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function atualizaAssinaturaDoCliente(
            $idAssinatura,
            $idClienteAssinaturaAntigo,
            $idClienteAssinaturaNovo,
            $idSistemaAntigo,
            $idSistemaNovo)
        {
            if (strlen($idAssinatura)
                && strlen($idClienteAssinaturaAntigo)
                && strlen($idClienteAssinaturaNovo))
            {
                $db = new Database();
                if ($idSistemaAntigo != $idSistemaNovo)
                {
                    $migrar = false;
                    switch ($idSistemaAntigo)
                    {
                        case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                            if ($idSistemaNovo == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS
                                || $idSistemaNovo == EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
                            {
                                $migrar = true;
                            }

                            break;

                        case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS:
                            if ($idSistemaNovo == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
                            {
                                $migrar = true;
                            }

                            break;

                        case EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                            if ($idSistemaNovo == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
                            {
                                $migrar = true;
                            }

                            break;

                    }
                    if ($migrar)
                    {
                        HelperLog::verbose("Adicionando pedido de migração de sistema", true);

                        HelperLog::verbose("Cancelando migracoes pendentes se existente...", true);
                        if(EXTDAO_Assinatura_migracao::existeMigracaoEmAndamento($idAssinatura, $db)){
                            return new Mensagem(
                                PROTOCOLO_SISTEMA::ACESSO_NEGADO,
                                "Já existe uma migração em andamento do seu grupo. Aguarde o término");
                        }
                        $dados = EXTDAO_Assinatura_migracao::getDadosDoUltimoSistemaPendenteParaMigracaoDaAssinatura(
                            $idAssinatura, $db);
                        if($dados  == null
                        || $dados != null && $dados[1] != $idSistemaAntigo || $dados[2] != $idSistemaNovo){
                            $msg = EXTDAO_Assinatura_migracao::cancelaTodaAssinaturaPendente(
                                $idAssinatura, $db);
                            if(Interface_mensagem::checkErro($msg)){
                                return $msg;
                            }

                            $objM = new EXTDAO_Assinatura_migracao($db);
                            $objM->setEstado_assinatura_migracao_id_INT(EXTDAO_Estado_assinatura_migracao::AGUARDANDO_MIGRACAO);
                            $objM->setAntigo_sicob_cliente_assinatura_INT($idClienteAssinaturaAntigo);
                            $objM->setAntigo_sistema_id_INT($idSistemaAntigo);
                            $objM->setAssinatura_id_INT($idAssinatura);
                            $objM->setData_cadastro_DATETIME(Helper::getDiaEHoraAtualSQL());
                            $objM->setNovo_sicob_cliente_assinatura_INT($idClienteAssinaturaNovo);
                            $objM->setNovo_sistema_id_INT($idSistemaNovo);
                            $objM->formatarParaSQL();
                            $objM->insert();

                            $idMigracao = $objM->getUltimoIdInserido();
                            HelperLog::verbose("Requisicao de migração $idMigracao empilhada!", true);
                        } else {
                            HelperLog::verbose("Já existia a requisicao de migracao {$dados[0]} empilhada!", true);
                        }
                    }
                    else
                    {
                        HelperLog::verbose("[1]Não será necessário realizar migração. Id Sistema Antigo: $idSistemaAntigo. Id Sistema Novo: $idSistemaNovo.", true);
                    }
                }
                else
                {
                    HelperLog::verbose("[2]N?o ser? necess?rio realizar migra??o. Id Sistema Antigo: $idSistemaAntigo. Id Sistema Novo: $idSistemaNovo.", true);
                }

                $obj = new EXTDAO_Assinatura();
                $obj->select($idAssinatura);
                $obj->setSicob_cliente_assinatura_INT($idClienteAssinaturaNovo);
                $obj->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA);

                $obj->formatarParaSQL();
                $obj->update($idAssinatura);

                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $obj->getId());
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "atualizaAssinaturaDoCliente",
                        array($idAssinatura, $idClienteAssinaturaAntigo, $idClienteAssinaturaNovo)
                    ),
                    array()
                )
            );
        }

        public function disponibilizarProximaAssinaturaMigrada($db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $msg = EXTDAO_Assinatura_migracao::getProximoIdParaDisponibilizar($db);
            if (!$msg->ok())
            {
                return $msg;
            }

            $idAM = $msg->mValor;

            return $this->disponibilizaAssinaturaMigracao($idAM, $db);
        }

        public function disponibilizaAssinaturaMigracao($idAM, Database $db)
        {
            $boAssinatura = new BO_Assinatura();
            $objAM = new EXTDAO_Assinatura_migracao($db);
            $objAM->select($idAM);
            HelperLog::verbose("Id Assinatura Migracao: $idAM. disponibilizarProximaAssinaturaMigrada - $idAM id_assinatura_migracao", true);
            $idAssinatura = $objAM->getAssinatura_id_INT();

            $objAssinaturaAntiga = new EXTDAO_Assinatura($db);
            $objAssinaturaAntiga->select($idAssinatura);

            $idCorporacao = $objAM->getId_corporacao_INT();
            HelperLog::logDisponibilizaHospedagem("Disponibilizando assinatura da corporacao: $idCorporacao", true);
            try
            {
                $msg = null;
                try
                {
                    //BANCO INDIVIDUAL
                    if ($objAM->getAntigo_sistema_id_INT() == EXTDAO_Sistema::OMEGA_EMPRESA)
                    {
                        $msg = $this->disponibilizarSistemaOmegaEmpresa(
                            $idAssinatura,
                            $db);
                        if ($msg != null && $msg->erro())
                        {
                            return $this->finalizaDisponibilizacaoComErro(
                                $msg,
                                $objAM
                            );
                        }
                    }
                    else
                    {
                        if ($objAM->getAntigo_sistema_id_INT() == EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO)
                        {
                            $msg = $this->disponibilizarOmegaEmpresaCorporacao(
                                $idAssinatura,
                                $db,
                                $idCorporacao);

                            if ($msg != null && $msg->erro())
                            {
                                return $this->finalizaDisponibilizacaoComErro(
                                    $msg,
                                    $objAM
                                );
                            }

                            $msg = $boAssinatura->finalizaDisponibilizacaoAssinatura(
                                $idAssinatura,
                                $db);
                            if ($msg != null && $msg->erro())
                            {
                                return $this->finalizaDisponibilizacaoComErro(
                                    $msg,
                                    $objAM
                                );
                            }
                        }
                        else
                        {
                            throw new Exception("Sistema não programado: " . $objAM->getNovo_sistema_id_INT(), true);
                        }
                    }
                }
                catch (Exception $ex)
                {
                    $msg = new Mensagem(null, null, $ex);

                    return $this->finalizaDisponibilizacaoComErro(
                        $msg,
                        $objAM
                    );
                }

                $objAM->setData_fim_disponibilizacao_assinatura_DATETIME(Helper::getDiaEHoraAtualSQL());
                $objAM->setEstado_assinatura_migracao_id_INT(EXTDAO_Estado_assinatura_migracao::ASSINATURA_DISPONIBILIZADA);
                $objAM->formatarParaSQL();
                $msg = $objAM->update($objAM->getId());
                if ($msg != null && $msg->erro())
                {
                    return $this->finalizaDisponibilizacaoComErro(
                        $msg,
                        $objAM
                    );
                }
                HelperLog::logDisponibilizaHospedagem("Disponibilizacao realizada com sucesso.", true);

                return new Mensagem(
                    null,
                    "Disponibilizacao realizada com sucesso da assinatura $idAssinatura ");
            }
            catch (Exception $exc)
            {
                throw $exc;
            }
        }

        public function disponibilizarSistemaOmegaEmpresa(
            $idAssinaturaAntiga,
            Database $db)
        {
            HelperLog::logDisponibilizaHospedagem("disponibilizarSistemaOmegaEmpresa: $idAssinaturaAntiga", true);

            $objAssinaturaAntiga = new EXTDAO_Assinatura($db);
            $objAssinaturaAntiga->select($idAssinaturaAntiga);

            $antigasHospedagensDb = EXTDAO_Assinatura::getConfiguracoesDeTodosOsBancosDaAssinatura(
                $idAssinaturaAntiga, $db);

            foreach ($antigasHospedagensDb as $tipo => $configHospdegamDb)
            {
                try
                {
                    $dbNovo = new Database($antigasHospedagensDb[ $tipo ]);
                    $dbNovo->deletaBancoDeDados($dbNovo->getDBName());
                }
                catch (Exception $ex)
                {
                    HelperLog::logDisponibilizaHospedagem(new Mensagem(null, null, $ex), true);
                }
            }
        }

        public function disponibilizarOmegaEmpresaCorporacao(
            $idAssinaturaAntiga,
            Database $db,
            $idCorporacao)
        {
            HelperLog::logDisponibilizaHospedagem("disponibilizarOmegaEmpresaCorporacao: $idAssinaturaAntiga. Id Corporacao: $idCorporacao", true);

            $objAssinaturaAntiga = new EXTDAO_Assinatura($db);
            $objAssinaturaAntiga->select($idAssinaturaAntiga);

            $antigasHospedagensDb = EXTDAO_Assinatura::getConfiguracoesDeTodosOsBancosDaAssinatura(
                $idAssinaturaAntiga, $db);

            foreach ($antigasHospedagensDb as $tipo => $configHospdegamDb)
            {
                $dbNovo = new Database($antigasHospedagensDb[ $tipo ]);
                $dbNovo->query("SET FOREIGN_KEY_CHECKS = 0");
                $dbAntigo = new Database($configHospdegamDb);
                $bancoPrincipal = $configHospdegamDb ["tipo"] == 1;

                $tabelasAntigo = $dbAntigo->getListaDosNomesDasTabelas();

                foreach ($tabelasAntigo as $tabela)
                {
                    if ($tabela == "corporacao")
                    {
                        $dbAntigo->queryMensagem("DELETE FROM corporacao WHERE id = $idCorporacao");
                    }
                    else
                    {
                        if ($tabela == "usuario")
                        {
                            if ($bancoPrincipal)
                            {
                                $dbAntigo->queryMensagem("DELETE
                                FROM
                                    usuario
                                WHERE
                                    (
                                        SELECT
                                            1
                                        FROM
                                            usuario_corporacao uc
                                        WHERE
                                            uc.usuario_id_INT = usuario.id
                                    ) > 0");
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {
                            if (!$dbAntigo->existeAtributo($tabela, "corporacao_id_INT"))
                            {
                                continue;
                            }
                            else
                            {
                                $q = "DELETE FROM $tabela WHERE corporacao_id_INT = $idCorporacao ";
                                $dbAntigo->queryMensagem($q);
                            }

                        }

                    }

                }

            }

        }

    }

<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of BO_Sistema
     *
     * @author home
     */
    class BO_Empresa_hosting
    {

        public static function getIdsSihopAssinaturaParaSincronizacao($idEmpresaHosting)
        {
            if (strlen($idEmpresaHosting))
            {
                $msg = EXTDAO_Empresa_hosting::getIdsSihopAssinaturaParaSincronizacao($idEmpresaHosting);
                return $msg;
            }

            return new Mensagem(
                PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                    "getIdsSihopAssinaturaParaSincronizacao",
                    array($idEmpresaHosting)
                )
            );
        }

    }

<?php

    /**
     * Description of BO_Hospedagem
     *
     * @author home
     */
    class BO_Hospedagem
    {

        public function getDominioDaHospedagem($idHospedagem)
        {
            try
            {
                if (strlen($idHospedagem))
                {
                    $obj = new EXTDAO_Hospedagem();
                    $obj->select($idHospedagem);
                    $dominio = $obj->getDominio();

                    return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $dominio);
                }

                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                    HelperMensagem::GERAL_FALHA_CADASTRO(
                        HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                            "getDominioDaHospedagem",
                            array()
                        ),
                        array($idHospedagem)
                    )
                );
            }
            catch (Exception $exc)
            {
                HelperLog::logException($exc, true);
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    null,
                    $exc);
            }
        }

        public function getConfiguracaoSiteOmegaEmpresaWeb($corporacao, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $q = "SELECT
                    h.path_raiz_conteudo,
                    hd.nome_db,
                    hd.senha_db,
                    hd.host_db,
                    hd.porta_db_INT,
                    hd.usuario_db,
                    hd.tipo_hospedagem_db_id_INT,
                    a.sistema_id_INT,
                    a.id_corporacao_INT
                FROM
                    assinatura a
                JOIN hospedagem h ON a.hospedagem_id_INT = h.id
                JOIN hospedagem_db hd ON h.id = hd.hospedagem_id_INT
                WHERE
                    a.nome_site LIKE '{$corporacao}'
                AND hd.tipo_hospedagem_db_id_INT IN(" . EXTDAO_Tipo_hospedagem_db::PRINCIPAL . ", " . EXTDAO_Tipo_hospedagem_db::PARAMETROS . ")";

            $msg = $db->queryMensagem($q);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }

            $objs = Helper::getResultSetToMatriz($db->result, 0, 1);
            if (!empty($objs))
            {
                $conf = new ConfiguracaoSite();
                $conf->PATH_CONTEUDO = $objs[0][0];
                $conf->TITULO_PAGINAS = "WorkOffline - $corporacao";
                $conf->NOME_CORPORACAO = $corporacao;
                $conf->IDENTIFICADOR_SESSAO = "OMG_{$corporacao}";
                $conf->ID_SISTEMA_SIHOP = $objs[0][7];
                $conf->ID_CORPORACAO = $objs[0][8];
                foreach ($objs as $obj)
                {
                    switch ($obj[6])
                    {
                        case EXTDAO_Tipo_hospedagem_db::PRINCIPAL:
                            $conf->NOME_BANCO_DE_DADOS_PRINCIPAL = $obj[1];
                            $conf->BANCO_DE_DADOS_SENHA = $obj[2];
                            $conf->BANCO_DE_DADOS_HOST = $obj[3];
                            $conf->BANCO_DE_DADOS_PORTA = $obj[4];
                            $conf->BANCO_DE_DADOS_USUARIO = $obj[5];
                            break;

                        case EXTDAO_Tipo_hospedagem_db::PARAMETROS:
                            $conf->NOME_BANCO_DE_DADOS_DE_CONFIGURACAO = $obj[1];
                            $conf->BANCO_DE_DADOS_CONFIGURACAO_SENHA = $obj[2];
                            $conf->BANCO_DE_DADOS_CONFIGURACAO_HOST = $obj[3];
                            $conf->BANCO_DE_DADOS_CONFIGURACAO_PORTA = $obj[4];
                            $conf->BANCO_DE_DADOS_CONFIGURACAO_USUARIO = $obj[5];
                            break;

                        default:
                            break;

                    }

                }

                return new Mensagem_generica($conf);
            }
            else
            {
                return null;
            }
        }

        public function getConfiguracaoSiteSincronizadorWeb($corporacao, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $q = "SELECT
                    h.path_raiz_conteudo,
                    hd.nome_db,
                    hd.senha_db,
                    hd.host_db,
                    hd.porta_db_INT,
                    hd.usuario_db,
                    hd.tipo_hospedagem_db_id_INT,
                    a.sistema_id_INT ,
                    a.id_corporacao_INT
                FROM
                    assinatura a
                JOIN hospedagem h ON a.hospedagem_id_INT = h.id
                JOIN hospedagem_db hd ON h.id = hd.hospedagem_id_INT
                WHERE
                    a.nome_site LIKE '{$corporacao}'
                AND hd.tipo_hospedagem_db_id_INT IN(" . EXTDAO_Tipo_hospedagem_db::PRINCIPAL . ", " . EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR . ")";

            $msg = $db->queryMensagem($q);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }

            $objs = Helper::getResultSetToMatriz($db->result, 0, 1);
            if (!empty($objs))
            {
                $conf = new ConfiguracaoSite();
                $conf->PATH_CONTEUDO = $objs[0][0];
                $conf->TITULO_PAGINAS = "WorkOffline - $corporacao";
                $conf->NOME_CORPORACAO = $corporacao;
                $conf->IDENTIFICADOR_SESSAO = "OMG_{$corporacao}";
                $conf->ID_SISTEMA_SIHOP = $objs[0][7];
                $conf->ID_CORPORACAO = $objs[0][8];
                foreach ($objs as $obj)
                {
                    switch ($obj[6])
                    {
                        case EXTDAO_Tipo_hospedagem_db::PRINCIPAL:
                            $conf->NOME_BANCO_DE_DADOS_SECUNDARIO = $obj[1];
                            $conf->BANCO_DE_DADOS_SECUNDARIO_SENHA = $obj[2];
                            $conf->BANCO_DE_DADOS_SECUNDARIO_HOST = $obj[3];
                            $conf->BANCO_DE_DADOS_SECUNDARIO_PORTA = $obj[4];
                            $conf->BANCO_DE_DADOS_SECUNDARIO_USUARIO = $obj[5];
                            break;

                        case EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR:

                            $conf->NOME_BANCO_DE_DADOS_PRINCIPAL = $obj[1];
                            $conf->BANCO_DE_DADOS_SENHA = $obj[2];
                            $conf->BANCO_DE_DADOS_HOST = $obj[3];
                            $conf->BANCO_DE_DADOS_PORTA = $obj[4];
                            $conf->BANCO_DE_DADOS_USUARIO = $obj[5];
                            break;

                        default:
                            break;

                    }

                }

                return new Mensagem_generica($conf);
            }
            else
            {
                return null;
            }

        }

    }


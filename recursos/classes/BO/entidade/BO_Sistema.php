<?php

    class BO_Sistema
    {
        public function getDominioSistema($idSistema)
        {
            if (strlen($idSistema))
            {
                $objS = new EXTDAO_Sistema();
                $objS->select($idSistema);
                $dominio = $objS->getDominio_raiz_assinatura();

                return new Mensagem_token(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO, null, $dominio);
            }

            return new Mensagem_token(
                PROTOCOLO_SISTEMA::ERRO_PARAMETRO_INVALIDO,
                HelperMensagem::GERAL_FALHA_CADASTRO(
                    HelperMensagem::GERAL_PARAMETRO_INVALIDO(
                        "getDominioSistema",
                        array($idSistema)
                    ),
                    array()

                )

            );

        }

    }

?>
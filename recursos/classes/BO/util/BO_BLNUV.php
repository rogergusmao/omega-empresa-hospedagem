<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of BO_Web_service_biblioteca_nuvem
     *
     * @author home
     */
    class BO_BLNUV
    {

        public static function getArvoreBancoOmegaEmpresa($idSistema)
        {
            $dominioWebService = DOMINIO_SERVICOS_BIBLIOTECA_NUVEM . "getArvoreBancoOmegaEmpresa";

            $json = Helper::chamadaCurlJson(
                $dominioWebService,
                array("id_sistema"),
                array($idSistema));

            if (Interface_mensagem::checkOk($json))
            {
                return $json->mVetorObj;
            }
            else
            {
                if (Interface_mensagem::checkErro($json))
                {
                    throw new Exception($json->mMensagem);
                }
            }

            return null;
        }

    }

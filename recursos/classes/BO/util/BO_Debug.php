<?php

    class BO_Debug
    {
        public function factory()
        {
            return new BO_Debug();
        }
        public function configuraServidorLinode(){
            $this->configuraServidorDesenvolvimento("192.168.1.104", "workoffline.com.br");
        }
        public function configuraServidorDesenvolvimento($ipAtual = null, $ip =null)
        {
            $db = new Database();
            $db->query("SELECT dominio_raiz_assinatura FROM sistema");
            $dominio = $db->getPrimeiraTuplaDoResultSet(0);
            if (strlen($dominio))
            {
                $constHttp = "http://";
                $pos = strpos($dominio, $constHttp);
                if ($pos >= 0)
                {
                    $pos = $pos + strlen($constHttp);
                }
                else
                {
                    $pos = 0;
                }

                $end = strpos($dominio, "/", $pos);
                if($ipAtual == null)
                    $ipAtual = substr($dominio, $pos, $end - $pos);
            }
            if($ip == null)
                $ip = Helper::getServerIp();
            if ($ipAtual != $ip)
            {
                $q = "UPDATE hospedagem 
                        SET dominio = REPLACE(dominio, '$ipAtual', '$ip'), 
                        dominio_webservice = REPLACE(dominio_webservice, '$ipAtual', '$ip'), 
                        dominio_sincronizador = REPLACE(dominio_sincronizador, '$ipAtual', '$ip')";
                $db->queryMensagem($q);

                $q = "UPDATE sistema 
                        SET dominio_raiz_assinatura = REPLACE(dominio_raiz_assinatura, '$ipAtual', '$ip'), 
                        dominio_raiz_sincronizador = REPLACE(dominio_raiz_sincronizador, '$ipAtual', '$ip')";
                $db->queryMensagem($q);

            }

        }

    }

?>
<?php

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of BO_Arquivo
     *
     * @author rogerfsg
     */
    class BO_GM
    {
        public static function factory(){
            return new BO_GM();
        }

        private function gerarSqlite($idHospedagem, $idCorporacao, $corporacao, $aForca = "false"){
            $db = new Database();
            $boSincw = new BO_SINCW($db, $idHospedagem);
            $msg = $boSincw->procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao(
                $idCorporacao, $corporacao, $aForca);
            return $msg;
        }
        private function executaComandosNoBancoDaAssinatura($idAssinatura, $idTipoHospedagemDb, $cmds)
        {
            $db = new Database();
            $objAssinaturaAntiga = new EXTDAO_Assinatura($db );
            $objAssinaturaAntiga->select($idAssinatura);

            $antigasHospedagensDb = EXTDAO_Assinatura::getConfiguracoesDeTodosOsBancosDaAssinatura(
                $idAssinatura, $db);

            $dadosConfigDatab = $antigasHospedagensDb[$idTipoHospedagemDb];

            $objBanco = new Database($dadosConfigDatab);


            if ($objBanco != null) {
                for ($i = 0; $i < count($cmds); $i++) {
                    $cmd= $cmds[$i];
                    if (strlen($cmd)) {
                        $msg = $objBanco->queryMensagem($cmd);
                        if($msg != null && Interface_mensagem::checkErro( $msg)) {
                            //HelperLog::logErro(null, null, "ERRO: $cmd");
                        }
                    }
                }
            }

            if($objBanco != null) $objBanco->close();

        }
        private function executaFileScriptNoBanco($script, $idAssinatura, $idTipoHospedagemDb){
            if($script  != ""){
                $content= file_get_contents($script );
                if(strlen($content)){
                    $cmds = Helper::explode("[;]", $content);
                    if(count($cmds)) {
                        BO_GM::executaComandosNoBancoDaAssinatura($idAssinatura, $idTipoHospedagemDb, $cmds);
                    }
                }
            }
        }

        private function executaScriptNoBanco($script, $idAssinatura, $idTipoHospedagemDb){

            $cmds = Helper::explode("[;]", $script);
            if(count($cmds)) {
                BO_GM::executaComandosNoBancoDaAssinatura($idAssinatura, $idTipoHospedagemDb, $cmds);
            }

        }
        //NAO FOI EXECUTADA
        public function executaGM_02032018()
        {

            $script="ALTER TABLE `registro_estado` DROP FOREIGN KEY `registro_estado_FK_27282714`;

ALTER TABLE `registro_estado` DROP FOREIGN KEY `registro_estado_FK_131805420`;

ALTER TABLE `registro_estado`
DROP INDEX `registro_estado_FK_27282714`,
DROP INDEX `registro_estado_FK_131805420`;

ALTER TABLE `registro_estado`
DROP COLUMN `responsavel_usuario_id_INT`,
DROP COLUMN `responsavel_categoria_permissao_id_INT`;
";
            $matrix = $this->getHospedagens();
            if($matrix != null){

                for($i = 0 ; $i < count($matrix); $i++){
                    $script = "";


                    $this->executaScriptNoBanco($script, $matrix[$i][1], $matrix[$i][2]);

                    Database::closeAll();
                }
            }

        }
        public function getHospedagens(){
            $q = "SELECT hd.id,  a.id, hd.tipo_hospedagem_db_id_INT, h.id, a.id_corporacao_INT, a.nome_site
              FROM hospedagem_db hd
                  join hospedagem h on hd.hospedagem_id_INT = h.id
                  join assinatura a on a.hospedagem_id_INT = h.id
              ";
            $db = new Database();
            $db->queryMensagemThrowException($q);
            $matrix = Helper::getResultSetToMatriz($db->result);
            return $matrix;
        }
        public function executaGM_11022018()
        {
            $path = "/var/www/hospedagem/public_html/gm/GM_11022018";
            $q = "SELECT hd.id,  a.id, hd.tipo_hospedagem_db_id_INT, h.id, a.id_corporacao_INT, a.nome_site
              FROM hospedagem_db hd
                  join hospedagem h on hd.hospedagem_id_INT = h.id
                  join assinatura a on a.hospedagem_id_INT = h.id
              ";
            $db = new Database();
            $db->queryMensagemThrowException($q);
            $matrix = Helper::getResultSetToMatriz($db->result);

            for($i = 0 ; $i < count($matrix); $i++){
                $script = "";
                if($matrix[$i][2] == EXTDAO_Tipo_hospedagem_db::PRINCIPAL){
                    $script = "{$path}/scriptsV4/projetos_versao_banco_banco_30/EstruturaWebHmgParaPrd_17_02_2018.sql";
                } else if($matrix[$i][2] == EXTDAO_Tipo_hospedagem_db::SINCRONIZADOR){
                    $script = "{$path}/scriptsV4/projetos_versao_banco_banco_33/EstruturaSincronizadorWebHmgParaPrd_17_02_2018.sql";
                }
                $this->executaFileScriptNoBanco($script, $matrix[$i][1], $matrix[$i][2]);


                    $script = "{$path}/sistema_atributo_e_tabela.sql";

                    $this->executaFileScriptNoBanco($script, $matrix[$i][1], $matrix[$i][2]);
                if($matrix[$i][2] == EXTDAO_Tipo_hospedagem_db::PRINCIPAL
                    && strlen($matrix[$i][4])){

                    $msg = $this->gerarSqlite($matrix[$i][3], $matrix[$i][4], $matrix[$i][5], "true");
                    print_r($msg);
                }

                Database::closeAll();
            }

        }
    }

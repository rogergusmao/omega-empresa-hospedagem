<?php

    class BO_SICOB
    {
        const PREFIXO = "SICOB:";

        public function factory()
        {
            return new BO_SICOB();
        }

        public static function getIdCorporacao($corporacao)
        {
            $nomeCorporacao = urlencode($corporacao);
            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "getIdCorporacao&corporacao=$nomeCorporacao";

            return Helper::chamadaCurlMensagemToken($url);
        }

        public static function updateIdSihopAssinatura($idClienteAssinaturaSicob, $idNovaAssinatura)
        {
            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "updateIdSihopAssinatura&id_cliente_assinatura=$idClienteAssinaturaSicob"
                . "&id_sihop_assinatura_novo=$idNovaAssinatura";

            return Helper::chamadaCurlMensagem($url);
        }

        public static function reservaCorporacaoEClienteAssinatura($idSistema)
        {
            $url = DOMINIO_DE_ACESSO_SERVICOS_SICOB . "reservaCorporacaoEClienteAssinatura&id_sistema=$idSistema";

            return Helper::chamadaCurlJson($url);
        }

    }

?>
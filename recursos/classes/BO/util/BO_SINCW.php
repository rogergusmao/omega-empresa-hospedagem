<?php

    class BO_SINCW
    {
        public function factory()
        {
            return new BO_SINCW();
        }

        public $dominio = null;
        public $dominioWebservice = null;
        public $dominioSincronizador = null;

        public function __construct(Database $db, $idHospedagem)
        {
            $obj = new EXTDAO_Hospedagem($db);
            $obj->select($idHospedagem);
            $this->dominio = $obj->getDominio();
            $this->dominioWebservice = $obj->getDominio_webservice();
            $this->dominioSincronizador = $obj->getDominio_sincronizador();
        }

        public function procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao(
                $idCorporacao
                , $corporacao
                , $aForca = "false")
        {
            $dominioWebService = $this->dominioSincronizador . "adm/webservice.php?";

            return Helper::chamadaCurlMensagem(
                $dominioWebService,
                array(
                    "class"
                    , "action"
                    , "corporacao"
                    , "id_corporacao"
                    , "aForca"),
                array(
                    "Servicos_web"
                    , "procedimentoGerarBancoSqliteEmCasoDeErroDeSincronizacao"
                    , $corporacao
                    , $idCorporacao
                    , $aForca
                )
            );
        }

        public function clearCacheBanco($idCorporacao, $corporacao)
        {
            $dominioWebService = $this->dominioSincronizador . "adm/webservice.php?";

            return Helper::chamadaCurlMensagem(
                $dominioWebService,
                array("class", "action", "corporacao", "id_corporacao"),
                array("Servicos_web", "clearCacheBanco", $corporacao, $idCorporacao));
        }

        public function empilhaPedidoGeracaoSqlite($idCorporacao, $corporacao, $idSistemaSihop)
        {
            $dominioWebService = $this->dominioSincronizador . "adm/webservice.php?";

            return Helper::chamadaCurlMensagem(
                $dominioWebService,
                array("class", "action", "corporacao", "id_corporacao", "id_sistema_sihop"),
                array("Servicos_web", "empilhaPedidoGeracaoSqlite", $corporacao, $idCorporacao, $idSistemaSihop));
        }

        public function clearCacheBancoEEmpilhaPedidoSqlite($idCorporacao, $corporacao, $idSistemaSihop)
        {
            $dominioWebService = $this->dominioSincronizador . "adm/webservice.php?";

            return Helper::chamadaCurlMensagem(
                $dominioWebService,
                array("class", "action", "corporacao", "id_corporacao", "id_sistema_sihop"),
                array("Servicos_web", "clearCacheBancoEEmpilhaPedidoSqlite", $corporacao, $idCorporacao, $idSistemaSihop));
        }

    }

?>
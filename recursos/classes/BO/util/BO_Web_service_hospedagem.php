<?php

    class BO_Web_service_hospedagem
    {

        public static function consultaHospedagensDoUsuario($emailUsuario, $idSistema)
        {
            $hospedagens = EXTDAO_Hospedagem::getHospedagensDoSistemaQueEstaoOcupadas($idSistema);

            $retorno = array();
            $objHospedagem = new EXTDAO_Hospedagem();
            $parametroEmail = urlencode($emailUsuario);
            for ($i = 0; $i < count($hospedagens); $i++)
            {
                $obj = $hospedagens[ $i ];
                $idHospedagem = $obj[0];
                $idAssinatura = $obj[1];
                $nomeSite = $obj[2];
                $objHospedagem->select($idHospedagem);

                $dominioWebService = $objHospedagem->getDominio_webservice() . "consultaUsuario&email=$parametroEmail";
//            echo "<br/>WebService: ".$dominioWebService."<br/>";
                $json = Helper::chamadaCurlJson($dominioWebService);
                if (Interface_mensagem::checkErro($json))
                {
                    return $json;
                }
                else
                {
                    if (Interface_mensagem::checkOk($json))
                    {
                        $retorno[ count($retorno) ] = Protocolo_dados_hospedagem::constroi(array(
                                                                                               $idAssinatura,
                                                                                               $objHospedagem->getDominio(),
                                                                                               $nomeSite));
                    }
                }
            }
            if (count($retorno) == 0)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "O usu�rio n�o est� cadastrado em nenhuma das hospedagens do sistema.");
            }
            else
            {
                return new Mensagem_vetor_protocolo(
                    null,
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    null,
                    $retorno);
            }

        }

        public static function getEspacoOcupadoPelaBancoDeDados($idHospedagem, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $obj = new EXTDAO_Hospedagem($db);
            $obj->select($idHospedagem);
            $dominioWebService = $obj->getDominio_webservice() . "getEspacoOcupadoPeloBanco";
            $idPorCorporacao = EXTDAO_Assinatura::getIdENomeCorporacaoDaHospedagem($idHospedagem, $db);
            if ($idPorCorporacao == null)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "Hospdagem inexistente: $idHospedagem");
            }

            return Helper::chamadaCurlMensagemToken(
                $dominioWebService
                . "&corporacao=" . $idPorCorporacao['corporacao']
                . "&id_corporacao=" . $idPorCorporacao['id']);
        }

        public static function criaPrimeiroUsuarioDaAssinatura(
            $idAssinatura, $nome,
            $corporacao, $email, $senha,
            $idCorporacao)
        {
            $obj = new EXTDAO_Hospedagem();
            $idHospedagem = EXTDAO_Assinatura::getHospedagemDaAssinatura($idAssinatura);
            $msg = $obj->select($idHospedagem);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $dominioWebService = $obj->getDominio_webservice() . "cadastraUsuarioECorporacao";

            $mensagem = Helper::chamadaCurlMensagem(
                $dominioWebService,
                array("email", "corporacao", "senha", "nome", "id_corporacao"),
                array($email, $corporacao, $senha, $nome, $idCorporacao));

            if (!$mensagem->ok())
            {
                return $mensagem;
            }

            $dominioWebService = $obj->getDominio_sincronizador()
                . "adm/webservice.php?class=Servicos_web&action=cadastraCorporacao";

            $mensagem = Helper::chamadaCurlMensagem(
                $dominioWebService,
                array("corporacao", "id_corporacao"),
                array($corporacao, $idCorporacao));

            return $mensagem;
        }

        public static function getTotalDeUsuariosDaHospedagem(
            $idCorporacao, $corporacao, $idHospedagem, $db)
        {
            $obj = new EXTDAO_Hospedagem($db);
            $obj->select($idHospedagem);

            $dominioWebService = $obj->getDominio_webservice() . "getTotalDeUsuarios";

            //echo $dominioWebService;
            return Helper::chamadaCurlMensagemToken(
                $dominioWebService,
                array("id_corporacao", "corporacao"),
                array($idCorporacao, $corporacao));
        }

        public $dominio = null;
        public $dominioWebservice = null;
        public $dominioSincronizador = null;

        public function __construct(Database $db, $idHospedagem)
        {
            $obj = new EXTDAO_Hospedagem($db);
            $obj->select($idHospedagem);
            $this->dominio = $obj->getDominio();
            $this->dominioWebservice = $obj->getDominio_webservice();
            $this->dominioSincronizador = $obj->getDominio_sincronizador();
        }

        public function gerarIdInterna(
            Database $db,
            $idHospedagem,
            $idCorporacao,
            $corporacao,
            $tabela,
            $totGerar
        )
        {
            $dominioWebService = $this->dominio . "adm/gerar_id_avulso.php?";

            $json = Helper::chamadaCurlJson(
                $dominioWebService,
                array("id_corporacao", "corporacao", "tabela", "tot_gerar"),
                array($idCorporacao, $corporacao, $tabela, $totGerar));

            if (Interface_mensagem::checkOk($json))
            {
                return $json->mValor;
            }
            else
            {
                if (Interface_mensagem::checkErro($json))
                {
                    throw new Exception($json->mMensagem);
                }
            }

            return null;
        }

        public function assinaturaMigrada(
            $idCorporacao,
            $corporacao,
            $idUltimaSincronizacao = ""
        )
        {
            $dominioWebService = $this->dominioSincronizador . "adm/webservice.php?class=Servicos_web&action=assinaturaMigrada";;

            //echo $dominioWebService;
            return Helper::chamadaCurlMensagem(
                $dominioWebService,
                array("id_corporacao", "corporacao", "id_ultima_sincronizacao"),
                array($idCorporacao, $corporacao, $idUltimaSincronizacao));
        }

        public function clearCacheBanco($idCorporacao, $corporacao)
        {
            $dominioWebService = $this->dominioWebservice . "clearCacheBanco";

            //echo $dominioWebService;
            return Helper::chamadaCurlMensagem(
                $dominioWebService,
                array("id_corporacao", "corporacao"),
                array($idCorporacao, $corporacao));
        }
    }

?>

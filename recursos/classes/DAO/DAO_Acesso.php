<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Acesso
    * DATA DE GERA��O: 21.10.2010
    * ARQUIVO:         DAO_Acesso.php
    * TABELA MYSQL:    acesso
    * BANCO DE DADOS:  DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Acesso extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $usuario_id_INT;
        public $objUsuario;
        public $data_login_DATETIME;
        public $data_logout_DATETIME;

        public $nomeEntidade;

        public $data_login_DATETIME_UNIX;
        public $data_logout_DATETIME_UNIX;

        public $label_id;
        public $label_usuario_id_INT;
        public $label_data_login_DATETIME;
        public $label_data_logout_DATETIME;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "acesso";
            $this->nomeTabela = "acesso";
            $this->campoId = "id";
            $this->campoLabel = "usuario_id_INT";

            $this->objUsuario = new EXTDAO_Usuario();
        }

        public function valorCampoLabel()
        {
            return $this->getUsuario_id_INT();
        }

        public function getComboBoxAllUsuario($objArgumentos)
        {
            $objArgumentos->nome = "usuario_id_INT";
            $objArgumentos->id = "usuario_id_INT";
            $objArgumentos->valueReplaceId = false;

            return $this->objUsuario->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi modificado com sucesso.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O acesso foi exclu�do com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_acesso", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_acesso", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }




        // **********************
        // M�TODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getUsuario_id_INT()
        {
            return $this->usuario_id_INT;
        }

        function getData_login_DATETIME_UNIX()
        {
            return $this->data_login_DATETIME_UNIX;
        }

        public function getData_login_DATETIME()
        {
            return $this->data_login_DATETIME;
        }

        function getData_logout_DATETIME_UNIX()
        {
            return $this->data_logout_DATETIME_UNIX;
        }

        public function getData_logout_DATETIME()
        {
            return $this->data_logout_DATETIME;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setUsuario_id_INT($val)
        {
            $this->usuario_id_INT = $val;
        }

        function setData_login_DATETIME($val)
        {
            $this->data_login_DATETIME = $val;
        }

        function setData_logout_DATETIME($val)
        {
            $this->data_logout_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(data_login_DATETIME) AS data_login_DATETIME_UNIX, UNIX_TIMESTAMP(data_logout_DATETIME) AS data_logout_DATETIME_UNIX FROM acesso WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->usuario_id_INT = $row->usuario_id_INT;
            if ($this->usuario_id_INT)
            {
                $this->objUsuario->select($this->usuario_id_INT);
            }

            $this->data_login_DATETIME = $row->data_login_DATETIME;
            $this->data_login_DATETIME_UNIX = $row->data_login_DATETIME_UNIX;

            $this->data_logout_DATETIME = $row->data_logout_DATETIME;
            $this->data_logout_DATETIME_UNIX = $row->data_logout_DATETIME_UNIX;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "DELETE FROM acesso WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO acesso ( usuario_id_INT,data_login_DATETIME,data_logout_DATETIME ) VALUES ( $this->usuario_id_INT,$this->data_login_DATETIME,$this->data_logout_DATETIME )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoUsuario_id_INT()
        {
            return "usuario_id_INT";
        }

        public function nomeCampoData_login_DATETIME()
        {
            return "data_login_DATETIME";
        }

        public function nomeCampoData_logout_DATETIME()
        {
            return "data_logout_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoUsuario_id_INT($objArguments)
        {
            $objArguments->nome = "usuario_id_INT";
            $objArguments->id = "usuario_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoData_login_DATETIME($objArguments)
        {
            $objArguments->nome = "data_login_DATETIME";
            $objArguments->id = "data_login_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoData_logout_DATETIME($objArguments)
        {
            $objArguments->nome = "data_logout_DATETIME";
            $objArguments->id = "data_logout_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->usuario_id_INT == "")
            {
                $this->usuario_id_INT = "null";
            }

            $this->data_login_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_login_DATETIME);
            $this->data_logout_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_logout_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->data_login_DATETIME = $this->formatarDataTimeParaExibicao($this->data_login_DATETIME);
            $this->data_logout_DATETIME = $this->formatarDataTimeParaExibicao($this->data_logout_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["usuario_id_INT"] = $this->usuario_id_INT;
            $_SESSION["data_login_DATETIME"] = $this->data_login_DATETIME;
            $_SESSION["data_logout_DATETIME"] = $this->data_logout_DATETIME;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["usuario_id_INT"]);
            unset($_SESSION["data_login_DATETIME"]);
            unset($_SESSION["data_logout_DATETIME"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_SESSION["usuario_id_INT{$numReg}"]);
            $this->data_login_DATETIME = $this->formatarDados($_SESSION["data_login_DATETIME{$numReg}"]);
            $this->data_logout_DATETIME = $this->formatarDados($_SESSION["data_logout_DATETIME{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_POST["usuario_id_INT{$numReg}"]);
            $this->data_login_DATETIME = $this->formatarDados($_POST["data_login_DATETIME{$numReg}"]);
            $this->data_logout_DATETIME = $this->formatarDados($_POST["data_logout_DATETIME{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_GET["usuario_id_INT{$numReg}"]);
            $this->data_login_DATETIME = $this->formatarDados($_GET["data_login_DATETIME{$numReg}"]);
            $this->data_logout_DATETIME = $this->formatarDados($_GET["data_logout_DATETIME{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "usuario_id_INT = $this->usuario_id_INT, ";
            }

            if (isset($tipo["data_login_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "data_login_DATETIME = $this->data_login_DATETIME, ";
            }

            if (isset($tipo["data_logout_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "data_logout_DATETIME = $this->data_logout_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE acesso SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>
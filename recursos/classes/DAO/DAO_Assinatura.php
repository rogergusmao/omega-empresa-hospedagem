<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Assinatura
    * DATA DE GERA��O: 18.08.2017
    * ARQUIVO:         DAO_Assinatura.php
    * TABELA MYSQL:    assinatura
    * BANCO DE DADOS:  hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Assinatura extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $nome_site;
        public $hospedagem_id_INT;
        public $objHospedagem;
        public $sistema_id_INT;
        public $objSistema;
        public $estado_assinatura_id_INT;
        public $objEstado_assinatura;
        public $sicob_cliente_assinatura_INT;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;
        public $id_sistema_versao_biblioteca_nuvem_cliente_INT;
        public $id_corporacao_INT;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_nome_site;
        public $label_hospedagem_id_INT;
        public $label_sistema_id_INT;
        public $label_estado_assinatura_id_INT;
        public $label_sicob_cliente_assinatura_INT;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;
        public $label_id_sistema_versao_biblioteca_nuvem_cliente_INT;
        public $label_id_corporacao_INT;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "";
            $this->nomeTabela = "assinatura";
            $this->campoId = "id";
            $this->campoLabel = "nome_site";
        }

        public function valorCampoLabel()
        {
            return $this->getNome_site();
        }

        public function getFkObjHospedagem()
        {
            if ($this->objHospedagem == null)
            {
                $this->objHospedagem = new EXTDAO_Hospedagem($this->getConfiguracaoDAO());
            }
            $idFK = $this->getHospedagem_id_INT();
            if (!strlen($idFK))
            {
                $this->objHospedagem->clear();
            }
            else
            {
                if ($this->objHospedagem->getId() != $idFK)
                {
                    $this->objHospedagem->select($idFK);
                }
            }

            return $this->objHospedagem;
        }

        public function getComboBoxAllHospedagem($objArgumentos)
        {
            $objArgumentos->nome = "hospedagem_id_INT";
            $objArgumentos->id = "hospedagem_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objHospedagem = $this->getFkObjHospedagem();

            return $this->objHospedagem->getComboBox($objArgumentos);
        }

        public function getFkObjSistema()
        {
            if ($this->objSistema == null)
            {
                $this->objSistema = new EXTDAO_Sistema($this->getConfiguracaoDAO());
            }
            $idFK = $this->getSistema_id_INT();
            if (!strlen($idFK))
            {
                $this->objSistema->clear();
            }
            else
            {
                if ($this->objSistema->getId() != $idFK)
                {
                    $this->objSistema->select($idFK);
                }
            }

            return $this->objSistema;
        }

        public function getComboBoxAllSistema($objArgumentos)
        {
            $objArgumentos->nome = "sistema_id_INT";
            $objArgumentos->id = "sistema_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objSistema = $this->getFkObjSistema();

            return $this->objSistema->getComboBox($objArgumentos);
        }

        public function getFkObjEstado_assinatura()
        {
            if ($this->objEstado_assinatura == null)
            {
                $this->objEstado_assinatura = new EXTDAO_Estado_assinatura($this->getConfiguracaoDAO());
            }
            $idFK = $this->getEstado_assinatura_id_INT();
            if (!strlen($idFK))
            {
                $this->objEstado_assinatura->clear();
            }
            else
            {
                if ($this->objEstado_assinatura->getId() != $idFK)
                {
                    $this->objEstado_assinatura->select($idFK);
                }
            }

            return $this->objEstado_assinatura;
        }

        public function getComboBoxAllEstado_assinatura($objArgumentos)
        {
            $objArgumentos->nome = "estado_assinatura_id_INT";
            $objArgumentos->id = "estado_assinatura_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objEstado_assinatura = $this->getFkObjEstado_assinatura();

            return $this->objEstado_assinatura->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A assinatura foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A assinatura foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A assinatura foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A assinatura foi exclu�da com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_assinatura", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_assinatura", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getNome_site()
        {
            return $this->nome_site;
        }

        public function getHospedagem_id_INT()
        {
            return $this->hospedagem_id_INT;
        }

        public function getSistema_id_INT()
        {
            return $this->sistema_id_INT;
        }

        public function getEstado_assinatura_id_INT()
        {
            return $this->estado_assinatura_id_INT;
        }

        public function getSicob_cliente_assinatura_INT()
        {
            return $this->sicob_cliente_assinatura_INT;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        public function getId_sistema_versao_biblioteca_nuvem_cliente_INT()
        {
            return $this->id_sistema_versao_biblioteca_nuvem_cliente_INT;
        }

        public function getId_corporacao_INT()
        {
            return $this->id_corporacao_INT;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setNome_site($val)
        {
            $this->nome_site = $val;
        }

        function setHospedagem_id_INT($val)
        {
            $this->hospedagem_id_INT = $val;
        }

        function setSistema_id_INT($val)
        {
            $this->sistema_id_INT = $val;
        }

        function setEstado_assinatura_id_INT($val)
        {
            $this->estado_assinatura_id_INT = $val;
        }

        function setSicob_cliente_assinatura_INT($val)
        {
            $this->sicob_cliente_assinatura_INT = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }

        function setId_sistema_versao_biblioteca_nuvem_cliente_INT($val)
        {
            $this->id_sistema_versao_biblioteca_nuvem_cliente_INT = $val;
        }

        function setId_corporacao_INT($val)
        {
            $this->id_corporacao_INT = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM assinatura WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->nome_site = $row->nome_site;

            $this->hospedagem_id_INT = $row->hospedagem_id_INT;

            $this->sistema_id_INT = $row->sistema_id_INT;

            $this->estado_assinatura_id_INT = $row->estado_assinatura_id_INT;

            $this->sicob_cliente_assinatura_INT = $row->sicob_cliente_assinatura_INT;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;

            $this->id_sistema_versao_biblioteca_nuvem_cliente_INT = $row->id_sistema_versao_biblioteca_nuvem_cliente_INT;

            $this->id_corporacao_INT = $row->id_corporacao_INT;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->nome_site = null;
            $this->hospedagem_id_INT = null;
            $this->sistema_id_INT = null;
            $this->estado_assinatura_id_INT = null;
            $this->sicob_cliente_assinatura_INT = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
            $this->id_sistema_versao_biblioteca_nuvem_cliente_INT = null;
            $this->id_corporacao_INT = null;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE assinatura SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO assinatura ( nome_site , hospedagem_id_INT , sistema_id_INT , estado_assinatura_id_INT , sicob_cliente_assinatura_INT , excluido_BOOLEAN , excluido_DATETIME , id_sistema_versao_biblioteca_nuvem_cliente_INT , id_corporacao_INT ) VALUES ( {$this->nome_site} , {$this->hospedagem_id_INT} , {$this->sistema_id_INT} , {$this->estado_assinatura_id_INT} , {$this->sicob_cliente_assinatura_INT} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} , {$this->id_sistema_versao_biblioteca_nuvem_cliente_INT} , {$this->id_corporacao_INT} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome_site()
        {
            return "nome_site";
        }

        public function nomeCampoHospedagem_id_INT()
        {
            return "hospedagem_id_INT";
        }

        public function nomeCampoSistema_id_INT()
        {
            return "sistema_id_INT";
        }

        public function nomeCampoEstado_assinatura_id_INT()
        {
            return "estado_assinatura_id_INT";
        }

        public function nomeCampoSicob_cliente_assinatura_INT()
        {
            return "sicob_cliente_assinatura_INT";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }

        public function nomeCampoId_sistema_versao_biblioteca_nuvem_cliente_INT()
        {
            return "id_sistema_versao_biblioteca_nuvem_cliente_INT";
        }

        public function nomeCampoId_corporacao_INT()
        {
            return "id_corporacao_INT";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome_site($objArguments)
        {
            $objArguments->nome = "nome_site";
            $objArguments->id = "nome_site";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoHospedagem_id_INT($objArguments)
        {
            $objArguments->nome = "hospedagem_id_INT";
            $objArguments->id = "hospedagem_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoSistema_id_INT($objArguments)
        {
            $objArguments->nome = "sistema_id_INT";
            $objArguments->id = "sistema_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoEstado_assinatura_id_INT($objArguments)
        {
            $objArguments->nome = "estado_assinatura_id_INT";
            $objArguments->id = "estado_assinatura_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoSicob_cliente_assinatura_INT($objArguments)
        {
            $objArguments->nome = "sicob_cliente_assinatura_INT";
            $objArguments->id = "sicob_cliente_assinatura_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoId_sistema_versao_biblioteca_nuvem_cliente_INT($objArguments)
        {
            $objArguments->nome = "id_sistema_versao_biblioteca_nuvem_cliente_INT";
            $objArguments->id = "id_sistema_versao_biblioteca_nuvem_cliente_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoId_corporacao_INT($objArguments)
        {
            $objArguments->nome = "id_corporacao_INT";
            $objArguments->id = "id_corporacao_INT";

            return $this->campoInteiro($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->hospedagem_id_INT == null)
            {
                $this->hospedagem_id_INT = "null";
            }

            if ($this->sistema_id_INT == null)
            {
                $this->sistema_id_INT = "null";
            }

            if ($this->estado_assinatura_id_INT == null)
            {
                $this->estado_assinatura_id_INT = "null";
            }

            if ($this->sicob_cliente_assinatura_INT == null)
            {
                $this->sicob_cliente_assinatura_INT = "null";
            }

            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            if ($this->id_sistema_versao_biblioteca_nuvem_cliente_INT == null)
            {
                $this->id_sistema_versao_biblioteca_nuvem_cliente_INT = "null";
            }

            if ($this->id_corporacao_INT == null)
            {
                $this->id_corporacao_INT = "null";
            }

            $this->nome_site = $this->formatarDadosParaSQL($this->nome_site);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("nome_site", $this->nome_site);
            Helper::setSession("hospedagem_id_INT", $this->hospedagem_id_INT);
            Helper::setSession("sistema_id_INT", $this->sistema_id_INT);
            Helper::setSession("estado_assinatura_id_INT", $this->estado_assinatura_id_INT);
            Helper::setSession("sicob_cliente_assinatura_INT", $this->sicob_cliente_assinatura_INT);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
            Helper::setSession("id_sistema_versao_biblioteca_nuvem_cliente_INT", $this->id_sistema_versao_biblioteca_nuvem_cliente_INT);
            Helper::setSession("id_corporacao_INT", $this->id_corporacao_INT);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("nome_site");
            Helper::clearSession("hospedagem_id_INT");
            Helper::clearSession("sistema_id_INT");
            Helper::clearSession("estado_assinatura_id_INT");
            Helper::clearSession("sicob_cliente_assinatura_INT");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
            Helper::clearSession("id_sistema_versao_biblioteca_nuvem_cliente_INT");
            Helper::clearSession("id_corporacao_INT");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = Helper::SESSION("id{$numReg}");
            $this->nome_site = Helper::SESSION("nome_site{$numReg}");
            $this->hospedagem_id_INT = Helper::SESSION("hospedagem_id_INT{$numReg}");
            $this->sistema_id_INT = Helper::SESSION("sistema_id_INT{$numReg}");
            $this->estado_assinatura_id_INT = Helper::SESSION("estado_assinatura_id_INT{$numReg}");
            $this->sicob_cliente_assinatura_INT = Helper::SESSION("sicob_cliente_assinatura_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::SESSION("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::SESSION("excluido_DATETIME{$numReg}");
            $this->id_sistema_versao_biblioteca_nuvem_cliente_INT = Helper::SESSION("id_sistema_versao_biblioteca_nuvem_cliente_INT{$numReg}");
            $this->id_corporacao_INT = Helper::SESSION("id_corporacao_INT{$numReg}");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = Helper::POST("id{$numReg}");
            $this->nome_site = Helper::POST("nome_site{$numReg}");
            $this->hospedagem_id_INT = Helper::POST("hospedagem_id_INT{$numReg}");
            $this->sistema_id_INT = Helper::POST("sistema_id_INT{$numReg}");
            $this->estado_assinatura_id_INT = Helper::POST("estado_assinatura_id_INT{$numReg}");
            $this->sicob_cliente_assinatura_INT = Helper::POST("sicob_cliente_assinatura_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::POST("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::POST("excluido_DATETIME{$numReg}");
            $this->id_sistema_versao_biblioteca_nuvem_cliente_INT = Helper::POST("id_sistema_versao_biblioteca_nuvem_cliente_INT{$numReg}");
            $this->id_corporacao_INT = Helper::POST("id_corporacao_INT{$numReg}");
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = Helper::GET("id{$numReg}");
            $this->nome_site = Helper::GET("nome_site{$numReg}");
            $this->hospedagem_id_INT = Helper::GET("hospedagem_id_INT{$numReg}");
            $this->sistema_id_INT = Helper::GET("sistema_id_INT{$numReg}");
            $this->estado_assinatura_id_INT = Helper::GET("estado_assinatura_id_INT{$numReg}");
            $this->sicob_cliente_assinatura_INT = Helper::GET("sicob_cliente_assinatura_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::GET("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::GET("excluido_DATETIME{$numReg}");
            $this->id_sistema_versao_biblioteca_nuvem_cliente_INT = Helper::GET("id_sistema_versao_biblioteca_nuvem_cliente_INT{$numReg}");
            $this->id_corporacao_INT = Helper::GET("id_corporacao_INT{$numReg}");
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["nome_site{$numReg}"]) || $tipo == null)
            {
                $upd .= "nome_site = $this->nome_site, ";
            }

            if (isset($tipo["hospedagem_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "hospedagem_id_INT = $this->hospedagem_id_INT, ";
            }

            if (isset($tipo["sistema_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "sistema_id_INT = $this->sistema_id_INT, ";
            }

            if (isset($tipo["estado_assinatura_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "estado_assinatura_id_INT = $this->estado_assinatura_id_INT, ";
            }

            if (isset($tipo["sicob_cliente_assinatura_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "sicob_cliente_assinatura_INT = $this->sicob_cliente_assinatura_INT, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            if (isset($tipo["id_sistema_versao_biblioteca_nuvem_cliente_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "id_sistema_versao_biblioteca_nuvem_cliente_INT = $this->id_sistema_versao_biblioteca_nuvem_cliente_INT, ";
            }

            if (isset($tipo["id_corporacao_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "id_corporacao_INT = $this->id_corporacao_INT, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE assinatura SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim

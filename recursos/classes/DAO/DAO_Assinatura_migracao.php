<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Assinatura_migracao
    * DATA DE GERA��O: 05.07.2017
    * ARQUIVO:         DAO_Assinatura_migracao.php
    * TABELA MYSQL:    assinatura_migracao
    * BANCO DE DADOS:  hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Assinatura_migracao extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $assinatura_id_INT;
        public $objAssinatura;
        public $antigo_sistema_id_INT;
        public $objAntigo_sistema;
        public $novo_sistema_id_INT;
        public $objNovo_sistema;
        public $data_cadastro_DATETIME;
        public $data_inicio_migracao_DATETIME;
        public $data_fim_migracao_DATETIME;
        public $antigo_sicob_cliente_assinatura_INT;
        public $novo_sicob_cliente_assinatura_INT;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;
        public $nova_assinatura_id_INT;
        public $objNova_assinatura;
        public $erro;
        public $estado_assinatura_migracao_id_INT;
        public $objEstado_assinatura_migracao;
        public $data_inicio_disponibilizacao_assinatura_DATETIME;
        public $data_fim_disponibilizacao_assinatura_DATETIME;
        public $id_corporacao_INT;

        public $nomeEntidade;

        public $data_cadastro_DATETIME_UNIX;
        public $data_inicio_migracao_DATETIME_UNIX;
        public $data_fim_migracao_DATETIME_UNIX;
        public $excluido_DATETIME_UNIX;
        public $data_inicio_disponibilizacao_assinatura_DATETIME_UNIX;
        public $data_fim_disponibilizacao_assinatura_DATETIME_UNIX;

        public $label_id;
        public $label_assinatura_id_INT;
        public $label_antigo_sistema_id_INT;
        public $label_novo_sistema_id_INT;
        public $label_data_cadastro_DATETIME;
        public $label_data_inicio_migracao_DATETIME;
        public $label_data_fim_migracao_DATETIME;
        public $label_antigo_sicob_cliente_assinatura_INT;
        public $label_novo_sicob_cliente_assinatura_INT;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;
        public $label_nova_assinatura_id_INT;
        public $label_erro;
        public $label_estado_assinatura_migracao_id_INT;
        public $label_data_inicio_disponibilizacao_assinatura_DATETIME;
        public $label_data_fim_disponibilizacao_assinatura_DATETIME;
        public $label_id_corporacao_INT;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "a";
            $this->nomeTabela = "assinatura_migracao";
            $this->campoId = "id";
            $this->campoLabel = "id";
        }

        public function valorCampoLabel()
        {
            return $this->getId();
        }

        public function getFkObjAssinatura()
        {
            if ($this->objAssinatura == null)
            {
                $this->objAssinatura = new EXTDAO_Assinatura($this->getConfiguracaoDAO());
            }
            $idFK = $this->getAssinatura_id_INT();
            if (!strlen($idFK))
            {
                $this->objAssinatura->clear();
            }
            else
            {
                if ($this->objAssinatura->getId() != $idFK)
                {
                    $this->objAssinatura->select($idFK);
                }
            }

            return $this->objAssinatura;
        }

        public function getComboBoxAllAssinatura($objArgumentos)
        {
            $objArgumentos->nome = "assinatura_id_INT";
            $objArgumentos->id = "assinatura_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objAssinatura = $this->getFkObjAssinatura();

            return $this->objAssinatura->getComboBox($objArgumentos);
        }

        public function getFkObjAntigo_sistema()
        {
            if ($this->objAntigo_sistema == null)
            {
                $this->objAntigo_sistema = new EXTDAO_Sistema($this->getConfiguracaoDAO());
            }
            $idFK = $this->getAntigo_sistema_id_INT();
            if (!strlen($idFK))
            {
                $this->objAntigo_sistema->clear();
            }
            else
            {
                if ($this->objAntigo_sistema->getId() != $idFK)
                {
                    $this->objAntigo_sistema->select($idFK);
                }
            }

            return $this->objAntigo_sistema;
        }

        public function getComboBoxAllAntigo_sistema($objArgumentos)
        {
            $objArgumentos->nome = "antigo_sistema_id_INT";
            $objArgumentos->id = "antigo_sistema_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objAntigo_sistema = $this->getFkObjAntigo_sistema();

            return $this->objAntigo_sistema->getComboBox($objArgumentos);
        }

        public function getFkObjNovo_sistema()
        {
            if ($this->objNovo_sistema == null)
            {
                $this->objNovo_sistema = new EXTDAO_Sistema($this->getConfiguracaoDAO());
            }
            $idFK = $this->getNovo_sistema_id_INT();
            if (!strlen($idFK))
            {
                $this->objNovo_sistema->clear();
            }
            else
            {
                if ($this->objNovo_sistema->getId() != $idFK)
                {
                    $this->objNovo_sistema->select($idFK);
                }
            }

            return $this->objNovo_sistema;
        }

        public function getComboBoxAllNovo_sistema($objArgumentos)
        {
            $objArgumentos->nome = "novo_sistema_id_INT";
            $objArgumentos->id = "novo_sistema_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objNovo_sistema = $this->getFkObjNovo_sistema();

            return $this->objNovo_sistema->getComboBox($objArgumentos);
        }

        public function getFkObjNova_assinatura()
        {
            if ($this->objNova_assinatura == null)
            {
                $this->objNova_assinatura = new EXTDAO_Assinatura($this->getConfiguracaoDAO());
            }
            $idFK = $this->getNova_assinatura_id_INT();
            if (!strlen($idFK))
            {
                $this->objNova_assinatura->clear();
            }
            else
            {
                if ($this->objNova_assinatura->getId() != $idFK)
                {
                    $this->objNova_assinatura->select($idFK);
                }
            }

            return $this->objNova_assinatura;
        }

        public function getComboBoxAllNova_assinatura($objArgumentos)
        {
            $objArgumentos->nome = "nova_assinatura_id_INT";
            $objArgumentos->id = "nova_assinatura_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objNova_assinatura = $this->getFkObjNova_assinatura();

            return $this->objNova_assinatura->getComboBox($objArgumentos);
        }

        public function getFkObjEstado_assinatura_migracao()
        {
            if ($this->objEstado_assinatura_migracao == null)
            {
                $this->objEstado_assinatura_migracao = new EXTDAO_Estado_assinatura_migracao($this->getConfiguracaoDAO());
            }
            $idFK = $this->getEstado_assinatura_migracao_id_INT();
            if (!strlen($idFK))
            {
                $this->objEstado_assinatura_migracao->clear();
            }
            else
            {
                if ($this->objEstado_assinatura_migracao->getId() != $idFK)
                {
                    $this->objEstado_assinatura_migracao->select($idFK);
                }
            }

            return $this->objEstado_assinatura_migracao;
        }

        public function getComboBoxAllEstado_assinatura_migracao($objArgumentos)
        {
            $objArgumentos->nome = "estado_assinatura_migracao_id_INT";
            $objArgumentos->id = "estado_assinatura_migracao_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objEstado_assinatura_migracao = $this->getFkObjEstado_assinatura_migracao();
            return $this->objEstado_assinatura_migracao->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A migra��o da assinatura foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A migra��o da assinatura foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A migra��o da assinatura foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A migra��o da assinatura foi exclu�da com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_assinatura_migracao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_assinatura_migracao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getAssinatura_id_INT()
        {
            return $this->assinatura_id_INT;
        }

        public function getAntigo_sistema_id_INT()
        {
            return $this->antigo_sistema_id_INT;
        }

        public function getNovo_sistema_id_INT()
        {
            return $this->novo_sistema_id_INT;
        }

        function getData_cadastro_DATETIME_UNIX()
        {
            return $this->data_cadastro_DATETIME_UNIX;
        }

        public function getData_cadastro_DATETIME()
        {
            return $this->data_cadastro_DATETIME;
        }

        function getData_inicio_migracao_DATETIME_UNIX()
        {
            return $this->data_inicio_migracao_DATETIME_UNIX;
        }

        public function getData_inicio_migracao_DATETIME()
        {
            return $this->data_inicio_migracao_DATETIME;
        }

        function getData_fim_migracao_DATETIME_UNIX()
        {
            return $this->data_fim_migracao_DATETIME_UNIX;
        }

        public function getData_fim_migracao_DATETIME()
        {
            return $this->data_fim_migracao_DATETIME;
        }

        public function getAntigo_sicob_cliente_assinatura_INT()
        {
            return $this->antigo_sicob_cliente_assinatura_INT;
        }

        public function getNovo_sicob_cliente_assinatura_INT()
        {
            return $this->novo_sicob_cliente_assinatura_INT;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        public function getNova_assinatura_id_INT()
        {
            return $this->nova_assinatura_id_INT;
        }

        public function getErro()
        {
            return $this->erro;
        }

        public function getEstado_assinatura_migracao_id_INT()
        {
            return $this->estado_assinatura_migracao_id_INT;
        }

        function getData_inicio_disponibilizacao_assinatura_DATETIME_UNIX()
        {
            return $this->data_inicio_disponibilizacao_assinatura_DATETIME_UNIX;
        }

        public function getData_inicio_disponibilizacao_assinatura_DATETIME()
        {
            return $this->data_inicio_disponibilizacao_assinatura_DATETIME;
        }

        function getData_fim_disponibilizacao_assinatura_DATETIME_UNIX()
        {
            return $this->data_fim_disponibilizacao_assinatura_DATETIME_UNIX;
        }

        public function getData_fim_disponibilizacao_assinatura_DATETIME()
        {
            return $this->data_fim_disponibilizacao_assinatura_DATETIME;
        }

        public function getId_corporacao_INT()
        {
            return $this->id_corporacao_INT;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setAssinatura_id_INT($val)
        {
            $this->assinatura_id_INT = $val;
        }

        function setAntigo_sistema_id_INT($val)
        {
            $this->antigo_sistema_id_INT = $val;
        }

        function setNovo_sistema_id_INT($val)
        {
            $this->novo_sistema_id_INT = $val;
        }

        function setData_cadastro_DATETIME($val)
        {
            $this->data_cadastro_DATETIME = $val;
        }

        function setData_inicio_migracao_DATETIME($val)
        {
            $this->data_inicio_migracao_DATETIME = $val;
        }

        function setData_fim_migracao_DATETIME($val)
        {
            $this->data_fim_migracao_DATETIME = $val;
        }

        function setAntigo_sicob_cliente_assinatura_INT($val)
        {
            $this->antigo_sicob_cliente_assinatura_INT = $val;
        }

        function setNovo_sicob_cliente_assinatura_INT($val)
        {
            $this->novo_sicob_cliente_assinatura_INT = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }

        function setNova_assinatura_id_INT($val)
        {
            $this->nova_assinatura_id_INT = $val;
        }

        function setErro($val)
        {
            $this->erro = $val;
        }

        function setEstado_assinatura_migracao_id_INT($val)
        {
            $this->estado_assinatura_migracao_id_INT = $val;
        }

        function setData_inicio_disponibilizacao_assinatura_DATETIME($val)
        {
            $this->data_inicio_disponibilizacao_assinatura_DATETIME = $val;
        }

        function setData_fim_disponibilizacao_assinatura_DATETIME($val)
        {
            $this->data_fim_disponibilizacao_assinatura_DATETIME = $val;
        }

        function setId_corporacao_INT($val)
        {
            $this->id_corporacao_INT = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(data_cadastro_DATETIME) AS data_cadastro_DATETIME_UNIX, UNIX_TIMESTAMP(data_inicio_migracao_DATETIME) AS data_inicio_migracao_DATETIME_UNIX, UNIX_TIMESTAMP(data_fim_migracao_DATETIME) AS data_fim_migracao_DATETIME_UNIX, UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX, UNIX_TIMESTAMP(data_inicio_disponibilizacao_assinatura_DATETIME) AS data_inicio_disponibilizacao_assinatura_DATETIME_UNIX, UNIX_TIMESTAMP(data_fim_disponibilizacao_assinatura_DATETIME) AS data_fim_disponibilizacao_assinatura_DATETIME_UNIX FROM assinatura_migracao WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->assinatura_id_INT = $row->assinatura_id_INT;

            $this->antigo_sistema_id_INT = $row->antigo_sistema_id_INT;

            $this->novo_sistema_id_INT = $row->novo_sistema_id_INT;

            $this->data_cadastro_DATETIME = $row->data_cadastro_DATETIME;
            $this->data_cadastro_DATETIME_UNIX = $row->data_cadastro_DATETIME_UNIX;

            $this->data_inicio_migracao_DATETIME = $row->data_inicio_migracao_DATETIME;
            $this->data_inicio_migracao_DATETIME_UNIX = $row->data_inicio_migracao_DATETIME_UNIX;

            $this->data_fim_migracao_DATETIME = $row->data_fim_migracao_DATETIME;
            $this->data_fim_migracao_DATETIME_UNIX = $row->data_fim_migracao_DATETIME_UNIX;

            $this->antigo_sicob_cliente_assinatura_INT = $row->antigo_sicob_cliente_assinatura_INT;

            $this->novo_sicob_cliente_assinatura_INT = $row->novo_sicob_cliente_assinatura_INT;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;

            $this->nova_assinatura_id_INT = $row->nova_assinatura_id_INT;

            $this->erro = $row->erro;

            $this->estado_assinatura_migracao_id_INT = $row->estado_assinatura_migracao_id_INT;

            $this->data_inicio_disponibilizacao_assinatura_DATETIME = $row->data_inicio_disponibilizacao_assinatura_DATETIME;
            $this->data_inicio_disponibilizacao_assinatura_DATETIME_UNIX = $row->data_inicio_disponibilizacao_assinatura_DATETIME_UNIX;

            $this->data_fim_disponibilizacao_assinatura_DATETIME = $row->data_fim_disponibilizacao_assinatura_DATETIME;
            $this->data_fim_disponibilizacao_assinatura_DATETIME_UNIX = $row->data_fim_disponibilizacao_assinatura_DATETIME_UNIX;

            $this->id_corporacao_INT = $row->id_corporacao_INT;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->assinatura_id_INT = null;
            $this->antigo_sistema_id_INT = null;
            $this->novo_sistema_id_INT = null;
            $this->data_cadastro_DATETIME = null;
            $this->data_inicio_migracao_DATETIME = null;
            $this->data_fim_migracao_DATETIME = null;
            $this->antigo_sicob_cliente_assinatura_INT = null;
            $this->novo_sicob_cliente_assinatura_INT = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
            $this->nova_assinatura_id_INT = null;
            $this->erro = null;
            $this->estado_assinatura_migracao_id_INT = null;
            $this->data_inicio_disponibilizacao_assinatura_DATETIME = null;
            $this->data_fim_disponibilizacao_assinatura_DATETIME = null;
            $this->id_corporacao_INT = null;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE assinatura_migracao SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO assinatura_migracao ( assinatura_id_INT , antigo_sistema_id_INT , novo_sistema_id_INT , data_cadastro_DATETIME , data_inicio_migracao_DATETIME , data_fim_migracao_DATETIME , antigo_sicob_cliente_assinatura_INT , novo_sicob_cliente_assinatura_INT , excluido_BOOLEAN , excluido_DATETIME , nova_assinatura_id_INT , erro , estado_assinatura_migracao_id_INT , data_inicio_disponibilizacao_assinatura_DATETIME , data_fim_disponibilizacao_assinatura_DATETIME , id_corporacao_INT ) VALUES ( {$this->assinatura_id_INT} , {$this->antigo_sistema_id_INT} , {$this->novo_sistema_id_INT} , {$this->data_cadastro_DATETIME} , {$this->data_inicio_migracao_DATETIME} , {$this->data_fim_migracao_DATETIME} , {$this->antigo_sicob_cliente_assinatura_INT} , {$this->novo_sicob_cliente_assinatura_INT} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} , {$this->nova_assinatura_id_INT} , {$this->erro} , {$this->estado_assinatura_migracao_id_INT} , {$this->data_inicio_disponibilizacao_assinatura_DATETIME} , {$this->data_fim_disponibilizacao_assinatura_DATETIME} , {$this->id_corporacao_INT} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoAssinatura_id_INT()
        {
            return "assinatura_id_INT";
        }

        public function nomeCampoAntigo_sistema_id_INT()
        {
            return "antigo_sistema_id_INT";
        }

        public function nomeCampoNovo_sistema_id_INT()
        {
            return "novo_sistema_id_INT";
        }

        public function nomeCampoData_cadastro_DATETIME()
        {
            return "data_cadastro_DATETIME";
        }

        public function nomeCampoData_inicio_migracao_DATETIME()
        {
            return "data_inicio_migracao_DATETIME";
        }

        public function nomeCampoData_fim_migracao_DATETIME()
        {
            return "data_fim_migracao_DATETIME";
        }

        public function nomeCampoAntigo_sicob_cliente_assinatura_INT()
        {
            return "antigo_sicob_cliente_assinatura_INT";
        }

        public function nomeCampoNovo_sicob_cliente_assinatura_INT()
        {
            return "novo_sicob_cliente_assinatura_INT";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }

        public function nomeCampoNova_assinatura_id_INT()
        {
            return "nova_assinatura_id_INT";
        }

        public function nomeCampoErro()
        {
            return "erro";
        }

        public function nomeCampoEstado_assinatura_migracao_id_INT()
        {
            return "estado_assinatura_migracao_id_INT";
        }

        public function nomeCampoData_inicio_disponibilizacao_assinatura_DATETIME()
        {
            return "data_inicio_disponibilizacao_assinatura_DATETIME";
        }

        public function nomeCampoData_fim_disponibilizacao_assinatura_DATETIME()
        {
            return "data_fim_disponibilizacao_assinatura_DATETIME";
        }

        public function nomeCampoId_corporacao_INT()
        {
            return "id_corporacao_INT";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoAssinatura_id_INT($objArguments)
        {
            $objArguments->nome = "assinatura_id_INT";
            $objArguments->id = "assinatura_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoAntigo_sistema_id_INT($objArguments)
        {
            $objArguments->nome = "antigo_sistema_id_INT";
            $objArguments->id = "antigo_sistema_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoNovo_sistema_id_INT($objArguments)
        {
            $objArguments->nome = "novo_sistema_id_INT";
            $objArguments->id = "novo_sistema_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoData_cadastro_DATETIME($objArguments)
        {
            $objArguments->nome = "data_cadastro_DATETIME";
            $objArguments->id = "data_cadastro_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoData_inicio_migracao_DATETIME($objArguments)
        {
            $objArguments->nome = "data_inicio_migracao_DATETIME";
            $objArguments->id = "data_inicio_migracao_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoData_fim_migracao_DATETIME($objArguments)
        {
            $objArguments->nome = "data_fim_migracao_DATETIME";
            $objArguments->id = "data_fim_migracao_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoAntigo_sicob_cliente_assinatura_INT($objArguments)
        {
            $objArguments->nome = "antigo_sicob_cliente_assinatura_INT";
            $objArguments->id = "antigo_sicob_cliente_assinatura_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoNovo_sicob_cliente_assinatura_INT($objArguments)
        {
            $objArguments->nome = "novo_sicob_cliente_assinatura_INT";
            $objArguments->id = "novo_sicob_cliente_assinatura_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoNova_assinatura_id_INT($objArguments)
        {
            $objArguments->nome = "nova_assinatura_id_INT";
            $objArguments->id = "nova_assinatura_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoErro($objArguments)
        {
            $objArguments->nome = "erro";
            $objArguments->id = "erro";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoEstado_assinatura_migracao_id_INT($objArguments)
        {
            $objArguments->nome = "estado_assinatura_migracao_id_INT";
            $objArguments->id = "estado_assinatura_migracao_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoData_inicio_disponibilizacao_assinatura_DATETIME($objArguments)
        {
            $objArguments->nome = "data_inicio_disponibilizacao_assinatura_DATETIME";
            $objArguments->id = "data_inicio_disponibilizacao_assinatura_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoData_fim_disponibilizacao_assinatura_DATETIME($objArguments)
        {
            $objArguments->nome = "data_fim_disponibilizacao_assinatura_DATETIME";
            $objArguments->id = "data_fim_disponibilizacao_assinatura_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoId_corporacao_INT($objArguments)
        {
            $objArguments->nome = "id_corporacao_INT";
            $objArguments->id = "id_corporacao_INT";

            return $this->campoInteiro($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->assinatura_id_INT == null)
            {
                $this->assinatura_id_INT = "null";
            }

            if ($this->antigo_sistema_id_INT == null)
            {
                $this->antigo_sistema_id_INT = "null";
            }

            if ($this->novo_sistema_id_INT == null)
            {
                $this->novo_sistema_id_INT = "null";
            }

            if ($this->antigo_sicob_cliente_assinatura_INT == null)
            {
                $this->antigo_sicob_cliente_assinatura_INT = "null";
            }

            if ($this->novo_sicob_cliente_assinatura_INT == null)
            {
                $this->novo_sicob_cliente_assinatura_INT = "null";
            }

            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            if ($this->nova_assinatura_id_INT == null)
            {
                $this->nova_assinatura_id_INT = "null";
            }

            if ($this->estado_assinatura_migracao_id_INT == null)
            {
                $this->estado_assinatura_migracao_id_INT = "null";
            }

            if ($this->id_corporacao_INT == null)
            {
                $this->id_corporacao_INT = "null";
            }

            $this->data_cadastro_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_cadastro_DATETIME);
            $this->data_inicio_migracao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicio_migracao_DATETIME);
            $this->data_fim_migracao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_fim_migracao_DATETIME);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
            $this->erro = $this->formatarDadosParaSQL($this->erro);
            $this->data_inicio_disponibilizacao_assinatura_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_inicio_disponibilizacao_assinatura_DATETIME);
            $this->data_fim_disponibilizacao_assinatura_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_fim_disponibilizacao_assinatura_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->data_cadastro_DATETIME = $this->formatarDataTimeParaExibicao($this->data_cadastro_DATETIME);
            $this->data_inicio_migracao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_inicio_migracao_DATETIME);
            $this->data_fim_migracao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_fim_migracao_DATETIME);
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
            $this->data_inicio_disponibilizacao_assinatura_DATETIME = $this->formatarDataTimeParaExibicao($this->data_inicio_disponibilizacao_assinatura_DATETIME);
            $this->data_fim_disponibilizacao_assinatura_DATETIME = $this->formatarDataTimeParaExibicao($this->data_fim_disponibilizacao_assinatura_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("assinatura_id_INT", $this->assinatura_id_INT);
            Helper::setSession("antigo_sistema_id_INT", $this->antigo_sistema_id_INT);
            Helper::setSession("novo_sistema_id_INT", $this->novo_sistema_id_INT);
            Helper::setSession("data_cadastro_DATETIME", $this->data_cadastro_DATETIME);
            Helper::setSession("data_inicio_migracao_DATETIME", $this->data_inicio_migracao_DATETIME);
            Helper::setSession("data_fim_migracao_DATETIME", $this->data_fim_migracao_DATETIME);
            Helper::setSession("antigo_sicob_cliente_assinatura_INT", $this->antigo_sicob_cliente_assinatura_INT);
            Helper::setSession("novo_sicob_cliente_assinatura_INT", $this->novo_sicob_cliente_assinatura_INT);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
            Helper::setSession("nova_assinatura_id_INT", $this->nova_assinatura_id_INT);
            Helper::setSession("erro", $this->erro);
            Helper::setSession("estado_assinatura_migracao_id_INT", $this->estado_assinatura_migracao_id_INT);
            Helper::setSession("data_inicio_disponibilizacao_assinatura_DATETIME", $this->data_inicio_disponibilizacao_assinatura_DATETIME);
            Helper::setSession("data_fim_disponibilizacao_assinatura_DATETIME", $this->data_fim_disponibilizacao_assinatura_DATETIME);
            Helper::setSession("id_corporacao_INT", $this->id_corporacao_INT);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("assinatura_id_INT");
            Helper::clearSession("antigo_sistema_id_INT");
            Helper::clearSession("novo_sistema_id_INT");
            Helper::clearSession("data_cadastro_DATETIME");
            Helper::clearSession("data_inicio_migracao_DATETIME");
            Helper::clearSession("data_fim_migracao_DATETIME");
            Helper::clearSession("antigo_sicob_cliente_assinatura_INT");
            Helper::clearSession("novo_sicob_cliente_assinatura_INT");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
            Helper::clearSession("nova_assinatura_id_INT");
            Helper::clearSession("erro");
            Helper::clearSession("estado_assinatura_migracao_id_INT");
            Helper::clearSession("data_inicio_disponibilizacao_assinatura_DATETIME");
            Helper::clearSession("data_fim_disponibilizacao_assinatura_DATETIME");
            Helper::clearSession("id_corporacao_INT");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = Helper::SESSION("id{$numReg}");
            $this->assinatura_id_INT = Helper::SESSION("assinatura_id_INT{$numReg}");
            $this->antigo_sistema_id_INT = Helper::SESSION("antigo_sistema_id_INT{$numReg}");
            $this->novo_sistema_id_INT = Helper::SESSION("novo_sistema_id_INT{$numReg}");
            $this->data_cadastro_DATETIME = Helper::SESSION("data_cadastro_DATETIME{$numReg}");
            $this->data_inicio_migracao_DATETIME = Helper::SESSION("data_inicio_migracao_DATETIME{$numReg}");
            $this->data_fim_migracao_DATETIME = Helper::SESSION("data_fim_migracao_DATETIME{$numReg}");
            $this->antigo_sicob_cliente_assinatura_INT = Helper::SESSION("antigo_sicob_cliente_assinatura_INT{$numReg}");
            $this->novo_sicob_cliente_assinatura_INT = Helper::SESSION("novo_sicob_cliente_assinatura_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::SESSION("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::SESSION("excluido_DATETIME{$numReg}");
            $this->nova_assinatura_id_INT = Helper::SESSION("nova_assinatura_id_INT{$numReg}");
            $this->erro = Helper::SESSION("erro{$numReg}");
            $this->estado_assinatura_migracao_id_INT = Helper::SESSION("estado_assinatura_migracao_id_INT{$numReg}");
            $this->data_inicio_disponibilizacao_assinatura_DATETIME = Helper::SESSION("data_inicio_disponibilizacao_assinatura_DATETIME{$numReg}");
            $this->data_fim_disponibilizacao_assinatura_DATETIME = Helper::SESSION("data_fim_disponibilizacao_assinatura_DATETIME{$numReg}");
            $this->id_corporacao_INT = Helper::SESSION("id_corporacao_INT{$numReg}");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = Helper::POST("id{$numReg}");
            $this->assinatura_id_INT = Helper::POST("assinatura_id_INT{$numReg}");
            $this->antigo_sistema_id_INT = Helper::POST("antigo_sistema_id_INT{$numReg}");
            $this->novo_sistema_id_INT = Helper::POST("novo_sistema_id_INT{$numReg}");
            $this->data_cadastro_DATETIME = Helper::POST("data_cadastro_DATETIME{$numReg}");
            $this->data_inicio_migracao_DATETIME = Helper::POST("data_inicio_migracao_DATETIME{$numReg}");
            $this->data_fim_migracao_DATETIME = Helper::POST("data_fim_migracao_DATETIME{$numReg}");
            $this->antigo_sicob_cliente_assinatura_INT = Helper::POST("antigo_sicob_cliente_assinatura_INT{$numReg}");
            $this->novo_sicob_cliente_assinatura_INT = Helper::POST("novo_sicob_cliente_assinatura_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::POST("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::POST("excluido_DATETIME{$numReg}");
            $this->nova_assinatura_id_INT = Helper::POST("nova_assinatura_id_INT{$numReg}");
            $this->erro = Helper::POST("erro{$numReg}");
            $this->estado_assinatura_migracao_id_INT = Helper::POST("estado_assinatura_migracao_id_INT{$numReg}");
            $this->data_inicio_disponibilizacao_assinatura_DATETIME = Helper::POST("data_inicio_disponibilizacao_assinatura_DATETIME{$numReg}");
            $this->data_fim_disponibilizacao_assinatura_DATETIME = Helper::POST("data_fim_disponibilizacao_assinatura_DATETIME{$numReg}");
            $this->id_corporacao_INT = Helper::POST("id_corporacao_INT{$numReg}");
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = Helper::GET("id{$numReg}");
            $this->assinatura_id_INT = Helper::GET("assinatura_id_INT{$numReg}");
            $this->antigo_sistema_id_INT = Helper::GET("antigo_sistema_id_INT{$numReg}");
            $this->novo_sistema_id_INT = Helper::GET("novo_sistema_id_INT{$numReg}");
            $this->data_cadastro_DATETIME = Helper::GET("data_cadastro_DATETIME{$numReg}");
            $this->data_inicio_migracao_DATETIME = Helper::GET("data_inicio_migracao_DATETIME{$numReg}");
            $this->data_fim_migracao_DATETIME = Helper::GET("data_fim_migracao_DATETIME{$numReg}");
            $this->antigo_sicob_cliente_assinatura_INT = Helper::GET("antigo_sicob_cliente_assinatura_INT{$numReg}");
            $this->novo_sicob_cliente_assinatura_INT = Helper::GET("novo_sicob_cliente_assinatura_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::GET("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::GET("excluido_DATETIME{$numReg}");
            $this->nova_assinatura_id_INT = Helper::GET("nova_assinatura_id_INT{$numReg}");
            $this->erro = Helper::GET("erro{$numReg}");
            $this->estado_assinatura_migracao_id_INT = Helper::GET("estado_assinatura_migracao_id_INT{$numReg}");
            $this->data_inicio_disponibilizacao_assinatura_DATETIME = Helper::GET("data_inicio_disponibilizacao_assinatura_DATETIME{$numReg}");
            $this->data_fim_disponibilizacao_assinatura_DATETIME = Helper::GET("data_fim_disponibilizacao_assinatura_DATETIME{$numReg}");
            $this->id_corporacao_INT = Helper::GET("id_corporacao_INT{$numReg}");
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["assinatura_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "assinatura_id_INT = $this->assinatura_id_INT, ";
            }

            if (isset($tipo["antigo_sistema_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "antigo_sistema_id_INT = $this->antigo_sistema_id_INT, ";
            }

            if (isset($tipo["novo_sistema_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "novo_sistema_id_INT = $this->novo_sistema_id_INT, ";
            }

            if (isset($tipo["data_cadastro_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "data_cadastro_DATETIME = $this->data_cadastro_DATETIME, ";
            }

            if (isset($tipo["data_inicio_migracao_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "data_inicio_migracao_DATETIME = $this->data_inicio_migracao_DATETIME, ";
            }

            if (isset($tipo["data_fim_migracao_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "data_fim_migracao_DATETIME = $this->data_fim_migracao_DATETIME, ";
            }

            if (isset($tipo["antigo_sicob_cliente_assinatura_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "antigo_sicob_cliente_assinatura_INT = $this->antigo_sicob_cliente_assinatura_INT, ";
            }

            if (isset($tipo["novo_sicob_cliente_assinatura_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "novo_sicob_cliente_assinatura_INT = $this->novo_sicob_cliente_assinatura_INT, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            if (isset($tipo["nova_assinatura_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "nova_assinatura_id_INT = $this->nova_assinatura_id_INT, ";
            }

            if (isset($tipo["erro{$numReg}"]) || $tipo == null)
            {
                $upd .= "erro = $this->erro, ";
            }

            if (isset($tipo["estado_assinatura_migracao_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "estado_assinatura_migracao_id_INT = $this->estado_assinatura_migracao_id_INT, ";
            }

            if (isset($tipo["data_inicio_disponibilizacao_assinatura_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "data_inicio_disponibilizacao_assinatura_DATETIME = $this->data_inicio_disponibilizacao_assinatura_DATETIME, ";
            }

            if (isset($tipo["data_fim_disponibilizacao_assinatura_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "data_fim_disponibilizacao_assinatura_DATETIME = $this->data_fim_disponibilizacao_assinatura_DATETIME, ";
            }

            if (isset($tipo["id_corporacao_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "id_corporacao_INT = $this->id_corporacao_INT, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE assinatura_migracao SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim

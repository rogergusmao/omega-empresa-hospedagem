<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Backup_automatico_controle
    * DATA DE GERA��O: 25.08.2010
    * ARQUIVO:         DAO_Backup_automatico_controle.php
    * TABELA MYSQL:    backup_automatico_controle
    * BANCO DE DADOS:  DEP_pesquisas_config
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Backup_automatico_controle extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $hora_base_TIME;
        public $hora_real_TIME;
        public $data_DATE;

        public $nomeEntidade;

        public $data_DATE_UNIX;

        public $label_id;
        public $label_hora_base_TIME;
        public $label_hora_real_TIME;
        public $label_data_DATE;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "controle de backup autom�tico";
            $this->nomeTabela = "backup_automatico_controle";
            $this->campoId = "id";
            $this->campoLabel = "id";
        }

        public function valorCampoLabel()
        {
            return $this->getId();
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O controle de backup autom�tico foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O controle de backup autom�tico foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O controle de backup autom�tico foi modificado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);
                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O controle de backup autom�tico foi exclu�do com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_backup_automatico_controle", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_backup_automatico_controle", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getHora_base_TIME()
        {
            return $this->hora_base_TIME;
        }

        public function getHora_real_TIME()
        {
            return $this->hora_real_TIME;
        }

        function getData_DATE_UNIX()
        {
            return $this->data_DATE_UNIX;
        }

        public function getData_DATE()
        {
            return $this->data_DATE;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setHora_base_TIME($val)
        {
            $this->hora_base_TIME = $val;
        }

        function setHora_real_TIME($val)
        {
            $this->hora_real_TIME = $val;
        }

        function setData_DATE($val)
        {
            $this->data_DATE = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(data_DATE) AS data_DATE_UNIX FROM backup_automatico_controle WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = mysqli_fetch_object($result);

            $this->id = $row->id;

            $this->hora_base_TIME = $row->hora_base_TIME;

            $this->hora_real_TIME = $row->hora_real_TIME;

            $this->data_DATE = $row->data_DATE;
            $this->data_DATE_UNIX = $row->data_DATE_UNIX;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "DELETE FROM backup_automatico_controle WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO backup_automatico_controle ( hora_base_TIME,hora_real_TIME,data_DATE ) VALUES ( '$this->hora_base_TIME','$this->hora_real_TIME',$this->data_DATE )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoHora_base_TIME()
        {
            return "hora_base_TIME";
        }

        public function nomeCampoHora_real_TIME()
        {
            return "hora_real_TIME";
        }

        public function nomeCampoData_DATE()
        {
            return "data_DATE";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoHora_base_TIME($objArguments)
        {
            $objArguments->nome = "hora_base_TIME";
            $objArguments->id = "hora_base_TIME";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoHora_real_TIME($objArguments)
        {
            $objArguments->nome = "hora_real_TIME";
            $objArguments->id = "hora_real_TIME";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoData_DATE($objArguments)
        {
            $objArguments->nome = "data_DATE";
            $objArguments->id = "data_DATE";

            return $this->campoData($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            $this->data_DATE = $this->formatarDataParaComandoSQL($this->data_DATE);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->data_DATE = $this->formatarDataParaExibicao($this->data_DATE);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["hora_base_TIME"] = $this->hora_base_TIME;
            $_SESSION["hora_real_TIME"] = $this->hora_real_TIME;
            $_SESSION["data_DATE"] = $this->data_DATE;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["hora_base_TIME"]);
            unset($_SESSION["hora_real_TIME"]);
            unset($_SESSION["data_DATE"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->hora_base_TIME = $this->formatarDados($_SESSION["hora_base_TIME{$numReg}"]);
            $this->hora_real_TIME = $this->formatarDados($_SESSION["hora_real_TIME{$numReg}"]);
            $this->data_DATE = $this->formatarDados($_SESSION["data_DATE{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->hora_base_TIME = $this->formatarDados($_POST["hora_base_TIME{$numReg}"]);
            $this->hora_real_TIME = $this->formatarDados($_POST["hora_real_TIME{$numReg}"]);
            $this->data_DATE = $this->formatarDados($_POST["data_DATE{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->hora_base_TIME = $this->formatarDados($_GET["hora_base_TIME{$numReg}"]);
            $this->hora_real_TIME = $this->formatarDados($_GET["hora_real_TIME{$numReg}"]);
            $this->data_DATE = $this->formatarDados($_GET["data_DATE{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["hora_base_TIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "hora_base_TIME = '$this->hora_base_TIME', ";
            }

            if (isset($tipo["hora_real_TIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "hora_real_TIME = '$this->hora_real_TIME', ";
            }

            if (isset($tipo["data_DATE{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "data_DATE = $this->data_DATE, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE backup_automatico_controle SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }


        //*******************************************************************
        //METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
        //*******************************************************************

        public function getTodosIDs()
        {
            $sql = "SELECT id FROM backup_automatico_controle";

            $result = $this->database->query($sql);

            $numeroRegistros = mysqli_num_rows($result);

            for ($i = 0; $i < $numeroRegistros; $i++)
            {
                $retorno[ $i ] = Database::mysqli_result($result, $i, 0);
            }

            return $retorno;
        }



        // ***************************************************************************************************
        // M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
        // ***************************************************************************************************

        function selectLike($field, $val)
        {
            $field = $this->formatarDados($field);
            $val = $this->formatarDados($val);

            $sql = "SELECT * FROM backup_automatico_controle WHERE $field LIKE '%$val%'";
            $this->database->query($sql);

            return $this->database->fetchObject();
        }


        // ***************************************************************************************************
        // METODO QUE ATUALIZA UM CAMPO
        // ***************************************************************************************************

        function updateCampo($id, $campo, $valor)
        {
            if (substr_count($campo, "_FLOAT") > 0 || substr_count($campo, "_INT") > 0 || substr_count($campo, "_BOOLEAN") > 0)
            {
                $aspas = "";
            }
            else
            {
                $aspas = "'";
            }

            $sql = "UPDATE backup_automatico_controle SET $campo = $aspas$valor$aspas WHERE id = $id";

            $this->database->query($sql);
        }

    } // classe: fim

?>
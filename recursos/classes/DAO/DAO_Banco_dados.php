<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Banco_dados
    * DATA DE GERA��O: 16.08.2010
    * ARQUIVO:         DAO_Banco_dados.php
    * TABELA MYSQL:    banco_dados
    * BANCO DE DADOS:  DEP_pesquisas_config
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Banco_dados extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $nome_arquivo;
        public $descricao;
        public $resposta_mysql;
        public $data_criacao_DATETIME;
        public $data_ultima_restauracao_DATETIME;

        public $nomeEntidade;

        public $data_criacao_DATETIME_UNIX;
        public $data_ultima_restauracao_DATETIME_UNIX;

        public $label_id;
        public $label_nome_arquivo;
        public $label_descricao;
        public $label_resposta_mysql;
        public $label_data_criacao_DATETIME;
        public $label_data_ultima_restauracao_DATETIME;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "backup do banco de dados";
            $this->nomeTabela = "banco_dados";
            $this->campoId = "id";
            $this->campoLabel = "nome_arquivo";
        }

        public function valorCampoLabel()
        {
            return $this->getNome_arquivo();
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O backup do banco de dados foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O backup do banco de dados foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O backup do banco de dados foi modificado com sucesso.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O backup do banco de dados foi exclu�do com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_banco_dados", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_banco_dados", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        // **********************
        // M�TODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getNome_arquivo()
        {
            return $this->nome_arquivo;
        }

        public function getDescricao()
        {
            return $this->descricao;
        }

        public function getResposta_mysql()
        {
            return $this->resposta_mysql;
        }

        function getData_criacao_DATETIME_UNIX()
        {
            return $this->data_criacao_DATETIME_UNIX;
        }

        public function getData_criacao_DATETIME()
        {
            return $this->data_criacao_DATETIME;
        }

        function getData_ultima_restauracao_DATETIME_UNIX()
        {
            return $this->data_ultima_restauracao_DATETIME_UNIX;
        }

        public function getData_ultima_restauracao_DATETIME()
        {
            return $this->data_ultima_restauracao_DATETIME;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setNome_arquivo($val)
        {
            $this->nome_arquivo = $val;
        }

        function setDescricao($val)
        {
            $this->descricao = $val;
        }

        function setResposta_mysql($val)
        {
            $this->resposta_mysql = $val;
        }

        function setData_criacao_DATETIME($val)
        {
            $this->data_criacao_DATETIME = $val;
        }

        function setData_ultima_restauracao_DATETIME($val)
        {
            $this->data_ultima_restauracao_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(data_criacao_DATETIME) AS data_criacao_DATETIME_UNIX, UNIX_TIMESTAMP(data_ultima_restauracao_DATETIME) AS data_ultima_restauracao_DATETIME_UNIX FROM banco_dados WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = mysqli_fetch_object($result);

            $this->id = $row->id;

            $this->nome_arquivo = $row->nome_arquivo;

            $this->descricao = $row->descricao;

            $this->resposta_mysql = $row->resposta_mysql;

            $this->data_criacao_DATETIME = $row->data_criacao_DATETIME;
            $this->data_criacao_DATETIME_UNIX = $row->data_criacao_DATETIME_UNIX;

            $this->data_ultima_restauracao_DATETIME = $row->data_ultima_restauracao_DATETIME;
            $this->data_ultima_restauracao_DATETIME_UNIX = $row->data_ultima_restauracao_DATETIME_UNIX;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "DELETE FROM banco_dados WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO banco_dados ( nome_arquivo,descricao,resposta_mysql,data_criacao_DATETIME,data_ultima_restauracao_DATETIME ) VALUES ( '$this->nome_arquivo','$this->descricao','$this->resposta_mysql',$this->data_criacao_DATETIME,$this->data_ultima_restauracao_DATETIME )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome_arquivo()
        {
            return "nome_arquivo";
        }

        public function nomeCampoDescricao()
        {
            return "descricao";
        }

        public function nomeCampoResposta_mysql()
        {
            return "resposta_mysql";
        }

        public function nomeCampoData_criacao_DATETIME()
        {
            return "data_criacao_DATETIME";
        }

        public function nomeCampoData_ultima_restauracao_DATETIME()
        {
            return "data_ultima_restauracao_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome_arquivo($objArguments)
        {
            $objArguments->nome = "nome_arquivo";
            $objArguments->id = "nome_arquivo";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoDescricao($objArguments)
        {
            $objArguments->nome = "descricao";
            $objArguments->id = "descricao";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoResposta_mysql($objArguments)
        {
            $objArguments->nome = "resposta_mysql";
            $objArguments->id = "resposta_mysql";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoData_criacao_DATETIME($objArguments)
        {
            $objArguments->nome = "data_criacao_DATETIME";
            $objArguments->id = "data_criacao_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoData_ultima_restauracao_DATETIME($objArguments)
        {
            $objArguments->nome = "data_ultima_restauracao_DATETIME";
            $objArguments->id = "data_ultima_restauracao_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            $this->data_criacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_criacao_DATETIME);
            $this->data_ultima_restauracao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_ultima_restauracao_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->data_criacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_criacao_DATETIME);
            $this->data_ultima_restauracao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_ultima_restauracao_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["nome_arquivo"] = $this->nome_arquivo;
            $_SESSION["descricao"] = $this->descricao;
            $_SESSION["resposta_mysql"] = $this->resposta_mysql;
            $_SESSION["data_criacao_DATETIME"] = $this->data_criacao_DATETIME;
            $_SESSION["data_ultima_restauracao_DATETIME"] = $this->data_ultima_restauracao_DATETIME;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["nome_arquivo"]);
            unset($_SESSION["descricao"]);
            unset($_SESSION["resposta_mysql"]);
            unset($_SESSION["data_criacao_DATETIME"]);
            unset($_SESSION["data_ultima_restauracao_DATETIME"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->nome_arquivo = $this->formatarDados($_SESSION["nome_arquivo{$numReg}"]);
            $this->descricao = $this->formatarDados($_SESSION["descricao{$numReg}"]);
            $this->resposta_mysql = $this->formatarDados($_SESSION["resposta_mysql{$numReg}"]);
            $this->data_criacao_DATETIME = $this->formatarDados($_SESSION["data_criacao_DATETIME{$numReg}"]);
            $this->data_ultima_restauracao_DATETIME = $this->formatarDados($_SESSION["data_ultima_restauracao_DATETIME{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->nome_arquivo = $this->formatarDados($_POST["nome_arquivo{$numReg}"]);
            $this->descricao = $this->formatarDados($_POST["descricao{$numReg}"]);
            $this->resposta_mysql = $this->formatarDados($_POST["resposta_mysql{$numReg}"]);
            $this->data_criacao_DATETIME = $this->formatarDados($_POST["data_criacao_DATETIME{$numReg}"]);
            $this->data_ultima_restauracao_DATETIME = $this->formatarDados($_POST["data_ultima_restauracao_DATETIME{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->nome_arquivo = $this->formatarDados($_GET["nome_arquivo{$numReg}"]);
            $this->descricao = $this->formatarDados($_GET["descricao{$numReg}"]);
            $this->resposta_mysql = $this->formatarDados($_GET["resposta_mysql{$numReg}"]);
            $this->data_criacao_DATETIME = $this->formatarDados($_GET["data_criacao_DATETIME{$numReg}"]);
            $this->data_ultima_restauracao_DATETIME = $this->formatarDados($_GET["data_ultima_restauracao_DATETIME{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["nome_arquivo{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "nome_arquivo = '$this->nome_arquivo', ";
            }

            if (isset($tipo["descricao{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "descricao = '$this->descricao', ";
            }

            if (isset($tipo["resposta_mysql{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "resposta_mysql = '$this->resposta_mysql', ";
            }

            if (isset($tipo["data_criacao_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "data_criacao_DATETIME = $this->data_criacao_DATETIME, ";
            }

            if (isset($tipo["data_ultima_restauracao_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "data_ultima_restauracao_DATETIME = $this->data_ultima_restauracao_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE banco_dados SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }


        //*******************************************************************
        //METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
        //*******************************************************************

        public function getTodosIDs()
        {
            $sql = "SELECT id FROM banco_dados";

            $result = $this->database->query($sql);
            $numeroRegistros = mysqli_num_rows($result);

            for ($i = 0; $i < $numeroRegistros; $i++)
            {
                $retorno[ $i ] = Database::mysqli_result($result, $i, 0);
            }

            return $retorno;
        }



        // ***************************************************************************************************
        // M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
        // ***************************************************************************************************

        function selectLike($field, $val)
        {
            $field = $this->formatarDados($field);
            $val = $this->formatarDados($val);

            $sql = "SELECT * FROM banco_dados WHERE $field LIKE '%$val%'";
            $this->database->query($sql);

            return $this->database->fetchObject();
        }


        // ***************************************************************************************************
        // METODO QUE ATUALIZA UM CAMPO
        // ***************************************************************************************************

        function updateCampo($id, $campo, $valor)
        {
            if (substr_count($campo, "_FLOAT") > 0 || substr_count($campo, "_INT") > 0 || substr_count($campo, "_BOOLEAN") > 0)
            {
                $aspas = "";
            }
            else
            {
                $aspas = "'";
            }

            $sql = "UPDATE banco_dados SET $campo = $aspas$valor$aspas WHERE id = $id";

            $this->database->query($sql);
        }

    } // classe: fim

?>
<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Configuracao
    * DATA DE GERAÇÃO: 13.10.2011
    * ARQUIVO:         DAO_Configuracao.php
    * TABELA MYSQL:    configuracao
    * BANCO DE DADOS:  estetica_solution
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Configuracao extends Generic_DAO
    {

        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************

        public $id;
        public $nome_projeto;
        public $url_site;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_nome_projeto;
        public $label_url_site;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // MÉTODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "configuração";
            $this->nomeTabela = "configuracao";
            $this->campoId = "id";
            $this->campoLabel = "nome_projeto";
        }

        public function valorCampoLabel()
        {
            return $this->getNome_projeto();
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A configuração foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A configuração foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A configuração foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);
                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A configuração foi excluída com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_configuracao", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_configuracao", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete("$registroRemover");

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // MÉTODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getNome_projeto()
        {
            return $this->nome_projeto;
        }

        public function getUrl_site()
        {
            return $this->url_site;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // MÉTODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setNome_projeto($val)
        {
            $this->nome_projeto = $val;
        }

        function setUrl_site($val)
        {
            $this->url_site = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM configuracao WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->nome_projeto = $row->nome_projeto;

            $this->url_site = $row->url_site;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE configuracao SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************
        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO configuracao ( nome_projeto,url_site,excluido_BOOLEAN,excluido_DATETIME ) VALUES ( '$this->nome_projeto','$this->url_site',$this->excluido_BOOLEAN,$this->excluido_DATETIME )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************
        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome_projeto()
        {
            return "nome_projeto";
        }

        public function nomeCampoUrl_site()
        {
            return "url_site";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome_projeto($objArguments)
        {
            $objArguments->nome = "nome_projeto";
            $objArguments->id = "nome_projeto";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoUrl_site($objArguments)
        {
            $objArguments->nome = "url_site";
            $objArguments->id = "url_site";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->excluido_BOOLEAN == "")
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["nome_projeto"] = $this->nome_projeto;
            $_SESSION["url_site"] = $this->url_site;
            $_SESSION["excluido_BOOLEAN"] = $this->excluido_BOOLEAN;
            $_SESSION["excluido_DATETIME"] = $this->excluido_DATETIME;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["nome_projeto"]);
            unset($_SESSION["url_site"]);
            unset($_SESSION["excluido_BOOLEAN"]);
            unset($_SESSION["excluido_DATETIME"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->nome_projeto = $this->formatarDados($_SESSION["nome_projeto{$numReg}"]);
            $this->url_site = $this->formatarDados($_SESSION["url_site{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_SESSION["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_SESSION["excluido_DATETIME{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->nome_projeto = $this->formatarDados($_POST["nome_projeto{$numReg}"]);
            $this->url_site = $this->formatarDados($_POST["url_site{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_POST["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_POST["excluido_DATETIME{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->nome_projeto = $this->formatarDados($_GET["nome_projeto{$numReg}"]);
            $this->url_site = $this->formatarDados($_GET["url_site{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_GET["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_GET["excluido_DATETIME{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["nome_projeto{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "nome_projeto = '$this->nome_projeto', ";
            }

            if (isset($tipo["url_site{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "url_site = '$this->url_site', ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE configuracao SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>

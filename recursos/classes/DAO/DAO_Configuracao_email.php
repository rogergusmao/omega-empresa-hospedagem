<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Configuracao_email
    * DATA DE GERAÇÃO: 04.08.2011
    * ARQUIVO:         DAO_Configuracao_email.php
    * TABELA MYSQL:    configuracao_email
    * BANCO DE DADOS:  email_marketing
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Configuracao_email extends Generic_DAO
    {

        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************

        public $id;
        public $nome_remetente;
        public $endereco_remetente;
        public $return_path;
        public $usuario_return_path;
        public $senha_return_path;
        public $numero_de_emails_por_vez_INT;
        public $intervalo_turno_em_segundos_INT;

        public $nomeEntidade;

        public $label_id;
        public $label_nome_remetente;
        public $label_endereco_remetente;
        public $label_return_path;
        public $label_usuario_return_path;
        public $label_senha_return_path;
        public $label_numero_de_emails_por_vez_INT;
        public $label_intervalo_turno_em_segundos_INT;

        // **********************
        // MÉTODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "";
            $this->nomeTabela = "configuracao_email";
            $this->campoId = "id";
            $this->campoLabel = "nome_remetente";
        }

        public function valorCampoLabel()
        {
            return $this->getNome_remetente();
        }

        public function __actionAdd()
        {
            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = "";

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = "";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = "";

            $urlSuccess = Helper::getUrlAction("list_configuracao_email", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_configuracao_email", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }




        // **********************
        // MÉTODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getNome_remetente()
        {
            return $this->nome_remetente;
        }

        public function getEndereco_remetente()
        {
            return $this->endereco_remetente;
        }

        public function getReturn_path()
        {
            return $this->return_path;
        }

        public function getUsuario_return_path()
        {
            return $this->usuario_return_path;
        }

        public function getSenha_return_path()
        {
            return $this->senha_return_path;
        }

        public function getNumero_de_emails_por_vez_INT()
        {
            return $this->numero_de_emails_por_vez_INT;
        }

        public function getIntervalo_turno_em_segundos_INT()
        {
            return $this->intervalo_turno_em_segundos_INT;
        }

        // **********************
        // MÉTODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setNome_remetente($val)
        {
            $this->nome_remetente = $val;
        }

        function setEndereco_remetente($val)
        {
            $this->endereco_remetente = $val;
        }

        function setReturn_path($val)
        {
            $this->return_path = $val;
        }

        function setUsuario_return_path($val)
        {
            $this->usuario_return_path = $val;
        }

        function setSenha_return_path($val)
        {
            $this->senha_return_path = $val;
        }

        function setNumero_de_emails_por_vez_INT($val)
        {
            $this->numero_de_emails_por_vez_INT = $val;
        }

        function setIntervalo_turno_em_segundos_INT($val)
        {
            $this->intervalo_turno_em_segundos_INT = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT *  FROM configuracao_email WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->nome_remetente = $row->nome_remetente;

            $this->endereco_remetente = $row->endereco_remetente;

            $this->return_path = $row->return_path;

            $this->usuario_return_path = $row->usuario_return_path;

            $this->senha_return_path = $row->senha_return_path;

            $this->numero_de_emails_por_vez_INT = $row->numero_de_emails_por_vez_INT;

            $this->intervalo_turno_em_segundos_INT = $row->intervalo_turno_em_segundos_INT;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "DELETE FROM configuracao_email WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO configuracao_email ( nome_remetente,endereco_remetente,return_path,usuario_return_path,senha_return_path,numero_de_emails_por_vez_INT,intervalo_turno_em_segundos_INT ) VALUES ( '$this->nome_remetente','$this->endereco_remetente','$this->return_path','$this->usuario_return_path','$this->senha_return_path',$this->numero_de_emails_por_vez_INT,$this->intervalo_turno_em_segundos_INT )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome_remetente()
        {
            return "nome_remetente";
        }

        public function nomeCampoEndereco_remetente()
        {
            return "endereco_remetente";
        }

        public function nomeCampoReturn_path()
        {
            return "return_path";
        }

        public function nomeCampoUsuario_return_path()
        {
            return "usuario_return_path";
        }

        public function nomeCampoSenha_return_path()
        {
            return "senha_return_path";
        }

        public function nomeCampoNumero_de_emails_por_vez_INT()
        {
            return "numero_de_emails_por_vez_INT";
        }

        public function nomeCampoIntervalo_turno_em_segundos_INT()
        {
            return "intervalo_turno_em_segundos_INT";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome_remetente($objArguments)
        {
            $objArguments->nome = "nome_remetente";
            $objArguments->id = "nome_remetente";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoEndereco_remetente($objArguments)
        {
            $objArguments->nome = "endereco_remetente";
            $objArguments->id = "endereco_remetente";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoReturn_path($objArguments)
        {
            $objArguments->nome = "return_path";
            $objArguments->id = "return_path";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoUsuario_return_path($objArguments)
        {
            $objArguments->nome = "usuario_return_path";
            $objArguments->id = "usuario_return_path";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoSenha_return_path($objArguments)
        {
            $objArguments->nome = "senha_return_path";
            $objArguments->id = "senha_return_path";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoNumero_de_emails_por_vez_INT($objArguments)
        {
            $objArguments->nome = "numero_de_emails_por_vez_INT";
            $objArguments->id = "numero_de_emails_por_vez_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoIntervalo_turno_em_segundos_INT($objArguments)
        {
            $objArguments->nome = "intervalo_turno_em_segundos_INT";
            $objArguments->id = "intervalo_turno_em_segundos_INT";

            return $this->campoInteiro($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->numero_de_emails_por_vez_INT == "")
            {
                $this->numero_de_emails_por_vez_INT = "null";
            }

            if ($this->intervalo_turno_em_segundos_INT == "")
            {
                $this->intervalo_turno_em_segundos_INT = "null";
            }
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
        //****************************************************************************

        public function formatarParaExibicao()
        {
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["nome_remetente"] = $this->nome_remetente;
            $_SESSION["endereco_remetente"] = $this->endereco_remetente;
            $_SESSION["return_path"] = $this->return_path;
            $_SESSION["usuario_return_path"] = $this->usuario_return_path;
            $_SESSION["senha_return_path"] = $this->senha_return_path;
            $_SESSION["numero_de_emails_por_vez_INT"] = $this->numero_de_emails_por_vez_INT;
            $_SESSION["intervalo_turno_em_segundos_INT"] = $this->intervalo_turno_em_segundos_INT;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["nome_remetente"]);
            unset($_SESSION["endereco_remetente"]);
            unset($_SESSION["return_path"]);
            unset($_SESSION["usuario_return_path"]);
            unset($_SESSION["senha_return_path"]);
            unset($_SESSION["numero_de_emails_por_vez_INT"]);
            unset($_SESSION["intervalo_turno_em_segundos_INT"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->nome_remetente = $this->formatarDados($_SESSION["nome_remetente{$numReg}"]);
            $this->endereco_remetente = $this->formatarDados($_SESSION["endereco_remetente{$numReg}"]);
            $this->return_path = $this->formatarDados($_SESSION["return_path{$numReg}"]);
            $this->usuario_return_path = $this->formatarDados($_SESSION["usuario_return_path{$numReg}"]);
            $this->senha_return_path = $this->formatarDados($_SESSION["senha_return_path{$numReg}"]);
            $this->numero_de_emails_por_vez_INT = $this->formatarDados($_SESSION["numero_de_emails_por_vez_INT{$numReg}"]);
            $this->intervalo_turno_em_segundos_INT = $this->formatarDados($_SESSION["intervalo_turno_em_segundos_INT{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->nome_remetente = $this->formatarDados($_POST["nome_remetente{$numReg}"]);
            $this->endereco_remetente = $this->formatarDados($_POST["endereco_remetente{$numReg}"]);
            $this->return_path = $this->formatarDados($_POST["return_path{$numReg}"]);
            $this->usuario_return_path = $this->formatarDados($_POST["usuario_return_path{$numReg}"]);
            $this->senha_return_path = $this->formatarDados($_POST["senha_return_path{$numReg}"]);
            $this->numero_de_emails_por_vez_INT = $this->formatarDados($_POST["numero_de_emails_por_vez_INT{$numReg}"]);
            $this->intervalo_turno_em_segundos_INT = $this->formatarDados($_POST["intervalo_turno_em_segundos_INT{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->nome_remetente = $this->formatarDados($_GET["nome_remetente{$numReg}"]);
            $this->endereco_remetente = $this->formatarDados($_GET["endereco_remetente{$numReg}"]);
            $this->return_path = $this->formatarDados($_GET["return_path{$numReg}"]);
            $this->usuario_return_path = $this->formatarDados($_GET["usuario_return_path{$numReg}"]);
            $this->senha_return_path = $this->formatarDados($_GET["senha_return_path{$numReg}"]);
            $this->numero_de_emails_por_vez_INT = $this->formatarDados($_GET["numero_de_emails_por_vez_INT{$numReg}"]);
            $this->intervalo_turno_em_segundos_INT = $this->formatarDados($_GET["intervalo_turno_em_segundos_INT{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["nome_remetente{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "nome_remetente = '$this->nome_remetente', ";
            }

            if (isset($tipo["endereco_remetente{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "endereco_remetente = '$this->endereco_remetente', ";
            }

            if (isset($tipo["return_path{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "return_path = '$this->return_path', ";
            }

            if (isset($tipo["usuario_return_path{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "usuario_return_path = '$this->usuario_return_path', ";
            }

            if (isset($tipo["senha_return_path{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "senha_return_path = '$this->senha_return_path', ";
            }

            if (isset($tipo["numero_de_emails_por_vez_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "numero_de_emails_por_vez_INT = $this->numero_de_emails_por_vez_INT, ";
            }

            if (isset($tipo["intervalo_turno_em_segundos_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "intervalo_turno_em_segundos_INT = $this->intervalo_turno_em_segundos_INT, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE configuracao_email SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>

<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Db_tipo
    * DATA DE GERAÇÃO: 17.08.2012
    * ARQUIVO:         DAO_Db_tipo.php
    * TABELA MYSQL:    db_tipo
    * BANCO DE DADOS:  omegasoftware_interno
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Db_tipo extends Generic_DAO
    {

        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************

        public $id;
        public $nome;
        public $porta_padrao_INT;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_nome;
        public $label_porta_padrao_INT;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // MÉTODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "tipo de bancos de dados";
            $this->nomeTabela = "db_tipo";
            $this->campoId = "id";
            $this->campoLabel = "nome";
        }

        public function valorCampoLabel()
        {
            return $this->getNome();
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de bancos de dados foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de bancos de dados foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de bancos de dados foi modificado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de bancos de dados foi excluído com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_db_tipo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_db_tipo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete("$registroRemover");

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // MÉTODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getNome()
        {
            return $this->nome;
        }

        public function getPorta_padrao_INT()
        {
            return $this->porta_padrao_INT;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // MÉTODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setNome($val)
        {
            $this->nome = $val;
        }

        function setPorta_padrao_INT($val)
        {
            $this->porta_padrao_INT = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM db_tipo WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->nome = $row->nome;

            $this->porta_padrao_INT = $row->porta_padrao_INT;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE db_tipo SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO db_tipo ( nome,porta_padrao_INT,excluido_BOOLEAN,excluido_DATETIME ) VALUES ( '$this->nome',$this->porta_padrao_INT,$this->excluido_BOOLEAN,$this->excluido_DATETIME )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome()
        {
            return "nome";
        }

        public function nomeCampoPorta_padrao_INT()
        {
            return "porta_padrao_INT";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome($objArguments)
        {
            $objArguments->nome = "nome";
            $objArguments->id = "nome";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPorta_padrao_INT($objArguments)
        {
            $objArguments->nome = "porta_padrao_INT";
            $objArguments->id = "porta_padrao_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->porta_padrao_INT == "")
            {
                $this->porta_padrao_INT = "null";
            }

            if ($this->excluido_BOOLEAN == "")
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->nome = $this->formatarDados($this->nome);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["nome"] = $this->nome;
            $_SESSION["porta_padrao_INT"] = $this->porta_padrao_INT;
            $_SESSION["excluido_BOOLEAN"] = $this->excluido_BOOLEAN;
            $_SESSION["excluido_DATETIME"] = $this->excluido_DATETIME;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["nome"]);
            unset($_SESSION["porta_padrao_INT"]);
            unset($_SESSION["excluido_BOOLEAN"]);
            unset($_SESSION["excluido_DATETIME"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]);
            $this->porta_padrao_INT = $this->formatarDados($_SESSION["porta_padrao_INT{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_SESSION["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_SESSION["excluido_DATETIME{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->nome = $this->formatarDados($_POST["nome{$numReg}"]);
            $this->porta_padrao_INT = $this->formatarDados($_POST["porta_padrao_INT{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_POST["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_POST["excluido_DATETIME{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->nome = $this->formatarDados($_GET["nome{$numReg}"]);
            $this->porta_padrao_INT = $this->formatarDados($_GET["porta_padrao_INT{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_GET["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_GET["excluido_DATETIME{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["nome{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "nome = '$this->nome', ";
            }

            if (isset($tipo["porta_padrao_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "porta_padrao_INT = $this->porta_padrao_INT, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE db_tipo SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>

<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Hospedagem
    * DATA DE GERA��O: 10.08.2017
    * ARQUIVO:         DAO_Hospedagem.php
    * TABELA MYSQL:    hospedagem
    * BANCO DE DADOS:  hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Hospedagem extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $dominio;
        public $dominio_webservice;
        public $empresa_hosting_id_INT;
        public $objEmpresa_hosting;
        public $path;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;
        public $dominio_sincronizador;
        public $dominio_sincronizador_webservice;
        public $path_sincronizador;
        public $path_raiz_conteudo;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_dominio;
        public $label_dominio_webservice;
        public $label_empresa_hosting_id_INT;
        public $label_path;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;
        public $label_dominio_sincronizador;
        public $label_dominio_sincronizador_webservice;
        public $label_path_sincronizador;
        public $label_path_raiz_conteudo;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "";
            $this->nomeTabela = "hospedagem";
            $this->campoId = "id";
            $this->campoLabel = "id";
        }

        public function valorCampoLabel()
        {
            return $this->getId();
        }

        public function getFkObjEmpresa_hosting()
        {
            if ($this->objEmpresa_hosting == null)
            {
                $this->objEmpresa_hosting = new EXTDAO_Empresa_hosting($this->getConfiguracaoDAO());
            }
            $idFK = $this->getEmpresa_hosting_id_INT();
            if (!strlen($idFK))
            {
                $this->objEmpresa_hosting->clear();
            }
            else
            {
                if ($this->objEmpresa_hosting->getId() != $idFK)
                {
                    $this->objEmpresa_hosting->select($idFK);
                }
            }

            return $this->objEmpresa_hosting;
        }

        public function getComboBoxAllEmpresa_hosting($objArgumentos)
        {
            $objArgumentos->nome = "empresa_hosting_id_INT";
            $objArgumentos->id = "empresa_hosting_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objEmpresa_hosting = $this->getFkObjEmpresa_hosting();

            return $this->objEmpresa_hosting->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A hospesagem foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A hospesagem foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A hospesagem foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A hospesagem foi exclu�da com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_hospedagem", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_hospedagem", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getDominio()
        {
            return $this->dominio;
        }

        public function getDominio_webservice()
        {
            return $this->dominio_webservice;
        }

        public function getEmpresa_hosting_id_INT()
        {
            return $this->empresa_hosting_id_INT;
        }

        public function getPath()
        {
            return $this->path;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        public function getDominio_sincronizador()
        {
            return $this->dominio_sincronizador;
        }

        public function getDominio_sincronizador_webservice()
        {
            return $this->dominio_sincronizador_webservice;
        }

        public function getPath_sincronizador()
        {
            return $this->path_sincronizador;
        }

        public function getPath_raiz_conteudo()
        {
            return $this->path_raiz_conteudo;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setDominio($val)
        {
            $this->dominio = $val;
        }

        function setDominio_webservice($val)
        {
            $this->dominio_webservice = $val;
        }

        function setEmpresa_hosting_id_INT($val)
        {
            $this->empresa_hosting_id_INT = $val;
        }

        function setPath($val)
        {
            $this->path = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }

        function setDominio_sincronizador($val)
        {
            $this->dominio_sincronizador = $val;
        }

        function setDominio_sincronizador_webservice($val)
        {
            $this->dominio_sincronizador_webservice = $val;
        }

        function setPath_sincronizador($val)
        {
            $this->path_sincronizador = $val;
        }

        function setPath_raiz_conteudo($val)
        {
            $this->path_raiz_conteudo = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM hospedagem WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->dominio = $row->dominio;

            $this->dominio_webservice = $row->dominio_webservice;

            $this->empresa_hosting_id_INT = $row->empresa_hosting_id_INT;

            $this->path = $row->path;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;

            $this->dominio_sincronizador = $row->dominio_sincronizador;

            $this->dominio_sincronizador_webservice = $row->dominio_sincronizador_webservice;

            $this->path_sincronizador = $row->path_sincronizador;

            $this->path_raiz_conteudo = $row->path_raiz_conteudo;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->dominio = null;
            $this->dominio_webservice = null;
            $this->empresa_hosting_id_INT = null;
            $this->path = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
            $this->dominio_sincronizador = null;
            $this->dominio_sincronizador_webservice = null;
            $this->path_sincronizador = null;
            $this->path_raiz_conteudo = null;
        }


        // **********************
        // DELETE
        // **********************
        public function delete($id)
        {
            $sql = "UPDATE hospedagem SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************
        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO hospedagem ( dominio , dominio_webservice , empresa_hosting_id_INT , path , excluido_BOOLEAN , excluido_DATETIME , dominio_sincronizador , dominio_sincronizador_webservice , path_sincronizador , path_raiz_conteudo ) VALUES ( {$this->dominio} , {$this->dominio_webservice} , {$this->empresa_hosting_id_INT} , {$this->path} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} , {$this->dominio_sincronizador} , {$this->dominio_sincronizador_webservice} , {$this->path_sincronizador} , {$this->path_raiz_conteudo} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************
        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoDominio()
        {
            return "dominio";
        }

        public function nomeCampoDominio_webservice()
        {
            return "dominio_webservice";
        }

        public function nomeCampoEmpresa_hosting_id_INT()
        {
            return "empresa_hosting_id_INT";
        }

        public function nomeCampoPath()
        {
            return "path";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }

        public function nomeCampoDominio_sincronizador()
        {
            return "dominio_sincronizador";
        }

        public function nomeCampoDominio_sincronizador_webservice()
        {
            return "dominio_sincronizador_webservice";
        }

        public function nomeCampoPath_sincronizador()
        {
            return "path_sincronizador";
        }

        public function nomeCampoPath_raiz_conteudo()
        {
            return "path_raiz_conteudo";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoDominio($objArguments)
        {
            $objArguments->nome = "dominio";
            $objArguments->id = "dominio";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoDominio_webservice($objArguments)
        {
            $objArguments->nome = "dominio_webservice";
            $objArguments->id = "dominio_webservice";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoEmpresa_hosting_id_INT($objArguments)
        {
            $objArguments->nome = "empresa_hosting_id_INT";
            $objArguments->id = "empresa_hosting_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoPath($objArguments)
        {
            $objArguments->nome = "path";
            $objArguments->id = "path";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoDominio_sincronizador($objArguments)
        {
            $objArguments->nome = "dominio_sincronizador";
            $objArguments->id = "dominio_sincronizador";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoDominio_sincronizador_webservice($objArguments)
        {
            $objArguments->nome = "dominio_sincronizador_webservice";
            $objArguments->id = "dominio_sincronizador_webservice";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPath_sincronizador($objArguments)
        {
            $objArguments->nome = "path_sincronizador";
            $objArguments->id = "path_sincronizador";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPath_raiz_conteudo($objArguments)
        {
            $objArguments->nome = "path_raiz_conteudo";
            $objArguments->id = "path_raiz_conteudo";

            return $this->campoTexto($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->empresa_hosting_id_INT == null)
            {
                $this->empresa_hosting_id_INT = "null";
            }

            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->dominio = $this->formatarDadosParaSQL($this->dominio);
            $this->dominio_webservice = $this->formatarDadosParaSQL($this->dominio_webservice);
            $this->path = $this->formatarDadosParaSQL($this->path);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
            $this->dominio_sincronizador = $this->formatarDadosParaSQL($this->dominio_sincronizador);
            $this->dominio_sincronizador_webservice = $this->formatarDadosParaSQL($this->dominio_sincronizador_webservice);
            $this->path_sincronizador = $this->formatarDadosParaSQL($this->path_sincronizador);
            $this->path_raiz_conteudo = $this->formatarDadosParaSQL($this->path_raiz_conteudo);

        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("dominio", $this->dominio);
            Helper::setSession("dominio_webservice", $this->dominio_webservice);
            Helper::setSession("empresa_hosting_id_INT", $this->empresa_hosting_id_INT);
            Helper::setSession("path", $this->path);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
            Helper::setSession("dominio_sincronizador", $this->dominio_sincronizador);
            Helper::setSession("dominio_sincronizador_webservice", $this->dominio_sincronizador_webservice);
            Helper::setSession("path_sincronizador", $this->path_sincronizador);
            Helper::setSession("path_raiz_conteudo", $this->path_raiz_conteudo);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("dominio");
            Helper::clearSession("dominio_webservice");
            Helper::clearSession("empresa_hosting_id_INT");
            Helper::clearSession("path");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
            Helper::clearSession("dominio_sincronizador");
            Helper::clearSession("dominio_sincronizador_webservice");
            Helper::clearSession("path_sincronizador");
            Helper::clearSession("path_raiz_conteudo");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = Helper::SESSION("id{$numReg}");
            $this->dominio = Helper::SESSION("dominio{$numReg}");
            $this->dominio_webservice = Helper::SESSION("dominio_webservice{$numReg}");
            $this->empresa_hosting_id_INT = Helper::SESSION("empresa_hosting_id_INT{$numReg}");
            $this->path = Helper::SESSION("path{$numReg}");
            $this->excluido_BOOLEAN = Helper::SESSION("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::SESSION("excluido_DATETIME{$numReg}");
            $this->dominio_sincronizador = Helper::SESSION("dominio_sincronizador{$numReg}");
            $this->dominio_sincronizador_webservice = Helper::SESSION("dominio_sincronizador_webservice{$numReg}");
            $this->path_sincronizador = Helper::SESSION("path_sincronizador{$numReg}");
            $this->path_raiz_conteudo = Helper::SESSION("path_raiz_conteudo{$numReg}");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = Helper::POST("id{$numReg}");
            $this->dominio = Helper::POST("dominio{$numReg}");
            $this->dominio_webservice = Helper::POST("dominio_webservice{$numReg}");
            $this->empresa_hosting_id_INT = Helper::POST("empresa_hosting_id_INT{$numReg}");
            $this->path = Helper::POST("path{$numReg}");
            $this->excluido_BOOLEAN = Helper::POST("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::POST("excluido_DATETIME{$numReg}");
            $this->dominio_sincronizador = Helper::POST("dominio_sincronizador{$numReg}");
            $this->dominio_sincronizador_webservice = Helper::POST("dominio_sincronizador_webservice{$numReg}");
            $this->path_sincronizador = Helper::POST("path_sincronizador{$numReg}");
            $this->path_raiz_conteudo = Helper::POST("path_raiz_conteudo{$numReg}");
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = Helper::GET("id{$numReg}");
            $this->dominio = Helper::GET("dominio{$numReg}");
            $this->dominio_webservice = Helper::GET("dominio_webservice{$numReg}");
            $this->empresa_hosting_id_INT = Helper::GET("empresa_hosting_id_INT{$numReg}");
            $this->path = Helper::GET("path{$numReg}");
            $this->excluido_BOOLEAN = Helper::GET("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::GET("excluido_DATETIME{$numReg}");
            $this->dominio_sincronizador = Helper::GET("dominio_sincronizador{$numReg}");
            $this->dominio_sincronizador_webservice = Helper::GET("dominio_sincronizador_webservice{$numReg}");
            $this->path_sincronizador = Helper::GET("path_sincronizador{$numReg}");
            $this->path_raiz_conteudo = Helper::GET("path_raiz_conteudo{$numReg}");
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["dominio{$numReg}"]) || $tipo == null)
            {
                $upd .= "dominio = $this->dominio, ";
            }

            if (isset($tipo["dominio_webservice{$numReg}"]) || $tipo == null)
            {
                $upd .= "dominio_webservice = $this->dominio_webservice, ";
            }

            if (isset($tipo["empresa_hosting_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "empresa_hosting_id_INT = $this->empresa_hosting_id_INT, ";
            }

            if (isset($tipo["path{$numReg}"]) || $tipo == null)
            {
                $upd .= "path = $this->path, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            if (isset($tipo["dominio_sincronizador{$numReg}"]) || $tipo == null)
            {
                $upd .= "dominio_sincronizador = $this->dominio_sincronizador, ";
            }

            if (isset($tipo["dominio_sincronizador_webservice{$numReg}"]) || $tipo == null)
            {
                $upd .= "dominio_sincronizador_webservice = $this->dominio_sincronizador_webservice, ";
            }

            if (isset($tipo["path_sincronizador{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_sincronizador = $this->path_sincronizador, ";
            }

            if (isset($tipo["path_raiz_conteudo{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_raiz_conteudo = $this->path_raiz_conteudo, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE hospedagem SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim

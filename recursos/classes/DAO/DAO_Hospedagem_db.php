<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Hospedagem_db
    * DATA DE GERA��O: 12.05.2017
    * ARQUIVO:         DAO_Hospedagem_db.php
    * TABELA MYSQL:    hospedagem_db
    * BANCO DE DADOS:  hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Hospedagem_db extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $hospedagem_id_INT;
        public $objHospedagem;
        public $nome_db;
        public $host_db;
        public $porta_db_INT;
        public $usuario_db;
        public $senha_db;
        public $tipo_hospedagem_db_id_INT;
        public $objTipo_hospedagem_db;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_hospedagem_id_INT;
        public $label_nome_db;
        public $label_host_db;
        public $label_porta_db_INT;
        public $label_usuario_db;
        public $label_senha_db;
        public $label_tipo_hospedagem_db_id_INT;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "hospedagem do banco de dados";
            $this->nomeTabela = "hospedagem_db";
            $this->campoId = "id";
            $this->campoLabel = "nome_db";
        }

        public function valorCampoLabel()
        {
            return $this->getNome_db();
        }

        public function getFkObjHospedagem()
        {
            if ($this->objHospedagem == null)
            {
                $this->objHospedagem = new EXTDAO_Hospedagem($this->getConfiguracaoDAO());
            }
            $idFK = $this->getHospedagem_id_INT();
            if (!strlen($idFK))
            {
                $this->objHospedagem->clear();
            }
            else
            {
                if ($this->objHospedagem->getId() != $idFK)
                {
                    $this->objHospedagem->select($idFK);
                }
            }

            return $this->objHospedagem;
        }

        public function getComboBoxAllHospedagem($objArgumentos)
        {
            $objArgumentos->nome = "hospedagem_id_INT";
            $objArgumentos->id = "hospedagem_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objHospedagem = $this->getFkObjHospedagem();

            return $this->objHospedagem->getComboBox($objArgumentos);
        }

        public function getFkObjTipo_hospedagem_db()
        {
            if ($this->objTipo_hospedagem_db == null)
            {
                $this->objTipo_hospedagem_db = new EXTDAO_Tipo_hospedagem_db($this->getConfiguracaoDAO());
            }
            $idFK = $this->getTipo_hospedagem_db_id_INT();
            if (!strlen($idFK))
            {
                $this->objTipo_hospedagem_db->clear();
            }
            else
            {
                if ($this->objTipo_hospedagem_db->getId() != $idFK)
                {
                    $this->objTipo_hospedagem_db->select($idFK);
                }
            }

            return $this->objTipo_hospedagem_db;
        }

        public function getComboBoxAllTipo_hospedagem_db($objArgumentos)
        {
            $objArgumentos->nome = "tipo_hospedagem_db_id_INT";
            $objArgumentos->id = "tipo_hospedagem_db_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objTipo_hospedagem_db = $this->getFkObjTipo_hospedagem_db();

            return $this->objTipo_hospedagem_db->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A hospedagem do banco de dados foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A hospedagem do banco de dados foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A hospedagem do banco de dados foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A hospedagem do banco de dados foi exclu�da com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_hospedagem_db", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_hospedagem_db", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getHospedagem_id_INT()
        {
            return $this->hospedagem_id_INT;
        }

        public function getNome_db()
        {
            return $this->nome_db;
        }

        public function getHost_db()
        {
            return $this->host_db;
        }

        public function getPorta_db_INT()
        {
            return $this->porta_db_INT;
        }

        public function getUsuario_db()
        {
            return $this->usuario_db;
        }

        public function getSenha_db()
        {
            return $this->senha_db;
        }

        public function getTipo_hospedagem_db_id_INT()
        {
            return $this->tipo_hospedagem_db_id_INT;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setHospedagem_id_INT($val)
        {
            $this->hospedagem_id_INT = $val;
        }

        function setNome_db($val)
        {
            $this->nome_db = $val;
        }

        function setHost_db($val)
        {
            $this->host_db = $val;
        }

        function setPorta_db_INT($val)
        {
            $this->porta_db_INT = $val;
        }

        function setUsuario_db($val)
        {
            $this->usuario_db = $val;
        }

        function setSenha_db($val)
        {
            $this->senha_db = $val;
        }

        function setTipo_hospedagem_db_id_INT($val)
        {
            $this->tipo_hospedagem_db_id_INT = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM hospedagem_db WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->hospedagem_id_INT = $row->hospedagem_id_INT;

            $this->nome_db = $row->nome_db;

            $this->host_db = $row->host_db;

            $this->porta_db_INT = $row->porta_db_INT;

            $this->usuario_db = $row->usuario_db;

            $this->senha_db = $row->senha_db;

            $this->tipo_hospedagem_db_id_INT = $row->tipo_hospedagem_db_id_INT;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->hospedagem_id_INT = null;
            $this->nome_db = null;
            $this->host_db = null;
            $this->porta_db_INT = null;
            $this->usuario_db = null;
            $this->senha_db = null;
            $this->tipo_hospedagem_db_id_INT = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
        }


        // **********************
        // DELETE
        // **********************
        public function delete($id)
        {
            $sql = "UPDATE hospedagem_db SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************
        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO hospedagem_db ( hospedagem_id_INT , nome_db , host_db , porta_db_INT , usuario_db , senha_db , tipo_hospedagem_db_id_INT , excluido_BOOLEAN , excluido_DATETIME ) VALUES ( {$this->hospedagem_id_INT} , {$this->nome_db} , {$this->host_db} , {$this->porta_db_INT} , {$this->usuario_db} , {$this->senha_db} , {$this->tipo_hospedagem_db_id_INT} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************
        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoHospedagem_id_INT()
        {
            return "hospedagem_id_INT";
        }

        public function nomeCampoNome_db()
        {
            return "nome_db";
        }

        public function nomeCampoHost_db()
        {
            return "host_db";
        }

        public function nomeCampoPorta_db_INT()
        {
            return "porta_db_INT";
        }

        public function nomeCampoUsuario_db()
        {
            return "usuario_db";
        }

        public function nomeCampoSenha_db()
        {
            return "senha_db";
        }

        public function nomeCampoTipo_hospedagem_db_id_INT()
        {
            return "tipo_hospedagem_db_id_INT";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoHospedagem_id_INT($objArguments)
        {
            $objArguments->nome = "hospedagem_id_INT";
            $objArguments->id = "hospedagem_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoNome_db($objArguments)
        {
            $objArguments->nome = "nome_db";
            $objArguments->id = "nome_db";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoHost_db($objArguments)
        {
            $objArguments->nome = "host_db";
            $objArguments->id = "host_db";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPorta_db_INT($objArguments)
        {
            $objArguments->nome = "porta_db_INT";
            $objArguments->id = "porta_db_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoUsuario_db($objArguments)
        {
            $objArguments->nome = "usuario_db";
            $objArguments->id = "usuario_db";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoSenha_db($objArguments)
        {
            $objArguments->nome = "senha_db";
            $objArguments->id = "senha_db";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoTipo_hospedagem_db_id_INT($objArguments)
        {
            $objArguments->nome = "tipo_hospedagem_db_id_INT";
            $objArguments->id = "tipo_hospedagem_db_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->hospedagem_id_INT == null)
            {
                $this->hospedagem_id_INT = "null";
            }

            if ($this->porta_db_INT == null)
            {
                $this->porta_db_INT = "null";
            }

            if ($this->tipo_hospedagem_db_id_INT == null)
            {
                $this->tipo_hospedagem_db_id_INT = "null";
            }

            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->nome_db = $this->formatarDadosParaSQL($this->nome_db);
            $this->host_db = $this->formatarDadosParaSQL($this->host_db);
            $this->usuario_db = $this->formatarDadosParaSQL($this->usuario_db);
            $this->senha_db = $this->formatarDadosParaSQL($this->senha_db);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("hospedagem_id_INT", $this->hospedagem_id_INT);
            Helper::setSession("nome_db", $this->nome_db);
            Helper::setSession("host_db", $this->host_db);
            Helper::setSession("porta_db_INT", $this->porta_db_INT);
            Helper::setSession("usuario_db", $this->usuario_db);
            Helper::setSession("senha_db", $this->senha_db);
            Helper::setSession("tipo_hospedagem_db_id_INT", $this->tipo_hospedagem_db_id_INT);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("hospedagem_id_INT");
            Helper::clearSession("nome_db");
            Helper::clearSession("host_db");
            Helper::clearSession("porta_db_INT");
            Helper::clearSession("usuario_db");
            Helper::clearSession("senha_db");
            Helper::clearSession("tipo_hospedagem_db_id_INT");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::SESSION("id{$numReg}"));
            $this->hospedagem_id_INT = $this->formatarDadosParaSQL(Helper::SESSION("hospedagem_id_INT{$numReg}"));
            $this->nome_db = $this->formatarDadosParaSQL(Helper::SESSION("nome_db{$numReg}"));
            $this->host_db = $this->formatarDadosParaSQL(Helper::SESSION("host_db{$numReg}"));
            $this->porta_db_INT = $this->formatarDadosParaSQL(Helper::SESSION("porta_db_INT{$numReg}"));
            $this->usuario_db = $this->formatarDadosParaSQL(Helper::SESSION("usuario_db{$numReg}"));
            $this->senha_db = $this->formatarDadosParaSQL(Helper::SESSION("senha_db{$numReg}"));
            $this->tipo_hospedagem_db_id_INT = $this->formatarDadosParaSQL(Helper::SESSION("tipo_hospedagem_db_id_INT{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::SESSION("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::SESSION("excluido_DATETIME{$numReg}"));
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::POST("id{$numReg}"));
            $this->hospedagem_id_INT = $this->formatarDadosParaSQL(Helper::POST("hospedagem_id_INT{$numReg}"));
            $this->nome_db = $this->formatarDadosParaSQL(Helper::POST("nome_db{$numReg}"));
            $this->host_db = $this->formatarDadosParaSQL(Helper::POST("host_db{$numReg}"));
            $this->porta_db_INT = $this->formatarDadosParaSQL(Helper::POST("porta_db_INT{$numReg}"));
            $this->usuario_db = $this->formatarDadosParaSQL(Helper::POST("usuario_db{$numReg}"));
            $this->senha_db = $this->formatarDadosParaSQL(Helper::POST("senha_db{$numReg}"));
            $this->tipo_hospedagem_db_id_INT = $this->formatarDadosParaSQL(Helper::POST("tipo_hospedagem_db_id_INT{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::POST("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::POST("excluido_DATETIME{$numReg}"));
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::GET("id{$numReg}"));
            $this->hospedagem_id_INT = $this->formatarDadosParaSQL(Helper::GET("hospedagem_id_INT{$numReg}"));
            $this->nome_db = $this->formatarDadosParaSQL(Helper::GET("nome_db{$numReg}"));
            $this->host_db = $this->formatarDadosParaSQL(Helper::GET("host_db{$numReg}"));
            $this->porta_db_INT = $this->formatarDadosParaSQL(Helper::GET("porta_db_INT{$numReg}"));
            $this->usuario_db = $this->formatarDadosParaSQL(Helper::GET("usuario_db{$numReg}"));
            $this->senha_db = $this->formatarDadosParaSQL(Helper::GET("senha_db{$numReg}"));
            $this->tipo_hospedagem_db_id_INT = $this->formatarDadosParaSQL(Helper::GET("tipo_hospedagem_db_id_INT{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::GET("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::GET("excluido_DATETIME{$numReg}"));
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["hospedagem_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "hospedagem_id_INT = $this->hospedagem_id_INT, ";
            }

            if (isset($tipo["nome_db{$numReg}"]) || $tipo == null)
            {
                $upd .= "nome_db = $this->nome_db, ";
            }

            if (isset($tipo["host_db{$numReg}"]) || $tipo == null)
            {
                $upd .= "host_db = $this->host_db, ";
            }

            if (isset($tipo["porta_db_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "porta_db_INT = $this->porta_db_INT, ";
            }

            if (isset($tipo["usuario_db{$numReg}"]) || $tipo == null)
            {
                $upd .= "usuario_db = $this->usuario_db, ";
            }

            if (isset($tipo["senha_db{$numReg}"]) || $tipo == null)
            {
                $upd .= "senha_db = $this->senha_db, ";
            }

            if (isset($tipo["tipo_hospedagem_db_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "tipo_hospedagem_db_id_INT = $this->tipo_hospedagem_db_id_INT, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE hospedagem_db SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim

<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Hospedagem_ftp
    * DATA DE GERAÇÃO: 17.08.2012
    * ARQUIVO:         DAO_Hospedagem_ftp.php
    * TABELA MYSQL:    hospedagem_ftp
    * BANCO DE DADOS:  omegasoftware_interno
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Hospedagem_ftp extends Generic_DAO
    {

        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************

        public $id;
        public $hospedagem_id_INT;
        public $objHospedagem;
        public $host_ftp;
        public $porta_ftp_INT;
        public $usuario_ftp;
        public $senha_ftp;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_hospedagem_id_INT;
        public $label_host_ftp;
        public $label_porta_ftp_INT;
        public $label_usuario_ftp;
        public $label_senha_ftp;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // MÉTODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "FTP da hospedagem";
            $this->nomeTabela = "hospedagem_ftp";
            $this->campoId = "id";
            $this->campoLabel = "hospedagem_id_INT";

            $this->objHospedagem = new EXTDAO_Hospedagem();
        }

        public function valorCampoLabel()
        {
            return $this->getHospedagem_id_INT();
        }

        public function getComboBoxAllHospedagem($objArgumentos)
        {
            $objArgumentos->nome = "hospedagem_id_INT";
            $objArgumentos->id = "hospedagem_id_INT";
            $objArgumentos->valueReplaceId = false;

            return $this->objHospedagem->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O FTP da hospedagem foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O FTP da hospedagem foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O FTP da hospedagem foi modificado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O FTP da hospedagem foi excluído com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_hospedagem_ftp", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_hospedagem_ftp", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $this->delete("$registroRemover");

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // MÉTODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getHospedagem_id_INT()
        {
            return $this->hospedagem_id_INT;
        }

        public function getHost_ftp()
        {
            return $this->host_ftp;
        }

        public function getPorta_ftp_INT()
        {
            return $this->porta_ftp_INT;
        }

        public function getUsuario_ftp()
        {
            return $this->usuario_ftp;
        }

        public function getSenha_ftp()
        {
            return $this->senha_ftp;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // MÉTODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setHospedagem_id_INT($val)
        {
            $this->hospedagem_id_INT = $val;
        }

        function setHost_ftp($val)
        {
            $this->host_ftp = $val;
        }

        function setPorta_ftp_INT($val)
        {
            $this->porta_ftp_INT = $val;
        }

        function setUsuario_ftp($val)
        {
            $this->usuario_ftp = $val;
        }

        function setSenha_ftp($val)
        {
            $this->senha_ftp = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM hospedagem_ftp WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->hospedagem_id_INT = $row->hospedagem_id_INT;
            if ($this->hospedagem_id_INT)
            {
                $this->objHospedagem->select($this->hospedagem_id_INT);
            }

            $this->host_ftp = $row->host_ftp;

            $this->porta_ftp_INT = $row->porta_ftp_INT;

            $this->usuario_ftp = $row->usuario_ftp;

            $this->senha_ftp = $row->senha_ftp;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE hospedagem_ftp SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO hospedagem_ftp ( hospedagem_id_INT,host_ftp,porta_ftp_INT,usuario_ftp,senha_ftp,excluido_BOOLEAN,excluido_DATETIME ) VALUES ( $this->hospedagem_id_INT,'$this->host_ftp',$this->porta_ftp_INT,'$this->usuario_ftp','$this->senha_ftp',$this->excluido_BOOLEAN,$this->excluido_DATETIME )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoHospedagem_id_INT()
        {
            return "hospedagem_id_INT";
        }

        public function nomeCampoHost_ftp()
        {
            return "host_ftp";
        }

        public function nomeCampoPorta_ftp_INT()
        {
            return "porta_ftp_INT";
        }

        public function nomeCampoUsuario_ftp()
        {
            return "usuario_ftp";
        }

        public function nomeCampoSenha_ftp()
        {
            return "senha_ftp";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoHospedagem_id_INT($objArguments)
        {
            $objArguments->nome = "hospedagem_id_INT";
            $objArguments->id = "hospedagem_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoHost_ftp($objArguments)
        {
            $objArguments->nome = "host_ftp";
            $objArguments->id = "host_ftp";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPorta_ftp_INT($objArguments)
        {
            $objArguments->nome = "porta_ftp_INT";
            $objArguments->id = "porta_ftp_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoUsuario_ftp($objArguments)
        {
            $objArguments->nome = "usuario_ftp";
            $objArguments->id = "usuario_ftp";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoSenha_ftp($objArguments)
        {
            $objArguments->nome = "senha_ftp";
            $objArguments->id = "senha_ftp";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->hospedagem_id_INT == "")
            {
                $this->hospedagem_id_INT = "null";
            }

            if ($this->porta_ftp_INT == "")
            {
                $this->porta_ftp_INT = "null";
            }

            if ($this->excluido_BOOLEAN == "")
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->host_ftp = $this->formatarDados($this->host_ftp);
            $this->usuario_ftp = $this->formatarDados($this->usuario_ftp);
            $this->senha_ftp = $this->formatarDados($this->senha_ftp);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["hospedagem_id_INT"] = $this->hospedagem_id_INT;
            $_SESSION["host_ftp"] = $this->host_ftp;
            $_SESSION["porta_ftp_INT"] = $this->porta_ftp_INT;
            $_SESSION["usuario_ftp"] = $this->usuario_ftp;
            $_SESSION["senha_ftp"] = $this->senha_ftp;
            $_SESSION["excluido_BOOLEAN"] = $this->excluido_BOOLEAN;
            $_SESSION["excluido_DATETIME"] = $this->excluido_DATETIME;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["hospedagem_id_INT"]);
            unset($_SESSION["host_ftp"]);
            unset($_SESSION["porta_ftp_INT"]);
            unset($_SESSION["usuario_ftp"]);
            unset($_SESSION["senha_ftp"]);
            unset($_SESSION["excluido_BOOLEAN"]);
            unset($_SESSION["excluido_DATETIME"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->hospedagem_id_INT = $this->formatarDados($_SESSION["hospedagem_id_INT{$numReg}"]);
            $this->host_ftp = $this->formatarDados($_SESSION["host_ftp{$numReg}"]);
            $this->porta_ftp_INT = $this->formatarDados($_SESSION["porta_ftp_INT{$numReg}"]);
            $this->usuario_ftp = $this->formatarDados($_SESSION["usuario_ftp{$numReg}"]);
            $this->senha_ftp = $this->formatarDados($_SESSION["senha_ftp{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_SESSION["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_SESSION["excluido_DATETIME{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->hospedagem_id_INT = $this->formatarDados($_POST["hospedagem_id_INT{$numReg}"]);
            $this->host_ftp = $this->formatarDados($_POST["host_ftp{$numReg}"]);
            $this->porta_ftp_INT = $this->formatarDados($_POST["porta_ftp_INT{$numReg}"]);
            $this->usuario_ftp = $this->formatarDados($_POST["usuario_ftp{$numReg}"]);
            $this->senha_ftp = $this->formatarDados($_POST["senha_ftp{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_POST["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_POST["excluido_DATETIME{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->hospedagem_id_INT = $this->formatarDados($_GET["hospedagem_id_INT{$numReg}"]);
            $this->host_ftp = $this->formatarDados($_GET["host_ftp{$numReg}"]);
            $this->porta_ftp_INT = $this->formatarDados($_GET["porta_ftp_INT{$numReg}"]);
            $this->usuario_ftp = $this->formatarDados($_GET["usuario_ftp{$numReg}"]);
            $this->senha_ftp = $this->formatarDados($_GET["senha_ftp{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_GET["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_GET["excluido_DATETIME{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["hospedagem_id_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "hospedagem_id_INT = $this->hospedagem_id_INT, ";
            }

            if (isset($tipo["host_ftp{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "host_ftp = '$this->host_ftp', ";
            }

            if (isset($tipo["porta_ftp_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "porta_ftp_INT = $this->porta_ftp_INT, ";
            }

            if (isset($tipo["usuario_ftp{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "usuario_ftp = '$this->usuario_ftp', ";
            }

            if (isset($tipo["senha_ftp{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "senha_ftp = '$this->senha_ftp', ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE hospedagem_ftp SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>

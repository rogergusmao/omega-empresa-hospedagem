<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Operacao_sistema
    * DATA DE GERA��O: 21.10.2010
    * ARQUIVO:         DAO_Operacao_sistema.php
    * TABELA MYSQL:    operacao_sistema
    * BANCO DE DADOS:  DEP_pesquisas
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Operacao_sistema extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $usuario_id_INT;
        public $objUsuario;
        public $tipo_operacao;
        public $pagina_operacao;
        public $entidade_operacao;
        public $chave_registro_operacao_INT;
        public $descricao_operacao;
        public $data_operacao_DATETIME;
        public $url_completa;

        public $nomeEntidade;

        public $data_operacao_DATETIME_UNIX;

        public $label_id;
        public $label_usuario_id_INT;
        public $label_tipo_operacao;
        public $label_pagina_operacao;
        public $label_entidade_operacao;
        public $label_chave_registro_operacao_INT;
        public $label_descricao_operacao;
        public $label_data_operacao_DATETIME;
        public $label_url_completa;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "opera��o no sistema";
            $this->nomeTabela = "operacao_sistema";
            $this->campoId = "id";
            $this->campoLabel = "tipo_operacao";

            $this->objUsuario = new EXTDAO_Usuario();
        }

        public function valorCampoLabel()
        {
            return $this->getTipo_operacao();
        }

        public function getComboBoxAllUsuario($objArgumentos)
        {
            $objArgumentos->nome = "usuario_id_INT";
            $objArgumentos->id = "usuario_id_INT";
            $objArgumentos->valueReplaceId = false;

            return $this->objUsuario->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A opera��o no sistema foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A opera��o no sistema foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A opera��o no sistema foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);
                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A opera��o no sistema foi exclu�da com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_operacao_sistema", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_operacao_sistema", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getUsuario_id_INT()
        {
            return $this->usuario_id_INT;
        }

        public function getTipo_operacao()
        {
            return $this->tipo_operacao;
        }

        public function getPagina_operacao()
        {
            return $this->pagina_operacao;
        }

        public function getEntidade_operacao()
        {
            return $this->entidade_operacao;
        }

        public function getChave_registro_operacao_INT()
        {
            return $this->chave_registro_operacao_INT;
        }

        public function getDescricao_operacao()
        {
            return $this->descricao_operacao;
        }

        function getData_operacao_DATETIME_UNIX()
        {
            return $this->data_operacao_DATETIME_UNIX;
        }

        public function getData_operacao_DATETIME()
        {
            return $this->data_operacao_DATETIME;
        }

        public function getUrl_completa()
        {
            return $this->url_completa;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setUsuario_id_INT($val)
        {
            $this->usuario_id_INT = $val;
        }

        function setTipo_operacao($val)
        {
            $this->tipo_operacao = $val;
        }

        function setPagina_operacao($val)
        {
            $this->pagina_operacao = $val;
        }

        function setEntidade_operacao($val)
        {
            $this->entidade_operacao = $val;
        }

        function setChave_registro_operacao_INT($val)
        {
            $this->chave_registro_operacao_INT = $val;
        }

        function setDescricao_operacao($val)
        {
            $this->descricao_operacao = $val;
        }

        function setData_operacao_DATETIME($val)
        {
            $this->data_operacao_DATETIME = $val;
        }

        function setUrl_completa($val)
        {
            $this->url_completa = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(data_operacao_DATETIME) AS data_operacao_DATETIME_UNIX FROM operacao_sistema WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->usuario_id_INT = $row->usuario_id_INT;
            if ($this->usuario_id_INT)
            {
                $this->objUsuario->select($this->usuario_id_INT);
            }

            $this->tipo_operacao = $row->tipo_operacao;

            $this->pagina_operacao = $row->pagina_operacao;

            $this->entidade_operacao = $row->entidade_operacao;

            $this->chave_registro_operacao_INT = $row->chave_registro_operacao_INT;

            $this->descricao_operacao = $row->descricao_operacao;

            $this->data_operacao_DATETIME = $row->data_operacao_DATETIME;
            $this->data_operacao_DATETIME_UNIX = $row->data_operacao_DATETIME_UNIX;

            $this->url_completa = $row->url_completa;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "DELETE FROM operacao_sistema WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO operacao_sistema ( usuario_id_INT,tipo_operacao,pagina_operacao,entidade_operacao,chave_registro_operacao_INT,descricao_operacao,data_operacao_DATETIME,url_completa ) VALUES ( $this->usuario_id_INT,'$this->tipo_operacao','$this->pagina_operacao','$this->entidade_operacao',$this->chave_registro_operacao_INT,'$this->descricao_operacao',$this->data_operacao_DATETIME,'$this->url_completa' )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoUsuario_id_INT()
        {
            return "usuario_id_INT";
        }

        public function nomeCampoTipo_operacao()
        {
            return "tipo_operacao";
        }

        public function nomeCampoPagina_operacao()
        {
            return "pagina_operacao";
        }

        public function nomeCampoEntidade_operacao()
        {
            return "entidade_operacao";
        }

        public function nomeCampoChave_registro_operacao_INT()
        {
            return "chave_registro_operacao_INT";
        }

        public function nomeCampoDescricao_operacao()
        {
            return "descricao_operacao";
        }

        public function nomeCampoData_operacao_DATETIME()
        {
            return "data_operacao_DATETIME";
        }

        public function nomeCampoUrl_completa()
        {
            return "url_completa";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoUsuario_id_INT($objArguments)
        {
            $objArguments->nome = "usuario_id_INT";
            $objArguments->id = "usuario_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoTipo_operacao($objArguments)
        {
            $objArguments->nome = "tipo_operacao";
            $objArguments->id = "tipo_operacao";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPagina_operacao($objArguments)
        {
            $objArguments->nome = "pagina_operacao";
            $objArguments->id = "pagina_operacao";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoEntidade_operacao($objArguments)
        {
            $objArguments->nome = "entidade_operacao";
            $objArguments->id = "entidade_operacao";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoChave_registro_operacao_INT($objArguments)
        {
            $objArguments->nome = "chave_registro_operacao_INT";
            $objArguments->id = "chave_registro_operacao_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoDescricao_operacao($objArguments)
        {
            $objArguments->nome = "descricao_operacao";
            $objArguments->id = "descricao_operacao";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoData_operacao_DATETIME($objArguments)
        {
            $objArguments->nome = "data_operacao_DATETIME";
            $objArguments->id = "data_operacao_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoUrl_completa($objArguments)
        {
            $objArguments->nome = "url_completa";
            $objArguments->id = "url_completa";

            return $this->campoTexto($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->usuario_id_INT == "")
            {
                $this->usuario_id_INT = "null";
            }

            if ($this->chave_registro_operacao_INT == "")
            {
                $this->chave_registro_operacao_INT = "null";
            }

            $this->data_operacao_DATETIME = $this->formatarDataTimeParaComandoSQL($this->data_operacao_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->data_operacao_DATETIME = $this->formatarDataTimeParaExibicao($this->data_operacao_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["usuario_id_INT"] = $this->usuario_id_INT;
            $_SESSION["tipo_operacao"] = $this->tipo_operacao;
            $_SESSION["pagina_operacao"] = $this->pagina_operacao;
            $_SESSION["entidade_operacao"] = $this->entidade_operacao;
            $_SESSION["chave_registro_operacao_INT"] = $this->chave_registro_operacao_INT;
            $_SESSION["descricao_operacao"] = $this->descricao_operacao;
            $_SESSION["data_operacao_DATETIME"] = $this->data_operacao_DATETIME;
            $_SESSION["url_completa"] = $this->url_completa;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["usuario_id_INT"]);
            unset($_SESSION["tipo_operacao"]);
            unset($_SESSION["pagina_operacao"]);
            unset($_SESSION["entidade_operacao"]);
            unset($_SESSION["chave_registro_operacao_INT"]);
            unset($_SESSION["descricao_operacao"]);
            unset($_SESSION["data_operacao_DATETIME"]);
            unset($_SESSION["url_completa"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_SESSION["usuario_id_INT{$numReg}"]);
            $this->tipo_operacao = $this->formatarDados($_SESSION["tipo_operacao{$numReg}"]);
            $this->pagina_operacao = $this->formatarDados($_SESSION["pagina_operacao{$numReg}"]);
            $this->entidade_operacao = $this->formatarDados($_SESSION["entidade_operacao{$numReg}"]);
            $this->chave_registro_operacao_INT = $this->formatarDados($_SESSION["chave_registro_operacao_INT{$numReg}"]);
            $this->descricao_operacao = $this->formatarDados($_SESSION["descricao_operacao{$numReg}"]);
            $this->data_operacao_DATETIME = $this->formatarDados($_SESSION["data_operacao_DATETIME{$numReg}"]);
            $this->url_completa = $this->formatarDados($_SESSION["url_completa{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_POST["usuario_id_INT{$numReg}"]);
            $this->tipo_operacao = $this->formatarDados($_POST["tipo_operacao{$numReg}"]);
            $this->pagina_operacao = $this->formatarDados($_POST["pagina_operacao{$numReg}"]);
            $this->entidade_operacao = $this->formatarDados($_POST["entidade_operacao{$numReg}"]);
            $this->chave_registro_operacao_INT = $this->formatarDados($_POST["chave_registro_operacao_INT{$numReg}"]);
            $this->descricao_operacao = $this->formatarDados($_POST["descricao_operacao{$numReg}"]);
            $this->data_operacao_DATETIME = $this->formatarDados($_POST["data_operacao_DATETIME{$numReg}"]);
            $this->url_completa = $this->formatarDados($_POST["url_completa{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_GET["usuario_id_INT{$numReg}"]);
            $this->tipo_operacao = $this->formatarDados($_GET["tipo_operacao{$numReg}"]);
            $this->pagina_operacao = $this->formatarDados($_GET["pagina_operacao{$numReg}"]);
            $this->entidade_operacao = $this->formatarDados($_GET["entidade_operacao{$numReg}"]);
            $this->chave_registro_operacao_INT = $this->formatarDados($_GET["chave_registro_operacao_INT{$numReg}"]);
            $this->descricao_operacao = $this->formatarDados($_GET["descricao_operacao{$numReg}"]);
            $this->data_operacao_DATETIME = $this->formatarDados($_GET["data_operacao_DATETIME{$numReg}"]);
            $this->url_completa = $this->formatarDados($_GET["url_completa{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "usuario_id_INT = $this->usuario_id_INT, ";
            }

            if (isset($tipo["tipo_operacao{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "tipo_operacao = '$this->tipo_operacao', ";
            }

            if (isset($tipo["pagina_operacao{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "pagina_operacao = '$this->pagina_operacao', ";
            }

            if (isset($tipo["entidade_operacao{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "entidade_operacao = '$this->entidade_operacao', ";
            }

            if (isset($tipo["chave_registro_operacao_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "chave_registro_operacao_INT = $this->chave_registro_operacao_INT, ";
            }

            if (isset($tipo["descricao_operacao{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "descricao_operacao = '$this->descricao_operacao', ";
            }

            if (isset($tipo["data_operacao_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "data_operacao_DATETIME = $this->data_operacao_DATETIME, ";
            }

            if (isset($tipo["url_completa{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "url_completa = '$this->url_completa', ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE operacao_sistema SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>
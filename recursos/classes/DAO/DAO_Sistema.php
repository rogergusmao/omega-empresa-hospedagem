<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema
    * DATA DE GERA��O: 10.08.2017
    * ARQUIVO:         DAO_Sistema.php
    * TABELA MYSQL:    sistema
    * BANCO DE DADOS:  hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $nome;
        public $sistema_biblioteca_nuvem_INT;
        public $path_raiz_assinatura;
        public $path_raiz_dump;
        public $dominio_raiz_assinatura;
        public $path_raiz_codigo_assinatura;
        public $empresa_hosting_id_INT;
        public $objEmpresa_hosting;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;
        public $sistema_tipo_id_INT;
        public $objSistema_tipo;
        public $path_raiz_codigo_sincronizador;
        public $dominio_raiz_sincronizador;
        public $max_assinatura_por_hospedagem_INT;
        public $path_raiz_conteudo;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_nome;
        public $label_sistema_biblioteca_nuvem_INT;
        public $label_path_raiz_assinatura;
        public $label_path_raiz_dump;
        public $label_dominio_raiz_assinatura;
        public $label_path_raiz_codigo_assinatura;
        public $label_empresa_hosting_id_INT;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;
        public $label_sistema_tipo_id_INT;
        public $label_path_raiz_codigo_sincronizador;
        public $label_dominio_raiz_sincronizador;
        public $label_max_assinatura_por_hospedagem_INT;
        public $label_path_raiz_conteudo;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "";
            $this->nomeTabela = "sistema";
            $this->campoId = "id";
            $this->campoLabel = "nome";
        }

        public function valorCampoLabel()
        {
            return $this->getNome();
        }

        public function getFkObjEmpresa_hosting()
        {
            if ($this->objEmpresa_hosting == null)
            {
                $this->objEmpresa_hosting = new EXTDAO_Empresa_hosting($this->getConfiguracaoDAO());
            }
            $idFK = $this->getEmpresa_hosting_id_INT();
            if (!strlen($idFK))
            {
                $this->objEmpresa_hosting->clear();
            }
            else
            {
                if ($this->objEmpresa_hosting->getId() != $idFK)
                {
                    $this->objEmpresa_hosting->select($idFK);
                }
            }

            return $this->objEmpresa_hosting;
        }

        public function getComboBoxAllEmpresa_hosting($objArgumentos)
        {
            $objArgumentos->nome = "empresa_hosting_id_INT";
            $objArgumentos->id = "empresa_hosting_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objEmpresa_hosting = $this->getFkObjEmpresa_hosting();

            return $this->objEmpresa_hosting->getComboBox($objArgumentos);
        }

        public function getFkObjSistema_tipo()
        {
            if ($this->objSistema_tipo == null)
            {
                $this->objSistema_tipo = new EXTDAO_Sistema_tipo($this->getConfiguracaoDAO());
            }
            $idFK = $this->getSistema_tipo_id_INT();
            if (!strlen($idFK))
            {
                $this->objSistema_tipo->clear();
            }
            else
            {
                if ($this->objSistema_tipo->getId() != $idFK)
                {
                    $this->objSistema_tipo->select($idFK);
                }
            }

            return $this->objSistema_tipo;
        }

        public function getComboBoxAllSistema_tipo($objArgumentos)
        {
            $objArgumentos->nome = "sistema_tipo_id_INT";
            $objArgumentos->id = "sistema_tipo_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objSistema_tipo = $this->getFkObjSistema_tipo();

            return $this->objSistema_tipo->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = "O sistema foi cadastrado com sucesso.";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = "O sistema foi cadastrado com sucesso.";
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = "O sistema foi modificado com sucesso.";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = "O sistema foi exclu�do com sucesso.";
            $urlSuccess = Helper::getUrlAction("list_sistema", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getNome()
        {
            return $this->nome;
        }

        public function getSistema_biblioteca_nuvem_INT()
        {
            return $this->sistema_biblioteca_nuvem_INT;
        }

        public function getPath_raiz_assinatura()
        {
            return $this->path_raiz_assinatura;
        }

        public function getPath_raiz_dump()
        {
            return $this->path_raiz_dump;
        }

        public function getDominio_raiz_assinatura()
        {
            return $this->dominio_raiz_assinatura;
        }

        public function getPath_raiz_codigo_assinatura()
        {
            return $this->path_raiz_codigo_assinatura;
        }

        public function getEmpresa_hosting_id_INT()
        {
            return $this->empresa_hosting_id_INT;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        public function getSistema_tipo_id_INT()
        {
            return $this->sistema_tipo_id_INT;
        }

        public function getPath_raiz_codigo_sincronizador()
        {
            return $this->path_raiz_codigo_sincronizador;
        }

        public function getDominio_raiz_sincronizador()
        {
            return $this->dominio_raiz_sincronizador;
        }

        public function getMax_assinatura_por_hospedagem_INT()
        {
            return $this->max_assinatura_por_hospedagem_INT;
        }

        public function getPath_raiz_conteudo()
        {
            return $this->path_raiz_conteudo;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setNome($val)
        {
            $this->nome = $val;
        }

        function setSistema_biblioteca_nuvem_INT($val)
        {
            $this->sistema_biblioteca_nuvem_INT = $val;
        }

        function setPath_raiz_assinatura($val)
        {
            $this->path_raiz_assinatura = $val;
        }

        function setPath_raiz_dump($val)
        {
            $this->path_raiz_dump = $val;
        }

        function setDominio_raiz_assinatura($val)
        {
            $this->dominio_raiz_assinatura = $val;
        }

        function setPath_raiz_codigo_assinatura($val)
        {
            $this->path_raiz_codigo_assinatura = $val;
        }

        function setEmpresa_hosting_id_INT($val)
        {
            $this->empresa_hosting_id_INT = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }

        function setSistema_tipo_id_INT($val)
        {
            $this->sistema_tipo_id_INT = $val;
        }

        function setPath_raiz_codigo_sincronizador($val)
        {
            $this->path_raiz_codigo_sincronizador = $val;
        }

        function setDominio_raiz_sincronizador($val)
        {
            $this->dominio_raiz_sincronizador = $val;
        }

        function setMax_assinatura_por_hospedagem_INT($val)
        {
            $this->max_assinatura_por_hospedagem_INT = $val;
        }

        function setPath_raiz_conteudo($val)
        {
            $this->path_raiz_conteudo = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM sistema WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->nome = $row->nome;

            $this->sistema_biblioteca_nuvem_INT = $row->sistema_biblioteca_nuvem_INT;

            $this->path_raiz_assinatura = $row->path_raiz_assinatura;

            $this->path_raiz_dump = $row->path_raiz_dump;

            $this->dominio_raiz_assinatura = $row->dominio_raiz_assinatura;

            $this->path_raiz_codigo_assinatura = $row->path_raiz_codigo_assinatura;

            $this->empresa_hosting_id_INT = $row->empresa_hosting_id_INT;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;

            $this->sistema_tipo_id_INT = $row->sistema_tipo_id_INT;

            $this->path_raiz_codigo_sincronizador = $row->path_raiz_codigo_sincronizador;

            $this->dominio_raiz_sincronizador = $row->dominio_raiz_sincronizador;

            $this->max_assinatura_por_hospedagem_INT = $row->max_assinatura_por_hospedagem_INT;

            $this->path_raiz_conteudo = $row->path_raiz_conteudo;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->nome = null;
            $this->sistema_biblioteca_nuvem_INT = null;
            $this->path_raiz_assinatura = null;
            $this->path_raiz_dump = null;
            $this->dominio_raiz_assinatura = null;
            $this->path_raiz_codigo_assinatura = null;
            $this->empresa_hosting_id_INT = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
            $this->sistema_tipo_id_INT = null;
            $this->path_raiz_codigo_sincronizador = null;
            $this->dominio_raiz_sincronizador = null;
            $this->max_assinatura_por_hospedagem_INT = null;
            $this->path_raiz_conteudo = null;
        }


        // **********************
        // DELETE
        // **********************
        public function delete($id)
        {
            $sql = "UPDATE sistema SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************
        public function insert()
        {
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO sistema ( id , nome , sistema_biblioteca_nuvem_INT , path_raiz_assinatura , path_raiz_dump , dominio_raiz_assinatura , path_raiz_codigo_assinatura , empresa_hosting_id_INT , excluido_BOOLEAN , excluido_DATETIME , sistema_tipo_id_INT , path_raiz_codigo_sincronizador , dominio_raiz_sincronizador , max_assinatura_por_hospedagem_INT , path_raiz_conteudo ) VALUES ( {$this->id} , {$this->nome} , {$this->sistema_biblioteca_nuvem_INT} , {$this->path_raiz_assinatura} , {$this->path_raiz_dump} , {$this->dominio_raiz_assinatura} , {$this->path_raiz_codigo_assinatura} , {$this->empresa_hosting_id_INT} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} , {$this->sistema_tipo_id_INT} , {$this->path_raiz_codigo_sincronizador} , {$this->dominio_raiz_sincronizador} , {$this->max_assinatura_por_hospedagem_INT} , {$this->path_raiz_conteudo} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome()
        {
            return "nome";
        }

        public function nomeCampoSistema_biblioteca_nuvem_INT()
        {
            return "sistema_biblioteca_nuvem_INT";
        }

        public function nomeCampoPath_raiz_assinatura()
        {
            return "path_raiz_assinatura";
        }

        public function nomeCampoPath_raiz_dump()
        {
            return "path_raiz_dump";
        }

        public function nomeCampoDominio_raiz_assinatura()
        {
            return "dominio_raiz_assinatura";
        }

        public function nomeCampoPath_raiz_codigo_assinatura()
        {
            return "path_raiz_codigo_assinatura";
        }

        public function nomeCampoEmpresa_hosting_id_INT()
        {
            return "empresa_hosting_id_INT";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }

        public function nomeCampoSistema_tipo_id_INT()
        {
            return "sistema_tipo_id_INT";
        }

        public function nomeCampoPath_raiz_codigo_sincronizador()
        {
            return "path_raiz_codigo_sincronizador";
        }

        public function nomeCampoDominio_raiz_sincronizador()
        {
            return "dominio_raiz_sincronizador";
        }

        public function nomeCampoMax_assinatura_por_hospedagem_INT()
        {
            return "max_assinatura_por_hospedagem_INT";
        }

        public function nomeCampoPath_raiz_conteudo()
        {
            return "path_raiz_conteudo";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome($objArguments)
        {
            $objArguments->nome = "nome";
            $objArguments->id = "nome";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoSistema_biblioteca_nuvem_INT($objArguments)
        {
            $objArguments->nome = "sistema_biblioteca_nuvem_INT";
            $objArguments->id = "sistema_biblioteca_nuvem_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoPath_raiz_assinatura($objArguments)
        {
            $objArguments->nome = "path_raiz_assinatura";
            $objArguments->id = "path_raiz_assinatura";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPath_raiz_dump($objArguments)
        {
            $objArguments->nome = "path_raiz_dump";
            $objArguments->id = "path_raiz_dump";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoDominio_raiz_assinatura($objArguments)
        {
            $objArguments->nome = "dominio_raiz_assinatura";
            $objArguments->id = "dominio_raiz_assinatura";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPath_raiz_codigo_assinatura($objArguments)
        {
            $objArguments->nome = "path_raiz_codigo_assinatura";
            $objArguments->id = "path_raiz_codigo_assinatura";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoEmpresa_hosting_id_INT($objArguments)
        {
            $objArguments->nome = "empresa_hosting_id_INT";
            $objArguments->id = "empresa_hosting_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }

        public function imprimirCampoSistema_tipo_id_INT($objArguments)
        {
            $objArguments->nome = "sistema_tipo_id_INT";
            $objArguments->id = "sistema_tipo_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoPath_raiz_codigo_sincronizador($objArguments)
        {
            $objArguments->nome = "path_raiz_codigo_sincronizador";
            $objArguments->id = "path_raiz_codigo_sincronizador";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoDominio_raiz_sincronizador($objArguments)
        {
            $objArguments->nome = "dominio_raiz_sincronizador";
            $objArguments->id = "dominio_raiz_sincronizador";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoMax_assinatura_por_hospedagem_INT($objArguments)
        {
            $objArguments->nome = "max_assinatura_por_hospedagem_INT";
            $objArguments->id = "max_assinatura_por_hospedagem_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoPath_raiz_conteudo($objArguments)
        {
            $objArguments->nome = "path_raiz_conteudo";
            $objArguments->id = "path_raiz_conteudo";

            return $this->campoTexto($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->sistema_biblioteca_nuvem_INT == null)
            {
                $this->sistema_biblioteca_nuvem_INT = "null";
            }

            if ($this->empresa_hosting_id_INT == null)
            {
                $this->empresa_hosting_id_INT = "null";
            }

            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            if ($this->sistema_tipo_id_INT == null)
            {
                $this->sistema_tipo_id_INT = "null";
            }

            if ($this->max_assinatura_por_hospedagem_INT == null)
            {
                $this->max_assinatura_por_hospedagem_INT = "null";
            }

            $this->nome = $this->formatarDadosParaSQL($this->nome);
            $this->path_raiz_assinatura = $this->formatarDadosParaSQL($this->path_raiz_assinatura);
            $this->path_raiz_dump = $this->formatarDadosParaSQL($this->path_raiz_dump);
            $this->dominio_raiz_assinatura = $this->formatarDadosParaSQL($this->dominio_raiz_assinatura);
            $this->path_raiz_codigo_assinatura = $this->formatarDadosParaSQL($this->path_raiz_codigo_assinatura);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
            $this->path_raiz_codigo_sincronizador = $this->formatarDadosParaSQL($this->path_raiz_codigo_sincronizador);
            $this->dominio_raiz_sincronizador = $this->formatarDadosParaSQL($this->dominio_raiz_sincronizador);
            $this->path_raiz_conteudo = $this->formatarDadosParaSQL($this->path_raiz_conteudo);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("nome", $this->nome);
            Helper::setSession("sistema_biblioteca_nuvem_INT", $this->sistema_biblioteca_nuvem_INT);
            Helper::setSession("path_raiz_assinatura", $this->path_raiz_assinatura);
            Helper::setSession("path_raiz_dump", $this->path_raiz_dump);
            Helper::setSession("dominio_raiz_assinatura", $this->dominio_raiz_assinatura);
            Helper::setSession("path_raiz_codigo_assinatura", $this->path_raiz_codigo_assinatura);
            Helper::setSession("empresa_hosting_id_INT", $this->empresa_hosting_id_INT);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
            Helper::setSession("sistema_tipo_id_INT", $this->sistema_tipo_id_INT);
            Helper::setSession("path_raiz_codigo_sincronizador", $this->path_raiz_codigo_sincronizador);
            Helper::setSession("dominio_raiz_sincronizador", $this->dominio_raiz_sincronizador);
            Helper::setSession("max_assinatura_por_hospedagem_INT", $this->max_assinatura_por_hospedagem_INT);
            Helper::setSession("path_raiz_conteudo", $this->path_raiz_conteudo);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("nome");
            Helper::clearSession("sistema_biblioteca_nuvem_INT");
            Helper::clearSession("path_raiz_assinatura");
            Helper::clearSession("path_raiz_dump");
            Helper::clearSession("dominio_raiz_assinatura");
            Helper::clearSession("path_raiz_codigo_assinatura");
            Helper::clearSession("empresa_hosting_id_INT");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
            Helper::clearSession("sistema_tipo_id_INT");
            Helper::clearSession("path_raiz_codigo_sincronizador");
            Helper::clearSession("dominio_raiz_sincronizador");
            Helper::clearSession("max_assinatura_por_hospedagem_INT");
            Helper::clearSession("path_raiz_conteudo");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = Helper::SESSION("id{$numReg}");
            $this->nome = Helper::SESSION("nome{$numReg}");
            $this->sistema_biblioteca_nuvem_INT = Helper::SESSION("sistema_biblioteca_nuvem_INT{$numReg}");
            $this->path_raiz_assinatura = Helper::SESSION("path_raiz_assinatura{$numReg}");
            $this->path_raiz_dump = Helper::SESSION("path_raiz_dump{$numReg}");
            $this->dominio_raiz_assinatura = Helper::SESSION("dominio_raiz_assinatura{$numReg}");
            $this->path_raiz_codigo_assinatura = Helper::SESSION("path_raiz_codigo_assinatura{$numReg}");
            $this->empresa_hosting_id_INT = Helper::SESSION("empresa_hosting_id_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::SESSION("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::SESSION("excluido_DATETIME{$numReg}");
            $this->sistema_tipo_id_INT = Helper::SESSION("sistema_tipo_id_INT{$numReg}");
            $this->path_raiz_codigo_sincronizador = Helper::SESSION("path_raiz_codigo_sincronizador{$numReg}");
            $this->dominio_raiz_sincronizador = Helper::SESSION("dominio_raiz_sincronizador{$numReg}");
            $this->max_assinatura_por_hospedagem_INT = Helper::SESSION("max_assinatura_por_hospedagem_INT{$numReg}");
            $this->path_raiz_conteudo = Helper::SESSION("path_raiz_conteudo{$numReg}");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = Helper::POST("id{$numReg}");
            $this->nome = Helper::POST("nome{$numReg}");
            $this->sistema_biblioteca_nuvem_INT = Helper::POST("sistema_biblioteca_nuvem_INT{$numReg}");
            $this->path_raiz_assinatura = Helper::POST("path_raiz_assinatura{$numReg}");
            $this->path_raiz_dump = Helper::POST("path_raiz_dump{$numReg}");
            $this->dominio_raiz_assinatura = Helper::POST("dominio_raiz_assinatura{$numReg}");
            $this->path_raiz_codigo_assinatura = Helper::POST("path_raiz_codigo_assinatura{$numReg}");
            $this->empresa_hosting_id_INT = Helper::POST("empresa_hosting_id_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::POST("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::POST("excluido_DATETIME{$numReg}");
            $this->sistema_tipo_id_INT = Helper::POST("sistema_tipo_id_INT{$numReg}");
            $this->path_raiz_codigo_sincronizador = Helper::POST("path_raiz_codigo_sincronizador{$numReg}");
            $this->dominio_raiz_sincronizador = Helper::POST("dominio_raiz_sincronizador{$numReg}");
            $this->max_assinatura_por_hospedagem_INT = Helper::POST("max_assinatura_por_hospedagem_INT{$numReg}");
            $this->path_raiz_conteudo = Helper::POST("path_raiz_conteudo{$numReg}");
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = Helper::GET("id{$numReg}");
            $this->nome = Helper::GET("nome{$numReg}");
            $this->sistema_biblioteca_nuvem_INT = Helper::GET("sistema_biblioteca_nuvem_INT{$numReg}");
            $this->path_raiz_assinatura = Helper::GET("path_raiz_assinatura{$numReg}");
            $this->path_raiz_dump = Helper::GET("path_raiz_dump{$numReg}");
            $this->dominio_raiz_assinatura = Helper::GET("dominio_raiz_assinatura{$numReg}");
            $this->path_raiz_codigo_assinatura = Helper::GET("path_raiz_codigo_assinatura{$numReg}");
            $this->empresa_hosting_id_INT = Helper::GET("empresa_hosting_id_INT{$numReg}");
            $this->excluido_BOOLEAN = Helper::GET("excluido_BOOLEAN{$numReg}");
            $this->excluido_DATETIME = Helper::GET("excluido_DATETIME{$numReg}");
            $this->sistema_tipo_id_INT = Helper::GET("sistema_tipo_id_INT{$numReg}");
            $this->path_raiz_codigo_sincronizador = Helper::GET("path_raiz_codigo_sincronizador{$numReg}");
            $this->dominio_raiz_sincronizador = Helper::GET("dominio_raiz_sincronizador{$numReg}");
            $this->max_assinatura_por_hospedagem_INT = Helper::GET("max_assinatura_por_hospedagem_INT{$numReg}");
            $this->path_raiz_conteudo = Helper::GET("path_raiz_conteudo{$numReg}");
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["nome{$numReg}"]) || $tipo == null)
            {
                $upd .= "nome = $this->nome, ";
            }

            if (isset($tipo["sistema_biblioteca_nuvem_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "sistema_biblioteca_nuvem_INT = $this->sistema_biblioteca_nuvem_INT, ";
            }

            if (isset($tipo["path_raiz_assinatura{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_raiz_assinatura = $this->path_raiz_assinatura, ";
            }

            if (isset($tipo["path_raiz_dump{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_raiz_dump = $this->path_raiz_dump, ";
            }

            if (isset($tipo["dominio_raiz_assinatura{$numReg}"]) || $tipo == null)
            {
                $upd .= "dominio_raiz_assinatura = $this->dominio_raiz_assinatura, ";
            }

            if (isset($tipo["path_raiz_codigo_assinatura{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_raiz_codigo_assinatura = $this->path_raiz_codigo_assinatura, ";
            }

            if (isset($tipo["empresa_hosting_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "empresa_hosting_id_INT = $this->empresa_hosting_id_INT, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            if (isset($tipo["sistema_tipo_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "sistema_tipo_id_INT = $this->sistema_tipo_id_INT, ";
            }

            if (isset($tipo["path_raiz_codigo_sincronizador{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_raiz_codigo_sincronizador = $this->path_raiz_codigo_sincronizador, ";
            }

            if (isset($tipo["dominio_raiz_sincronizador{$numReg}"]) || $tipo == null)
            {
                $upd .= "dominio_raiz_sincronizador = $this->dominio_raiz_sincronizador, ";
            }

            if (isset($tipo["max_assinatura_por_hospedagem_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "max_assinatura_por_hospedagem_INT = $this->max_assinatura_por_hospedagem_INT, ";
            }

            if (isset($tipo["path_raiz_conteudo{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_raiz_conteudo = $this->path_raiz_conteudo, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE sistema SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim

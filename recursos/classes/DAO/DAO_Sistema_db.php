<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Sistema_db
    * DATA DE GERA��O: 12.05.2017
    * ARQUIVO:         DAO_Sistema_db.php
    * TABELA MYSQL:    sistema_db
    * BANCO DE DADOS:  hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Sistema_db extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $identificador;
        public $assinatura_host_db;
        public $assinatura_porta_db_INT;
        public $assinatura_usuario_db;
        public $assinatura_senha_db;
        public $path_arquivo_dump;
        public $tipo_hospedagem_db_id_INT;
        public $objTipo_hospedagem_db;
        public $sistema_id_INT;
        public $objSistema;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_identificador;
        public $label_assinatura_host_db;
        public $label_assinatura_porta_db_INT;
        public $label_assinatura_usuario_db;
        public $label_assinatura_senha_db;
        public $label_path_arquivo_dump;
        public $label_tipo_hospedagem_db_id_INT;
        public $label_sistema_id_INT;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "banco do sistema";
            $this->nomeTabela = "sistema_db";
            $this->campoId = "id";
            $this->campoLabel = "id";
        }

        public function valorCampoLabel()
        {
            return $this->getId();
        }

        public function getFkObjTipo_hospedagem_db()
        {
            if ($this->objTipo_hospedagem_db == null)
            {
                $this->objTipo_hospedagem_db = new EXTDAO_Tipo_hospedagem_db($this->getConfiguracaoDAO());
            }
            $idFK = $this->getTipo_hospedagem_db_id_INT();
            if (!strlen($idFK))
            {
                $this->objTipo_hospedagem_db->clear();
            }
            else
            {
                if ($this->objTipo_hospedagem_db->getId() != $idFK)
                {
                    $this->objTipo_hospedagem_db->select($idFK);
                }
            }

            return $this->objTipo_hospedagem_db;
        }

        public function getComboBoxAllTipo_hospedagem_db($objArgumentos)
        {
            $objArgumentos->nome = "tipo_hospedagem_db_id_INT";
            $objArgumentos->id = "tipo_hospedagem_db_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objTipo_hospedagem_db = $this->getFkObjTipo_hospedagem_db();

            return $this->objTipo_hospedagem_db->getComboBox($objArgumentos);
        }

        public function getFkObjSistema()
        {
            if ($this->objSistema == null)
            {
                $this->objSistema = new EXTDAO_Sistema($this->getConfiguracaoDAO());
            }
            $idFK = $this->getSistema_id_INT();
            if (!strlen($idFK))
            {
                $this->objSistema->clear();
            }
            else
            {
                if ($this->objSistema->getId() != $idFK)
                {
                    $this->objSistema->select($idFK);
                }
            }

            return $this->objSistema;
        }

        public function getComboBoxAllSistema($objArgumentos)
        {
            $objArgumentos->nome = "sistema_id_INT";
            $objArgumentos->id = "sistema_id_INT";
            $objArgumentos->valueReplaceId = false;

            $this->objSistema = $this->getFkObjSistema();

            return $this->objSistema->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O banco do sistema foi cadastrado com sucesso.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O banco do sistema foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O banco do sistema foi modificado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O banco do sistema foi exclu�do com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_sistema_db", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_sistema_db", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }




        // **********************
        // M�TODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getIdentificador()
        {
            return $this->identificador;
        }

        public function getAssinatura_host_db()
        {
            return $this->assinatura_host_db;
        }

        public function getAssinatura_porta_db_INT()
        {
            return $this->assinatura_porta_db_INT;
        }

        public function getAssinatura_usuario_db()
        {
            return $this->assinatura_usuario_db;
        }

        public function getAssinatura_senha_db()
        {
            return $this->assinatura_senha_db;
        }

        public function getPath_arquivo_dump()
        {
            return $this->path_arquivo_dump;
        }

        public function getTipo_hospedagem_db_id_INT()
        {
            return $this->tipo_hospedagem_db_id_INT;
        }

        public function getSistema_id_INT()
        {
            return $this->sistema_id_INT;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setIdentificador($val)
        {
            $this->identificador = $val;
        }

        function setAssinatura_host_db($val)
        {
            $this->assinatura_host_db = $val;
        }

        function setAssinatura_porta_db_INT($val)
        {
            $this->assinatura_porta_db_INT = $val;
        }

        function setAssinatura_usuario_db($val)
        {
            $this->assinatura_usuario_db = $val;
        }

        function setAssinatura_senha_db($val)
        {
            $this->assinatura_senha_db = $val;
        }

        function setPath_arquivo_dump($val)
        {
            $this->path_arquivo_dump = $val;
        }

        function setTipo_hospedagem_db_id_INT($val)
        {
            $this->tipo_hospedagem_db_id_INT = $val;
        }

        function setSistema_id_INT($val)
        {
            $this->sistema_id_INT = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM sistema_db WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->identificador = $row->identificador;

            $this->assinatura_host_db = $row->assinatura_host_db;

            $this->assinatura_porta_db_INT = $row->assinatura_porta_db_INT;

            $this->assinatura_usuario_db = $row->assinatura_usuario_db;

            $this->assinatura_senha_db = $row->assinatura_senha_db;

            $this->path_arquivo_dump = $row->path_arquivo_dump;

            $this->tipo_hospedagem_db_id_INT = $row->tipo_hospedagem_db_id_INT;

            $this->sistema_id_INT = $row->sistema_id_INT;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->identificador = null;
            $this->assinatura_host_db = null;
            $this->assinatura_porta_db_INT = null;
            $this->assinatura_usuario_db = null;
            $this->assinatura_senha_db = null;
            $this->path_arquivo_dump = null;
            $this->tipo_hospedagem_db_id_INT = null;
            $this->sistema_id_INT = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE sistema_db SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO sistema_db ( identificador , assinatura_host_db , assinatura_porta_db_INT , assinatura_usuario_db , assinatura_senha_db , path_arquivo_dump , tipo_hospedagem_db_id_INT , sistema_id_INT , excluido_BOOLEAN , excluido_DATETIME ) VALUES ( {$this->identificador} , {$this->assinatura_host_db} , {$this->assinatura_porta_db_INT} , {$this->assinatura_usuario_db} , {$this->assinatura_senha_db} , {$this->path_arquivo_dump} , {$this->tipo_hospedagem_db_id_INT} , {$this->sistema_id_INT} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoIdentificador()
        {
            return "identificador";
        }

        public function nomeCampoAssinatura_host_db()
        {
            return "assinatura_host_db";
        }

        public function nomeCampoAssinatura_porta_db_INT()
        {
            return "assinatura_porta_db_INT";
        }

        public function nomeCampoAssinatura_usuario_db()
        {
            return "assinatura_usuario_db";
        }

        public function nomeCampoAssinatura_senha_db()
        {
            return "assinatura_senha_db";
        }

        public function nomeCampoPath_arquivo_dump()
        {
            return "path_arquivo_dump";
        }

        public function nomeCampoTipo_hospedagem_db_id_INT()
        {
            return "tipo_hospedagem_db_id_INT";
        }

        public function nomeCampoSistema_id_INT()
        {
            return "sistema_id_INT";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoIdentificador($objArguments)
        {
            $objArguments->nome = "identificador";
            $objArguments->id = "identificador";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoAssinatura_host_db($objArguments)
        {
            $objArguments->nome = "assinatura_host_db";
            $objArguments->id = "assinatura_host_db";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoAssinatura_porta_db_INT($objArguments)
        {
            $objArguments->nome = "assinatura_porta_db_INT";
            $objArguments->id = "assinatura_porta_db_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoAssinatura_usuario_db($objArguments)
        {
            $objArguments->nome = "assinatura_usuario_db";
            $objArguments->id = "assinatura_usuario_db";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoAssinatura_senha_db($objArguments)
        {
            $objArguments->nome = "assinatura_senha_db";
            $objArguments->id = "assinatura_senha_db";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoPath_arquivo_dump($objArguments)
        {
            $objArguments->nome = "path_arquivo_dump";
            $objArguments->id = "path_arquivo_dump";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoTipo_hospedagem_db_id_INT($objArguments)
        {
            $objArguments->nome = "tipo_hospedagem_db_id_INT";
            $objArguments->id = "tipo_hospedagem_db_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoSistema_id_INT($objArguments)
        {
            $objArguments->nome = "sistema_id_INT";
            $objArguments->id = "sistema_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->assinatura_porta_db_INT == null)
            {
                $this->assinatura_porta_db_INT = "null";
            }

            if ($this->tipo_hospedagem_db_id_INT == null)
            {
                $this->tipo_hospedagem_db_id_INT = "null";
            }

            if ($this->sistema_id_INT == null)
            {
                $this->sistema_id_INT = "null";
            }

            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->identificador = $this->formatarDadosParaSQL($this->identificador);
            $this->assinatura_host_db = $this->formatarDadosParaSQL($this->assinatura_host_db);
            $this->assinatura_usuario_db = $this->formatarDadosParaSQL($this->assinatura_usuario_db);
            $this->assinatura_senha_db = $this->formatarDadosParaSQL($this->assinatura_senha_db);
            $this->path_arquivo_dump = $this->formatarDadosParaSQL($this->path_arquivo_dump);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("identificador", $this->identificador);
            Helper::setSession("assinatura_host_db", $this->assinatura_host_db);
            Helper::setSession("assinatura_porta_db_INT", $this->assinatura_porta_db_INT);
            Helper::setSession("assinatura_usuario_db", $this->assinatura_usuario_db);
            Helper::setSession("assinatura_senha_db", $this->assinatura_senha_db);
            Helper::setSession("path_arquivo_dump", $this->path_arquivo_dump);
            Helper::setSession("tipo_hospedagem_db_id_INT", $this->tipo_hospedagem_db_id_INT);
            Helper::setSession("sistema_id_INT", $this->sistema_id_INT);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("identificador");
            Helper::clearSession("assinatura_host_db");
            Helper::clearSession("assinatura_porta_db_INT");
            Helper::clearSession("assinatura_usuario_db");
            Helper::clearSession("assinatura_senha_db");
            Helper::clearSession("path_arquivo_dump");
            Helper::clearSession("tipo_hospedagem_db_id_INT");
            Helper::clearSession("sistema_id_INT");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::SESSION("id{$numReg}"));
            $this->identificador = $this->formatarDadosParaSQL(Helper::SESSION("identificador{$numReg}"));
            $this->assinatura_host_db = $this->formatarDadosParaSQL(Helper::SESSION("assinatura_host_db{$numReg}"));
            $this->assinatura_porta_db_INT = $this->formatarDadosParaSQL(Helper::SESSION("assinatura_porta_db_INT{$numReg}"));
            $this->assinatura_usuario_db = $this->formatarDadosParaSQL(Helper::SESSION("assinatura_usuario_db{$numReg}"));
            $this->assinatura_senha_db = $this->formatarDadosParaSQL(Helper::SESSION("assinatura_senha_db{$numReg}"));
            $this->path_arquivo_dump = $this->formatarDadosParaSQL(Helper::SESSION("path_arquivo_dump{$numReg}"));
            $this->tipo_hospedagem_db_id_INT = $this->formatarDadosParaSQL(Helper::SESSION("tipo_hospedagem_db_id_INT{$numReg}"));
            $this->sistema_id_INT = $this->formatarDadosParaSQL(Helper::SESSION("sistema_id_INT{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::SESSION("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::SESSION("excluido_DATETIME{$numReg}"));
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::POST("id{$numReg}"));
            $this->identificador = $this->formatarDadosParaSQL(Helper::POST("identificador{$numReg}"));
            $this->assinatura_host_db = $this->formatarDadosParaSQL(Helper::POST("assinatura_host_db{$numReg}"));
            $this->assinatura_porta_db_INT = $this->formatarDadosParaSQL(Helper::POST("assinatura_porta_db_INT{$numReg}"));
            $this->assinatura_usuario_db = $this->formatarDadosParaSQL(Helper::POST("assinatura_usuario_db{$numReg}"));
            $this->assinatura_senha_db = $this->formatarDadosParaSQL(Helper::POST("assinatura_senha_db{$numReg}"));
            $this->path_arquivo_dump = $this->formatarDadosParaSQL(Helper::POST("path_arquivo_dump{$numReg}"));
            $this->tipo_hospedagem_db_id_INT = $this->formatarDadosParaSQL(Helper::POST("tipo_hospedagem_db_id_INT{$numReg}"));
            $this->sistema_id_INT = $this->formatarDadosParaSQL(Helper::POST("sistema_id_INT{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::POST("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::POST("excluido_DATETIME{$numReg}"));
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::GET("id{$numReg}"));
            $this->identificador = $this->formatarDadosParaSQL(Helper::GET("identificador{$numReg}"));
            $this->assinatura_host_db = $this->formatarDadosParaSQL(Helper::GET("assinatura_host_db{$numReg}"));
            $this->assinatura_porta_db_INT = $this->formatarDadosParaSQL(Helper::GET("assinatura_porta_db_INT{$numReg}"));
            $this->assinatura_usuario_db = $this->formatarDadosParaSQL(Helper::GET("assinatura_usuario_db{$numReg}"));
            $this->assinatura_senha_db = $this->formatarDadosParaSQL(Helper::GET("assinatura_senha_db{$numReg}"));
            $this->path_arquivo_dump = $this->formatarDadosParaSQL(Helper::GET("path_arquivo_dump{$numReg}"));
            $this->tipo_hospedagem_db_id_INT = $this->formatarDadosParaSQL(Helper::GET("tipo_hospedagem_db_id_INT{$numReg}"));
            $this->sistema_id_INT = $this->formatarDadosParaSQL(Helper::GET("sistema_id_INT{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::GET("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::GET("excluido_DATETIME{$numReg}"));
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["identificador{$numReg}"]) || $tipo == null)
            {
                $upd .= "identificador = $this->identificador, ";
            }

            if (isset($tipo["assinatura_host_db{$numReg}"]) || $tipo == null)
            {
                $upd .= "assinatura_host_db = $this->assinatura_host_db, ";
            }

            if (isset($tipo["assinatura_porta_db_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "assinatura_porta_db_INT = $this->assinatura_porta_db_INT, ";
            }

            if (isset($tipo["assinatura_usuario_db{$numReg}"]) || $tipo == null)
            {
                $upd .= "assinatura_usuario_db = $this->assinatura_usuario_db, ";
            }

            if (isset($tipo["assinatura_senha_db{$numReg}"]) || $tipo == null)
            {
                $upd .= "assinatura_senha_db = $this->assinatura_senha_db, ";
            }

            if (isset($tipo["path_arquivo_dump{$numReg}"]) || $tipo == null)
            {
                $upd .= "path_arquivo_dump = $this->path_arquivo_dump, ";
            }

            if (isset($tipo["tipo_hospedagem_db_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "tipo_hospedagem_db_id_INT = $this->tipo_hospedagem_db_id_INT, ";
            }

            if (isset($tipo["sistema_id_INT{$numReg}"]) || $tipo == null)
            {
                $upd .= "sistema_id_INT = $this->sistema_id_INT, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE sistema_db SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim

<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Tipo_hospedagem_db
    * DATA DE GERA��O: 12.05.2017
    * ARQUIVO:         DAO_Tipo_hospedagem_db.php
    * TABELA MYSQL:    tipo_hospedagem_db
    * BANCO DE DADOS:  hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Tipo_hospedagem_db extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $nome;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_nome;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************
        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeEntidade = "tipo de hospedagem db";
            $this->nomeTabela = "tipo_hospedagem_db";
            $this->campoId = "id";
            $this->campoLabel = "nome";
        }

        public function valorCampoLabel()
        {
            return $this->getNome();
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de hospedagem db foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de hospedagem db foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $msg = $this->insert();
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de hospedagem db foi modificado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $msg = $this->update($this->getId(), $_POST, $i);
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O tipo de hospedagem db foi exclu�do com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_tipo_hospedagem_db", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_tipo_hospedagem_db", Helper::GET("id"));

            $registroRemover = Helper::GET("id");

            $msg = $this->delete("$registroRemover");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }




        // **********************
        // M�TODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getNome()
        {
            return $this->nome;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setNome($val)
        {
            $this->nome = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM tipo_hospedagem_db WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                $this->database->closeResult();

                return $msg;
            }
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);
            $this->database->closeResult();

            $this->id = $row->id;

            $this->nome = $row->nome;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }

        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $this->id = null;
            $this->nome = null;
            $this->excluido_BOOLEAN = null;
            $this->excluido_DATETIME = null;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE tipo_hospedagem_db SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO tipo_hospedagem_db ( nome , excluido_BOOLEAN , excluido_DATETIME ) VALUES ( {$this->nome} , {$this->excluido_BOOLEAN} , {$this->excluido_DATETIME} )";
            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome()
        {
            return "nome";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome($objArguments)
        {
            $objArguments->nome = "nome";
            $objArguments->id = "nome";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->excluido_BOOLEAN == null)
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->nome = $this->formatarDadosParaSQL($this->nome);
            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            Helper::setSession("id", $this->id);
            Helper::setSession("nome", $this->nome);
            Helper::setSession("excluido_BOOLEAN", $this->excluido_BOOLEAN);
            Helper::setSession("excluido_DATETIME", $this->excluido_DATETIME);
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            Helper::clearSession("id");
            Helper::clearSession("nome");
            Helper::clearSession("excluido_BOOLEAN");
            Helper::clearSession("excluido_DATETIME");
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::SESSION("id{$numReg}"));
            $this->nome = $this->formatarDadosParaSQL(Helper::SESSION("nome{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::SESSION("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::SESSION("excluido_DATETIME{$numReg}"));
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::POST("id{$numReg}"));
            $this->nome = $this->formatarDadosParaSQL(Helper::POST("nome{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::POST("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::POST("excluido_DATETIME{$numReg}"));
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDadosParaSQL(Helper::GET("id{$numReg}"));
            $this->nome = $this->formatarDadosParaSQL(Helper::GET("nome{$numReg}"));
            $this->excluido_BOOLEAN = $this->formatarDadosParaSQL(Helper::GET("excluido_BOOLEAN{$numReg}"));
            $this->excluido_DATETIME = $this->formatarDadosParaSQL(Helper::GET("excluido_DATETIME{$numReg}"));
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = null, $numReg = 1)
        {
            $upd = "";
            if (isset($tipo["nome{$numReg}"]) || $tipo == null)
            {
                $upd .= "nome = $this->nome, ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == null)
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE tipo_hospedagem_db SET $upd WHERE id = $id ";

            $msg = $this->database->queryMensagem($sql);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
        }

    } // classe: fim

<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Usuario
    * DATA DE GERAÇÃO: 17.10.2011
    * ARQUIVO:         DAO_Usuario.php
    * TABELA MYSQL:    usuario
    * BANCO DE DADOS:  gestao_compras_publicas_privado
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class DAO_Usuario extends Generic_DAO
    {

        // *************************
        // DECLARAÇÃO DE ATRIBUTOS
        // *************************

        public $id;
        public $nome;
        public $email;
        public $senha;
        public $usuario_tipo_id_INT;
        public $objUsuario_tipo;
        public $status_BOOLEAN;
        public $pagina_inicial;
        public $excluido_BOOLEAN;
        public $excluido_DATETIME;

        public $nomeEntidade;

        public $excluido_DATETIME_UNIX;

        public $label_id;
        public $label_nome;
        public $label_email;
        public $label_senha;
        public $label_usuario_tipo_id_INT;
        public $label_status_BOOLEAN;
        public $label_pagina_inicial;
        public $label_excluido_BOOLEAN;
        public $label_excluido_DATETIME;

        // **********************
        // MÉTODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "";
            $this->nomeTabela = "usuario";
            $this->campoId = "id";
            $this->campoLabel = "nome";

            $this->objUsuario_tipo = new EXTDAO_Usuario_tipo();
        }

        public function valorCampoLabel()
        {
            return $this->getNome();
        }

        public function getComboBoxAllUsuario_tipo($objArgumentos)
        {
            $objArgumentos->nome = "usuario_tipo_id_INT";
            $objArgumentos->id = "usuario_tipo_id_INT";
            $objArgumentos->valueReplaceId = false;

            return $this->objUsuario_tipo->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = "O usuário foi cadastrado com sucesso.";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = "O usuário foi cadastrado com sucesso.";
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = "O usuário foi modificado com sucesso.";
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = "O usuário foi excluído com sucesso.";
            $urlSuccess = Helper::getUrlAction("list_usuario", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_usuario", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete("$registroRemover");

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }




        // **********************
        // MÉTODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getNome()
        {
            return $this->nome;
        }

        public function getEmail()
        {
            return $this->email;
        }

        public function getSenha()
        {
            return $this->senha;
        }

        public function getUsuario_tipo_id_INT()
        {
            return $this->usuario_tipo_id_INT;
        }

        public function getStatus_BOOLEAN()
        {
            return $this->status_BOOLEAN;
        }

        public function getPagina_inicial()
        {
            return $this->pagina_inicial;
        }

        public function getExcluido_BOOLEAN()
        {
            return $this->excluido_BOOLEAN;
        }

        function getExcluido_DATETIME_UNIX()
        {
            return $this->excluido_DATETIME_UNIX;
        }

        public function getExcluido_DATETIME()
        {
            return $this->excluido_DATETIME;
        }

        // **********************
        // MÉTODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setNome($val)
        {
            $this->nome = $val;
        }

        function setEmail($val)
        {
            $this->email = $val;
        }

        function setSenha($val)
        {
            $this->senha = $val;
        }

        function setUsuario_tipo_id_INT($val)
        {
            $this->usuario_tipo_id_INT = $val;
        }

        function setStatus_BOOLEAN($val)
        {
            $this->status_BOOLEAN = $val;
        }

        function setPagina_inicial($val)
        {
            $this->pagina_inicial = $val;
        }

        function setExcluido_BOOLEAN($val)
        {
            $this->excluido_BOOLEAN = $val;
        }

        function setExcluido_DATETIME($val)
        {
            $this->excluido_DATETIME = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT * , UNIX_TIMESTAMP(excluido_DATETIME) AS excluido_DATETIME_UNIX FROM usuario WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->nome = $row->nome;

            $this->email = $row->email;

            $this->senha = $row->senha;

            $this->usuario_tipo_id_INT = $row->usuario_tipo_id_INT;
            if ($this->usuario_tipo_id_INT)
            {
                $this->objUsuario_tipo->select($this->usuario_tipo_id_INT);
            }

            $this->status_BOOLEAN = $row->status_BOOLEAN;

            $this->pagina_inicial = $row->pagina_inicial;

            $this->excluido_BOOLEAN = $row->excluido_BOOLEAN;

            $this->excluido_DATETIME = $row->excluido_DATETIME;
            $this->excluido_DATETIME_UNIX = $row->excluido_DATETIME_UNIX;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "UPDATE usuario SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento
            $this->excluido_BOOLEAN = "0";

            $sql = "INSERT INTO usuario ( nome,email,senha,usuario_tipo_id_INT,status_BOOLEAN,pagina_inicial,excluido_BOOLEAN,excluido_DATETIME ) VALUES ( '$this->nome','$this->email','$this->senha',$this->usuario_tipo_id_INT,$this->status_BOOLEAN,'$this->pagina_inicial',$this->excluido_BOOLEAN,$this->excluido_DATETIME )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome()
        {
            return "nome";
        }

        public function nomeCampoEmail()
        {
            return "email";
        }

        public function nomeCampoSenha()
        {
            return "senha";
        }

        public function nomeCampoUsuario_tipo_id_INT()
        {
            return "usuario_tipo_id_INT";
        }

        public function nomeCampoStatus_BOOLEAN()
        {
            return "status_BOOLEAN";
        }

        public function nomeCampoPagina_inicial()
        {
            return "pagina_inicial";
        }

        public function nomeCampoExcluido_BOOLEAN()
        {
            return "excluido_BOOLEAN";
        }

        public function nomeCampoExcluido_DATETIME()
        {
            return "excluido_DATETIME";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome($objArguments)
        {
            $objArguments->nome = "nome";
            $objArguments->id = "nome";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoEmail($objArguments)
        {
            $objArguments->nome = "email";
            $objArguments->id = "email";

            return $this->campoEmail($objArguments);
        }

        public function imprimirCampoSenha($objArguments)
        {
            $objArguments->nome = "senha";
            $objArguments->id = "senha";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoUsuario_tipo_id_INT($objArguments)
        {
            $objArguments->nome = "usuario_tipo_id_INT";
            $objArguments->id = "usuario_tipo_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoStatus_BOOLEAN($objArguments)
        {
            $objArguments->nome = "status_BOOLEAN";
            $objArguments->id = "status_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoPagina_inicial($objArguments)
        {
            $objArguments->nome = "pagina_inicial";
            $objArguments->id = "pagina_inicial";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoExcluido_BOOLEAN($objArguments)
        {
            $objArguments->nome = "excluido_BOOLEAN";
            $objArguments->id = "excluido_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoExcluido_DATETIME($objArguments)
        {
            $objArguments->nome = "excluido_DATETIME";
            $objArguments->id = "excluido_DATETIME";

            return $this->campoDataTime($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->usuario_tipo_id_INT == "")
            {
                $this->usuario_tipo_id_INT = "null";
            }

            if ($this->status_BOOLEAN == "")
            {
                $this->status_BOOLEAN = "null";
            }

            if ($this->excluido_BOOLEAN == "")
            {
                $this->excluido_BOOLEAN = "null";
            }

            $this->excluido_DATETIME = $this->formatarDataTimeParaComandoSQL($this->excluido_DATETIME);
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBIÇÃO
        //****************************************************************************

        public function formatarParaExibicao()
        {
            $this->excluido_DATETIME = $this->formatarDataTimeParaExibicao($this->excluido_DATETIME);
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["nome"] = $this->nome;
            $_SESSION["email"] = $this->email;
            $_SESSION["senha"] = $this->senha;
            $_SESSION["usuario_tipo_id_INT"] = $this->usuario_tipo_id_INT;
            $_SESSION["status_BOOLEAN"] = $this->status_BOOLEAN;
            $_SESSION["pagina_inicial"] = $this->pagina_inicial;
            $_SESSION["excluido_BOOLEAN"] = $this->excluido_BOOLEAN;
            $_SESSION["excluido_DATETIME"] = $this->excluido_DATETIME;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["nome"]);
            unset($_SESSION["email"]);
            unset($_SESSION["senha"]);
            unset($_SESSION["usuario_tipo_id_INT"]);
            unset($_SESSION["status_BOOLEAN"]);
            unset($_SESSION["pagina_inicial"]);
            unset($_SESSION["excluido_BOOLEAN"]);
            unset($_SESSION["excluido_DATETIME"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]);
            $this->email = $this->formatarDados($_SESSION["email{$numReg}"]);
            $this->senha = $this->formatarDados($_SESSION["senha{$numReg}"]);
            $this->usuario_tipo_id_INT = $this->formatarDados($_SESSION["usuario_tipo_id_INT{$numReg}"]);
            $this->status_BOOLEAN = $this->formatarDados($_SESSION["status_BOOLEAN{$numReg}"]);
            $this->pagina_inicial = $this->formatarDados($_SESSION["pagina_inicial{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_SESSION["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_SESSION["excluido_DATETIME{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->nome = $this->formatarDados($_POST["nome{$numReg}"]);
            $this->email = $this->formatarDados($_POST["email{$numReg}"]);
            $this->senha = $this->formatarDados($_POST["senha{$numReg}"]);
            $this->usuario_tipo_id_INT = $this->formatarDados($_POST["usuario_tipo_id_INT{$numReg}"]);
            $this->status_BOOLEAN = $this->formatarDados($_POST["status_BOOLEAN{$numReg}"]);
            $this->pagina_inicial = $this->formatarDados($_POST["pagina_inicial{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_POST["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_POST["excluido_DATETIME{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->nome = $this->formatarDados($_GET["nome{$numReg}"]);
            $this->email = $this->formatarDados($_GET["email{$numReg}"]);
            $this->senha = $this->formatarDados($_GET["senha{$numReg}"]);
            $this->usuario_tipo_id_INT = $this->formatarDados($_GET["usuario_tipo_id_INT{$numReg}"]);
            $this->status_BOOLEAN = $this->formatarDados($_GET["status_BOOLEAN{$numReg}"]);
            $this->pagina_inicial = $this->formatarDados($_GET["pagina_inicial{$numReg}"]);
            $this->excluido_BOOLEAN = $this->formatarDados($_GET["excluido_BOOLEAN{$numReg}"]);
            $this->excluido_DATETIME = $this->formatarDados($_GET["excluido_DATETIME{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["nome{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "nome = '$this->nome', ";
            }

            if (isset($tipo["email{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "email = '$this->email', ";
            }

            if (isset($tipo["senha{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "senha = '$this->senha', ";
            }

            if (isset($tipo["usuario_tipo_id_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "usuario_tipo_id_INT = $this->usuario_tipo_id_INT, ";
            }

            if (isset($tipo["status_BOOLEAN{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "status_BOOLEAN = $this->status_BOOLEAN, ";
            }

            if (isset($tipo["pagina_inicial{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "pagina_inicial = '$this->pagina_inicial', ";
            }

            if (isset($tipo["excluido_BOOLEAN{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_BOOLEAN = $this->excluido_BOOLEAN, ";
            }

            if (isset($tipo["excluido_DATETIME{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "excluido_DATETIME = $this->excluido_DATETIME, ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE usuario SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>

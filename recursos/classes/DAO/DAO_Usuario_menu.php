<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Usuario_menu
    * DATA DE GERA��O: 21.10.2010
    * ARQUIVO:         DAO_Usuario_menu.php
    * TABELA MYSQL:    usuario_menu
    * BANCO DE DADOS:  DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Usuario_menu extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $usuario_id_INT;
        public $objUsuario;
        public $area_menu;

        public $nomeEntidade;

        public $label_id;
        public $label_usuario_id_INT;
        public $label_area_menu;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "�rea do menu do usu�rio";
            $this->nomeTabela = "usuario_menu";
            $this->campoId = "id";
            $this->campoLabel = "area_menu";

            $this->objUsuario = new EXTDAO_Usuario();
        }

        public function valorCampoLabel()
        {
            return $this->getArea_menu();
        }

        public function getComboBoxAllUsuario($objArgumentos)
        {
            $objArgumentos->nome = "usuario_id_INT";
            $objArgumentos->id = "usuario_id_INT";
            $objArgumentos->valueReplaceId = false;

            return $this->objUsuario->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A �rea do menu do usu�rio foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A �rea do menu do usu�rio foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A �rea do menu do usu�rio foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A �rea do menu do usu�rio foi exclu�da com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_usuario_menu", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_usuario_menu", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getUsuario_id_INT()
        {
            return $this->usuario_id_INT;
        }

        public function getArea_menu()
        {
            return $this->area_menu;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************
        function setId($val)
        {
            $this->id = $val;
        }

        function setUsuario_id_INT($val)
        {
            $this->usuario_id_INT = $val;
        }

        function setArea_menu($val)
        {
            $this->area_menu = $val;
        }


        // **********************
        // SELECT
        // **********************
        function select($id)
        {
            $sql = "SELECT *  FROM usuario_menu WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->usuario_id_INT = $row->usuario_id_INT;
            if ($this->usuario_id_INT)
            {
                $this->objUsuario->select($this->usuario_id_INT);
            }

            $this->area_menu = $row->area_menu;
        }


        // **********************
        // DELETE
        // **********************
        public function delete($id)
        {
            $sql = "DELETE FROM usuario_menu WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************
        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO usuario_menu ( usuario_id_INT,area_menu ) VALUES ( $this->usuario_id_INT,'$this->area_menu' )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************
        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoUsuario_id_INT()
        {
            return "usuario_id_INT";
        }

        public function nomeCampoArea_menu()
        {
            return "area_menu";
        }


        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************
        public function imprimirCampoUsuario_id_INT($objArguments)
        {
            $objArguments->nome = "usuario_id_INT";
            $objArguments->id = "usuario_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoArea_menu($objArguments)
        {
            $objArguments->nome = "area_menu";
            $objArguments->id = "area_menu";

            return $this->campoTexto($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************
        public function formatarParaSQL()
        {
            if ($this->usuario_id_INT == "")
            {
                $this->usuario_id_INT = "null";
            }
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************
        public function formatarParaExibicao()
        {
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************
        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["usuario_id_INT"] = $this->usuario_id_INT;
            $_SESSION["area_menu"] = $this->area_menu;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************
        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["usuario_id_INT"]);
            unset($_SESSION["area_menu"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************
        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_SESSION["usuario_id_INT{$numReg}"]);
            $this->area_menu = $this->formatarDados($_SESSION["area_menu{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************
        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_POST["usuario_id_INT{$numReg}"]);
            $this->area_menu = $this->formatarDados($_POST["area_menu{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************
        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->usuario_id_INT = $this->formatarDados($_GET["usuario_id_INT{$numReg}"]);
            $this->area_menu = $this->formatarDados($_GET["area_menu{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["usuario_id_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "usuario_id_INT = $this->usuario_id_INT, ";
            }

            if (isset($tipo["area_menu{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "area_menu = '$this->area_menu', ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE usuario_menu SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>
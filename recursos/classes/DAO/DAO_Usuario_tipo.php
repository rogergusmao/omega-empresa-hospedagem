<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Usuario_tipo
    * DATA DE GERA��O: 04.12.2010
    * ARQUIVO:         DAO_Usuario_tipo.php
    * TABELA MYSQL:    usuario_tipo
    * BANCO DE DADOS:  DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Usuario_tipo extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $nome;
        public $nome_visivel;
        public $status_BOOLEAN;
        public $pagina_inicial;

        public $nomeEntidade;

        public $label_id;
        public $label_nome;
        public $label_nome_visivel;
        public $label_status_BOOLEAN;
        public $label_pagina_inicial;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "classe de usu�rio";
            $this->nomeTabela = "usuario_tipo";
            $this->campoId = "id";
            $this->campoLabel = "nome";
        }

        public function valorCampoLabel()
        {
            return $this->getNome();
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A classe de usu�rio foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("A classe de usu�rio foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->selectUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A classe de usu�rio foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A classe de usu�rio foi exclu�da com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_usuario_tipo", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_usuario_tipo", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        // **********************
        // M�TODOS GETTER's
        // **********************
        public function getId()
        {
            return $this->id;
        }

        public function getNome()
        {
            return $this->nome;
        }

        public function getNome_visivel()
        {
            return $this->nome_visivel;
        }

        public function getStatus_BOOLEAN()
        {
            return $this->status_BOOLEAN;
        }

        public function getPagina_inicial()
        {
            return $this->pagina_inicial;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setNome($val)
        {
            $this->nome = $val;
        }

        function setNome_visivel($val)
        {
            $this->nome_visivel = $val;
        }

        function setStatus_BOOLEAN($val)
        {
            $this->status_BOOLEAN = $val;
        }

        function setPagina_inicial($val)
        {
            $this->pagina_inicial = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT *  FROM usuario_tipo WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = $this->database->fetchObject($result);

            $this->id = $row->id;

            $this->nome = $row->nome;

            $this->nome_visivel = $row->nome_visivel;

            $this->status_BOOLEAN = $row->status_BOOLEAN;

            $this->pagina_inicial = $row->pagina_inicial;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "DELETE FROM usuario_tipo WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO usuario_tipo ( nome,nome_visivel,status_BOOLEAN,pagina_inicial ) VALUES ( '$this->nome','$this->nome_visivel',$this->status_BOOLEAN,'$this->pagina_inicial' )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoNome()
        {
            return "nome";
        }

        public function nomeCampoNome_visivel()
        {
            return "nome_visivel";
        }

        public function nomeCampoStatus_BOOLEAN()
        {
            return "status_BOOLEAN";
        }

        public function nomeCampoPagina_inicial()
        {
            return "pagina_inicial";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoNome($objArguments)
        {
            $objArguments->nome = "nome";
            $objArguments->id = "nome";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoNome_visivel($objArguments)
        {
            $objArguments->nome = "nome_visivel";
            $objArguments->id = "nome_visivel";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoStatus_BOOLEAN($objArguments)
        {
            $objArguments->nome = "status_BOOLEAN";
            $objArguments->id = "status_BOOLEAN";

            return $this->campoBoolean($objArguments);
        }

        public function imprimirCampoPagina_inicial($objArguments)
        {
            $objArguments->nome = "pagina_inicial";
            $objArguments->id = "pagina_inicial";

            return $this->campoTexto($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->status_BOOLEAN == "")
            {
                $this->status_BOOLEAN = "null";
            }
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["nome"] = $this->nome;
            $_SESSION["nome_visivel"] = $this->nome_visivel;
            $_SESSION["status_BOOLEAN"] = $this->status_BOOLEAN;
            $_SESSION["pagina_inicial"] = $this->pagina_inicial;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["nome"]);
            unset($_SESSION["nome_visivel"]);
            unset($_SESSION["status_BOOLEAN"]);
            unset($_SESSION["pagina_inicial"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->nome = $this->formatarDados($_SESSION["nome{$numReg}"]);
            $this->nome_visivel = $this->formatarDados($_SESSION["nome_visivel{$numReg}"]);
            $this->status_BOOLEAN = $this->formatarDados($_SESSION["status_BOOLEAN{$numReg}"]);
            $this->pagina_inicial = $this->formatarDados($_SESSION["pagina_inicial{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->nome = $this->formatarDados($_POST["nome{$numReg}"]);
            $this->nome_visivel = $this->formatarDados($_POST["nome_visivel{$numReg}"]);
            $this->status_BOOLEAN = $this->formatarDados($_POST["status_BOOLEAN{$numReg}"]);
            $this->pagina_inicial = $this->formatarDados($_POST["pagina_inicial{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->nome = $this->formatarDados($_GET["nome{$numReg}"]);
            $this->nome_visivel = $this->formatarDados($_GET["nome_visivel{$numReg}"]);
            $this->status_BOOLEAN = $this->formatarDados($_GET["status_BOOLEAN{$numReg}"]);
            $this->pagina_inicial = $this->formatarDados($_GET["pagina_inicial{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["nome{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "nome = '$this->nome', ";
            }

            if (isset($tipo["nome_visivel{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "nome_visivel = '$this->nome_visivel', ";
            }

            if (isset($tipo["status_BOOLEAN{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "status_BOOLEAN = $this->status_BOOLEAN, ";
            }

            if (isset($tipo["pagina_inicial{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "pagina_inicial = '$this->pagina_inicial', ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE usuario_tipo SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }

    } // classe: fim

?>
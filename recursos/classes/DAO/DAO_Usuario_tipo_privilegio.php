<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  DAO_Usuario_tipo_privilegio
    * DATA DE GERA��O: 13.07.2010
    * ARQUIVO:         DAO_Usuario_tipo_privilegio.php
    * TABELA MYSQL:    usuario_tipo_privilegio
    * BANCO DE DADOS:  DEP_pesquisas
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class DAO_Usuario_tipo_privilegio extends Generic_DAO
    {

        // *************************
        // DECLARA��O DE ATRIBUTOS
        // *************************

        public $id;
        public $usuario_tipo_id_INT;
        public $objUsuario_tipo;
        public $identificador_funcionalidade;

        public $nomeEntidade;

        public $label_id;
        public $label_usuario_tipo_id_INT;
        public $label_identificador_funcionalidade;

        // **********************
        // M�TODO CONSTRUTOR
        // **********************

        public function __construct($niveisRaiz = 2)
        {
            parent::__construct($niveisRaiz);

            $this->nomeEntidade = "privil�gio do tipo de usu�rio";
            $this->nomeTabela = "usuario_tipo_privilegio";
            $this->campoId = "id";
            $this->campoLabel = "id";

            $this->objUsuario_tipo = new EXTDAO_Usuario_tipo();
        }

        public function valorCampoLabel()
        {
            return $this->getId();
        }

        public function getComboBoxAllUsuario_tipo($objArgumentos)
        {
            $objArgumentos->nome = "usuario_tipo_id_INT";
            $objArgumentos->id = "usuario_tipo_id_INT";
            $objArgumentos->valueReplaceId = false;

            return $this->objUsuario_tipo->getComboBox($objArgumentos);
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O privil�gio do tipo de usu�rio foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->getIdDoUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionAddAjax()
        {
            $mensagemSucesso = I18N::getExpression("O privil�gio do tipo de usu�rio foi cadastrado com sucesso.");
            $numeroRegistros = Helper::POST("numero_registros_ajax");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                $this->formatarParaSQL();

                $this->insert();
                $this->getIdDoUltimoRegistroInserido();
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O privil�gio do tipo de usu�rio foi modificado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);
                $this->select($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("O privil�gio do tipo de usu�rio foi exclu�do com sucesso.");

            $urlSuccess = Helper::getUrlAction("list_usuario_tipo_privilegio", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_usuario_tipo_privilegio", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $this->delete($registroRemover);

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }




        // **********************
        // M�TODOS GETTER's
        // **********************

        public function getId()
        {
            return $this->id;
        }

        public function getUsuario_tipo_id_INT()
        {
            return $this->usuario_tipo_id_INT;
        }

        public function getIdentificador_funcionalidade()
        {
            return $this->identificador_funcionalidade;
        }

        // **********************
        // M�TODOS SETTER's
        // **********************

        function setId($val)
        {
            $this->id = $val;
        }

        function setUsuario_tipo_id_INT($val)
        {
            $this->usuario_tipo_id_INT = $val;
        }

        function setIdentificador_funcionalidade($val)
        {
            $this->identificador_funcionalidade = $val;
        }


        // **********************
        // SELECT
        // **********************

        function select($id)
        {
            $sql = "SELECT *  FROM usuario_tipo_privilegio WHERE id = $id;";
            $this->database->query($sql);
            $result = $this->database->result;
            $row = mysqli_fetch_object($result);

            $this->id = $row->id;

            $this->usuario_tipo_id_INT = $row->usuario_tipo_id_INT;
            if ($this->usuario_tipo_id_INT)
            {
                $this->objUsuario_tipo->select($this->usuario_tipo_id_INT);
            }

            $this->identificador_funcionalidade = $row->identificador_funcionalidade;
        }


        // **********************
        // DELETE
        // **********************

        public function delete($id)
        {
            $sql = "DELETE FROM usuario_tipo_privilegio WHERE id = $id;";
            $this->database->query($sql);
        }

        // **********************
        // INSERT
        // **********************

        public function insert()
        {
            $this->id = ""; //limpar chave com autoincremento

            $sql = "INSERT INTO usuario_tipo_privilegio ( usuario_tipo_id_INT,identificador_funcionalidade ) VALUES ( $this->usuario_tipo_id_INT,'$this->identificador_funcionalidade' )";
            $this->database->query($sql);
        }


        //*************************************************
        //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
        //*************************************************

        public function nomeCampoId()
        {
            return "id";
        }

        public function nomeCampoUsuario_tipo_id_INT()
        {
            return "usuario_tipo_id_INT";
        }

        public function nomeCampoIdentificador_funcionalidade()
        {
            return "identificador_funcionalidade";
        }




        //************************************************************************
        //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
        //************************************************************************

        public function imprimirCampoUsuario_tipo_id_INT($objArguments)
        {
            $objArguments->nome = "usuario_tipo_id_INT";
            $objArguments->id = "usuario_tipo_id_INT";

            return $this->campoInteiro($objArguments);
        }

        public function imprimirCampoIdentificador_funcionalidade($objArguments)
        {
            $objArguments->nome = "identificador_funcionalidade";
            $objArguments->id = "identificador_funcionalidade";

            return $this->campoTexto($objArguments);
        }




        //**********************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
        //**********************************************************************************

        public function formatarParaSQL()
        {
            if ($this->usuario_tipo_id_INT == "")
            {
                $this->usuario_tipo_id_INT = "null";
            }
        }


        //****************************************************************************
        //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
        //****************************************************************************

        public function formatarParaExibicao()
        {
        }


        // ****************************
        // CRIAR VARIAVEIS DE SESSAO
        // ****************************

        public function createSession()
        {
            $_SESSION["id"] = $this->id;
            $_SESSION["usuario_tipo_id_INT"] = $this->usuario_tipo_id_INT;
            $_SESSION["identificador_funcionalidade"] = $this->identificador_funcionalidade;
        }

        // ***************************
        // LIMPAR SESSAO
        // ***************************

        public function limparSession()
        {
            unset($_SESSION["id"]);
            unset($_SESSION["usuario_tipo_id_INT"]);
            unset($_SESSION["identificador_funcionalidade"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL SESSION
        // ****************************

        public function setBySession($numReg)
        {
            $this->id = $this->formatarDados($_SESSION["id{$numReg}"]);
            $this->usuario_tipo_id_INT = $this->formatarDados($_SESSION["usuario_tipo_id_INT{$numReg}"]);
            $this->identificador_funcionalidade = $this->formatarDados($_SESSION["identificador_funcionalidade{$numReg}"]);
        }


        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL POST
        // ****************************

        public function setByPost($numReg)
        {
            $this->id = $this->formatarDados($_POST["id{$numReg}"]);
            $this->usuario_tipo_id_INT = $this->formatarDados($_POST["usuario_tipo_id_INT{$numReg}"]);
            $this->identificador_funcionalidade = $this->formatarDados($_POST["identificador_funcionalidade{$numReg}"]);
        }

        // ****************************
        // SETAR CAMPOS POR SUPERGLOBAL GET
        // ****************************

        public function setByGet($numReg)
        {
            $this->id = $this->formatarDados($_GET["id{$numReg}"]);
            $this->usuario_tipo_id_INT = $this->formatarDados($_GET["usuario_tipo_id_INT{$numReg}"]);
            $this->identificador_funcionalidade = $this->formatarDados($_GET["identificador_funcionalidade{$numReg}"]);
        }

        // **********************
        // UPDATE
        // **********************

        public function update($id, $tipo = "vazio", $numReg = 1)
        {
            if (isset($tipo["usuario_tipo_id_INT{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "usuario_tipo_id_INT = $this->usuario_tipo_id_INT, ";
            }

            if (isset($tipo["identificador_funcionalidade{$numReg}"]) || $tipo == "vazio")
            {
                $upd .= "identificador_funcionalidade = '$this->identificador_funcionalidade', ";
            }

            $upd = substr($upd, 0, -2);

            $sql = " UPDATE usuario_tipo_privilegio SET $upd WHERE id = $id ";

            $result = $this->database->query($sql);
        }


        //*******************************************************************
        //METODO QUE RETORNA ARRAY DE TODOS OS ID's EXISTENTES NA TABELA
        //*******************************************************************

        public function getTodosIDs()
        {
            $sql = "SELECT id FROM usuario_tipo_privilegio";

            $result = $this->database->query($sql);

            $numeroRegistros = mysqli_num_rows($result);

            for ($i = 0; $i < $numeroRegistros; $i++)
            {
                $retorno[ $i ] = Database::mysqli_result($result, $i, 0);
            }

            return $retorno;
        }



        //************************************************************
        //METODO QUE RETORNA ID DO ULTIMO REGISTRO CRIADO NA TABELA
        //************************************************************

        function getIdDoUltimoRegistroInserido()
        {
            $sql = "SELECT MAX(id) FROM usuario_tipo_privilegio";

            $result = $this->database->query($sql);

            if ($this->database->rows() > 0)
            {
                $retorno = Database::mysqli_result($result, 0, 0);
            }

            else
            {
                print "Nenhum registro foi inserido anteriormente.";
                $retorno = "null";
            }

            return $retorno;
        }



        // ***************************************************************************************************
        // M�TODO  - SELECT APROXIMADO - RETORNA LINHAS QUE ATENDEM A CONDI��O
        // ***************************************************************************************************

        function selectLike($field, $val)
        {
            $field = $this->formatarDados($field);
            $val = $this->formatarDados($val);

            $sql = "SELECT * FROM usuario_tipo_privilegio WHERE $field LIKE '%$val%'";
            $this->database->query($sql);

            return $this->database->fetchObject();
        }


        // ***************************************************************************************************
        // METODO QUE ATUALIZA UM CAMPO
        // ***************************************************************************************************

        function updateCampo($id, $campo, $valor)
        {
            if (substr_count($campo, "_FLOAT") > 0 || substr_count($campo, "_INT") > 0 || substr_count($campo, "_BOOLEAN") > 0)
            {
                $aspas = "";
            }
            else
            {
                $aspas = "'";
            }

            $sql = "UPDATE usuario_tipo_privilegio SET $campo = $aspas$valor$aspas WHERE id = $id";

            $this->database->query($sql);
        }

    } // classe: fim

?>
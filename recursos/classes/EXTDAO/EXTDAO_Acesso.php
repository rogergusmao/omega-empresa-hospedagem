<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Acesso
    * NOME DA CLASSE DAO: DAO_Acesso
    * DATA DE GERAÇÃO:    08.02.2010
    * ARQUIVO:            EXTDAO_Acesso.php
    * TABELA MYSQL:       acesso
    * BANCO DE DADOS:     dep_pesquisas
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Acesso extends DAO_Acesso
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Acesso";
        }

        public function acoesPrimeiroLoginDoDia()
        {
            //remove todos os arquivos temporarios antigos
            Helper::removerArquivosTemporarios();
        }

        public function acoesTodosOsLogins()
        {
            return true;
        }

        public function verificarPrimeiroLoginDoDia()
        {
            $dataInicioDia = date("'Y-m-d 00:00:01'");

            $this->database->query("SELECT COUNT(id) FROM acesso WHERE data_login_DATETIME >= {$dataInicioDia} ");

            if ($this->database->getPrimeiraTuplaDoResultSet(0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function gravarLogin($idUsuario)
        {
            $primeiroLogin = $this->verificarPrimeiroLoginDoDia();

            $this->setUsuario_id_INT($idUsuario);
            $this->setData_login_DATETIME(date("Y-m-d H:i:s"));

            $this->formatarParaSQL();
            $this->insert();

            $this->acoesTodosOsLogins();

            if ($primeiroLogin)
            {
                $this->acoesPrimeiroLoginDoDia();
            }

            return true;
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_usuario_id_INT = I18N::getExpression("Usuário");
            $this->label_data_login_DATETIME = I18N::getExpression("Data do Login");
            $this->label_data_logout_DATETIME = I18N::getExpression("Data do Logout");
        }

        public static function factory()
        {
            return new EXTDAO_Acesso();
        }

    }


<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Assinatura
    * NOME DA CLASSE DAO: DAO_Assinatura
    * DATA DE GERAÇÃO:    31.10.2013
    * ARQUIVO:            EXTDAO_Assinatura.php
    * TABELA MYSQL:       assinatura
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Assinatura extends DAO_Assinatura
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Assinatura";
        }

        public function __actionRemove()
        {
            $mensagemSucesso = I18N::getExpression("A assinatura foi excluída com sucesso.");
            $urlSuccess = Helper::getUrlAction("list_assinatura", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("list_assinatura", Helper::GET("id"));

            $registroRemover = Helper::GET("id");
            $idHospedagem = EXTDAO_Assinatura::getHospedagemDaAssinatura($registroRemover);

            if (strlen($idHospedagem))
            {
                $this->getFkObjHospedagem()->delete($idHospedagem);
            }

            $this->delete("$registroRemover");

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso", $registroRemover);
        }

        public static function disponibilizarAssinatura($idAssinatura)
        {
            $objAssinatura = new EXTDAO_Assinatura();
            $objAssinatura->select($idAssinatura);

            $objHospedagem = $objAssinatura->getFkObjHospedagem();

            $objSistema = new EXTDAO_Sistema();
            $objSistema->select($objAssinatura->getSistema_id_INT());
            $pathRaiz = $objSistema->getPath_raiz_assinatura();
            $objHospedagem->setDominio(null);
            $objHospedagem->setDominio_webservice(null);

            switch ($objSistema->getSistema_tipo_id_INT())
            {
                case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS:

                    $pathHospedagem = Helper::formatarPath($objHospedagem->getPath());
                    $corporacao = $objAssinatura->getNome_site();
                    $pathDatabaseConfig = $pathHospedagem . "recursos/php/database_config_$corporacao.php";

                    if (file_exists($pathDatabaseConfig))
                    {
                        unlink($pathDatabaseConfig);
                    }

                    break;

                case EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:

                    $pathHospedagemAntigo = Helper::formataPathDiretorio($objHospedagem->getPath());
                    $pathHospedagemNovo = Helper::formataPathDiretorio($pathRaiz, $objHospedagem->getId());

                    if (!Helper::renomearDiretorio($pathHospedagemAntigo, $pathHospedagemNovo))
                    {
                        throw new Exception(I18N::getExpression("Ocorreu um erro durante a execução, tente novamente mais tarde. Mas já estamos analisando o problema, desculpe o transtorno."));
                    }

                    $objHospedagem->setPath($pathHospedagemNovo);
                    break;
            }

            $objHospedagem->formatarParaSQL();
            $objHospedagem->update($objHospedagem->getId());
            $objAssinatura->setId_corporacao_INT(null);
            $objAssinatura->setSicob_cliente_assinatura_INT(null);
            $objAssinatura->setEstado_assinatura_id_INT(EXTDAO_Estado_assinatura::DISPONIVEL);
            $objAssinatura->setNome_site(null);
            $objAssinatura->formatarParaSQL();
            $objAssinatura->update($objAssinatura->getId());

            return true;
        }

        public static function deletarAssinatura($idAssinatura)
        {
            $objAssinatura = new EXTDAO_Assinatura();
            $msg = $objAssinatura->select($idAssinatura);
            if ($msg != null)
            {
                if ($msg->erro())
                {
                    return $msg;
                }
                else
                {
                    if ($msg->resultadoVazio())
                    {
                        return new Mensagem(
                            PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                            "A assinatura $idAssinatura já não existia ");
                    }
                }
            }

            $objHospedagem = new EXTDAO_Hospedagem();
            $msg = $objHospedagem->select($objAssinatura->getHospedagem_id_INT());
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $objSistema = new EXTDAO_Sistema();
            $msg = $objSistema->select($objAssinatura->getSistema_id_INT());
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            switch ($objSistema->getSistema_tipo_id_INT())
            {
                case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS:
                    //NAO DELETA O DIRETORIO
                    break;
                case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                case EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                    //DELETA O DIRETORIO
                    $pathHospedagemAntigo = Helper::formataPathDiretorio($objHospedagem->getPath());

                    if (strlen($pathHospedagemAntigo))
                    {
                        Helper::deletaDiretorio($pathHospedagemAntigo);
                    }
                    break;
            }

            $idsHospedagensDb = EXTDAO_Hospedagem_db::getIdsHospedagemDb($objHospedagem->getId());
            if (count($idsHospedagensDb))
            {
                for ($i = 0; $i < count($idsHospedagensDb); $i++)
                {
                    $idHospedagemDb = $idsHospedagensDb[ $i ];
                    $objHospedagemDb = new EXTDAO_Hospedagem_db();
                    $objHospedagemDb->select($idHospedagemDb);
                    $database = $objHospedagemDb->getObjDatabase();

                    switch ($objSistema->getSistema_tipo_id_INT())
                    {
                        case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS:
                            //NAO DELETA O DIRETORIO
                            break;
                        case EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                            //TODO
                            //criar rotina para remover a corporacao da base compartilhada
                            break;
                        case EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA:
                            //DELETA O DIRETORIO
                            //TODO o delete do banco realmente deve ocorrer, mas por uma rotina, e nao por acao do usuario
                            //TODO lembre de criar um novo estado para hospedagem, para marcar para a rotina saber que tem que
                            //TODO ser deletado ou removido o banco
                            //$database->deletaBancoDeDados($objHospedagemDb->getNome_db());
                            break;
                    }
//
                    $msg = $objHospedagemDb->delete($objHospedagemDb->getId());
                    if ($msg != null && $msg->erro())
                    {
                        return $msg;
                    }
                }
            }

            $msg = $objHospedagem->delete($objHospedagem->getId());
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $msg = $objAssinatura->delete($idAssinatura);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            return new Mensagem (PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                 I18N::getExpression("A assinatura foi excluída com sucesso."));
        }

        public static function getHospedagemDaAssinatura($idAssinatura, Database $db = null)
        {
            $q = "SELECT hospedagem_id_INT "
                . " FROM assinatura "
                . " WHERE id = $idAssinatura "
                . " AND excluido_DATETIME IS NULL";

            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getAssinaturaDaHospedagem($idHospedagem, $db = null)
        {
            $q = "SELECT id "
                . " FROM assinatura "
                . " WHERE hospedagem_id_INT = $idHospedagem "
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }

            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getCorporacaoDaHospedagem($idHospedagem, $db = null)
        {
            $q = "SELECT nome_site "
                . " FROM assinatura "
                . " WHERE hospedagem_id_INT = $idHospedagem "
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }

            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getIdENomeCorporacaoDaHospedagem($idHospedagem, $db = null)
        {
            $q = "SELECT id_corporacao_INT, nome_site "
                . " FROM assinatura "
                . " WHERE hospedagem_id_INT = $idHospedagem "
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }

            $db->query($q);

            return array(
                "id" => $db->getPrimeiraTuplaDoResultSet(0),
                "corporacao" => $db->getPrimeiraTuplaDoResultSet(1));
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_cliente_assinatura_cobranca_INT = I18N::getExpression("Chave Para Cliente_assinatura da Base de Cobrança");
            $this->label_hospedagem_id_INT = I18N::getExpression("Hospedagem");
            $this->label_sistema_id_INT = I18N::getExpression("Sistema");
            $this->label_excluido_BOOLEAN = I18N::getExpression("Foi Excluído?");
            $this->label_excluido_DATETIME = I18N::getExpression("Data de Exclusão");
            $this->label_nome_site = I18N::getExpression("Grupo");
            $this->label_estado_assinatura_id_INT = I18N::getExpression("Estado");
        }

        public static function factory()
        {
            return new EXTDAO_Assinatura();
        }

        public static function getDominioDaAssinatura($idAssinatura)
        {
            $obj = new EXTDAO_Assinatura();
            $msg = $obj->select($idAssinatura);
            if ($msg != null && ($msg->erro() || $msg->resultadoVazio()))
            {
                return $msg;
            }
            else
            {
                return new Mensagem_token (null, null, $obj->getFkObjHospedagem()->getDominio());
            }
        }

        public static function getConfiguracoesDeBancoDaAssinatura(
            $idAssinatura, Database $db = null)
        {
            $objRetorno = new stdClass();
            if ($db == null)
            {
                $db = new Database();
            }

            $objHospedagemDb = new EXTDAO_Hospedagem_db($db);

            $db->query("SELECT hd.id 
                    FROM assinatura a 
                        JOIN hospedagem h 
                          ON a.hospedagem_id_INT = h.id
                        JOIN hospedagem_db hd
                          ON hd.hospedagem_id_INT = h.id
                    WHERE a.id={$idAssinatura}
                      AND hd.tipo_hospedagem_db_id_INT= " . EXTDAO_Tipo_hospedagem_db::PRINCIPAL . "
                      AND hd.excluido_DATETIME IS NULL
                      AND a.excluido_DATETIME IS NULL
                      AND h.excluido_DATETIME IS NULL
                    LIMIT 0, 1");

            if ($db->rows() > 0)
            {
                $objHospedagemDb->select($db->getPrimeiraTuplaDoResultSet(0));

                $objRetorno->host = $objHospedagemDb->getHost_db();
                $objRetorno->porta = $objHospedagemDb->getPorta_db_INT();
                $objRetorno->usuario = $objHospedagemDb->getUsuario_db();
                $objRetorno->senha = $objHospedagemDb->getSenha_db();
                $objRetorno->database = $objHospedagemDb->getNome_db();

                return $objRetorno;
            }
            else
            {
                return null;
            }
        }

        public static function getConfiguracoesDeTodosOsBancosDaAssinatura(
            $idAssinatura,
            Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $db->query("SELECT 
                hd.id, 
                hd.tipo_hospedagem_db_id_INT tipo, 
                hd.host_db host,
                hd.porta_db_INT porta,
                hd.usuario_db usuario,
                hd.senha_db senha,
                hd.nome_db  DBName
                    FROM assinatura a 
                        JOIN hospedagem h 
                          ON a.hospedagem_id_INT = h.id
                        JOIN hospedagem_db hd
                          ON hd.hospedagem_id_INT = h.id
                    WHERE a.id={$idAssinatura}
                      AND hd.excluido_DATETIME IS NULL
                      AND a.excluido_DATETIME IS NULL
                      AND h.excluido_DATETIME IS NULL");

            if ($db->rows() > 0)
            {
                $tipoPorConfiguracoes = Helper::getResultSetToMatriz($db->result, 1, 0, 'tipo');

                return $tipoPorConfiguracoes;
            }
            else
            {
                return null;
            }
        }

        public static function getDominioDoWebServiceDaAssinatura($idAssinatura, $db = null)
        {
            try
            {
                if ($db == null)
                {
                    $db = new Database();
                }
                $db = new Database();
                $msg = $db->queryMensagem(
                    "SELECT h.dominio_webservice 
                            FROM assinatura a join hospedagem h on a.hospedagem_id_INT = h.id"
                            . " WHERE a.id= $idAssinatura");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                if ($db->rows() == 0)
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                        "Assinatura não encontrada.");
                }

                return new Mensagem_token(null, null, $db->getPrimeiraTuplaDoResultSet(0));
            }
            catch (Exception $exc)
            {
                return new Mensagem(null, null, $exc);
            }
        }

        public static function getDominioDoWebServiceDoSincronizadorWebAssinatura($idAssinatura, $db = null)
        {
            try
            {
                if ($db == null)
                {
                    $db = new Database();
                }
                $db = new Database();
                $msg = $db->queryMensagem("SELECT h.dominio_sincronizador
                          FROM assinatura a join hospedagem h on a.hospedagem_id_INT = h.id"
                          . " WHERE a.id= $idAssinatura");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                if ($db->rows() == 0)
                {
                    return new Mensagem(
                        PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                        I18N::getExpression("Assinatura não encontrada."));
                }

                return new Mensagem_token(null, null, $db->getPrimeiraTuplaDoResultSet(0));
            }
            catch (Exception $exc)
            {
                return new Mensagem(null, null, $exc);
            }
        }

        public static function getIdAssinaturaLivreDoSistema(
            $idSistema,
            $db = null)
        {
            $q = "SELECT id"
                . " FROM assinatura "
                . " WHERE sicob_cliente_assinatura_INT IS NULL "
                . " AND estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::DISPONIVEL
                . " AND sistema_id_INT = $idSistema "
                . " AND excluido_DATETIME IS NULL"
                . " LIMIT 0,1 ";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getNumeroDeAssinaturaLivreDoSistema($idSistema, Database $db)
        {
            $q = "SELECT COUNT(id)"
                . " FROM assinatura "
                . " WHERE sicob_cliente_assinatura_INT IS NULL "
                . " AND estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::DISPONIVEL
                . " AND sistema_id_INT = $idSistema "
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getNumeroDeAssinaturaDisponivelParaMigracaoDoSistema($idSistema, Database $db)
        {
            $q = "SELECT COUNT(id)"
                . " FROM assinatura "
                . " WHERE sicob_cliente_assinatura_INT IS NULL "
                . " AND estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::DISPONIVEL_PARA_MIGRACAO
                . " AND sistema_id_INT = $idSistema "
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getNumeroDeAssinaturaReservadasDoSistema($idSistema, Database $db)
        {
            $q = "SELECT COUNT(id)"
                . " FROM assinatura "
                . " WHERE  "
                . "  estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA
                . " AND sistema_id_INT = $idSistema "
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getNumeroDeAssinaturaNoEstado($idSistema, $idEstado, Database $db = null)
        {
            $q = "SELECT COUNT(id)"
                . " FROM assinatura "
                . " WHERE estado_assinatura_id_INT = " . $idEstado
                . " AND sistema_id_INT = $idSistema "
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getIdCorporacaoPorNome($idAssinatura, Database $db = null)
        {
            $q = "SELECT id_corporacao_INT idCorporacao, nome_site corporacao"
                . " FROM assinatura "
                . " WHERE id = " . $idAssinatura
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return Helper::getPrimeiroObjeto($db->result, 1, 0);
        }

        public static function getNomeSiteDaAssinatura($idAssinatura, Database $db = null)
        {
            $q = "SELECT nome_site"
                . " FROM assinatura "
                . " WHERE id = " . $idAssinatura
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getIdCorporacaoDaAssinatura($idAssinatura, Database $db = null)
        {
            $q = "SELECT id_corporacao_INT"
                . " FROM assinatura "
                . " WHERE id = " . $idAssinatura
                . " AND excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public function getStringArquivoDatabaseConfigSincronizador(
            $nomeCorporacao,
            $hospedagemDbPrincipal,
            $hospedagemDbSincronizador)
        {
            $nome = $hospedagemDbPrincipal->getNome_db();
            $hostWeb = $hospedagemDbPrincipal->getHost_db();
            $portaWeb = $hospedagemDbPrincipal->getPorta_db_INT();
            $usuarioWeb = $hospedagemDbPrincipal->getUsuario_db();
            $senhaWeb = $hospedagemDbPrincipal->getSenha_db();

            $nomeSincronizador = $hospedagemDbSincronizador->getNome_db();
            $hostSincronizador = $hospedagemDbSincronizador->getHost_db();
            $portaSincronizador = $hospedagemDbSincronizador->getPorta_db_INT();
            $usuarioSincronizador = $hospedagemDbSincronizador->getUsuario_db();
            $senhaSincronizador = $hospedagemDbSincronizador->getSenha_db();

            $str = "";
            $str .= "<?php\n";

            $str .= "        define('IDENTIFICADOR_SESSAO', \"sincronizador_web_$nomeCorporacao\");\n";
            $str .= "        define('TITULO_PAGINAS', \"Sincronizador Web Ponto Eletr&ocirc;nico - $nomeCorporacao\");\n";
            $str .= "        define('NOME_BANCO_DE_DADOS_PRINCIPAL', \"{$nomeSincronizador}\");\n\n";

            $str .= "        define('NOME_BANCO_DE_DADOS_SINCRONIZADOR', \"{$nomeSincronizador}\");\n";
            $str .= "        define('NOME_BANCO_DE_DADOS_WEB', \"{$nome}\");\n\n";

            $str .= "        define('BANCO_DE_DADOS_HOST', \"{$hostSincronizador}\");\n";
            $str .= "        define('BANCO_DE_DADOS_PORTA', \"{$portaSincronizador}\");\n";
            $str .= "        define('BANCO_DE_DADOS_USUARIO', \"{$usuarioSincronizador}\");\n";
            $fSenha = $senhaSincronizador == null ? "null" : "\"$senhaSincronizador\"";

            $str .= "        define('BANCO_DE_DADOS_SENHA', $fSenha);\n\n";

            $str .= "        define('BANCO_DE_DADOS_WEB_HOST', \"{$hostWeb}\");\n";
            $str .= "        define('BANCO_DE_DADOS_WEB_PORTA', \"{$portaWeb}\");\n";
            $str .= "        define('BANCO_DE_DADOS_WEB_USUARIO', \"{$usuarioWeb}\");\n";
            $str .= "        define('BANCO_DE_DADOS_WEB_SENHA', \"{$senhaWeb}\");\n\n";

            $str .= "\n";

            return $str;
        }

        public function getStringArquivoJsonDatabaseConfigSincronizador(
            $nomeCorporacao,
            $hospedagemDbPrincipal,
            $hospedagemDbSincronizador)
        {
            $nomeWeb = $hospedagemDbPrincipal->getNome_db();
            $hostWeb = $hospedagemDbPrincipal->getHost_db();
            $portaWeb = $hospedagemDbPrincipal->getPorta_db_INT();
            $usuarioWeb = $hospedagemDbPrincipal->getUsuario_db();
            $senhaWeb = $hospedagemDbPrincipal->getSenha_db();

            $nomeSincronizador = $hospedagemDbSincronizador->getNome_db();
            $hostSincronizador = $hospedagemDbSincronizador->getHost_db();
            $portaSincronizador = $hospedagemDbSincronizador->getPorta_db_INT();
            $usuarioSincronizador = $hospedagemDbSincronizador->getUsuario_db();
            $senhaSincronizador = $hospedagemDbSincronizador->getSenha_db();

            $configuracaoSite = new ConfiguracaoSiteSincronizador();
            //MANTER O MESMO IDENTIFICADOR QUE O SINCRONIZADOR WEB
            $configuracaoSite->IDENTIFICADOR_SESSAO = "OMG_$nomeCorporacao";
            $configuracaoSite->TITULO_PAGINAS = "Sincronizador Web Ponto Eletr&ocirc;nico - $nomeCorporacao";

            $configuracaoSite->NOME_BANCO_DE_DADOS_SINCRONIZADOR = $nomeSincronizador;
            $configuracaoSite->NOME_BANCO_DE_DADOS_WEB = $nomeWeb;

            $configuracaoSite->BANCO_DE_DADOS_WEB_HOST = $hostWeb;
            $configuracaoSite->BANCO_DE_DADOS_WEB_PORTA = $portaWeb;
            $configuracaoSite->BANCO_DE_DADOS_WEB_USUARIO = $usuarioWeb;
            $configuracaoSite->BANCO_DE_DADOS_WEB_SENHA = $senhaWeb;

            $configuracaoSite->BANCO_DE_DADOS_SINCRONIZADOR_HOST = $hostSincronizador;
            $configuracaoSite->BANCO_DE_DADOS_SINCRONIZADOR_PORTA = $portaSincronizador;
            $configuracaoSite->BANCO_DE_DADOS_SINCRONIZADOR_USUARIO = $usuarioSincronizador;
            $configuracaoSite->BANCO_DE_DADOS_SINCRONIZADOR_SENHA = $senhaSincronizador;

            return json_encode($configuracaoSite);
        }

        public function getNomeClasseConstantDatabaseConfigCorporacao($nomeCorporacao)
        {
            return "ConstantDatabaseConfig_$nomeCorporacao";
        }

        public function getStringArquivoObjetoDatabaseConfigSincronizador(
            $identificadorDominio,
            $nomeCorporacao,
            $hospedagemDbPrincipal,
            $hospedagemDbSincronizador)
        {
            $nome = $hospedagemDbPrincipal->getNome_db();
            $hostWeb = $hospedagemDbPrincipal->getHost_db();
            $portaWeb = $hospedagemDbPrincipal->getPorta_db_INT();
            $usuarioWeb = $hospedagemDbPrincipal->getUsuario_db();
            $senhaWeb = $hospedagemDbPrincipal->getSenha_db();

            $nomeSincronizador = $hospedagemDbSincronizador->getNome_db();
            $hostSincronizador = $hospedagemDbSincronizador->getHost_db();
            $portaSincronizador = $hospedagemDbSincronizador->getPorta_db_INT();
            $usuarioSincronizador = $hospedagemDbSincronizador->getUsuario_db();
            $senhaSincronizador = $hospedagemDbSincronizador->getSenha_db();

            $classeCorporacao = $this->getNomeClasseConstantDatabaseConfigCorporacao($nomeCorporacao);

            $str = "<?php\n";

            //$str .= "if(substr_count(\$_SERVER[\"HTTP_HOST\"],  \"$dominio\") >= 1){\n";
            $str .= "class $classeCorporacao extends InterfaceConstantDatabaseConfig {

    public function BANCO_DE_DADOS_HOST() {
        return \"$hostSincronizador\";
    }

    public function BANCO_DE_DADOS_PORTA() {
        return \"$portaSincronizador\";
    }

    public function BANCO_DE_DADOS_SENHA() {
        ";
            $fSenha = $senhaSincronizador == null ? "null" : "\"$senhaSincronizador\"";
            $str .= "   
        return $fSenha;
    }

    public function BANCO_DE_DADOS_USUARIO() {
        return \"$usuarioSincronizador\";
    }

    public function BANCO_DE_DADOS_WEB_HOST() {
        return \"$hostWeb\";
    }

    public function BANCO_DE_DADOS_WEB_PORTA() {
        return \"$portaWeb\";
    }

    public function BANCO_DE_DADOS_WEB_SENHA() {
        return \"$senhaWeb\";
    }

    public function BANCO_DE_DADOS_WEB_USUARIO() {
        return \"$usuarioWeb\";
    }

    public function IDENTIFICADOR_SESSAO() {
        return \"Sincronizador Web $nomeCorporacao\";
    }

    public function NOME_BANCO_DE_DADOS_PRINCIPAL() {
        return \"$nomeSincronizador\";
    }

    public function NOME_BANCO_DE_DADOS_SINCRONIZADOR() {
        return \"$nomeSincronizador\";
    }

    public function NOME_BANCO_DE_DADOS_WEB() {
        return \"$nome\";
    }

    public function TITULO_PAGINAS() {
        return \"sincronizador_web_$identificadorDominio\";
    }
            
            
            
		
		
		public function __construct()
		{

		}

		public static function factory()
		{

			return new ConstantDatabaseConfig_$nomeCorporacao();
		}
}";

            $str .= "\n";

            return $str;
        }

        public function getStringArquivoDatabaseConfigFactu(
            $nomeCorporacao,
            EXTDAO_Hospedagem_db $hospedagemDbPrincipal,
            EXTDAO_Hospedagem_db $hospedagemDbParametros,
            $identificadorDominio,
            $hospedagemDbSincronizador)
        {
            $nome = $hospedagemDbPrincipal->getNome_db();
            $host = $hospedagemDbPrincipal->getHost_db();
            $porta = $hospedagemDbPrincipal->getPorta_db_INT();
            $usuario = $hospedagemDbPrincipal->getUsuario_db();
            $senha = $hospedagemDbPrincipal->getSenha_db();

            $nomeParametros = $hospedagemDbParametros->getNome_db();
            $hostParametros = $hospedagemDbParametros->getHost_db();
            $portaParametros = $hospedagemDbParametros->getPorta_db_INT();
            $usuarioParametros = $hospedagemDbParametros->getUsuario_db();
            $senhaParametros = $hospedagemDbParametros->getSenha_db();
            $str = "";
            $str .= "<?php\n";

            $str .= "        define('NOME_CORPORACAO', \"$identificadorDominio\");\n";

            //MANTER O MESMO IDENTIFICADOR QUE O SINCRONIZADOR WEB
            $str .= "        define('IDENTIFICADOR_SESSAO', \"OMG_$identificadorDominio\");\n";
            $str .= "        define('TITULO_PAGINAS', \"Ponto Eletr&ocirc;nico - $nomeCorporacao\");\n";

            $str .= "        define('NOME_BANCO_DE_DADOS_PRINCIPAL', \"{$nome}\");\n";
            $str .= "        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', \"{$nomeParametros}\");\n";

            $str .= "        define('BANCO_DE_DADOS_HOST', \"{$host}\");\n";
            $str .= "        define('BANCO_DE_DADOS_PORTA', \"{$porta}\");\n";
            $str .= "        define('BANCO_DE_DADOS_USUARIO', \"{$usuario}\");\n";
            $fSenha = $senha == null ? "null" : "\"$senha\"";
            $str .= "        define('BANCO_DE_DADOS_SENHA', {$fSenha});\n";

            $str .= "        define('BANCO_DE_DADOS_CONFIGURACAO_HOST', \"{$hostParametros}\");\n";
            $str .= "        define('BANCO_DE_DADOS_CONFIGURACAO_PORTA', \"{$portaParametros}\");\n";

            $fSenhaParametros = $senhaParametros == null ? "null" : "\"$senhaParametros\"";
            $str .= "        define('BANCO_DE_DADOS_CONFIGURACAO_USUARIO', \"{$usuarioParametros}\");\n";
            $str .= "        define('BANCO_DE_DADOS_CONFIGURACAO_SENHA', {$fSenhaParametros});\n";

            $str .= "\n";

            return $str;
        }

        public function getStringArquivoJsonDatabaseConfigFactu(
            $nomeCorporacao,
            EXTDAO_Hospedagem_db $hospedagemDbPrincipal,
            EXTDAO_Hospedagem_db $hospedagemDbParametros,
            $identificadorDominio,
            $hospedagemDbSincronizador)
        {
            $identificadorDominio = str_replace(' ', "_", $identificadorDominio);
            $nome = $hospedagemDbPrincipal->getNome_db();
            $host = $hospedagemDbPrincipal->getHost_db();
            $porta = $hospedagemDbPrincipal->getPorta_db_INT();
            $usuario = $hospedagemDbPrincipal->getUsuario_db();
            $senha = $hospedagemDbPrincipal->getSenha_db();

            $nomeParametros = $hospedagemDbParametros->getNome_db();
            $hostParametros = $hospedagemDbParametros->getHost_db();
            $portaParametros = $hospedagemDbParametros->getPorta_db_INT();
            $usuarioParametros = $hospedagemDbParametros->getUsuario_db();
            $senhaParametros = $hospedagemDbParametros->getSenha_db();

            $configuracaoSite = new ConfiguracaoSite();
            $configuracaoSite->NOME_CORPORACAO = $identificadorDominio;
            $configuracaoSite->IDENTIFICADOR_SESSAO = "OMG_$identificadorDominio";
            $configuracaoSite->TITULO_PAGINAS = "Ponto Eletr&ocirc;nico - $nomeCorporacao";
            $configuracaoSite->NOME_BANCO_DE_DADOS_PRINCIPAL = $nome;
            $configuracaoSite->NOME_BANCO_DE_DADOS_DE_CONFIGURACAO = $nomeParametros;

            $configuracaoSite->BANCO_DE_DADOS_HOST = $host;
            $configuracaoSite->BANCO_DE_DADOS_PORTA = $porta;
            $configuracaoSite->BANCO_DE_DADOS_USUARIO = $usuario;
            $configuracaoSite->BANCO_DE_DADOS_SENHA = $senha;

            $configuracaoSite->BANCO_DE_DADOS_CONFIGURACAO_HOST = $hostParametros;
            $configuracaoSite->BANCO_DE_DADOS_CONFIGURACAO_PORTA = $portaParametros;
            $configuracaoSite->BANCO_DE_DADOS_CONFIGURACAO_USUARIO = $usuarioParametros;
            $configuracaoSite->BANCO_DE_DADOS_CONFIGURACAO_SENHA = $senhaParametros;

            return json_encode($configuracaoSite);
        }

        public function getStringArquivoConstantsFactu(
            $idHospedagem,
            $nomeDaCorporacao,
            $dominioDeAcesso,
            $identificadorDominio)
        {
            $str = "<?php\n";
            $str .= "//CONSTANTES RELATIVAS AO PROJETO ESPECIFICO\n";
            $str .= "define('WINDOWS', 1);\n";
            $str .= "define('LINUX', 2);\n";

            $str .= "define('SESSAO_TIPO_LIMITER', \"nocache\");\n";
            $str .= "define('SESSAO_TEMPO_EXPIRACAO', 120);  //em minutos\n";
            $str .= "define('SESSAO_TEMPO_DE_VIDA', 60); //em minutos\n";

            $str .= "define('PORTA_PADRAO_HTTP', 80);\n";
            $str .= "define('PORTA_PADRAO_HTTPS', 443);\n";

            $str .= "define('PORTA_HTTP', PORTA_PADRAO_HTTP);\n";
            $str .= "define('PORTA_HTTPS', PORTA_PADRAO_HTTPS);\n";

            $str .= "define('SISTEMA_OPERACIONAL', WINDOWS);\n";
            $str .= "define('LINGUA_PADRAO', \"pt-br\");\n";

            $str .= "define('EMAIL_PADRAO_RELATORIO_ERROS', \"automatico@omegasoftware.com.br\");\n";
            $str .= "define('PAGINA_INICIAL_PADRAO', \"pages/pagina_inicial.php\");\n";
            $str .= "define('DOMINIO_DE_ACESSO', \"$dominioDeAcesso\");\n";

            $str .= "define('MENSAGEM_DESINSCRICAO', \"Você está cadastrado em nossa lista de emails, caso não queira mais receber nossos informativos de eventos\");\n";

            $str .= "define('HOST_SERVICO_SMS', \"omegasoftware.dyndns.org\");\n";
            $str .= "define('LIMITE_CARACTERES_SMS', 160);\n";
            $str .= "define('LIMITE_SMS_POR_VEZ', 1);\n";

            $str .= "define('ENDERECO_DE_ACESSO', \"http://\" + DOMINIO_DE_ACESSO + \"/\");\n";
            $str .= "define('ENDERECO_DE_ACESSO_SSL', \"https://\" + DOMINIO_DE_ACESSO + \"/\");\n";

            $str .= "define('CODIGO_DA_CONTA_GOOGLE_ANALYTICS', \"\");\n";
            $str .= "define('CHAVE_DE_ACESSO', \"123\");\n";

            $str .= "define('MODO_DE_DEPURACAO', true);\n";

            $str .= "define('PREFIXO_PADRAO_TELEFONE', \"31\");\n";

            $str .= "//CONSTANTES DO SISTEMA\n";
            $str .= "define('LIMITE_TENTATICA_ENVIO_SMS', 3);\n";

            $str .= "define('CAMPO_TEXTO', 1);\n";
            $str .= "define('CAMPO_INTEIRO', 2);\n";
            $str .= "define('CAMPO_MOEDA', 3);\n";
            $str .= "define('CAMPO_DATA', 4);\n";
            $str .= "define('CAMPO_DATATIME', 5);\n";
            $str .= "define('CAMPO_TELEFONE', 6);\n";
            $str .= "define('CAMPO_CPF', 7);\n";
            $str .= "define('CAMPO_CNPJ', 8);\n";

            $str .= "define('REGISTROS_POR_PAGINA', 100);\n";

            $str .= "define('MENSAGEM_OK', 1);\n";
            $str .= "define('MENSAGEM_INFO', 2);\n";
            $str .= "define('MENSAGEM_WARNING', 3);\n";
            $str .= "define('MENSAGEM_ERRO', 4);\n";
            $str .= "define('MENSAGEM_ASK', 5);\n";
            $str .= "define('MENSAGEM_ANDROID', 6);\n";

            $str .= "define('CALENDARIO_INDIVIDUAL', \"individual\");\n";
            $str .= "define('CALENDARIO_INICIAL', \"inicial\");\n";
            $str .= "define('CALENDARIO_FINAL', \"final\");\n";

            $str .= "define('INVISIVEL', 0);\n";
            $str .= "define('VISIVEL', 1);\n";

            $str .= "define('OBJ_SEGURANCA', \"SECURITY\");\n";

            $str .= "define('TIMEOUT_TEMPORARIOS_EM_HORAS', 24);\n";

            $str .= "define('LARGURA_PADRAO_GREYBOX', 1000);\n";
            $str .= "define('ALTURA_PADRAO_GREYBOX', 600);\n";

            $str .= "define('LARGURA_PADRAO_RELATORIO', 800);\n";
            $str .= "define('ALTURA_PADRAO_RELATORIO', 600);\n";

            $str .= "define('LARGURA_GRAFICOS_INICIAL', 500);\n";
            $str .= "define('ALTURA_GRAFICOS_INICIAL', 500);\n";

            $str .= "define('LARGURA_GRAFICOS_AMPLIADO', 700);\n";
            $str .= "define('ALTURA_GRAFICOS_AMPLIADO', 700);\n";

            $str .= "define('COLUNAS_FORMULARIOS', 2);\n";

            $str .= "define('SEGUNDOS_EM_UM_MINUTO', 60);\n";
            $str .= "define('SEGUNDOS_EM_UMA_HORA', 3600);\n";
            $str .= "define('SEGUNDOS_EM_UM_DIA', 86400);\n";

            $str .= "define('QUEBRA_LINHA_PADRAO', \"\\r\\n\");\n";

            $str .= "define('COMANDO_VOLTAR_PAGINA', \"window.history.go(-1);\");\n";

            $str .= "define('COMANDO_FECHAR_DIALOG', \"parent.$('#div_dialog').dialog('close');\");\n";

            $str .= "define('COMANDO_FECHAR_DIALOG_ATUALIZANDO', \"parent.document.location = parent.document.location.toString().replace(/nivel_visualizacao/gi, \\\"old\\\").replace(/vaga_visualizacao/gi, \\\"old\\\")\");\n";

            $str .= "define ('TEMPO_PADRAO_FECHAR_DIALOG', 1.8);\n";

            $str .= "define('IMAGEM_PADRAO_MENU', \"padrao.png\");\n";

            $str .= "\n";

            return $str;
        }

        public static function criaNovaAssinaturas($idSistema, $estadoAssinatura = EXTDAO_Estado_assinatura::DISPONIVEL)
        {
            set_time_limit(0);
            $db = new Database();
            $objSistema = new EXTDAO_Sistema($db);
            $objSistema->select($idSistema);

            $strPathSrc = $objSistema->getPath_raiz_codigo_assinatura();
            $totalHospedagemASerInserida = 1;

            if ($objSistema->getSistema_tipo_id_INT() == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
            {
                $tempTotalHospedagemASerInserida = EXTDAO_Sistema::getMaxAssinaturaPorHospedagem($idSistema);
                if (is_numeric($tempTotalHospedagemASerInserida))
                {
                    $totalHospedagemASerInserida = $tempTotalHospedagemASerInserida;
                }
            }
            $sistemaDbs = $objSistema->getSistemaDbs();

            $objHospedagem = new EXTDAO_Hospedagem($db);

            $objHospedagem->formatarParaSQL();
            $objHospedagem->insert();

            $idNovaHospedagem = $objHospedagem->getUltimoIdInserido();

            if (!strlen($idNovaHospedagem))
            {
                return new Mensagem(PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR, I18N::getExpression("Falha ao inserir uma nova hospedagem no banco de dados."));
            }
            $objHospedagem->select($idNovaHospedagem);
            $strPathRaizHospedagem = $objSistema->getPath_raiz_assinatura();
            $atingiuMax = false;
            if ($objSistema->getSistema_tipo_id_INT() == EXTDAO_Sistema_tipo::COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
            {
                $strPathNovaHospedagem = Helper::getPathComBarra($strPathRaizHospedagem) . $idNovaHospedagem . "/";
                //$strPathNovaHospedagem = str_replace("/", "\\", $strPathNovaHospedagem);
                Helper::copiaDiretorio($strPathSrc, $strPathNovaHospedagem);
            }
            else
            {
                if ($objSistema->getSistema_tipo_id_INT() == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS
                    || $objSistema->getSistema_tipo_id_INT() == EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA)
                {
                    $strPathNovaHospedagem = $strPathRaizHospedagem;
                }
            }

            $strPathRaizSincronizador = $objSistema->getPath_raiz_codigo_sincronizador();
            $objHospedagem->setPath_sincronizador($strPathRaizSincronizador);
            $objHospedagem->setDominio_sincronizador($objSistema->getDominio_raiz_sincronizador());
            $objHospedagem->setEmpresa_hosting_id_INT($objSistema->getEmpresa_hosting_id_INT());
            $objHospedagem->setDominio($objSistema->getDominio_raiz_assinatura());

            if (count($sistemaDbs))
            {
                $objSrc = new EXTDAO_Sistema_db($db);
                for ($i = 0; $i < count($sistemaDbs); $i++)
                {
                    $idSistemaDb = $sistemaDbs[ $i ];

                    $objSrc->select($idSistemaDb);
                    $objSrc->cadastrarHospedagemDbDoSistema($objSistema, $idNovaHospedagem);
                }
            }

            $objHospedagem->setPath($strPathNovaHospedagem);

            $objHospedagem->formatarParaSQL();
            $objHospedagem->update($idNovaHospedagem);
            $ids = array();
            for ($i = 0; $i < $totalHospedagemASerInserida; $i++)
            {
                $objAssinatura = new EXTDAO_Assinatura($db);
                $objAssinatura->setEstado_assinatura_id_INT($estadoAssinatura);
                $objAssinatura->setHospedagem_id_INT($objHospedagem->getId());
                $objAssinatura->setSistema_id_INT($idSistema);

                $objAssinatura->formatarParaSQL();
                $objAssinatura->insert();
                $ids[ count($ids) ] = $objAssinatura->getUltimoIdInserido();
            }

            return $ids;
        }

        public static function getIdHospedageLivreParaCadastroCompartilhado($db = null)
        {
            $q = "select count(a.id) total, a.hospedagem_id_INT
                    from assinatura a 
                        join sistema s on a.sistema_id_INT = s.id
                    where s.sistema_tipo_id_INT = " . EXTDAO_Sistema_tipo::COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA
                        ." AND a.estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::DISPONIVEL
                        ." AND a.excluido_DATETIME IS NULL "
                        ." group by a.hospedagem_id_INT "
                        ." order by total asc";
            if ($db != null)
            {
                $db = new Database();
            }

            $db->query($q);

            if (mysqli_data_seek($db->result, 0))
            {
                $linha = $db->result->fetch_array(MYSQLI_NUM);

                $vetor = array();
                $vetor["id_hospedagem"] = $linha[1];
                $vetor["quantidade"] = $linha[0];

                return $vetor;
            }

            return null;
        }

        public static function limpaDadosDaAssinatura($idAssinatura, $db = null)
        {
            $q = "UPDATE assinatura SET nome_site = null, sicob_cliente_assinatura_INT = null 
                  WHERE id = $idAssinatura";
            if ($db != null)
            {
                $db = new Database();
            }

            return $db->queryMensagem($q);
        }

        public static function setaDadosDaAssinatura(
            $idCorporacao,
            $idAssinatura,
            $nomeSite,
            $idSicobClienteAssinatura,
            Database $db = null)
        {
            $q = "UPDATE assinatura 
                  SET nome_site = '$nomeSite', 
                    sicob_cliente_assinatura_INT = $idSicobClienteAssinatura ,
                    id_corporacao_INT = $idCorporacao
                  WHERE id = $idAssinatura";
            if ($db != null)
            {
                $db = new Database();
            }

            return $db->queryMensagem($q);
        }

        public static function updateNomeSite($idAssinatura, $nomeSite, Database $db = null)
        {
            $q = "UPDATE assinatura 
                  SET nome_site = '$nomeSite' 
                  WHERE id = $idAssinatura";
            if ($db == null)
            {
                $db = new Database();
            }

            return $db->queryMensagem($q);
        }

        public static function getIdHospedagem($idAssinatura, Database $db = null)
        {
            $q = "SELECT hospedagem_id_INT FROM assinatura WHERE id = $idAssinatura";
            if ($db == null)
            {
                $db = new Database();
            }
            $msg = $db->queryMensagem($q);
            if ($msg != null && !$msg->ok())
            {
                return null;
            }

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

    }

    

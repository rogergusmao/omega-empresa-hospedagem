<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Assinatura_migracao
    * NOME DA CLASSE DAO: DAO_Assinatura_migracao
    * DATA DE GERAÇÃO:    22.04.2017
    * ARQUIVO:            EXTDAO_Assinatura_migracao.php
    * TABELA MYSQL:       assinatura_migracao
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Assinatura_migracao extends DAO_Assinatura_migracao
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Assinatura_migracao";
        }

        public function getNomeTabela(){
            return "assinatura_migracao";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("A");
            $this->label_assinatura_id_INT = I18N::getExpression("A");
            $this->label_antigo_sistema_id_INT = I18N::getExpression("A");
            $this->label_novo_sistema_id_INT = I18N::getExpression("A");
            $this->label_data_cadastro_DATETIME = I18N::getExpression("A");
            $this->label_data_migracao_realizada_DATETIME = I18N::getExpression("A");
            $this->label_antigo_sicob_cliente_assinatura_INT = I18N::getExpression("A");
            $this->label_novo_sicob_cliente_assinatura_INT = I18N::getExpression("A");
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function getIdUltimaMigracaoPendenteOuEmAndamento(
            $idClienteAssinatura, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $q = "SELECT id, estado_assinatura_migracao_id_INT estado
FROM assinatura_migracao
WHERE excluido_DATETIME IS NULL 
  AND novo_sicob_cliente_assinatura_INT = $idClienteAssinatura
  AND estado_assinatura_migracao_id_INT IN( ".EXTDAO_Estado_assinatura_migracao::AGUARDANDO_MIGRACAO.",
  ".EXTDAO_Estado_assinatura_migracao::AGUARDANDO_ESTADO_MANUTENCAO.",
  ".EXTDAO_Estado_assinatura_migracao::CONTINUACAO_MIGRACAO.")
ORDER BY id DESC
LIMIT 0, 1 ";
            $db->queryMensagemThrowException($q);

            $obj = Helper::getPrimeiroObjeto($db->result, 1, 0);
            if (isset($obj) && isset($obj['id']))
            {
                return $obj;
            }

            return null;
        }

        //Esse processo atualmente funciona entre 00 as 04 da madrugada horario de SP
        //Ele nao migra ninguem com menos de 24 horas desde a data de contratacao do novo serviço
        public static function getProximoIdParaMigracao(
            Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $objAM = new EXTDAO_Assinatura_migracao($db);
            $objAM->setData_inicio_migracao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $msg = $db->queryMensagem("SET @update_id := -1");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            //Restrição do prazo de no minimo 24 horas para realizar a migração de dados
            $umDiaAtras = new DateTime();
            $umDiaAtras = $umDiaAtras->sub(new DateInterval("P1D"));
            $strUmDiaAtras = Helper::toStringDatetimeAmerican($umDiaAtras );

            $msg = $db->queryMensagem("UPDATE assinatura_migracao join ("
                                      . "      select min(aa1.id) as minId "
                                      . "      from assinatura_migracao aa1 "
                                      . "      where aa1.estado_assinatura_migracao_id_INT in ("
                                      . EXTDAO_Estado_assinatura_migracao::AGUARDANDO_MIGRACAO
                                      . ", " . EXTDAO_Estado_assinatura_migracao::FALTA_ASSINATURA_DISPONIVEL . ") "
                                      . "     ) amin "
                                      . "     on assinatura_migracao.id = amin.minId "
                                      . " SET data_inicio_migracao_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',"
                                      . "     id = (SELECT @update_id := id) "
                                      . " WHERE excluido_DATETIME IS NULL AND data_cadastro_DATETIME < '$strUmDiaAtras'");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $msg = $db->queryMensagem("SELECT @update_id");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $id = $db->getPrimeiraTuplaDoResultSet(0);

            if ($id > 0)
            {
                return new Mensagem_token(null, null, $id);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Não existem migrações pendentes");
            }
        }

        public static function getProximoIdParaContinuarMigracao(
            Database $db = null)
        {
            try
            {
                if ($db == null)
                {
                    $db = new Database();
                }

                $objAM = new EXTDAO_Assinatura_migracao($db);
                $objAM->setData_inicio_migracao_DATETIME(Helper::getDiaEHoraAtualSQL());
                $msg = $db->queryMensagem("SET @update_id := -1");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $msg = $db->queryMensagem("UPDATE assinatura_migracao join ("
                                          . "      SELECT min(aa1.id) as minId "
                                          . "      FROM assinatura_migracao aa1 "
                                          . "      WHERE aa1.estado_assinatura_migracao_id_INT in ("
                                          . EXTDAO_Estado_assinatura_migracao::AGUARDANDO_ESTADO_MANUTENCAO . ") "
                                          . "         AND ADDTIME(aa1.data_espera_manutencao_DATETIME, '00:" . Manutencao::TEMPO_CACHE_APC_MINUTOS . ":00')"
                                          . "             < '" . Helper::getDiaEHoraAtualSQL() . "' ) amin "
                                          . "     ON assinatura_migracao.id = amin.minId "
                                          . " SET data_retorno_migracao_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',"
                                          . "     estado_assinatura_migracao_id_INT = '" . EXTDAO_Estado_assinatura_migracao::CONTINUACAO_MIGRACAO . "', "
                                          . "     id = (SELECT @update_id := id) "
                                          . " WHERE excluido_DATETIME IS NULL ");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $msg = $db->queryMensagem("SELECT @update_id");
                if ($msg != null && $msg->erro())
                {
                    return $msg;
                }

                $id = $db->getPrimeiraTuplaDoResultSet(0);

                if ($id > 0)
                {
                    return new Mensagem_token(null, null, $id);
                }
                else
                {
                    return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO, "Não existem migrações pendentes");
                }
            }
            catch (Exception $ex)
            {
                return new Mensagem(null, null, $ex);
            }
        }

        public static function factory()
        {
            return new EXTDAO_Assinatura_migracao();
        }

        public static function getProximoIdParaDisponibilizar(
            Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $objAM = new EXTDAO_Assinatura_migracao($db);
            $objAM->setData_inicio_migracao_DATETIME(Helper::getDiaEHoraAtualSQL());
            $msg = $db->queryMensagem("SET @update_id := -1");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $date = new DateTime();
            $date->sub(new DateInterval('P15D'));
            $doisDiasAtras = $date->format('Y-m-d');

            $msg = $db->queryMensagem("UPDATE assinatura_migracao join ("
                                      . "      select min(aa1.id) as minId "
                                      . "      from assinatura_migracao aa1 "
                                      . "      where aa1.estado_assinatura_migracao_id_INT in ("
                                      . EXTDAO_Estado_assinatura_migracao::MIGRADO_COM_SUCESSO . ") "
                                      . "     ) amin "
                                      . "     on assinatura_migracao.id = amin.minId "
                                      . " SET data_inicio_disponibilizacao_assinatura_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',"
                                      . "     id = (SELECT @update_id := id) "
                                      . " WHERE excluido_DATETIME IS NULL ");

            // " AND data_fim_migracao_DATETIME > '".$doisDiasAtras."' "

            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $msg = $db->queryMensagem("SELECT @update_id");
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }

            $id = $db->getPrimeiraTuplaDoResultSet(0);

            if ($id > 0)
            {
                return new Mensagem_token(null, null, $id);
            }
            else
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::RESULTADO_VAZIO,
                    "Não existem migrações para disponibilizar.");
            }
        }

        public function getIdAssinaturaGratuitaPendenteDeDisponibilizacao($idAssinatura, $idCorporacao)
        {
            $db = Registry::get("Database");
            $idHospedagemNova = EXTDAO_Assinatura::getHospedagemDaAssinatura($idAssinatura, $db);

            $q = "select max(am.id), a.id
from assinatura_migracao am
	left join assinatura a
		on am.assinatura_id_INT = a.id
where am.estado_assinatura_migracao_id_INT = " . EXTDAO_Estado_assinatura_migracao::MIGRADO_COM_SUCESSO . "
            AND a.hospedagem_id_INT = $idHospedagemNova
            AND am.antigo_sistema_id_INT = " . EXTDAO_Sistema::OMEGA_EMPRESA_CORPORACAO . " 
            AND am.excluido_DATETIME IS NULL
            AND a.excluido_DATETIME IS NULL
            AND am.id_corporacao_INT = $idCorporacao 
    GROUP BY a.id";

            $msg = $db->queryMensagem($q);
            if ($msg != null && !$msg->ok())
            {
                return $msg;
            }
            $id = $db->getPrimeiraTuplaDoResultSet(0);
            if ($id != null)
            {
                return new Mensagem_token(
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    null,
                    $id);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
        }

        public static function iniciaEsperaParaEstadoManutencao($idAssinaturaMigracao, $idNovaAssinatura, $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $msg = $db->queryMensagem("UPDATE assinatura_migracao 
             SET data_espera_manutencao_DATETIME = '" . Helper::getDiaEHoraAtualSQL() . "',
                estado_assinatura_migracao_id_INT = " . EXTDAO_Estado_assinatura_migracao::AGUARDANDO_ESTADO_MANUTENCAO . ",
                nova_assinatura_id_INT = $idNovaAssinatura
             WHERE id = $idAssinaturaMigracao");

            return $msg;
        }

        public static function getDataDaProximaMigracaoMarcada(
            $corporacao,
            Database $db = null){

            if ($db == null)
            {
                $db = new Database();
            }

            $db->queryMensagemThrowException(
                "SELECT UNIX_TIMESTAMP(am.data_cadastro_DATETIME)
FROM assinatura_migracao am JOIN assinatura a ON am.assinatura_id_INT = a.id
WHERE  a.nome_site = '$corporacao'
AND am.data_fim_migracao_DATETIME IS NULL
 AND a.excluido_DATETIME IS NULL
AND am.excluido_DATETIME IS NULL");

            $r = $db->getPrimeiraTuplaDoResultSet(0);

            if(strlen($r)){
                $dataFuturaSegundos = $r+ 24 * 60 * 60;
                $dataFutura = new DateTime("@$dataFuturaSegundos", new DateTimeZone("America/Sao_Paulo"));

                return $dataFutura ;
            } else
            {
                return null;
            }
        }


        public static function existeMigracaoPendente($corporacao, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $dataFutura = EXTDAO_Assinatura_migracao::getDataDaProximaMigracaoMarcada($corporacao, $db);

            if($dataFutura!=null){
                $hora = $dataFutura->format('H');
                $now = new DateTime();
                if($now > $dataFutura
                    && $hora >= HORA_INICIO_FUNCIONAMENTO_MIGRACAO_DE_ASSINATURAS
                    && $hora <= HORA_FIM_FUNCIONAMENTO_MIGRACAO_DE_ASSINATURAS){

                    return new Mensagem(PROTOCOLO_SISTEMA::SIM,

                        I18N::getExpression("Dentro de alguns instantes a migração irá ocorrer, entre 00:00 e 00:04 GMT Brasil/Brasilia"));
                } else {
                    //adiciona um dia a mais a data futura
                    if($hora >= 0 && $hora <= 4){
                        //A data futura já é a data viavél para inicio da sincronização
                    } else {
                        $dataViavel = $dataFutura->add(new DateInterval('P1D'));
                    }

                    $strDataViavel = Helper::toStringDate($dataViavel);
                    return new Mensagem(PROTOCOLO_SISTEMA::SIM,
                        I18N::getExpression(
                            "Por favor, realize a sincronização dos telefone antes do dia {0} 00:00."
                            ." Entre 00:00 e 04:00 haverá manutenção e os dados não sincronizados serão perdidos.",
                            $strDataViavel));
                }
            } else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::NAO,
                    I18N::getExpression("Não existe migração pendente."));
            }
        }


        public function getDataMinimaParaMigracao(){
            //$this->getData_cadastro_DATETIME_UNIX() tempo do cadastro em segundos
            //mais a adicao de um dia
            $dataFuturaSegundos = $this->getData_cadastro_DATETIME_UNIX() + 24 * 60 * 60;

            $dataFutura = new DateTime("@$dataFuturaSegundos", new DateTimeZone("America/Sao_Paulo"));

            return Helper::toStringDatetime($dataFutura);
        }

        public static function existeMigracaoEmAndamento(
            $idAssinatura, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $q = "SELECT 1 
            FROM assinatura_migracao 
            WHERE excluido_DATETIME IS NULL 
              AND assinatura_id_INT =$idAssinatura
              AND estado_assinatura_migracao_id_INT IN (
                ".EXTDAO_Estado_assinatura_migracao::CONTINUACAO_MIGRACAO.",
                ".EXTDAO_Estado_assinatura_migracao::CONTINUACAO_MIGRACAO."
              )
            LIMIT 0,1 ";
            $db->queryMensagemThrowException($q);
            return $db->getPrimeiraTuplaDoResultSet(0);

        }


        public static function cancelaTodaAssinaturaPendente(
            $idAssinatura, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $q = "UPDATE assinatura_migracao 
            SET excluido_BOOLEAN = '1',
              excluido_DATETIME = '".Helper::getDiaEHoraAtualSQL()."'
            WHERE assinatura_id_INT = '$idAssinatura' 
                AND estado_assinatura_migracao_id_INT = ".EXTDAO_Estado_assinatura_migracao::AGUARDANDO_MIGRACAO."
                AND excluido_DATETIME IS NULL
            ";
            return $db->queryMensagem($q);

        }


        public static function getDadosDoUltimoSistemaPendenteParaMigracaoDaAssinatura(
            $idAssinatura, Database $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }


            $q = "SELECT id, antigo_sistema_id_INT, novo_sistema_id_INT 
            FROM assinatura_migracao
            WHERE assinatura_id_INT = '$idAssinatura' 
                AND estado_assinatura_migracao_id_INT = ".EXTDAO_Estado_assinatura_migracao::AGUARDANDO_MIGRACAO."
                AND excluido_DATETIME IS NULL
            ORDER BY id DESC
            LIMIT 0,1";
            $db->queryMensagemThrowException($q);

            $n = Helper::getResultSetToMatriz($db->result,1,0);
            if(count($n)) return $n[0];
            else return null;

        }

    }

    

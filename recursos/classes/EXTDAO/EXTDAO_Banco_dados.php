<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Banco_dados
    * NOME DA CLASSE DAO: DAO_Banco_dados
    * DATA DE GERAÇÃO:    12.12.2009
    * ARQUIVO:            EXTDAO_Banco_dados.php
    * TABELA MYSQL:       banco_dados
    * BANCO DE DADOS:     DEP_pesquisas_config
    * -------------------------------------------------------
    *
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Banco_dados extends DAO_Banco_dados
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->database = new Database(NOME_BANCO_DE_DADOS_DE_CONFIGURACAO);
            $this->nomeClasse = "EXTDAO_Banco_dados";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome_arquivo = I18N::getExpression("Nome do Arquivo");
            $this->label_descricao = I18N::getExpression("Descrição do Backup");
            $this->label_data_criacao = I18N::getExpression("Data de Criação");
            $this->label_data_ultima_restauracao = I18N::getExpression("Data da Última Restauração");
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O backup do banco de dados foi realizado com sucesso.");
            $mensagemErro = I18N::getExpression("Falha ao realizar o backup.");

            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            $objBackup = new Database_Backup();

            if ($objBackup->fazerBackup())
            {
                return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
            }
            else
            {
                return array("location: $urlErro&msgSucesso=$mensagemErro");
            }
        }

        public function __actionRestaurar()
        {
            $mensagemSucesso = I18N::getExpression("A restauração do banco de dados foi realizada com sucesso.");
            $mensagemErro = I18N::getExpression("Falha ao restaurar o backup.");

            $urlSuccess = Helper::getUrlAction("pages_restaurar_backup_banco_dados", Helper::GET("id"));
            $urlErro = Helper::getUrlAction("pages_restaurar_backup_banco_dados", Helper::GET("id"));

            $objBackup = new Database_Backup();
            $datetime = date("d/m/Y h:i:s");
            $_POST["descricao"] = htmlentities("Backup automático do banco de dados integral feito em $datetime");

            if (!$objBackup->fazerBackup())
            {
                $msg = I18N::getExpression("Não foi possível executar o backup automático.");
                return array("location: $urlErro&msgErro={$msg}");
            }

            if ($objBackup->restaurarBackup(Helper::GET("id")))
            {
                return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
            }
            else
            {
                return array("location: $urlErro&msgErro=$mensagemErro");
            }
        }

        public static function factory()
        {
            return new EXTDAO_Banco_dados();
        }

    }


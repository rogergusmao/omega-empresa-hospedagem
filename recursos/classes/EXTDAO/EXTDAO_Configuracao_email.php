<?php

//@@NAO_MODIFICAR

    /*
     *
     * -------------------------------------------------------
     * NOME DA CLASSE:     EXTDAO_Configuracao_email
     * NOME DA CLASSE DAO: DAO_Configuracao_email
     * DATA DE GERAÇÃO:    26.07.2011
     * ARQUIVO:            EXTDAO_Configuracao_email.php
     * TABELA MYSQL:       configuracao_email
     * BANCO DE DADOS:     email_marketing
     * -------------------------------------------------------
     *
     */

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

    class EXTDAO_Configuracao_email extends DAO_Configuracao_email
    {

        public $usuarioReturnPath = "";
        public $senhaReturnPath = "";

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Configuracao_email";
        }

        public function imprimirCampoEndereco_remetente($objArguments)
        {
            $objArguments->nome = "endereco_remetente";
            $objArguments->id = "endereco_remetente";

            return $this->campoEmail($objArguments);
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome_remetente = I18N::getExpression("Nome do Remetente Padrão");
            $this->label_endereco_remetente = I18N::getExpression("Endereço do Remetente Padrão");
            $this->label_numero_de_emails_por_vez_INT = I18N::getExpression("Número de Emails Enviados Por Turno");
            $this->label_intervalo_turno_em_segundos_INT = I18N::getExpression("Intervalo Entre Os Turnos (em Segundos)");
            $this->label_return_path = I18N::getExpression("Return Path");
            $this->label_usuario_return_path = I18N::getExpression("Usuário do Return Path");
            $this->label_senha_return_path = I18N::getExpression("Senha do Return Path");
        }

        public function imprimirCampoReturn_path($objArguments)
        {
            $objArguments->nome = "return_path";
            $objArguments->id = "return_path";

            return $this->campoEmail($objArguments);
        }

        public function imprimirCampoUsuario_return_path($objArguments)
        {
            $objArguments->nome = "usuario_return_path";
            $objArguments->id = "usuario_return_path";

            return $this->campoTexto($objArguments);
        }

        public function imprimirCampoSenha_return_path($objArguments)
        {
            $objArguments->nome = "senha_return_path";
            $objArguments->id = "senha_return_path";
            $objArguments->campoSenha = true;

            return $this->campoTexto($objArguments);
        }

        public static function factory()
        {
            return new EXTDAO_Configuracao_email();
        }

    }


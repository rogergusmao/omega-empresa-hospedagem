<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Db_tipo
    * NOME DA CLASSE DAO: DAO_Db_tipo
    * DATA DE GERAÇÃO:    17.08.2012
    * ARQUIVO:            EXTDAO_Db_tipo.php
    * TABELA MYSQL:       db_tipo
    * BANCO DE DADOS:     omegasoftware_interno
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Db_tipo extends DAO_Db_tipo
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Db_tipo";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_porta_padrao_INT = I18N::getExpression("Porta Padrão");
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function factory()
        {
            return new EXTDAO_Db_tipo();
        }

    }

    

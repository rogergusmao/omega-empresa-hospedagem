<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Empresa_hosting
    * NOME DA CLASSE DAO: DAO_Empresa_hosting
    * DATA DE GERAÇÃO:    17.08.2012
    * ARQUIVO:            EXTDAO_Empresa_hosting.php
    * TABELA MYSQL:       empresa_hosting
    * BANCO DE DADOS:     omegasoftware_interno
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Empresa_hosting extends DAO_Empresa_hosting
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Empresa_hosting";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_endereco_painel_controle = I18N::getExpression("Endereço do Painel de Controle");
            $this->label_endereco_cobranca = I18N::getExpression("Endereço de Cobrança");
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function factory()
        {
            return new EXTDAO_Empresa_hosting();
        }

        public static function getIdsSihopAssinaturaParaSincronizacao($idEmpresaHosting, $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $q = "select DISTINCT a.id id_assinatura, h.id id_hospedagem, a.sistema_id_INT id_sistema, a.id_corporacao_INT id_corporacao
from assinatura a join hospedagem h on a.hospedagem_id_INT = h.id
where a.estado_assinatura_id_INT IN ("
                . EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA . ", "
                . EXTDAO_Estado_assinatura::EM_MANUTENCAO . ", "
                . EXTDAO_Estado_assinatura::OCUPADA . ")
	and a.excluido_BOOLEAN != 1
	and h.empresa_hosting_id_INT = $idEmpresaHosting 
	and h.excluido_DATETIME IS NULL
	and a.excluido_DATETIME IS NULL
order by a.id ";

            $msg = $db->queryMensagem($q);
            if ($msg != null && $msg->erro())
            {
                return $msg;
            }
            $objs = Helper::getResultSetToMatriz($db->result, 0, 1);

            if (!empty($objs))
            {
//                $retorno = array();
//                foreach ($objs as $obj)
//                {
//                    $retorno[ count($retorno) ] = Protocolo_empresa_hosting::constroi(array(
//                                                                                          $obj[0],
//                                                                                          $obj[1],
//                                                                                          $obj[2]));
//                }

                return new Mensagem_vetor_protocolo(
                    null,
                    PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                    null,
                    $objs);
            }
            else
            {
                return new Mensagem(PROTOCOLO_SISTEMA::RESULTADO_VAZIO);
            }
        }

    }

    

<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Estado_assinatura
    * NOME DA CLASSE DAO: DAO_Estado_assinatura
    * DATA DE GERAÇÃO:    01.11.2013
    * ARQUIVO:            EXTDAO_Estado_assinatura.php
    * TABELA MYSQL:       estado_assinatura
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Estado_assinatura extends DAO_Estado_assinatura
    {
//1	disponível;
        const DISPONIVEL = 1;
//2	ocupada - ocupada passou a ser uma assinatura com um cliente artificial
        const OCUPADA = 2;

        const CANCELADA = 3;

        const EM_MIGRACAO = 4;

        const EM_MANUTENCAO = 5;

        const ASSINATURA_FOI_MIGRADA = 6;

        const ERRO_DURANTE_DISPONIBILIZACAO = 7;
// assinatura reserva significa que um cliente real esta ocupando a assinatura
        const ASSINATURA_RESERVADA = 8;

        const DISPONIVEL_PARA_MIGRACAO = 9;

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Estado_assinatura";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function factory()
        {
            return new EXTDAO_Estado_assinatura();
        }

    }

    

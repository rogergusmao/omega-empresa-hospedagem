<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Estado_assinatura_migracao
    * NOME DA CLASSE DAO: DAO_Estado_assinatura_migracao
    * DATA DE GERA��O:    02.07.2017
    * ARQUIVO:            EXTDAO_Estado_assinatura_migracao.php
    * TABELA MYSQL:       estado_assinatura_migracao
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class EXTDAO_Estado_assinatura_migracao extends DAO_Estado_assinatura_migracao
    {
        const AGUARDANDO_MIGRACAO = 1;

        const AGUARDANDO_ESTADO_MANUTENCAO = 2;
        const CONTINUACAO_MIGRACAO = 3;

        const MIGRADO_COM_SUCESSO = 4;
        const FALTA_ASSINATURA_DISPONIVEL = 5;
        const ERRO = 6;

        const ASSINATURA_DISPONIBILIZADA = 7;

        const ERRO_DURANTE_DISPONIBILIZACAO = 8;

        const REALIZANDO_MIGRACAO = 9;

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Estado_assinatura_migracao";
        }

        public function setLabels()
        {
            $this->label_id = "";
            $this->label_nome = "";
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function factory()
        {
            return new EXTDAO_Estado_assinatura_migracao();
        }

    }

    
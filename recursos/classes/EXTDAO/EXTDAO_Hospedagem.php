<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Hospedagem
    * NOME DA CLASSE DAO: DAO_Hospedagem
    * DATA DE GERAÇÃO:    31.10.2013
    * ARQUIVO:            EXTDAO_Hospedagem.php
    * TABELA MYSQL:       hospedagem
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Hospedagem extends DAO_Hospedagem
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Hospedagem";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_dominio = I18N::getExpression("Domínio");
            $this->label_empresa_hosting_id_INT = I18N::getExpression("Hosting da Empresa");
            $this->label_usuario_hospedagem = I18N::getExpression("Usuário da Hospedagem");
            $this->label_senha_hospedagem = I18N::getExpression("Senha da Hospedagem");
            $this->label_dominio_webservice = I18N::getExpression("Domínio do WebService");
            $this->label_path = I18N::getExpression("Path");
            $this->label_excluido_BOOLEAN = I18N::getExpression("Excluído?");
            $this->label_excluido_DATETIME = I18N::getExpression("Data de Exclusão");
        }

        public static function getHospedagensDoSistemaQueEstaoOcupadas($idSistema)
        {
            $q = "SELECT h.id hospedagem, a.id assinatura, a.nome_site nomeSite "
                . " FROM assinatura a "
                . "      JOIN hospedagem h "
                . "             ON a.hospedagem_id_INT = h.id "
                . " WHERE a.estado_assinatura_id_INT = " . EXTDAO_Estado_assinatura::OCUPADA . ""
                . "       AND a.sistema_id_INT = $idSistema "
                . "           AND a.excluido_DATETIME IS NULL";
            $db = new Database();
            $db->query($q);

            return Helper::getResultSetToMatriz($db->result);
        }

        public function getHospendagensDb()
        {
            $q = "SELECT id "
                . " FROM hospedagem_db"
                . " WHERE hospedagem_id_INT = " . $this->getId()
                . " AND excluido_DATETIME IS NULL";
            $this->database->query($q);

            return Helper::getResultSetToArrayDeUmCampo($this->database->result);
        }

        public static function factory()
        {
            return new EXTDAO_Hospedagem();
        }

        public function getEspacoOcupadoPelosArquivosDaHospedagemEmMB()
        {
            $pathHospedagem = $this->getPath();

            return Helper::getTamanhoDoDiretorio($pathHospedagem);
        }

        public function getPath()
        {
            $path = $this->path;
            switch (Seguranca::getSistemaOperacionalDoServidor())
            {
                case WINDOWS:
                    $path = str_replace("\\", "/", $path);

                    break;
                case LINUX:
                    $path = str_replace("/", "\\", $path);

                    break;
                default:
                    break;
            }

            return $path;
        }

    }

    

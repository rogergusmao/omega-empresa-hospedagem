<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Hospedagem_db
    * NOME DA CLASSE DAO: DAO_Hospedagem_db
    * DATA DE GERA��O:    17.08.2012
    * ARQUIVO:            EXTDAO_Hospedagem_db.php
    * TABELA MYSQL:       hospedagem_db
    * BANCO DE DADOS:     omegasoftware_interno
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class EXTDAO_Hospedagem_db extends DAO_Hospedagem_db
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Hospedagem_db";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_hospedagem_id_INT = I18N::getExpression("Hospedagem");
            $this->label_nome_db = I18N::getExpression("Nome do Banco de Dados");
            $this->label_host_db = I18N::getExpression("Host");
            $this->label_porta_db_INT = I18N::getExpression("Porta");
            $this->label_usuario_db = I18N::getExpression("Usu�rio");
            $this->label_senha_db = I18N::getExpression("Senha");
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function factory()
        {
            return new EXTDAO_Hospedagem_db();
        }

        public function getObjDatabase()
        {
            $db = new Database(
                $this->getNome_db(),
                $this->getHost_db(),
                $this->getPorta_db_INT(),
                $this->getUsuario_db());

            return $db;
        }

        public static function getIdHospedagemDbDoTipo($idHospedagem, $idTipoHospedagemDb, Database $db = null)
        {
            $q = "SELECT id "
                . " FROM hospedagem_db "
                . " WHERE hospedagem_id_INT = $idHospedagem "
                . "     AND tipo_hospedagem_db_id_INT = $idTipoHospedagemDb "
                . "     AND excluido_DATETIME IS NULL ";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getIdsHospedagemDb($idHospedagem, Database $db = null)
        {
            $q = "SELECT id "
                . " FROM hospedagem_db "
                . " WHERE hospedagem_id_INT = $idHospedagem "
                . "     AND excluido_DATETIME IS NULL ";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }

//
        public function cadastrarHospedagemDbDoSistema($sistema, $hospedagemDest)
        {
            $obj = new EXTDAO_Hospedagem_db();
            $obj->setHospedagem_id_INT($hospedagemDest);
            $obj->setHost_db($this->getHost_db());
            $nomeDbDest = $this->getNome_db() . "_" . $hospedagemDest;
            $obj->setNome_db($nomeDbDest);
            $obj->setPorta_db_INT($this->getPorta_db_INT());
            $obj->setUsuario_db($this->getUsuario_db());
            $obj->setSenha_db($this->getSenha_db());
            $obj->formatarParaSQL();
            $obj->insert();

            $idNovaHospedagemDb = $obj->getUltimoIdInserido();
            $obj->select($idNovaHospedagemDb);
            $database = new Database();
            $database->criaBancoDeDados($nomeDbDest);

            $objSistema = new EXTDAO_Sistema();
            $objSistema->select($sistema);
            $raizDump = $objSistema->getPath_raiz_dump();
            $pathDump = null;

            if (!strlen($this->getPath_arquivo_dump()))
            {
                $databaseSrc = $this->getObjDatabase();
                $pathDump = Helper::getPathComBarra($raizDump) . $this->getNome_db() . ".sql";
                $databaseSrc->dumpBancoDeDados($pathDump);
                $objSrc = new EXTDAO_Hospedagem_db();
                $objSrc->select($this->getId());
                $objSrc->setPath_arquivo_dump($this->getNome_db() . ".sql");
                $objSrc->formatarParaSQL();
                $objSrc->update($this->getId());
            }
            else
            {
                $pathDump = Helper::getPathComBarra($raizDump) . $this->getPath_arquivo_dump();
            }
            $databaseTgt = $obj->getObjDatabase();
            $databaseTgt->executaScriptNoBancoDeDados($pathDump);
        }

        public static function getIdSistema($idHospedagem, $db = null)
        {
            $q = "select a.sistema_id_INT
from hospedagem h join assinatura a on a.hospedagem_id_INT = h.id
where h.id=$idHospedagem";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }
    }

    
<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Hospedagem_ftp
    * NOME DA CLASSE DAO: DAO_Hospedagem_ftp
    * DATA DE GERAÇÃO:    17.08.2012
    * ARQUIVO:            EXTDAO_Hospedagem_ftp.php
    * TABELA MYSQL:       hospedagem_ftp
    * BANCO DE DADOS:     omegasoftware_interno
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Hospedagem_ftp extends DAO_Hospedagem_ftp
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Hospedagem_ftp";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_hospedagem_id_INT = I18N::getExpression("Hospedagem");
            $this->label_host_ftp = I18N::getExpression("Host");
            $this->label_porta_ftp_INT = I18N::getExpression("Porta");
            $this->label_usuario_ftp = I18N::getExpression("Usuário");
            $this->label_senha_ftp = I18N::getExpression("Senha");
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function factory()
        {
            return new EXTDAO_Hospedagem_ftp();
        }

    }

    

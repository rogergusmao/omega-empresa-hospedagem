<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Operacao_sistema
    * NOME DA CLASSE DAO: DAO_Operacao_sistema
    * DATA DE GERAÇÃO:    23.10.2009
    * ARQUIVO:            EXTDAO_Operacao_sistema.php
    * TABELA MYSQL:       operacao_sistema
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Operacao_sistema extends DAO_Operacao_sistema
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Operacao_sistema";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_usuario_id_INT = I18N::getExpression("Usuário");
            $this->label_tipo_operacao = I18N::getExpression("Tipo de Operação");
            $this->label_pagina_operacao = I18N::getExpression("Página de Origem da Operação");
            $this->label_entidade_operacao = I18N::getExpression("Entidade Operação");
            $this->label_chave_registro_operacao_INT = I18N::getExpression("Chave do Registro");
            $this->label_descricao_operacao = I18N::getExpression("Descrição da Operação");
            $this->label_data_operacao_DATETIME = I18N::getExpression("Data da Operação");
        }

        public static function factory()
        {
            return new EXTDAO_Operacao_sistema();
        }

    }    
    

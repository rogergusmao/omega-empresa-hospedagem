<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema
    * NOME DA CLASSE DAO: DAO_Sistema
    * DATA DE GERAÇÃO:    01.11.2013
    * ARQUIVO:            EXTDAO_Sistema.php
    * TABELA MYSQL:       sistema
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema extends DAO_Sistema
    {
        const OMEGA_EMPRESA = 1;
        const OMEGA_EMPRESA_CORPORACAO = 3;

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Sistema";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_sistema_biblioteca_nuvem_INT = I18N::getExpression("Código do Sistema No Biblioteca_nuvem");
            $this->label_path_raiz_assinatura = I18N::getExpression("Path Raiz do Código dos Clientes");
            $this->label_path_raiz_dump = I18N::getExpression("Path do Arquivo do Banco de Dados");
            $this->label_dominio_raiz_assinatura = I18N::getExpression("Domínio Raíz dos Clientes do Sistema");
            $this->label_path_raiz_codigo_assinatura = I18N::getExpression("Path do código");
            $this->label_empresa_hosting_id_INT = I18N::getExpression("Hosting dos Clientes");

            $this->label_excluido_BOOLEAN = I18N::getExpression("Excluído?");
            $this->label_excluido_DATETIME = I18N::getExpression("Data de Exclusão");
        }

        public static function factory()
        {
            return new EXTDAO_Sistema();
        }

        public static function getSistemas($db = null)
        {
            $q = "SELECT id "
                . " FROM sistema "
                . " WHERE excluido_DATETIME IS NULL";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }

        public static function getIdSistemaDoIdSistemaBibliotecaNuvem($idSistemaBibliotecaNuvem, $db = null)
        {
            $q = "SELECT id "
                . " FROM sistema "
                . " WHERE excluido_DATETIME IS NULL"
                . "     AND sistema_biblioteca_nuvem_INT = $idSistemaBibliotecaNuvem";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return Helper::getResultSetToArrayDeUmCampo($db->result);
        }

        public function getSistemaDbs()
        {
            $q = "SELECT id "
                . " FROM sistema_db"
                . " WHERE sistema_id_INT = " . $this->getId()
                . " AND excluido_DATETIME IS NULL";
            $this->database->query($q);

            return Helper::getResultSetToArrayDeUmCampo($this->database->result);
        }

        public static function removerDumpDosBancosModelosDoSistema($idSistema)
        {
            try
            {
                set_time_limit(40 * 60);
                $objSistema = new EXTDAO_Sistema();
                $objSistema->select($idSistema);
                $sistemaDbs = $objSistema->getSistemaDbs();

                if ($sistemaDbs != null)
                {
                    $objSB = new EXTDAO_Sistema_db();
                    for ($i = 0; $i < count($sistemaDbs); $i++)
                    {
                        $objSB->select($sistemaDbs[ $i ]);
                        $objSB->removeDumpSql();
                    }
                }

                return new Mensagem(PROTOCOLO_SISTEMA::OPERACAO_REALIZADA_COM_SUCESSO,
                                    "Dumps sql's foram removidos.");
            }
            catch (Exception $ex)
            {
                return new Mensagem(null, null, $ex);
            }
        }

        public static function getMaxAssinaturaPorHospedagem(
            $idSistema, $db = null)
        {
            $q = "SELECT max_assinatura_por_hospedagem_INT "
                . " FROM sistema "
                . " WHERE excluido_DATETIME IS NULL"
                . "     AND id = $idSistema";
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

    }

    

<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_db
    * NOME DA CLASSE DAO: DAO_Sistema_db
    * DATA DE GERAÇÃO:    08.11.2013
    * ARQUIVO:            EXTDAO_Sistema_db.php
    * TABELA MYSQL:       sistema_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_db extends DAO_Sistema_db
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Sistema_db";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome_db = I18N::getExpression("Nome");
            $this->label_assinatura_host_db = I18N::getExpression("Host do Banco das Assinaturas");
            $this->label_assinatura_porta_db_INT = I18N::getExpression("Porta do Banco das Assinaturas");
            $this->label_assinatura_usuario_db = I18N::getExpression("Usuário do Banco das Assinaturas");
            $this->label_assinatura_senha_db = I18N::getExpression("Senha do Banco das Assinaturas");
            $this->label_path_arquivo_dump = I18N::getExpression("Path dos Arquivos de Dump do Banco");
            $this->label_excluido_BOOLEAN = I18N::getExpression("Excluído?");
            $this->label_excluido_DATETIME = I18N::getExpression("Data de Exclusão");
        }

        public static function factory()
        {
            return new EXTDAO_Sistema_db();
        }

        public static function getHospedagemDbDoSistemaDb($identificador)
        {
            $q = "SELECT hospedagem_db_id_INT "
                . " FROM sistema_db"
                . " WHERE identificador = '" . $identificador . "' "
                . " AND excluido_DATETIME IS NULL"
                . " LIMIT 0,1";

            $db = new Database();
            $db->query($q);

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public function removeDumpSql()
        {
            $pathDump = $this->getPath_arquivo_dump();
            if (file_exists($pathDump))
            {
                unlink($pathDump);
                $this->setPath_arquivo_dump(null);
                $this->formatarParaSQL();
                $this->update($this->getId());
            }
        }

        public function cadastrarHospedagemDbDoSistema($objSistema, $idHospedagemDest, $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }

            $objHospedagemDb = new EXTDAO_Hospedagem_db($db);
            $objHospedagemDb->setHospedagem_id_INT($idHospedagemDest);
            $objHospedagemDb->setHost_db($this->getAssinatura_host_db());
            $nomeDbDest = $this->getIdentificador() . "_" . $idHospedagemDest;
            $objHospedagemDb->setNome_db($nomeDbDest);
            $objHospedagemDb->setPorta_db_INT($this->getAssinatura_porta_db_INT());
            $objHospedagemDb->setUsuario_db($this->getAssinatura_usuario_db());
            $objHospedagemDb->setSenha_db($this->getAssinatura_senha_db());
            $objHospedagemDb->setTipo_hospedagem_db_id_INT($this->getTipo_hospedagem_db_id_INT());
            $objHospedagemDb->formatarParaSQL();
            $objHospedagemDb->insert();

            $idNovaHospedagemDb = $objHospedagemDb->getUltimoIdInserido();
            $objHospedagemDb->select($idNovaHospedagemDb);

            $database = new Database();
            if (!$database->criaBancoDeDados($nomeDbDest))
            {
                throw new Exception("Falha ao criar o banco de dados $nomeDbDest durante a criação do sistema_db.");
            }


            $pathRaizDump = Helper::getPathComBarra($objSistema->getPath_raiz_dump());



            $nomArquivoDump = $this->getPath_arquivo_dump();
//            echo "DUMP: $nomArquivoDump";
//            echo "DUMP: $pathRaizDump . $nomArquivoDump";
            if (!strlen($nomArquivoDump) || !file_exists($pathRaizDump . $nomArquivoDump))
            {
                $databaseSrc = $this->getObjDatabase();
                $nomArquivoDump = $this->getIdentificador() . ".sql";

                $databaseSrc->dumpBancoDeDados($pathRaizDump . $nomArquivoDump);
                $objSrc = new EXTDAO_Sistema_db();
                $objSrc->select($this->getId());
                $objSrc->setPath_arquivo_dump($nomArquivoDump);
                $objSrc->formatarParaSQL();
                $objSrc->update($this->getId());

                $this->setPath_arquivo_dump($nomArquivoDump);
            }

            $nomArquivoDump = $pathRaizDump . $this->getPath_arquivo_dump();
            $databaseTgt = $objHospedagemDb->getObjDatabase();
            if (!$databaseTgt->executaScriptNoBancoDeDados($nomArquivoDump))
            {
                throw new Exception("Falha ao executar o script do arquivo $nomArquivoDump no banco de dados " . $databaseTgt->getDBName());
            }
        }

        public function getObjDatabase()
        {
            $db = new Database(
                $this->getIdentificador(),
                $this->getAssinatura_host_db(),
                $this->getAssinatura_porta_db_INT(),
                $this->getAssinatura_usuario_db());

            return $db;
        }
    }

    

<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Sistema_tipo
    * NOME DA CLASSE DAO: DAO_Sistema_tipo
    * DATA DE GERAÇÃO:    09.09.2014
    * ARQUIVO:            EXTDAO_Sistema_tipo.php
    * TABELA MYSQL:       sistema_tipo
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Sistema_tipo extends DAO_Sistema_tipo
    {
        const COPIA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA = 1;
        const COMPARTILHA_ARQUIVOS_E_COPIA_BANCO_DE_DADOS_DO_SIS = 2;
        const COMPARTILHA_ARQUIVOS_E_BANCO_DE_DADOS_DO_SISTEMA = 3;

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Sistema_tipo";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
        }

        public function setDiretorios()
        {
        }

        public function setDimensoesImagens()
        {
        }

        public static function factory()
        {
            return new EXTDAO_Sistema_tipo();
        }

    }

    

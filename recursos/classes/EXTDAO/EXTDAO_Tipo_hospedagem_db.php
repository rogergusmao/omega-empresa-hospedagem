<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Tipo_hospedagem_db
    * NOME DA CLASSE DAO: DAO_Tipo_hospedagem_db
    * DATA DE GERAÇÃO:    19.04.2014
    * ARQUIVO:            EXTDAO_Tipo_hospedagem_db.php
    * TABELA MYSQL:       tipo_hospedagem_db
    * BANCO DE DADOS:     hospedagem
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Tipo_hospedagem_db extends DAO_Tipo_hospedagem_db
    {

        const PRINCIPAL = 1;
        const PARAMETROS = 2;
        const SINCRONIZADOR = 3;

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Tipo_hospedagem_db";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_excluido_BOOLEAN = "";
            $this->label_excluido_DATETIME = "";
        }

        public static function factory()
        {
            return new EXTDAO_Tipo_hospedagem_db();
        }

    }

    

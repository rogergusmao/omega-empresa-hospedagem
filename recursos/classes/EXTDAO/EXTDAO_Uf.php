<?php //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     EXTDAO_Uf
    * NOME DA CLASSE DAO: DAO_Uf
    * DATA DE GERAÇÃO:    23.10.2009
    * ARQUIVO:            EXTDAO_Uf.php
    * TABELA MYSQL:       uf
    * BANCO DE DADOS:     engenharia
    * -------------------------------------------------------
    * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
    *
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARAÇÃO DA CLASSE
    // **********************

    class EXTDAO_Uf extends DAO_Uf
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Uf";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_sigla = I18N::getExpression("Sigla");
            $this->label_dataCadastro = I18N::getExpression("Data de Cadastro");
            $this->label_dataEdicao = I18N::getExpression("Data de Edição");
        }

        public function setDiretorios()
        {
        }

        public function setDimensoesImagens()
        {
        }

        public static function factory()
        {
            return new EXTDAO_Uf();
        }

    }
    
    

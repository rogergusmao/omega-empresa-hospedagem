<?php

//@@NAO_MODIFICAR

    /*
     *
     * -------------------------------------------------------
     * NOME DA CLASSE:     EXTDAO_Usuario
     * NOME DA CLASSE DAO: DAO_Usuario
     * DATA DE GERAÇÃO:    17.10.2009
     * ARQUIVO:            EXTDAO_Usuario.php
     * TABELA MYSQL:       usuario
     * BANCO DE DADOS:     engenharia
     * -------------------------------------------------------
     * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
     * GERADOR DE CLASSES DO EDUARDO
     * -------------------------------------------------------
     *
     */

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

    class EXTDAO_Usuario extends DAO_Usuario
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Usuario";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_email = I18N::getExpression("Email");
            $this->label_senha = I18N::getExpression("Senha");
            $this->label_usuario_tipo_id_INT = I18N::getExpression("Tipo de Usuário");
            $this->label_status_BOOLEAN = I18N::getExpression("Status");
            $this->label_profissional_id_INT = I18N::getExpression("Profissional Associado");
            $this->label_pagina_inicial = I18N::getExpression("Pagina Inicial");
        }

        public function lembrarSenha()
        {
            $emailCadastrado = Helper::GET("email");

            $this->database->query("SELECT id, nome, email, senha
                                FROM usuario
                                WHERE email='{$emailCadastrado}'"
                                   . " AND NOT excluido_BOOLEAN IS NULL");

            if ($this->database->rows() == 1)
            {
                $nomeUsuario = $this->database->getPrimeiraTuplaDoResultSet(1);
                $emailDestino = $this->database->getPrimeiraTuplaDoResultSet(2);
                $senha = $this->databse->getPrimeiraTuplaDoResultSet(3);

                $objCrypt = new Crypt();
                $senhaDescriptografada = $objCrypt->decrypt($senha);

                $mensagem = "--- Lembrete de senha de acesso ao " . TITULO_PAGINAS . " ---\n
        					 Endereço para Acesso: " . ENDERECO_DE_ACESSO . "
        					 Usuário: {$emailDestino}
        					 Senha  : {$senhaDescriptografada}";

                $objEmail = new Email_Sender(EMAIL_PADRAO_REMETENTE_MENSAGENS, $emailDestino);
                $objEmail->setAssunto(ASSUNTO_DO_EMAIL_DE_REENVIO_SENHA);
                $objEmail->setConteudo($mensagem, true);
                $objEmail->enviarEmail();

                Helper::imprimirCabecalhoParaFormatarAction();
                Helper::imprimirMensagem(MENSAGEM_EMAIL_REENVIO_SENHA_SUCESSO, MENSAGEM_OK);
                Helper::imprimirComandoJavascriptComTimer(COMANDO_FECHAR_DIALOG, TEMPO_PADRAO_FECHAR_DIALOG, false);

                return;
            }
            elseif ($this->database->rows() == 0)
            {
                Helper::imprimirCabecalhoParaFormatarAction();
                Helper::imprimirMensagem(MENSAGEM_EMAIL_REENVIO_SENHA_ERRO, MENSAGEM_ERRO);
                Helper::imprimirComandoJavascriptComTimer(COMANDO_FECHAR_DIALOG, TEMPO_PADRAO_FECHAR_DIALOG, false);
                exit();
            }
        }

        public function alterarSenha()
        {
            $mensagemSucesso = "Sua senha foi alterada com sucesso.";
            $mensagemErro = "A senha digitada não confere com a confirmacao digitada";

            $objCriptografia = new Crypt();

            $senha = Helper::POST("senha1");
            $confirmacao = Helper::POST("txtSenhaNovaConfirm");

            if ($senha == $confirmacao)
            {
                $this->setSenha($objCriptografia->crypt($senha));
                $this->update($_SESSION["usuario_id"], $_POST);

                Helper::imprimirMensagem($mensagemSucesso, MENSAGEM_OK);
            }
            else
            {
                Helper::imprimirMensagem($mensagemErro, MENSAGEM_ERRO);
            }
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("O usuário foi cadastrado com sucesso.");

            $objCriptografia = new Crypt();
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);

                //senha criptgrafada
                $this->setSenha($objCriptografia->crypt($this->getSenha()));

                $this->formatarParaSQL();

                $this->insert();
                $idDoUsuario = $this->getIdDoUltimoRegistroInserido();

                $this->gravarPermissoesDoMenu($idDoUsuario);
                $this->gravarPermissoesDeAcesso($idDoUsuario);
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("O usuário foi modificado com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());
                $this->gravarPermissoesDoMenu($this->getId());
                $this->gravarPermissoesDeAcesso($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function gravarPermissoesDeAcesso($idDoUsuario = false)
        {
            if (!$idDoUsuario)
            {
                $idDoUsuario = $this->getId();
            }

            $this->database->query("DELETE FROM usuario_privilegio WHERE usuario_id_INT={$idDoUsuario} "
                                   . " AND excluido_DATETIME IS NULL");

            $arrayDePermissoes = Helper::POST("funcionalidades");

            foreach ($arrayDePermissoes as $valor)
            {
                $this->database->query("INSERT INTO usuario_privilegio(usuario_id_INT, identificador_funcionalidade) VALUES($idDoUsuario, '$valor')");
            }
        }

        public function gravarPermissoesDoMenu($idDoUsuario = false)
        {
            if (!$idDoUsuario)
            {
                $idDoUsuario = $this->getId();
            }

            $this->database->query("DELETE FROM usuario_menu WHERE usuario_id_INT={$idDoUsuario}");

            $arrayDePermissoes = Helper::POST("areas_menu");

            foreach ($arrayDePermissoes as $valor)
            {
                $this->database->query("INSERT INTO usuario_menu(usuario_id_INT, area_menu) VALUES($idDoUsuario, '$valor')");
            }

            if ($idDoUsuario == Seguranca::getIdDoUsuarioLogado())
            {
                $objMenu = new Menu();
                $_SESSION["usuario_menu"] = $objMenu->getArrayDoMenuDoUsuarioLogado();
            }
        }

        public static function factory()
        {
            return new EXTDAO_Usuario();
        }

        public static function getSenhaUsuarioDoEmail($email, $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query("SELECT senha"
                       . " FROM usuario"
                       . " WHERE email = '$email'");

            return $db->getPrimeiraTuplaDoResultSet(0);
        }

        public static function getIdUsuarioDoEmail($email, $db = null)
        {
            if ($db == null)
            {
                $db = new Database();
            }
            $db->query("SELECT id "
                       . " FROM usuario "
                       . " WHERE email = '$email'");

            return $db->getPrimeiraTuplaDoResultSet(0);
        }
    }

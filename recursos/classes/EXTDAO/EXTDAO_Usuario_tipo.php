<?php

//@@NAO_MODIFICAR

    /*
     *
     * -------------------------------------------------------
     * NOME DA CLASSE:     EXTDAO_Usuario_tipo
     * NOME DA CLASSE DAO: DAO_Usuario_tipo
     * DATA DE GERAÇÃO:    23.10.2009
     * ARQUIVO:            EXTDAO_Usuario_tipo.php
     * TABELA MYSQL:       usuario_tipo
     * BANCO DE DADOS:     engenharia
     * -------------------------------------------------------
     * DESENVOLVIDO POR: EDUARDO C. DE O. ALVES
     *
     * -------------------------------------------------------
     *
     */

// **********************
// DECLARAÇÃO DA CLASSE
// **********************

    class EXTDAO_Usuario_tipo extends DAO_Usuario_tipo
    {

        public function __construct($configDAO = null)
        {
            parent::__construct($configDAO);

            $this->nomeClasse = "EXTDAO_Usuario_tipo";
        }

        public function setLabels()
        {
            $this->label_id = I18N::getExpression("Id");
            $this->label_nome = I18N::getExpression("Nome");
            $this->label_nome_visivel = I18N::getExpression("Nome Visivel");
            $this->label_status_BOOLEAN = I18N::getExpression("Status");
            $this->label_pagina_inicial = I18N::getExpression("Pagina Inicial");
        }

        public function __actionAdd()
        {
            $mensagemSucesso = I18N::getExpression("A classe de usuário foi cadastrada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();
                $this->insert();

                $idDoTipoDeUsuario = $this->getIdDoUltimoRegistroInserido();
                $this->gravarPermissoesDoMenu($idDoTipoDeUsuario);
                $this->gravarPermissoesDeAcesso($idDoTipoDeUsuario);
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function __actionEdit()
        {
            $mensagemSucesso = I18N::getExpression("A classe de usuário foi modificada com sucesso.");
            $numeroRegistros = Helper::POST("numeroRegs");

            $urlSuccess = Helper::getUrlAction(Helper::POST("next_action"), Helper::POST("id"));
            $urlErro = Helper::getUrlAction(Helper::POST("origin_action"), Helper::POST("id"));

            for ($i = 1; $i <= $numeroRegistros; $i++)
            {
                $this->setByPost($i);
                $this->formatarParaSQL();

                $this->update($this->getId(), $_POST, $i);

                $this->select($this->getId());

                $this->gravarPermissoesDoMenu($this->getId());
                $this->gravarPermissoesDeAcesso($this->getId());
            }

            return array("location: $urlSuccess&msgSucesso=$mensagemSucesso");
        }

        public function gravarPermissoesDeAcesso($idDoTipoDeUsuario = false)
        {
            if (!$idDoTipoDeUsuario)
            {
                $idDoTipoDeUsuario = $this->getId();
            }

            $this->database->query("DELETE FROM usuario_tipo_privilegio WHERE usuario_tipo_id_INT={$idDoTipoDeUsuario}");
            $arrayDePermissoes = Helper::POST("funcionalidades");

            foreach ($arrayDePermissoes as $valor)
            {
                $this->database->query("INSERT INTO usuario_tipo_privilegio(usuario_tipo_id_INT, identificador_funcionalidade) VALUES($idDoTipoDeUsuario, '$valor')");
            }

        }

        public function gravarPermissoesDoMenu($idDoTipoDeUsuario = false)
        {
            if (!$idDoTipoDeUsuario)
            {
                $idDoTipoDeUsuario = $this->getId();
            }

            $this->database->query("DELETE FROM usuario_tipo_menu WHERE usuario_tipo_id_INT={$idDoTipoDeUsuario}");

            $arrayDePermissoes = Helper::POST("areas_menu");

            foreach ($arrayDePermissoes as $valor)
            {
                $this->database->query("INSERT INTO usuario_tipo_menu(usuario_tipo_id_INT, area_menu) VALUES($idDoTipoDeUsuario, '$valor')");
            }

            if ($idDoTipoDeUsuario == Seguranca::getIdDoTipoDoUsuarioLogado())
            {
                $objMenu = new Menu();
                $_SESSION["usuario_menu"] = $objMenu->getArrayDoMenuDoUsuarioLogado();
            }
        }

        public function montarTabelaDePermissoes()
        {
            $strRetorno = "<table class=\"tabela_form\">";

            $valor = new Seguranca_Funcionalidade();
            $objSeguranca = new Seguranca();
            $objSeguranca->montarListaDeFuncionalidades();

            $listaDeFuncionalidades = $objSeguranca->getListaDeFuncionalidades();

            $entidadeAnterior = "";

            foreach ($listaDeFuncionalidades as $chave => $valor)
            {
                $entidade = $valor->entidade;
                $funcionalidade = $valor->nomeFuncionalidade;
                $somenteSuaPropriaArea = $valor->somenteSuaPropriaArea;

                $classe = $valor->classe;
                $metodo = $valor->metodo;

                if ($somenteSuaPropriaArea)
                {
                    $msg = I18N::getExpression("somente na área de abrangência do usuário");
                    $strSomenteSuaPropriaArea = " ({$msg})";
                }
                else
                {
                    $strSomenteSuaPropriaArea = "";
                }

                if ($entidade != $entidadeAnterior)
                {
                    $strRetorno .=
                        "<tr>
        					<td colspan=\"2\">&nbsp;</td>
        				 </tr>
        				 <tr>
        					<td colspan=\"2\">{$entidade}</td>
        				 </tr>";

                    $strRetorno .=
                        "<tr>
        					<td><input type=\"checkbox\" name=\"{$classe}_{$metodo}\" value=\"1\"/></td>
        					<td>{$funcionalidade} {$strSomenteSuaPropriaArea}</td>
        				 </tr>";

                    $entidadeAnterior = $entidade;
                }
            }

            $strRetorno .= "</table>";

            return $strRetorno;
        }

        public static function factory()
        {
            return new EXTDAO_Usuario_tipo();
        }

    }


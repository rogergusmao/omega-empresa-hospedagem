<?php

class BkpGeneric_DAO {

    private $niveisRaiz;
    public $strToRaiz;
    public $nomeClasse;
    public $nomeTabela;
    public $campoId;
    public $campoLabel;
    public $database;
    public $camposObrigatorios;
    public $labels;
    public $campos;
    public $tipos;

    public function __construct($niveisRaiz="2") {

        $this->database = new Database();

        $this->niveisRaiz = $niveisRaiz;
        $this->camposObrigatorios = array();
        $this->labels = array();
        $this->campos = array();
        $this->tipos = array();

        $this->setStrToRaiz();
    }
public function getUltimoIdInserido(){
        return $this->database->lastInsertedId;
    }
    public function setStrToRaiz() {

        $this->strToRaiz = "";

        for ($i = 0; $i < $this->niveisRaiz; $i++) {

            $this->strToRaiz .= "../";
        }
    }

    //********************************************************************************
    //M�todo que adiciona informa��es dos campos na lista
    //********************************************************************************
    public function addInfoCampos($nome, $label, $tipo, $obrigatorio) {

        $this->camposObrigatorios[] = $obrigatorio ? "true" : "false";
        $this->campos[] = $nome;
        $this->labels[] = $label;
        $this->tipos[] = $tipo;
    }

    protected function imprimirCalendarios() {

        $strRetorno = "";

        return $strRetorno;
    }

    public function getCabecalhoFormulario($action, $onSubmit="validarCampos(this)", $metodo="POST", $idFormulario=false) {

        if (Helper::GET("visualizacao") != "true") {
            
            if($idFormulario === false){
                
                $idFormulario = $this->nomeTabela;
                
            }

            $strRetorno = "<form name=\"{$this->nomeTabela}\" id=\"{$idFormulario}\" action=\"{$action}\" method=\"{$metodo}\" enctype=\"multipart/form-data\" onReset=\"return confirmarReset('O formul�rio ser� limpado, deseja prosseguir?');\" onSubmit=\"return {$onSubmit};\" >";

            return $strRetorno;
        }
    }

    public function getCabecalhoFiltro($action, $onSubmit="validarCampos(this)") {

        $strRetorno = "<form name=\"{$this->nomeTabela}\" id=\"{$this->nomeTabela}\" action=\"{$action}\" method=\"GET\" onreset=\"return confirmarReset('O formul�rio ser� limpado, deseja prosseguir?');\" onsubmit=\"return {$onSubmit};\">";

        return $strRetorno;
    }

    public function getRodapeFormulario() {

        if (Helper::GET("visualizacao") != "true") {

            $strRetorno = "</form>";
        }

        $strRetorno .= $this->imprimirCalendarios();

        return $strRetorno;
    }

    public function getInformacoesDeValidacaoDosCampos() {

        $strRetorno .= "<script language=\"javascript\">\n\n";

        for ($i = 0; $i < count($this->campos); $i++) {

            $campos = $this->campos[$i];
            $strCampos .= "\"{$campos}\",";

            $labels = $this->labels[$i];
            $strLabels .= "\"{$labels}\",";

            $tipos = $this->tipos[$i];
            $strTipos .= "\"{$tipos}\",";

            $required = $this->camposObrigatorios[$i];
            $strRequired .= "{$required},";
        }

        if (strlen($strRequired) > 0) {

            $strRetorno .= "\tvar obrigatorios = new Array(" . substr($strRequired, 0, strlen($strRequired) - 1) . ");\n";
            $strRetorno .= "\tvar campos = new Array(" . substr($strCampos, 0, strlen($strCampos) - 1) . ");\n";
            $strRetorno .= "\tvar labels = new Array(" . substr($strLabels, 0, strlen($strLabels) - 1) . ");\n";
            $strRetorno .= "\tvar tipos = new Array(" . substr($strTipos, 0, strlen($strTipos) - 1) . ");\n";
        }

        $strRetorno .= "\n</script>\n";

        return $strRetorno;
    }

    //********************************************************************************
    //M�todo que implementa a l�gica de renderiza��o de <select> com relacionamento
    //********************************************************************************

    public function getOptionsRelacionamento($tabelasFiltro, $strFiltro, $idFiltro, $tabelaPrincipal, $campoId, $campoLabel, $idTabelaPrincipal = false) {

        if (!$strFiltro) {

            echo "<option value=''></option>";
        } else {

            if (!$idTabelaPrincipal || $idTabelaPrincipal == "")
                echo "<option value=''></option>";
            else
                $select = true;

            $this->database->query("SELECT tp.$campoId, tp.$campoLabel FROM $tabelaPrincipal tp, $tabelasFiltro WHERE $strFiltro");

            if ($this->database->rows() < 1) {

                echo "<option value=''>Nenhum aluno cadastrado nesta disciplina</option>";
            }

            while ($dados = $this->database->fetchArray()) {
                ?>

                <option value="<?= $dados[0] ?>" <?= ($select && $dados[0] == $idTabelaPrincipal) ? "selected" : "" ?>><?= $dados[1] ?></option>

                <?
            }
        }
    }

    //********************************************************************************
    //M�todo que implementa a l�gica de renderiza��o de <options>
    //********************************************************************************
    public function getOptions($valorSelecionado=false, $strFiltro="", $valorReplaceId=false, $primeiroEmBranco=true) {

        $strRetorno = "";

        if (strlen($strFiltro) > 0) {

            $strFiltro = "AND " . $strFiltro;
        }

        if ($primeiroEmBranco)
            $strRetorno .= "\t\t\t\t<option value=\"\"></option>\n";

        if ($valorSelecionado)
            $select = true;

        $query = "SELECT tp.{$this->campoId}, tp.{$this->campoLabel} FROM {$this->nomeTabela} tp WHERE excluido_BOOLEAN=0 {$strFiltro} ORDER BY tp.{$this->campoLabel}";

        $this->database->query($query);

        if ($this->database->rows() < 1) {

            $strRetorno .= "\t\t\t\t<option value=\"\">Nenhuma op��o encontrada</option>\n";
        }

        while ($dados = $this->database->fetchArray()) {

            if (!$valorReplaceId) {

                $strRetorno .= "\t\t\t\t<option value=\"{$dados[0]}\" " . (($select && $dados[0] == $valorSelecionado) ? "selected" : "") . ">{$dados[1]}</option>\n";
            } else {

                $strRetorno .= "\t\t\t\t<option value=\"{$dados[1]}\" " . (($select && $dados[1] == $valorSelecionado) ? "selected" : "") . ">{$dados[1]}</option>\n";
            }
        }

        return $strRetorno;
    }

    public function getOptionsArrayValores($valorSelecionado=false, $array, $imprimirPrimeiroEmBranco=true) {

        if ($imprimirPrimeiroEmBranco)
            $strRetorno .= "\t\t\t\t<option value=\"\"></option>\n";

        if ($valorSelecionado !== false)
            $select = true;

        foreach ($array as $chave => $valor) {

            if (is_array($valor)) {

                $style = "style=\"{$valor[0]}\"";
                $texto = $valor[1];
            } else {

                if (substr_count($style, "bold") || substr_count($texto, "&nbsp;")) {

                    $texto = "&nbsp;&nbsp;&nbsp;";
                } else {

                    $texto = "";
                }

                $style = "";
                $texto .= $valor;
            }

            $strRetorno .= "\t\t\t\t<option {$style} value=\"{$chave}\" " . (($select && $chave == $valorSelecionado) ? "selected" : "") . ">{$texto}</option>\n";
        }

        return $strRetorno;
    }

    //********************************************************************************
    //M�todo que implementa a l�gica de renderiza��o de <select>
    //********************************************************************************
    public function getComboBox($objArgumento) {

        $nomeComboBox = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $idComboBox = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $valorSelecionado = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $strFiltro = $objArgumento->strFiltro;
        $valueReplaceId = $objArgumento->valueReplaceId;
        $onChange = $objArgumento->onChange;
        $primeiroEmBranco = $objArgumento->primeiroEmBranco;
        $funcaoGetOptions = $objArgumento->funcaoGetOptions;

        if ($onChange)
            $strOnChange = "onchange=\"$onChange\"";

        if ($obrigatorio)
            $strObrigatorio = "true";
        else
            $strObrigatorio = "false";

        $this->addInfoCampos($idComboBox, $label, "TEXTO", $obrigatorio);

        $strFocus = "onfocus=\"$(this).addClass('$classeCssFocus')\" onblur=\"$(this).removeClass('$classeCssFocus')\"";

        $strRetorno = "<span id=\"s_{$idComboBox}\">";

        $strRetorno .= "<select name=\"$nomeComboBox\" id=\"$idComboBox\" class=\"$classeCss\" $strFocus $strOnChange style=\"width: {$largura}px;\">\n";

        if (!strlen($funcaoGetOptions)) {

            $funcaoGetOptions = "getOptions";
        }

        $strRetorno .= call_user_func_array(array($this, "{$funcaoGetOptions}"), array($valorSelecionado, $strFiltro, $valueReplaceId, $primeiroEmBranco));

        $strRetorno .= "\t\t\t\t</select>\n";

        $strRetorno .= "<span class=\"selectInvalidMsg\">Valor inv�lido</span>
                        <span class=\"selectRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";

        $strRetorno .= Helper::getComandoJavascript("var select_{$idComboBox} = new Spry.Widget.ValidationSelect(\"s_{$idComboBox}\", {validateOn:[\"blur\"], isRequired:{$strObrigatorio}});");

        return $strRetorno;
    }

    public function getListaDeOptionsVazia($valorSelecionado, $strFiltro, $valueReplaceId, $primeiroEmBranco) {

        return "";
    }

    public function getRadioList($objArgumento) {

        $nomeComboBox = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $idComboBox = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $valorSelecionado = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $strFiltro = $objArgumento->strFiltro;
        $valueReplaceId = $objArgumento->valueReplaceId;
        $onChange = $objArgumento->onChange;
        $orderBy = $objArgumento->orderBy;

        if (!$orderBy)
            $orderBy = $this->campoLabel;

        if ($onChange)
            $strOnChange = "onchange=\"$onChange\"";

        $this->addInfoCampos($idComboBox, $label, "TEXTO", $obrigatorio);

        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        if (strlen($strFiltro) > 0) {

            $strFiltro = "WHERE " . $strFiltro;
        }

        if (strlen($valorSelecionado))
            $select = true;

        $query = "SELECT tp.{$this->campoId}, tp.{$this->campoLabel} FROM {$this->nomeTabela} tp $strFiltro ORDER BY tp.{$orderBy}";

        $this->database->query($query);

        for ($j = 0; $dados = $this->database->fetchArray(); $j++) {

            if (!$valueReplaceId) {

                $valorReal = $dados[0];
            } else {

                $valorReal = $dados[1];
            }

            $strRetorno .= "\t\t\t<input type=\"radio\" name=\"$nomeComboBox\" id=\"{$idComboBox}_{$j}\" value=\"{$valorReal}\" " . (($select && $valorReal == $valorSelecionado) ? "checked" : "") . " />{$dados[1]}\n&nbsp;&nbsp;";
        }

        return $strRetorno;
    }

    public function getComboBoxArrayValores($objArgumento, $valores) {

        $nomeComboBox = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $idComboBox = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $valorSelecionado = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $onChange = $objArgumento->onChange;
        $primeiroEmBranco = $objArgumento->primeiroEmBranco;

        if ($onChange)
            $strOnChange = "onchange=\"$onChange\"";
        
        if ($obrigatorio)
            $strObrigatorio = "true";
        else
            $strObrigatorio = "false";

        $this->addInfoCampos($idComboBox, $label, "TEXTO", $obrigatorio);

        $strFocus = "onfocus=\"$(this).addClass('$classeCssFocus')\" onblur=\"$(this).removeClass('$classeCssFocus')\"";

        $strRetorno = "<span id=\"s_{$idComboBox}\">";
        
        $strRetorno .= "<select name=\"$nomeComboBox\" id=\"$idComboBox\" class=\"$classeCss\" $strFocus $strOnChange style=\"width: {$largura}px;\">\n";

        $strRetorno .= $this->getOptionsArrayValores($valorSelecionado, $valores, $primeiroEmBranco);

        $strRetorno .= "\t\t\t\t</select>\n";
        
        $strRetorno .= "<span class=\"selectInvalidMsg\">Valor inv�lido</span>
                        <span class=\"selectRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";
        
        $strRetorno .= Helper::getComandoJavascript("var select_{$idComboBox} = new Spry.Widget.ValidationSelect(\"s_{$idComboBox}\", {validateOn:[\"blur\"], isRequired:{$strObrigatorio}});");

        return $strRetorno;
    }

    public function getCheckbox($objArgumento) {

        $nomeComboBox = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $idComboBox = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $isChecked = $objArgumento->isChecked;

        if ($isChecked) {

            $strComplemento = "checked=\"checked\"";
        } else {

            $strComplemento = "";
        }

        $onChange = $objArgumento->onChange;

        if ($onChange)
            $strOnChange = "onchange=\"$onChange\"";

        $strRetorno = "<input type=\"checkbox\" value=\"$valor\" name=\"$nomeComboBox\" id=\"$idComboBox\" $strFocus $strOnChange $strComplemento>\n";

        $strRetorno .= "\t\t\t\t</input>\n";

        return $strRetorno;
    }

    //*********************************************
    //M�todos wrapper do argumento objeto
    //*********************************************
    public function campoCNPJ($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;

        $this->addInfoCampos($objArgumento->id, $label, "CNPJ", $obrigatorio);

        $formato = "999.999.999/9999-99";
        $length = strlen($formato);
        $strMascara = "onkeypress=\"return mascara(this, '$formato', event);\" maxlength=\"$length\"";



        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno = "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" $strMascara $strFocus $strDisabled $strReadOnly/>";

        return $strRetorno;
    }

    public function campoCPF($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;

        $this->addInfoCampos($objArgumento->id, $label, "CPF", $obrigatorio);

        $formato = "999.999.999-99";
        $length = strlen($formato);
        $strMascara = "onkeypress=\"return mascara(this, '$formato', event);\" maxlength=\"$length\"";



        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno = "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" $strMascara $strFocus $strDisabled $strReadOnly/>";

        return $strRetorno;
    }

    public function campoTelefone($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;

        $this->addInfoCampos($objArgumento->id, $label, "TELEFONE", $obrigatorio);

        $formato = "(00) 0000-0000";
        $length = strlen($formato);

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }

        $strFocus = "this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'";

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" onfocus=\"$strFocus\" onblur=\"$strBlur\" $strDisabled $strReadOnly />";
        $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">Telefone inv�lido</span>
        				<span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";

        $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"phone_number\", {validateOn:[\"blur\"], useCharacterMasking:true, isRequired:{$strObrigatorio}, format:\"phone_custom\", pattern:\"{$formato}\"});");

        return $strRetorno;
    }

    public function campoEmail($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;

        $this->addInfoCampos($objArgumento->id, $label, "EMAIL", $obrigatorio);

        $strFocus = "this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'";

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }


        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        if ($maxLength) {

            $strMaxLength = ", maxChars={$maxLength}";
        }

        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" onfocus=\"$strFocus\" onblur=\"$strBlur\" $strMascara $strDisabled $strReadOnly />";
        $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">Endere�o de Email inv�lido</span>
        				<span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";

        $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"email\", {validateOn:[\"blur\"], isRequired:{$strObrigatorio} {$strMaxLength}});");

        return $strRetorno;
    }

    public function campoTexto($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $textArea = $objArgumento->textArea;
        $upperCase = $objArgumento->upperCase;
        $onFocus = $objArgumento->onFocus;
        $onBlur = $objArgumento->onBlur;
        $style = $objArgumento->style;
        $maxLength = $objArgumento->maxLength;
        $minLength = $objArgumento->minLength;
        $campoSenha = $objArgumento->campoSenha;

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }

        if (strlen($onFocus)) {

            $onFocus .= ";";
        }

        if (strlen($onBlur)) {

            $onBlur .= ";";
        }
                
        if($campoSenha === true){
            
            $tipoCampo = "password";
            
        }
        else{
            
            $tipoCampo = "text";
            
        }

        $this->addInfoCampos($objArgumento->id, $label, "TEXTO", $obrigatorio);

        $strFocus = "{$onFocus}this.className='{$classeCssFocus}'";
        $strBlur = "{$onBlur}this.className='{$classeCss}'";

        if ($textArea) {

            $inicioTag = "<textarea";
            $finalTag = ">$valor</textarea>";
        } else {

            $inicioTag = "<input type=\"{$tipoCampo}\"";
            $finalTag = "/>";
        }

        if ($upperCase) {

            $strUpperCase = "onkeypress=\"uppercase();\"";
        }

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        if (is_numeric($maxLength)){

            $strMaxLength = ", maxChars:{$maxLength}";
            
        }
        
        if (is_numeric($minLength)){

            $strMinLength = ", minChars:{$minLength}";
            
        }

        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "{$inicioTag} name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px; {$style}\" onfocus=\"$strFocus\" onblur=\"$strBlur\" $strDisabled $strReadOnly {$finalTag}";
        
        if(!$objArgumento->textArea){
        
            $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">Valor inv�lido</span>
                            <span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";

        }
        else{
            
            $strRetorno .= "<span class=\"textareaRequiredMsg\">Preenchimento obrigat�rio</span>";

        }
        
        if(is_numeric($maxLength)){
            
            if(!$objArgumento->textArea){
            
                $strRetorno .= "<span class=\"textfieldMaxCharsMsg\">N�mero m�ximo de caracteres: {$maxLength}</span>";
            
            }
            else{
                
                $strRetorno .= "<span class=\"textareaMaxCharsMsg\">N�mero m�ximo de caracteres: {$maxLength}</span>";
                            
            }
            
        }
        
        if(is_numeric($minLength)){
            
            if(!$objArgumento->textArea){
            
                $strRetorno .= "<span class=\"textfieldMinCharsMsg\">N�mero m�nimo de caracteres: {$minLength}</span>";
                
            }
            else{
                
                $strRetorno .= "<span class=\"textareaMinCharsMsg\">N�mero m�nimo de caracteres: {$minLength}</span>";
                          
            }            
                    
        }
        
        $strRetorno .= "</span>";

        if(!$objArgumento->textArea){
        
            $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"none\", {validateOn:[\"blur\"], isRequired:{$strObrigatorio} {$strMaxLength} {$strMinLength}});");

        }
        else{
            
            $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextarea(\"tf_{$id}\", {validateOn:[\"blur\"], isRequired:{$strObrigatorio} {$strMaxLength} {$strMinLength}});");
            
        }
        
        return $strRetorno;
        
    }

    public function campoMoeda($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $onFocus = $objArgumento->onFocus;
        $onBlur = $objArgumento->onBlur;
        $valorMinimo = $objArgumento->valorMinimo;
        $valorMaximo = $objArgumento->valorMaximo;
        $aceitarValorNegativo = $objArgumento->aceitarValorNegativo;
        $prefixoMoeda = $objArgumento->prefixoMoeda;
        $casasDecimais = $objArgumento->casasDecimais;

        if ($prefixoMoeda) {

            $prefixoMoeda = trim($prefixoMoeda) . " ";
        }

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }

        if ($aceitarValorNegativo) {

            $strAceitarValorNeg = "true";
        } else {

            $strAceitarValorNeg = "false";
        }

        if ($valorMinimo !== false) {

            $strValorMinimo = "minValue:{$valorMinimo},";
        }

        if ($valorMaximo !== false) {

            $strValorMaximo = "maxValue:{$valorMaximo},";
        }

        if (strlen($onFocus)) {

            $onFocus .= ";";
        }

        if (strlen($onBlur)) {

            $onBlur .= ";";
        }

        $this->addInfoCampos($objArgumento->id, $label, "MOEDA", $obrigatorio);

        $strFocus = "onfocus=\"{$onFocus}this.className='{$classeCssFocus}'\" onblur=\"{$onBlur}this.className='{$classeCss}'\"";

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }
        
        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" {$strFocus} onblur=\"$onBlur\" $strDisabled $strReadOnly />";

        $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">Formato do n�mero decimal inv�lido</span>
        				<span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";
        
        $strRetorno .= Helper::getComandoJavascript("\$(function(){\$('#{$id}').maskMoney({symbol: '{$prefixoMoeda}', showSymbol: true, decimal: ',', thousands: '.', defaultZero: false, precision: {$casasDecimais}, allowZero:true});
    														   \$('#{$id}').css('text-align', 'right');});");

        $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"none\", {validateOn:[\"blur\"], isRequired:{$strObrigatorio} {$strValorMinimo} {$strValorMaximo}});");
        
        return $strRetorno;
    }

    public function campoInteiro($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $onFocus = $objArgumento->onFocus;
        $onBlur = $objArgumento->onBlur;
        $valorMinimo = $objArgumento->valorMinimo;
        $valorMaximo = $objArgumento->valorMaximo;
        $aceitarValorNegativo = $objArgumento->aceitarValorNegativo;
        $maxLength = $objArgumento->maxLength;

        if (!$obrigatorio) {

            $strObrigatorio = "false";
            
        } else {

            $strObrigatorio = "true";
            
        }


        if ($aceitarValorNegativo) {

            $strAceitarValorNeg = "true";
        } else {

            $strAceitarValorNeg = "false";
        }

        if ($valorMinimo !== false) {

            $strValorMinimo = ", minValue:{$valorMinimo}";
        }

        if ($valorMaximo !== false) {

            $strValorMaximo = ", maxValue:{$valorMaximo}";
        }

        if (strlen($onFocus)) {

            $onFocus .= ";";
        }

        if (strlen($onBlur)) {

            $onBlur .= ";";
        }
        
        if($maxLength){
            
            $strMaxLength = "maxlength=\"{$maxLength}\"";
            
        }

        $this->addInfoCampos($objArgumento->id, $label, "INTEIRO", $obrigatorio);

        $strFocus = "onfocus=\"{$onFocus}this.className='{$classeCssFocus}'\" onblur=\"{$onBlur}this.className='{$classeCss}'\"";

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px; text-align: right;\" {$strFocus} {$strMascara} {$strDisabled} {$strReadOnly} {$strMaxLength} />";
        $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">Formato do n�mero inteiro inv�lido</span>
        				<span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";

        $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"integer\", {validateOn:[\"blur\"], useCharacterMasking:false, allowNegative:{$strAceitarValorNeg}, isRequired:{$strObrigatorio} {$strValorMinimo} {$strValorMaximo}});");

        return $strRetorno;
    }

    public function campoData($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $formato = $objArgumento->formato;
        $calendario = $objArgumento->calendario;
        $tipoCalendario = $objArgumento->tipoCalendario;
        $idOutroCalendario = $objArgumento->idOutroCalendario;
        $focus = $objArgumento->onFocus;
        $blur = $objArgumento->onBlur;
        $aceitarDatasComZeros = $objArgumento->aceitarDatasComZeros;

        $this->addInfoCampos($objArgumento->id, $label, "DATA", $obrigatorio);

        $strRetorno = "";

        $strFocus = "this.className='$classeCssFocus';{$focus}";

        $strBlur = "this.className='$classeCss';{$blur}";

        $strDisabled = "";
        $strReadOnly = "";

        if ($formato == "") {

            $formato = "dd/mm/yyyy";
        }

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }


        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $validateOn = "\"blur\"";

        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" onfocus=\"$strFocus\" onblur=\"$strBlur\" $strMascara $strDisabled $strReadOnly />";
        $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">Formato da data inv�lido</span>
        				<span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";


        if ($aceitarDatasComZeros) {

            $formato = preg_replace("/[A-Za-z0-9]/", '0', $formato);

            $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"custom\", {pattern:\"{$formato}\", validateOn:[{$validateOn}], useCharacterMasking:true, isRequired:{$strObrigatorio}});");
        } else {

            $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"date\", {format:\"{$formato}\", validateOn:[{$validateOn}], useCharacterMasking:true, isRequired:{$strObrigatorio}});");
        }

        if ($calendario) {

            $this->strToRaiz = Helper::acharRaiz();

            if ($tipoCalendario == "individual") {

                $strRetorno .= "

                <script type=\"text/javascript\">

                    $(function() {

                    	$( \"#{$id}\" ).datepicker({dateFormat: 'dd/mm/yy',
                									numberOfMonths: [1, 2],
                									stepMonths: 2,
                                                    autoSize: true});

                    });

                </script>

                ";
            } elseif ($tipoCalendario == "inicial" && !is_null($idOutroCalendario)) {

                Helper::imprimirComandoJavascriptComTimer("$( \"#{$id}\" ).datepicker({dateFormat: 'dd/mm/yy',
                            									numberOfMonths: [1, 2],
                            									stepMonths: 2,
                                                                autoSize: true,
                            									onSelect: function(dateText, inst) {

                                             				  			$(\"#{$idOutroCalendario}\").datepicker(\"option\", \"minDate\", dateText);

                                							  	   }

                            									});", 0);
            } elseif ($tipoCalendario == "final" && !is_null($idOutroCalendario)) {

                Helper::imprimirComandoJavascriptComTimer("$( \"#{$id}\" ).datepicker({dateFormat: 'dd/mm/yy',
                            									numberOfMonths: [1, 2],
                            									stepMonths: 2,
                                                                autoSize: true,
                            									onSelect: function(dateText, inst) {

                                             				  			$(\"#{$idOutroCalendario}\").datepicker(\"option\", \"maxDate\", dateText);

                                							  	   }

                            									});", 0);
            }
        }

        $strRetorno .= "</span>";

        return $strRetorno;
    }

    public function campoHora($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $formato = $objArgumento->formato;
        $calendario = $objArgumento->calendario;
        $tipoCalendario = $objArgumento->tipoCalendario;
        $idOutroCalendario = $objArgumento->idOutroCalendario;
        $focus = $objArgumento->onFocus;
        $blur = $objArgumento->onBlur;

        $this->addInfoCampos($objArgumento->id, $label, "TIME", $obrigatorio);

        $strRetorno = "";

        $strFocus = "this.className='$classeCssFocus';{$focus}";

        $strBlur = "this.className='$classeCss';{$blur}";

        $strDisabled = "";
        $strReadOnly = "";

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }

        if ($formato == "") {

            $formato = "HH:mm";
        }

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }


        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" onfocus=\"$strFocus\" onblur=\"$strBlur\" $strMascara $strDisabled $strReadOnly />";
        $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">Formato da hora inv�lido</span>
        				<span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";

        $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"time\", {format:\"{$formato}\", validateOn:[\"blur\"], useCharacterMasking:true, isRequired:{$strObrigatorio}});");

        if ($calendario) {

            $this->strToRaiz = Helper::acharRaiz();

            if ($tipoCalendario == "individual") {

                $strRetorno .= "

                <script type=\"text/javascript\">

                	$(\"#{$id}\").timepicker({

                    	showSecond: false,
                    	timeFormat: 'hh:mm',
                    	timeOnlyTitle: 'Hor�rio',
                        timeText: 'Valor',
                        hourText: 'Hora',
                        minuteText: 'Minuto',
                        secondText: 'Segundo',
                        currentText: 'Hora Atual',
                        closeText: 'OK',
                        hourGrid: 4,
                        minuteGrid: 30,
                        stepMinute: 30,
                        minuteMax: 30,
                        ampm: false

                    });

                </script>

                ";
            } elseif ($tipoCalendario == "inicial" && !is_null($idOutroCalendario)) {

                $strRetorno .= "

                <script type=\"text/javascript\">

                	$( \"#{$id}\" ).datepicker();

                </script>

                ";
            } elseif ($tipoCalendario == "final" && !is_null($idOutroCalendario)) {

                $strRetorno .= "

                <script type=\"text/javascript\">

                	$( \"#{$id}\" ).datepicker();

                </script>

                ";
            }
        }

        return $strRetorno;
    }

    public function campoDataTime($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $formato = $objArgumento->formato;
        $calendario = $objArgumento->calendario;
        $tipoCalendario = $objArgumento->tipoCalendario;
        $idOutroCalendario = $objArgumento->idOutroCalendario;

        $this->addInfoCampos($objArgumento->id, $label, "DATATIME", $obrigatorio);

        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        $strDisabled = "";
        $strReadOnly = "";

        if ($formato != "") {

            $length = strlen($formato);

            $strMascara = "onkeypress=\"return mascara(this, '$formato', event);\" maxlength=\"$length\"";
        }

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno = "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" $strFocus $strMascara $strDisabled $strReadOnly/>";

        if ($calendario) {

            if ($tipoCalendario == "individual") {

                $strRetorno .= "&nbsp;&nbsp;<img id=\"button_$id\" align=\"absmiddle\" src=\"{$this->strToRaiz}recursos/libs/jscalendar/calbtn.gif\" border=\"0\" alt=\"\" style=\"vertical-align:middle;\">

                <script type=\"text/javascript\">

                	cal.manageFields(\"button_$id\", \"$id\", \"%d/%m/%Y\");

                </script>

                ";
            } elseif ($tipoCalendario == "inicial" && !is_null($idOutroCalendario)) {

                $strRetorno .= "&nbsp;&nbsp;<img id=\"button_$id\" align=\"absmiddle\" src=\"{$this->strToRaiz}recursos/libs/jscalendar/calbtn.gif\" border=\"0\" alt=\"\" style=\"vertical-align:middle;\">

                <script type=\"text/javascript\">

                	cal.manageFields(\"button_$id\", \"$id\", \"%m/%Y\");

                </script>

                ";
            } elseif ($tipoCalendario == "final" && !is_null($idOutroCalendario)) {

                $strRetorno .= "&nbsp;&nbsp;<img id=\"button_$id\" align=\"absmiddle\" src=\"{$this->strToRaiz}recursos/libs/jscalendar/calbtn.gif\" border=\"0\" alt=\"\" style=\"vertical-align:middle;\">

                <script type=\"text/javascript\">

                	cal.manageFields(\"button_$id\", \"$id\", \"%m/%Y\");

                </script>

                ";
            }
        }

        return $strRetorno;
    }

    public function campoCep($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;

        $this->addInfoCampos($objArgumento->id, $label, "CEP", $obrigatorio);

        $formato = "00.000-000";

        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        $strDisabled = "";
        $strReadOnly = "";

        if (!$obrigatorio) {

            $strObrigatorio = "false";
        } else {

            $strObrigatorio = "true";
        }

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno .= "<span id=\"tf_{$id}\">";

        $strRetorno .= "<input type=\"text\" name=\"$nome\" value=\"$valor\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" $strFocus $strDisabled $strReadOnly />";
        $strRetorno .= "<span class=\"textfieldInvalidFormatMsg\">CEP inv�lido</span>
        				<span class=\"textfieldRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";

        $strRetorno .= Helper::getComandoJavascript("var textfield_{$id} = new Spry.Widget.ValidationTextField(\"tf_{$id}\", \"zip_code\", {validateOn:[\"blur\"], useCharacterMasking:true, isRequired:{$strObrigatorio}, format:\"zip_custom\", pattern:\"{$formato}\"});");

        return $strRetorno;
    }

    public function campoArquivo($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;

        $this->addInfoCampos($objArgumento->id, $label, "ARQUIVO", $obrigatorio);

        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno = "<input type=\"file\" name=\"$nome\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" $strFocus $strDisabled $strReadOnly/>";

        return $strRetorno;
    }

    public function campoImagem($objArgumento) {

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;

        $this->addInfoCampos($objArgumento->id, $label, "IMAGEM", $obrigatorio);

        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $strRetorno = "<input type=\"file\" name=\"$nome\" id=\"$id\" class=\"$classeCss\" style=\"width: {$largura}px;\" $strFocus $strDisabled $strReadOnly/>";

        return $strRetorno;
    }

    public function campoBoolean($objArgumento){

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valorSelecionado = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $labelTrue = $objArgumento->labelTrue;
        $labelFalse = $objArgumento->labelFalse;
        $primeiroEmBranco = $objArgumento->primeiroEmBranco;

        if ($onChange)
            $strOnChange = "onchange=\"$onChange\"";
        
        if ($obrigatorio)
            $strObrigatorio = "true";
        else
            $strObrigatorio = "false";
        
        
        if(!strlen($valorSelecionado)){
            
            $valorSelecionado = false;
            
        }
        
        $arrOpcoes = array("0" => $labelFalse, "1" => $labelTrue);
        
        $this->addInfoCampos($objArgumento->id, $label, "BOOLEAN", $obrigatorio);

        $strFocus = "onfocus=\"this.className='$classeCssFocus'\" onblur=\"this.className='$classeCss'\"";

        $strRetorno = "<span id=\"s_{$id}\">";
        
        $strRetorno .= "<select name=\"$nome\" id=\"$id\" class=\"$classeCss\" $strFocus $strOnChange style=\"width: {$largura}px;\">\n";

        $strRetorno .= $this->getOptionsArrayValores($valorSelecionado, $arrOpcoes, $primeiroEmBranco);
        
        $strRetorno .= "</select>";
        
        $strRetorno .= "<span class=\"selectInvalidMsg\">Valor inv�lido</span>
                        <span class=\"selectRequiredMsg\">Preenchimento obrigat�rio</span>";

        $strRetorno .= "</span>";
        
        $strRetorno .= Helper::getComandoJavascript("var select_{$id} = new Spry.Widget.ValidationSelect(\"s_{$id}\", {validateOn:[\"blur\"], isRequired:{$strObrigatorio}});");

        return $strRetorno;

    }
    
    public function campoBooleanRadioButtons($objArgumento){

        $nome = $objArgumento->nome . $objArgumento->numeroDoRegistro;
        $valor = $objArgumento->valor;
        $label = $objArgumento->label;
        $largura = $objArgumento->largura;
        $obrigatorio = $objArgumento->obrigatorio;
        $id = $objArgumento->id . $objArgumento->numeroDoRegistro;
        $classeCss = $objArgumento->classeCss;
        $classeCssFocus = $objArgumento->classeCssFocus;
        $readOnly = $objArgumento->readOnly;
        $disabled = $objArgumento->disabled;
        $labelTrue = $objArgumento->labelTrue;
        $labelFalse = $objArgumento->labelFalse;

        $this->addInfoCampos($objArgumento->id, $label, "BOOLEAN", $obrigatorio);

        if ($readOnly) {

            $strReadOnly = "readOnly=\"readOnly\"";
        }

        if ($disabled) {

            $strDisabled = "disabled=\"disabled\"";
        }

        $checked1 = $checked0 = "";

        if ($valor == "1")
            $checked1 = "checked=\"checked\"";
        if ($valor == "0")
            $checked0 = "checked=\"checked\"";


        $strRetorno = "<input type=\"radio\" name=\"$nome\" id=\"{$id}1\" value=\"0\" $checked0 style=\"width: {$largura}px;\" $strDisabled $strReadOnly/>&nbsp;$labelFalse";
        $strRetorno .= "<input type=\"radio\" name=\"$nome\" id=\"{$id}0\" value=\"1\" $checked1 style=\"width: {$largura}px;\" $strDisabled $strReadOnly/>&nbsp;$labelTrue";

        return $strRetorno;
    }

    //*******************************************
    //Fun��es para formata��o de dados
    //*******************************************
    public function formatarFloatParaComandoSQL($numero) {

        return Helper::formatarFloatParaComandoSQL($numero);
    }

    public function formatarFloatParaExibicao($numero, $casasDecimais=2) {

        return Helper::formatarFloatParaExibicao($numero, $casasDecimais);
    }

    public function formatarDataParaComandoSQL($valor) {

        return Helper::formatarDataParaComandoSQL($valor);
    }

    public function formatarDataParaExibicao($valor) {

        return Helper::formatarDataParaExibicao($valor);
    }

    public function formatarHoraParaComandoSQL($valor) {

        return Helper::formatarHoraParaComandoSQL($valor);
    }

    public function formatarHoraParaExibicao($valor) {

        return Helper::formatarHoraParaExibicao($valor);
    }

    public function formatarDataTimeParaComandoSQL($valor) {

        return Helper::formatarDataTimeParaComandoSQL($valor);
    }

    public function formatarDataTimeParaExibicao($valor) {

        return Helper::formatarDataTimeParaExibicao($valor);
    }

    public function formatarDados($valor) {
//        if(strlen($valor)){
//            
//            if(substr ($valor, -1) == '\\'){
//                $novoValor = substr($valor, 0, -1);
//                $valor = $novoValor."\\\\";
//            }
//        }
        $tratado = str_replace("\\", "\\\\", $valor);
        $tratado = str_replace("'", "\'", $tratado);
        
        return $tratado;
    }

    //implementado por classes herdeiras
    public function select($id) {
        
    }

    //************************************************************
    //METODO QUE RETORNA ID DO ULTIMO REGISTRO CRIADO NA TABELA
    //************************************************************

    function getIdDoUltimoRegistroInserido() {

        $sql = "SELECT MAX({$this->campoId}) FROM {$this->nomeTabela}";

        $result = $this->database->query($sql);

        if ($this->database->rows() > 0) {

            $retorno = Database::mysqli_result($result, 0, 0);
        } else {

            $retorno = "null";
        }

        return $retorno;
    }

    function selectUltimoRegistroInserido() {

        $sql = "SELECT MAX({$this->campoId}) FROM {$this->nomeTabela}";

        $result = $this->database->query($sql);

        if ($this->database->rows() > 0) {

            $id = Database::mysqli_result($result, 0, 0);
            $this->select($id);
            return;
        } else {

            return;
        }
    }

    function setValorDosAtributosAtravesDeArray($array, $contador, $considerarValoresNulos=false){

           $nomeTabela = $this->nomeTabela; 
           $arrAtributos = array_keys(get_object_vars($this));

           for($i=0; $i < count($arrAtributos); $i++){

                if(!is_array($arrAtributos[$i]) && ($considerarValoresNulos || !is_null($array["{$nomeTabela}_{$arrAtributos[$i]}_{$contador}"]))){

                    $this->$arrAtributos[$i] = $array["{$nomeTabela}_{$arrAtributos[$i]}_{$contador}"];
                    
                }

           }

      }
    
    function getArrayDeAtualizacaoDoComandoUpdate($array, $contador){
          
      $nomeTabela = $this->nomeTabela;  
      $campoChave = $this->campoId;
      $arrAtributos = array_keys(get_object_vars($this));

      for($i=0; $i < count($arrAtributos); $i++){

            if(!is_array($arrAtributos[$i]) && 
                    isset($array["{$nomeTabela}_{$arrAtributos[$i]}_{$contador}"]) && 
                    $arrAtributos[$i] != $campoChave){

                $arrUpdate["{$arrAtributos[$i]}_{$contador}"] = true;

            }

       }

       return $arrUpdate;

  }

    public function updateCampo($id, $campo, $valor) {

        $this->updateCampos($id, array($campo), array($valor));
        
    }
    
    public function updateCampos($id, $arrCampos, $arrValores) {

        if (is_numeric($id) && is_array($arrCampos) && is_array($arrValores) && count($arrCampos) == count($arrValores)) {

            for($i=0; $i < count($arrCampos); $i++){
            
                if($arrValores[$i] == "null"){
                    
                    $valor = "null";
                    
                }
                else{
                
                    $valor = "'{$arrValores[$i]}'";
                
                }
                
                $strComplemento .= "{$arrCampos[$i]}={$valor}, ";
            
            }
            
            if(strlen($strComplemento)){
                
                $strComplemento = Helper::removerOsUltimosCaracteresDaString($strComplemento, 2);
                
            }
            
            $this->database->query("UPDATE {$this->nomeTabela} SET {$strComplemento} WHERE {$this->campoId}={$id}");
        
        }
    }
    
    public function getListaJSON(){

        $textoDigitado = Helper::GET("text");
        $campo = Helper::GET("campo");
        
        $this->database->Query("SELECT DISTINCT {$campo} FROM {$this->nomeTabela}  WHERE {$campo} LIKE '{$textoDigitado}%'");

        for($i=1; $registro = $this->database->fetchArray(MYSQL_NUM); $i++){

            $array[] = array($i => utf8_encode($registro[0]));

        }

        echo json_encode($array);

    }
    
    public function getListaJSONChaveValor(){

        $textoDigitado = Helper::GET("text");
        $campo = Helper::GET("campo");
        
        if(!strlen($campo)){
            
            $campo = $this->campoLabel;
            
        }
        
        $this->database->Query("SELECT DISTINCT {$this->campoId}, {$campo} FROM {$this->nomeTabela}  WHERE {$campo} LIKE '{$textoDigitado}%' AND excluido_BOOLEAN=0");

        for($i=1; $registro = $this->database->fetchArray(MYSQL_NUM); $i++){

            $array[] = array($registro[0] => utf8_encode($registro[1]));

        }

        echo json_encode($array);

    }
    
    public function getValorDoCampo(){
        
        $chavePrimaria = Helper::GET("id");
        $campo = Helper::GET("campo");
        
        $this->select($chavePrimaria);
        $this->formatarParaExibicao();
        
        echo $this->$campo;
                
    }

    public function getConfiguracaoDAO(){
        $config = new ConfiguracaoDAO();
        $config->db = $this->database;
        return $config;
    }
    public function  getDatabase() { return $this->database ; }
}
?>

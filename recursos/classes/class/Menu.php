<?php

class Menu extends InterfaceMenu{

    public function getArrayDoConteudoDoMenuCompleto() {

        $arrMenu = array(
            "Assinaturas" => array(

                "config" => new MenuConfig("Clientes", MENUCONFIG_MENU_ABA),
                "Cadastrar Assinatura" => new MenuItem("forms_assinatura"),
                "Gerenciar Assinaturas" => new MenuItem("lists_assinatura"),
                "Criar Novas Assinaturas" => new MenuItem("pages_cria_nova_assinatura_disponivel"),
            ),
            "Bancos de Dados" => array(

                "config" => new MenuConfig("Clientes", MENUCONFIG_MENU_ABA),
                "Cadastrar Banco de Dados" => new MenuItem("forms_hospedagem_db"),
                "Visualizar Bancos de Banco de Dados" => new MenuItem("lists_hospedagem_db"),

            ),

            "Sistemas" => array(

                "config" => new MenuConfig("Clientes", MENUCONFIG_MENU_ABA),
                "Listar Sistemas" => new MenuItem("filters_sistema"),
                "Cadastrar Sistema" => new MenuItem("forms_sistema")

            ),


            "Sites" => array(

                "config" => new MenuConfig("Clientes", MENUCONFIG_MENU_ABA),
                "Listar Hostings" => new MenuItem("filters_empresa_hosting"),
                "Cadastrar Hostings" => new MenuItem("forms_empresa_hosting"),

                "Cadastrar Hospedagens" => new MenuItem("forms_hospedagem"),
                "Visualizar Hospedagens" => new MenuItem("lists_hospedagem"),

            ),

            "Cadastros Gerais" => array(

                "Tipos de Bancos de Dados" => array(

                    "config" => new MenuConfig("Mensagens", MENUCONFIG_MENU_ABA),
                    "Cadastrar Tipo de Banco de Dados" => new MenuItem("forms_db_tipo"),
                    "Gerenciar Tipos de Bancos de Dados" => new MenuItem("lists_db_tipo"),

                ),

            ),

            "Sistema" => array(



                "Banco de Dados"  => array(

                    "config" => new MenuConfig("Backup do Banco de Dados", MENUCONFIG_LISTA_VERTICAL),
                    "Restaurar um Backup do Banco de Dados" => new MenuItem("pages_restaurar_backup_banco_dados", "restauracao_backup.png"),
                    "Fazer backup do Banco de Dados" => new MenuItem("pages_fazer_backup_banco_dados", "backup.png"),
                    "Gerenciar Rotinas de Backup Autom�tico" => new MenuItem("lists_backup_automatico", "gerenciar_backups.png"),
                    "Fazer Limpeza do Banco de Dados" => new MenuItem("pages_limpar_banco_dados", "gerenciar_backups.png")

                ),

                "Configura��es de Envio" => new MenuItem("forms_configuracao_email", "faixas_salariais.png"),

                "Usu�rios" => array(

                    "config" => new MenuConfig("Usu�rios", MENUCONFIG_MENU_ABA),

                    "Usu�rios" => array(
                        "config" => new MenuConfig("Usu�rios", MENUCONFIG_LISTA_VERTICAL),
                        "Cadastrar Usu�rio do Sistema" => new MenuItem("forms_usuario", "cadastrar_usuario.png"),
                        "Gerenciar Usu�rios do Sistema" => new MenuItem("lists_usuario", "usuarios.png"),
                        "Visualizar Opera��es de Usu�rios no sistema" => new MenuItem("lists_operacao_sistema", "operacoes_usuarios.png"),
                    ),

                ),

                "Classes de Usu�rios" => array(
                    "config" => new MenuConfig("Classes de Usu�rios", MENUCONFIG_LISTA_VERTICAL),
                    "Cadastrar Classe de Usu�rio" => new MenuItem("forms_usuario_tipo", "classe_usuarios.png"),
                    "Gerenciar Classes de Usu�rio" => new MenuItem("lists_usuario_tipo", "grupos_de_usuarios.png")
                ),

            ),


        );

        return $arrMenu;
    }

}

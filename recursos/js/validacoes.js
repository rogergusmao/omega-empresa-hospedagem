
function confirmarExclusao(url, mensagem){
    
    if(confirm(mensagem)){
    
        location.href = url;
        
    }
    else{
    
        return false;   
        
    }
    
}

function confirmarReset(mensagem){
    
    if(confirm(mensagem)){
    
        return true;
        
    }
    else{
    
        return false;   
        
    }
    
}

function uppercase()
{

	key = window.event.keyCode;
	
	if ((key > 0x60) && (key < 0x7B))
		window.event.keyCode = key-0x20;

}

function validarEmail(componente, label) {

	var db = true;
	var comp = document.getElementById(componente);
	var addr = comp.value;
	
	if (addr == '') return true;
	
	var invalidChars = '\/\'\\ ";:?!()[]\{\}^|';
	for (i=0; i<invalidChars.length; i++) {
	   if (addr.indexOf(invalidChars.charAt(i),0) > -1) {
	      if (db) alerta('Endere�o de Email cont�m caracteres inv�lidos');
	      comp.focus();
	      return false;
	   }
	}
	for (i=0; i<addr.length; i++) {
	   if (addr.charCodeAt(i)>127) {
	      if (db) alerta("Endere�o de email deve conter somente caracteres ASCII.");
	      componente.focus();
	      return false;
	   }
	}
	
	var atPos = addr.indexOf('@',0);
	if (atPos == -1) {
	   if (db) alerta('Endere�o de Email deve conter um @');
	   return false;
	}
	if (atPos == 0) {
	   if (db) alerta('Endere�o de Email n�o pode come�ar um @');
	   return false;
	}
	if (addr.indexOf('@', atPos + 1) > - 1) {
	   if (db) alerta('Endere�o de Email deve conter somente um @');
	   return false;
	}
	if (addr.indexOf('.', atPos) == -1) {
	   if (db) alerta('Endere�o de Email deve conter um . na parte do dom�nio');
	   return false;
	}
	if (addr.indexOf('@.',0) != -1) {
	   if (db) alerta('O ponto n�o deve ser imediatamente depois do @');
	   return false;
	}
	if (addr.indexOf('.@',0) != -1){
	   if (db) alerta('O ponto n�o deve preceder o @');
	   return false;
	}
	if (addr.indexOf('..',0) != -1) {
	   if (db) alerta('N�o podem haver 2 pontos juntos');
	   return false;
	}
	
	var suffix = addr.substring(addr.lastIndexOf('.')+1);
	if (suffix.length != 2 && suffix != 'com' && suffix != 'net' && suffix != 'org' && suffix != 'edu' && suffix != 'int' && suffix != 'mil' && suffix != 'gov' & suffix != 'arpa' && suffix != 'biz' && suffix != 'aero' && suffix != 'name' && suffix != 'coop' && suffix != 'info' && suffix != 'pro' && suffix != 'museum') {
	   if (db) alerta('Dom�nio prim�rio inv�lido');
	   return false;
	}
	
	return true;
	
}


function validarCPF(componente){

     var cpf = document.getElementById(componente).value;

     erro = new String;
     
     if (cpf.length < 11) erro += "S�o necess�rios 11 digitos para verifica��o do CPF! \n\n";
     var nonNumbers = /\D/;
     if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n";
     if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
             erro += "N�mero de CPF invalido!"
     }
      var a = [];
      var b = new Number;
      var c = 11;
      for (i=0; i<11; i++){
              a[i] = cpf.charAt(i);
              if (i < 9) b += (a[i] * --c);
      }
      if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
      b = 0;
      c = 11;
      for (y=0; y<10; y++) b += (a[y] * c--);
      if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
      if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
              erro +="Digito verificador com problema!";
      }
      if (erro.length > 0){
              alerta(erro);
              return false;
      }
      return true;
      
}

function validarCampos(formulario){

	var retorno = Spry.Widget.Form.validate(formulario);
	
	if(retorno == false){
		
            imprimirMensagemErro("Existem erros no preenchimento dos dados, verifique as mensagens respectivas a cada campo.");
        	
	}
	
	return retorno;
	
}

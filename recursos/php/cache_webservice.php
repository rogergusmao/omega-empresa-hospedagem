<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once Helper::acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."/classes/SingletonCacheWebservice.php";

$singletonFuncoes = SingletonCacheWebservice::getSingleton();

$singletonFuncoes->setWebservices(
    "Hospedagem",
    array("getEstadoDaCorporacao",
        "getEspacoEmMBOcupadoPelaAssinatura",
        "getCaracteristicasDoPacoteDaAssinatura",
        "getDominioDaCorporacaoId",
        "getNomeDaCorporacao",
        "getConfiguracoesDeBancoDaCorporacao"));

$singletonFuncoes->exitIfCached();

$singletonFuncoes->startCachingIfNecessary();
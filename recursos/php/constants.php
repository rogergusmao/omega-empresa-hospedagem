<?php

//valida horairo de funcionamento da migracao
// em producao dever� ser durante a madruagada apenas
define('HORA_INICIO_FUNCIONAMENTO_MIGRACAO_DE_ASSINATURAS', 0);
define('HORA_FIM_FUNCIONAMENTO_MIGRACAO_DE_ASSINATURAS', 4);

define('MODO_BANCO_COM_CORPORACAO', false);
define('BANCO_COM_TRATAMENTO_EXCLUSAO', false);
define('PATH_RELATIVO_PROJETO', 'adm/');
define('IDENTIFICADOR_SISTEMA', 'HOS_');
define('TITULO_PAGINAS_CLIENTE', "Sistema de Hospedagem - WorkOffline");

define('DOMINIO_DE_ACESSO', "hospedagem.omegasoftware.com.br");

define('ENDERECO_DE_ACESSO', "http://" + DOMINIO_DE_ACESSO + "/");
define('ENDERECO_DE_ACESSO_SSL', "https://" + DOMINIO_DE_ACESSO + "/");

define('PAGINA_INICIAL_PADRAO',"pages/cria_nova_assinatura_disponivel.php");

//define('FORCAR_ATIVACAO_GOOGLE_ANALYTICS_QUANTO_DESATIVADO',false);

//TODO alterar
define('MAXIMO_ASSINATURA_OMEGA_EMPRESA_CORPORACAO_DISPONIVEL',1);
define('MAXIMO_ASSINATURA_OMEGA_EMPRESA_DISPONIVEL',1);

define('MAXIMO_ASSINATURA_OMEGA_EMPRESA_PARA_MIGRACAO_CORPORACAO_DISPONIVEL', 1);
define('MAXIMO_ASSINATURA_OMEGA_EMPRESA_PARA_MIGRACAO_DISPONIVEL', 1);


if(php_sapi_name() != 'cli'){
    if( substr_count($_SERVER["HTTP_HOST"],  "workoffline.com.br") >= 1){

        define('DOMINIO_SERVICOS_BIBLIOTECA_NUVEM', "http://cloud.workoffline.com.br/public_html/adm/webservice.php?class=Servico_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");

        define('CLASSES_PIPELINE', 'ProcessoRotinas');
        define('CATEGORIAS_PIPELINE', '1');
        define('PATH_RAIZ_PIPELINE', '/var/www/hospedagem/public_html/adm/pipeline.php');

    }
    else if (strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp64/www")
        && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br")
    {
        define('DOMINIO_SERVICOS_BIBLIOTECA_NUVEM', "http://127.0.0.1/OmegaEmpresa/BibliotecaNuvem/Trunk/public_html/adm/webservice.php?class=Servico_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/OmegaEmpresa/Cobranca/Trunk/adm/webservice.php?class=Servicos_web&action=");

        define('CLASSES_PIPELINE', 'ProcessoRotinas');
        define('CATEGORIAS_PIPELINE', '1');
        define('PATH_RAIZ_PIPELINE', 'C:/wamp64/www/Hospedagem/HO10002/adm/pipeline.php');
    }
    else
    {
        define('DOMINIO_SERVICOS_BIBLIOTECA_NUVEM', "http://127.0.0.1/BibliotecaNuvem/BN10003Corporacao/public_html/adm/webservice.php?class=Servico_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");

        define('CLASSES_PIPELINE', 'ProcessoRotinas');
        define('CATEGORIAS_PIPELINE', '1');
        define('PATH_RAIZ_PIPELINE', 'C:/wamp64/www/Hospedagem/HO10002/adm/pipeline.php');
    }
} else {

    $nomeMaquina = strtoupper(php_uname("n"));


    if ($nomeMaquina == "LI1047-176.MEMBERS.LINODE.COM") {
        define('DOMINIO_SERVICOS_BIBLIOTECA_NUVEM', "http://cloud.workoffline.com.br/public_html/adm/webservice.php?class=Servico_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://my.workoffline.com.br/adm/webservice.php?class=Servicos_web&action=");

        define('CLASSES_PIPELINE', 'ProcessoRotinas');
        define('CATEGORIAS_PIPELINE', '1');
        define('PATH_RAIZ_PIPELINE', '/var/www/hospedagem/public_html/adm/pipeline.php');

    } else {
        define('DOMINIO_SERVICOS_BIBLIOTECA_NUVEM', "http://127.0.0.1/BibliotecaNuvem/BN10003Corporacao/public_html/adm/webservice.php?class=Servico_web&action=");
        define('DOMINIO_DE_ACESSO_SERVICOS_SICOB', "http://127.0.0.1/Cobranca/CO10003Corporacao/adm/webservice.php?class=Servicos_web&action=");

        define('CLASSES_PIPELINE', 'ProcessoRotinas');
        define('CATEGORIAS_PIPELINE', '1');
        define('PATH_RAIZ_PIPELINE', 'C:/wamp64/www/Hospedagem/HO10002/adm/pipeline.php');


    }
}
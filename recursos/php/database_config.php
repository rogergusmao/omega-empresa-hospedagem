<?php





if(php_sapi_name() != 'cli'){
    if(substr_count($_SERVER["HTTP_HOST"],  "workoffline.com.br") >= 1){
        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "database1.workoffline.com.br");
        define('REDIS_PORT', 7333);

        define('IDENTIFICADOR_SESSAO', "OMG_HO");
        define('TITULO_PAGINAS', "Host - WorkOffline");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "host");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "");

        define('BANCO_DE_DADOS_HOST', "database1.workoffline.com.br");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "cobige@4");

        define('PATH_MYSQL', '');

        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');

    } else {
        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "127.0.0.1");
        define('REDIS_PORT', 6379);
        if(strtolower($_SERVER["DOCUMENT_ROOT"]) == strtolower("D:/wamp64/www")
            && $_SERVER["SERVER_ADMIN"] == "eduardo@omegasoftware.com.br"){

            define('IDENTIFICADOR_SESSAO', "OMG_HO");
            define('TITULO_PAGINAS', "WorkOffline - Hospedagem");

            define('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
            define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

            define('BANCO_DE_DADOS_HOST', "localhost");
            define('BANCO_DE_DADOS_PORTA', "3306");
            define('BANCO_DE_DADOS_USUARIO', "root");
            define('BANCO_DE_DADOS_SENHA', "");

            define('PATH_MYSQL', 'D:\wamp64\bin\mysql\mysql5.7.19\bin');
            define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');

        }
        else{

            define('IDENTIFICADOR_SESSAO', "OMG_HO");
            define('TITULO_PAGINAS', "WorkOffline - Hospedagem");

            define('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
            define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

            define('BANCO_DE_DADOS_HOST', "localhost");
            define('BANCO_DE_DADOS_PORTA', "3306");
            define('BANCO_DE_DADOS_USUARIO', "root");
            define('BANCO_DE_DADOS_SENHA', "");

            define('PATH_MYSQL', 'C:\wamp64\bin\mysql\mysql5.7.14\bin');
            define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');

        }
    }

}
else{
    $nomeMaquina = strtoupper(php_uname("n"));
    if ($nomeMaquina == "LI1047-176.MEMBERS.LINODE.COM")
    {
        define('IDENTIFICADOR_SESSAO', "OMG_HO");
        define('TITULO_PAGINAS', "Host - WorkOffline.com.br");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "host");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "");

        define('BANCO_DE_DADOS_HOST', "database1.workoffline.com.br");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "cobige@4");

        define('PATH_MYSQL', '');
        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'biblioteca-compartilhada/public_html/');
        define('PATH_CURL_CMD', '');

        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "database1.workoffline.com.br");
        define('REDIS_PORT', 7333);


    }
    else if($nomeMaquina == 'DESKTOP-1DFD9PC'){
        define('IDENTIFICADOR_SESSAO', "OMG_HO");
        define('TITULO_PAGINAS', "WorkOffline - Hospedagem");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

        define('BANCO_DE_DADOS_HOST', "localhost");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "");

        define('PATH_MYSQL', 'C:\wamp64\bin\mysql\mysql5.7.14\bin');
        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecasCompartilhadas/BC10002Corporacao/');
        define('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');
    } else {
        define('IDENTIFICADOR_SESSAO', "OMG_HO");
        define('TITULO_PAGINAS', "WorkOffline - Hospedagem");

        define('NOME_BANCO_DE_DADOS_PRINCIPAL', "hospedagem");
        define('NOME_BANCO_DE_DADOS_DE_CONFIGURACAO', "OMG_HO_parametros");

        define('BANCO_DE_DADOS_HOST', "localhost");
        define('BANCO_DE_DADOS_PORTA', "3306");
        define('BANCO_DE_DADOS_USUARIO', "root");
        define('BANCO_DE_DADOS_SENHA', "");

        define('PATH_MYSQL', 'D:\wamp64\bin\mysql\mysql5.7.19\bin');
        define('DIRETORIO_BIBLIOTECAS_COMPARTILHADAS', 'BibliotecaCompartilhada/Trunk/');
        define('PATH_CURL_CMD', 'C:\Program Files\curl_7_53_1_openssl_nghttp2_x64');

        define('REDIS_SCHEME', "tcp");
        define('REDIS_HOST', "127.0.0.1");
        define('REDIS_PORT', 6379);
    }


}


class SingletonRaizWorkspace{
    public static $raizWorkspace = null;
}

function acharRaizWorkspace(){
    if(SingletonRaizWorkspace::$raizWorkspace != null)
        return SingletonRaizWorkspace::$raizWorkspace ;
    $pathDir = '';
    $niveis = 0;

    if(php_sapi_name() != 'cli') {

        $pathDir = $_SERVER["SCRIPT_FILENAME"];
        $niveis = substr_count($pathDir,"/");
    }
    else {

        $pathDir = getcwd();
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
            $niveis = substr_count($pathDir,"\\");
        } else {
            $niveis = substr_count($pathDir,"/");
        }

    }


    $root = "";

    for($i = 0; $i < $niveis; $i++)
    {
        if(is_dir("{$root}__workspaceRoot")) {

            SingletonRaizWorkspace::$raizWorkspace  = $root;
            return SingletonRaizWorkspace::$raizWorkspace ;

        }
        else
            $root .= "../";
    }
    SingletonRaizWorkspace::$raizWorkspace = "";
    return SingletonRaizWorkspace::$raizWorkspace ;
}
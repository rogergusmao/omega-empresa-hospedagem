<?php

$rw = acharRaizWorkspace();

require_once $rw. DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/classes/Funcoes.php';

$singletonFuncoes = Funcoes::getSingleton();

$raizWorkspace = Helper::acharRaizWorkspace();
$raiz = Helper::acharRaiz();

$classe = Helper::POSTGET("class");

$singletonFuncoes->setDiretorios(array(
    array($classe, $raiz."recursos/classes/", array("class/", "EXTDAO/", "DAO/", "BO/")),
    array($classe, $raiz."recursos/classes/BO/", array("entidade/", "util/")),
    array($classe, $raiz."protocolo/", array("in/", "out/")),
    array($classe, $raiz."web_service/", array("/")),
    array($classe, $raiz."pipeline/", array("/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS, array("classes/", "php/", "adm_flatty/", "adm_padrao/", "imports/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."classes/", array( "protocolo/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."UI/", array("I18N/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."adm_flatty/", array("imports/")),
    array($classe, $raizWorkspace.DIRETORIO_BIBLIOTECAS_COMPARTILHADAS."adm_padrao/", array("imports/"))   
));

require_once acharRaizWorkspace().DIRETORIO_BIBLIOTECAS_COMPARTILHADAS.'/imports/instancias.php';
require_once '../'.PATH_RELATIVO_PROJETO.'imports/instancias.php';

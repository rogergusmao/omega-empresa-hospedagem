<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Servicos_web
     *
     * @author home
     */
    class Servicos_web
    {
        //Como chamar algum webservice da classe:
        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=

        public function __construct()
        {
        }

        public static function factory()
        {
            return new Servicos_web();
        }

        public static function isServidorOnline()
        {
            echo "TRUE";
        }

        public static function cadastraPrimeiroUsuarioDaAssinatura()
        {
            //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=cadastraPrimeiroUsuarioDaAssinatura&id_assinatura=58&nome=ROGER&corporacao=CARAMBA&email=roger@o.com.br&senha=123456
//            $objCrypt = new Crypt();
//            $emailCrypt = $objCrypt->crypt("rogerfsg@gmail.com");
//
//            $url = "http://www.smartmilhas.com.br/emailmktresponses/email_marketing?email=";
//            echo $url.$emailCrypt;
//            return;

            $idAssinatura = Helper::POSTGET("id_assinatura");
            $nome = Helper::POSTGET("nome");
            $corporacao = Helper::POSTGET("corporacao");
            $idCorporacao = Helper::POSTGET("id_corporacao");
            $email = Helper::POSTGET("email");
            $senha = Helper::POSTGET("senha");
            $msg = BO_Web_service_hospedagem::criaPrimeiroUsuarioDaAssinatura(
                $idAssinatura, $nome,
                $corporacao, $email, $senha,
                $idCorporacao);

            return $msg;
        }

        public static function getDominioDaAssinatura()
        {
            $id = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();
            $msg = $obj->getDominioDaAssinatura($id);

            return $msg;
        }

        public static function getConfiguracoesDeBancoDaAssinatura()
        {
            $id = Helper::POSTGET("id_assinatura");
            $obj = new BO_Assinatura();
            $msg = $obj->getConfiguracoesDeBancoDaAssinatura($id);

            return $msg;
        }

        public static function getDominioWebServiceDaAssinatura()
        {
            $id = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();
            $msg = $obj->getDominioWebServiceDaAssinatura($id);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=consultaHospedagensDoUsuario&email=roger%40omegasoftware.com.br&sistema=1
        public static function consultaHospedagensDoUsuario()
        {
            $emailUsuario = Helper::POSTGET("email");
            $idSistema = Helper::POSTGET("id_sistema");
            $obj = new BO_Web_service_hospedagem();
            $msg = $obj->consultaHospedagensDoUsuario($emailUsuario, $idSistema);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=getNumeroDeUsuariosDaAssinatura&id_assinatura=21
        public static function getNumeroDeUsuariosDaAssinatura()
        {
            $id = Helper::POSTGET("id_assinatura");
            $corporacao = Helper::POSTGET("corporacao");
            $idCorporacao = Helper::POSTGET("id_corporacao");

            $obj = new BO_Assinatura();
            $msg = $obj->getTotalUsuariosDaAssinatura($id, $idCorporacao, $corporacao);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=getEspacoEmMBOcupadoPelaAssinatura&id_assinatura=21
        public static function getEspacoEmMBOcupadoPelaAssinatura()
        {
            $id = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();
            $msg = $obj->getEspacoEmMBOcupadoPelaAssinatura($id);

            return $msg;
        }

        public static function getDominioDoSincronizadorWeb()
        {
            $id = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();
            $msg = $obj->getDominioDoSincronizadorWeb($id);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=getDominioDaHospedagem&id_hospedagem=21
        public static function getDominioDaHospedagem()
        {
            $id = Helper::POSTGET("id_hospedagem");

            $obj = new BO_Hospedagem();
            $msg = $obj->getDominioDaHospedagem($id);

            return $msg;
        }

        public static function getDominioDoSistema()
        {
            $id = Helper::POSTGET("id_sistema");

            $obj = new BO_Sistema();
            $msg = $obj->getDominioSistema($id);

            return $msg;
        }

        public static function atualizaAssinaturaDoCliente()
        {
            $idAssinatura = Helper::POSTGET("id_assinatura");
            $idClienteAssinaturaNovo = Helper::POSTGET("id_cliente_assinatura_novo");
            $idClienteAssinaturaAntigo = Helper::POSTGET("id_cliente_assinatura_antigo");

            $idSistemaAntigo = Helper::POSTGET("id_sistema_antigo");
            $idSistemaNovo = Helper::POSTGET("id_sistema_novo");

            $obj = new BO_Assinatura();
            $msg = $obj->atualizaAssinaturaDoCliente(
                $idAssinatura,
                $idClienteAssinaturaAntigo,
                $idClienteAssinaturaNovo,
                $idSistemaAntigo,
                $idSistemaNovo);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=cadastraAssinaturaDoCliente&id_sistema=1&id_cliente_assinatura=1&nome_corporacao=FACTU&identificador_corporacao=factu
        public static function cadastraAssinaturaDoCliente()
        {
            $idSistema = Helper::POSTGET("id_sistema");
            $idClienteAssinatura = Helper::POSTGET("id_cliente_assinatura");
            $nomeSite = Helper::POSTGET("nome_corporacao");
            $identificadorDominio = Helper::POSTGET("identificador_corporacao");
            $idCorporacao = Helper::POSTGET("id_corporacao");

            $obj = new BO_Assinatura();
            $msg = $obj->cadastraAssinaturaDoCliente(
                $idSistema,
                $idClienteAssinatura,
                $nomeSite,
                $identificadorDominio,
                null,
                EXTDAO_Estado_assinatura::OCUPADA,
                $idCorporacao);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=disponibilizaAssinatura&id_assinatura=59
        public static function disponibilizaAssinatura()
        {
            $idAssinatura = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();
            $msg = $obj->disponibilizaAssinatura($idAssinatura);

            return $msg;
        }

        public static function deletarAssinatura()
        {
            $idAssinatura = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();
            $msg = $obj->deletarAssinatura($idAssinatura);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=cadastraHospedagem&id_sistema=1
        public static function criaNovaAssinaturaDoSistema()
        {
            $idSistema = Helper::POSTGET("id_sistema");

            $obj = new BO_Assinatura();
            $msg = $obj->criaNovaAssinatura($idSistema);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10001/adm/actions.php?class=Servicos_web&action=reservaHospedagensDosSistemas
        public static function criaNovasAssinaturasDosSistemas()
        {
            $obj = new BO_Assinatura();
            $msg = $obj->criaNovasAssinaturasDeTodosOsSistemas();

            return $msg;
        }

        public static function removerDumpDosBancosModelosDoSistema()
        {
            $idSistemaBibliotecaNuvem = Helper::POSTGET("id_sistema_biblioteca_nuvem");
            $msg = BO_Sistema::removerDumpDosBancosModelosDoSistema($idSistemaBibliotecaNuvem);

            return $msg;
        }

        //http://127.0.0.1/hospedagem/10002Corporacao/adm/webservice.php?class=Servicos_web&action=teste
        public static function teste()
        {
//            $objS = new EXTDAO_Assinatura();
//            $msg = $objS->select(10);
            $o = new EXTDAO_Assinatura_migracao();
            $o->select(1);
            echo $o->getDataMinimaParaMigracao();

            exit();
        }

        public static function getIdsSihopAssinaturaParaSincronizacao()
        {
            $idEmpresaHosting = Helper::POSTGET("id_empresa_hosting");

            $msg = BO_Empresa_hosting::getIdsSihopAssinaturaParaSincronizacao($idEmpresaHosting);
            return $msg;
        }

        public static function cadastraAssinaturaDoClienteReservado()
        {
            $nomeSite = Helper::POSTGET("corporacao");
            $idCorporacao = Helper::POSTGET("id_corporacao");
            $idAssinatura = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();

            $msg = $obj->cadastraAssinaturaDoClienteReservado($nomeSite, $idAssinatura, $idCorporacao);

            return $msg;
        }

        public static function disponibilizaAssinaturaReservada()
        {
            $nomeSite = Helper::POSTGET("corporacao");
            $idAssinatura = Helper::POSTGET("id_assinatura");

            $obj = new BO_Assinatura();
            $msg = $obj->disponibilizaAssinaturaReservada($idAssinatura, $nomeSite);

            return $msg;
        }

        public static function cadastraAssinaturaDoClienteParaReserva()
        {
            $idSistema = Helper::POSTGET("id_sistema");
            $idClienteAssinatura = Helper::POSTGET("id_cliente_assinatura");
            $nomeSite = Helper::POSTGET("nome_corporacao");
            $identificadorDominio = Helper::POSTGET("identificador_corporacao");
            $idCorporacao = Helper::POSTGET("id_corporacao");
            $obj = new BO_Assinatura();
            $msg = $obj->cadastraAssinaturaDoCliente(
                $idSistema,
                $idClienteAssinatura,
                $nomeSite,
                $identificadorDominio,
                null,
                EXTDAO_Estado_assinatura::ASSINATURA_RESERVADA,
                $idCorporacao);

            return $msg;
        }

        public static function getConfiguracaoSite()
        {
            $nomeSite = Helper::POSTGET("corporacao");
            $projeto = Helper::POSTGET("projeto");
            $obj = new BO_Hospedagem();
            $msg = null;
            switch ($projeto)
            {
                case 'SIW':
                    $msg = $obj->getConfiguracaoSiteSincronizadorWeb($nomeSite);
                    break;

                case 'OEW':
                default:
                    $msg = $obj->getConfiguracaoSiteOmegaEmpresaWeb($nomeSite);
                    break;

            }

            if ($msg == null)
            {
                return new Mensagem(
                    PROTOCOLO_SISTEMA::ERRO_COM_SERVIDOR,
                    I18N::getExpression("N�o existem dados de configura��o a corpora��o {0}",
                        $nomeSite));
            }

            return $msg;
        }

        public static function getConfiguracaoSiteSincronizadorWeb()
        {
            $nomeSite = Helper::POSTGET("corporacao");

            $obj = new BO_Hospedagem();
            $msg = $obj->getConfiguracaoSiteSincronizadorWeb($nomeSite);

            return $msg;
        }

        public static function removerBancos(){
            $i = Helper::POSTGET("i");
            $j = Helper::POSTGET("j");
            $database = new Database();
            for(; $i < $j ; $i++){

                $database->deletaBancoDeDados("omega_empresa_web_prod_{$i}");
                $database->deletaBancoDeDados("omega_empresa_parametros_{$i}");
                $database->deletaBancoDeDados("sincronizador_web_corporacao_prod_{$i}");
            }

        }

        public static function existeMigracaoPendente(){
            $corporacao = Helper::POSTGET("corporacao");

            if(!strlen($corporacao))
                return Mensagem::factoryParametroInvalido();


            $msg = EXTDAO_Assinatura_migracao::existeMigracaoPendente($corporacao);

            return $msg;
        }

    }
